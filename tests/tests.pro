TEMPLATE = subdirs
SUBDIRS=\
    common \
    wt_import \
    workbook_import

wt_import.depends = common
workbook_import.depends = common
