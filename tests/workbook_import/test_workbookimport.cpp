#include <QtTest>
#include <QDebug>
#include "../common/common.h"
#include "../../src/importlmmworkbook.h"

class test_workbookimport : public QObject
{
    Q_OBJECT

public:
    test_workbookimport();
    ~test_workbookimport();

private slots:
    void initTestCase();
    void init();
    void cleanup();
    void cleanupTestCase();
    void test_case1_data();
    void test_case1();
};

test_workbookimport::test_workbookimport()
{

}

test_workbookimport::~test_workbookimport()
{

}

void test_workbookimport::initTestCase()
{
    common::initDataBase();
}

void test_workbookimport::init()
{
    qDebug() << "INIT";
}

void test_workbookimport::cleanup()
{
    qDebug() << "CLEANUP";
}

void test_workbookimport::cleanupTestCase()
{
    common::clearDatabase();
}

void test_workbookimport::test_case1_data()
{
    // Columns
    QTest::addColumn<QString>("filename");
    QTest::addColumn<QDate>("firstdate");
    QTest::addColumn<QString>("downloadlink");

    // Rows
    QTest::addRow("October 2019 en") << "mwb_E_201910.epub" << QDate(2019, 10, 7)
                                     << "https://download-a.akamaihd.net/files/media_periodical/79/mwb_E_201910.epub";
    QTest::addRow("November 2019 en") << "mwb_E_201911.epub" << QDate(2019, 11, 4)
                                     << "https://download-a.akamaihd.net/files/media_periodical/af/mwb_E_201911.epub";
}

void test_workbookimport::test_case1()
{
    QFETCH(QString, filename);
    QFETCH(QDate, firstdate);
    QFETCH(QString, downloadlink);
    int daysInMonth = firstdate.daysInMonth();
    QDate lastDate(firstdate.year(), firstdate.month(), daysInMonth);
    int weeks = (lastDate.day() - firstdate.day()) / 7 + 1;
    int dayOfWeek = lastDate.dayOfWeek();
    lastDate = lastDate.addDays(-(dayOfWeek-1));

    QString testPath = QT_TESTCASE_BUILDDIR;
    QString srcDir = SRCDIR;
    QVERIFY(common::downloadTestFile(filename, downloadlink, srcDir));

    QString epubFile = QFINDTESTDATA(filename);
    importlmmworkbook wb;
    QString result = wb.importFile(epubFile);
    QString resultText = QString("Imported %1 weeks from %2 thru %3").arg(weeks).arg(
                firstdate.toString(Qt::DefaultLocaleShortDate), lastDate.toString(Qt::DefaultLocaleShortDate));    
    QCOMPARE(result, resultText);

    LMM_Meeting mtg(nullptr);
    QDate tmpDate(firstdate);
    for (int i = 0;i < weeks; i++) {
        mtg.loadMeeting(tmpDate);
        QVERIFY(!mtg.bibleReading().isEmpty());
        QVERIFY(mtg.songBeginning() > 0);
        QVERIFY(mtg.songMiddle() > 0);
        QVERIFY(mtg.songEnd() > 0);
        mtg.loadAssignments();
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_Treasures));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_Digging));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_Digging));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_BibleReading));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_LivingTalk1));
        QVERIFY(mtg.getAssignment(LMM_Schedule::TalkType_CBS));
        tmpDate = tmpDate.addDays(7);
    }
}

QTEST_MAIN(test_workbookimport)

#include "test_workbookimport.moc"
