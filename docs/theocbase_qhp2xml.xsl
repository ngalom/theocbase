<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                version='1.1'
                extension-element-prefixes="exsl">

<xsl:output method="xml" encoding="utf-8" indent="yes"/>

<xsl:strip-space elements="keyword"/>

<xsl:template match="text()|@*"/>
<xsl:template match="text()|@*" mode="keywords"/>
<xsl:template match="text()|@*" mode="files"/>

<xsl:template match="/">
    <xsl:element name="QtHelpProject">
        <xsl:attribute name="version">1.0</xsl:attribute>
        <xsl:attribute name="namespace">net.theocbase</xsl:attribute>
        <xsl:attribute name="virtualFolder">doc</xsl:attribute>
        <xsl:element name="filterSection">
            <xsl:element name="toc">
                <xsl:apply-templates/>
            </xsl:element>
            <xsl:element name="keywords">
                <xsl:apply-templates mode="keywords"/>
            </xsl:element>
            <xsl:element name="files">
                <xsl:apply-templates mode="files"/>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="keyword" mode="keywords">
    <xsl:element name="keyword">
        <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
        <xsl:attribute name="ref"><xsl:value-of select="@ref"/></xsl:attribute>
        <xsl:element name="name"><xsl:value-of select="@name"/></xsl:element>
    </xsl:element>
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="section">
    <xsl:element name="section">
        <xsl:attribute name="id"><xsl:value-of select="generate-id()"/></xsl:attribute>
        <xsl:attribute name="ref"><xsl:value-of select="@ref"/></xsl:attribute>
        <xsl:element name="title"><xsl:value-of select="@title"/></xsl:element>
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

<xsl:template match="file" mode="files">
    <xsl:element name="file">
        <xsl:attribute name="file"><xsl:value-of select="."/></xsl:attribute>
    </xsl:element>
    <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>
