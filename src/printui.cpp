/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "printui.h"
#include "ui_printui.h"

printui::printui(QDate date, QWidget *parent)
    : QDialog(parent), ui(new Ui::printui), c(), myCongregation(c.getMyCongregation())
{
    isLoaded = false; // part of attempt to fix blank preview issue
    ui->setupUi(this);
    sql = &Singleton<sql_class>::Instance();
    ac = &Singleton<AccessControl>::Instance();
    mWebView = new QWebEngineView(this);
    mWebView->resize(0, 0);
    mWebView->show();
    NoUIEvents = 0;

    templatePath1 = qApp->applicationDirPath();
#ifdef Q_OS_MAC
    ui->splitter->setStyleSheet("");
    templatePath1 += "/../Resources";
#endif
    templatePath1 += "/templates/";

    pr = new QPrinter(QPrinter::ScreenResolution);
    if (!pr->isValid()) {
        if (!QPrinterInfo::defaultPrinter().isNull()) {
            pr = new QPrinter(QPrinterInfo::defaultPrinter(), QPrinter::ScreenResolution);
        } else if (QPrinterInfo::availablePrinters().count() > 0) {
            pr = new QPrinter(QPrinterInfo::availablePrinters().at(0), QPrinter::ScreenResolution);
        }
    }

    QDir myDir(templatePath1);
    QStringList nameFilter;
    nameFilter << "*.htm*"
               << "*.pdf"
               << "*.jp*"
               << "*.png";
    templateList = myDir.entryList(nameFilter);
    templatePath2 = sql->getSetting("customTemplateDirectory");
    if (!templatePath2.isEmpty()) {
        if (!templatePath2.endsWith("/") && !templatePath2.endsWith("\\"))
            templatePath2 += "/";
        myDir.setPath(templatePath2);
        QStringList customTemplates = myDir.entryList(nameFilter);
        for (QString f : customTemplates) {

            // pdf to jpg
            bool include(true);
            if (f.contains(QRegExp("S-(12|89)", Qt::CaseInsensitive)) && f.endsWith(".pdf")) {
                include = false; // don't include the pdf if we have the jpg
                QString jpgname = QFileInfo(f).completeBaseName() + ".jpg";
                if (!customTemplates.contains(jpgname)) {
                    if (generateSlipPdfToJpg(myDir.path() + "/" + f, myDir.path() + "/" + jpgname)) {
                        templateList.append(jpgname);
                    } else {
                        include = true; // something went wrong
                    }
                }
            }

            if (!templateList.contains(f) && include)
                templateList.append(f);
        }
        std::sort(templateList.begin(), templateList.end());
    }
    templates.insert(templateData::midweekScheduleTemplate, new templateData(templateData::midweekScheduleTemplate, "schoolTemplate", "MW-Schedule_1.htm", "mw-"));
    templates.insert(templateData::midweekWorksheetTemplate, new templateData(templateData::midweekWorksheetTemplate, "schoolTemplateWS", "MW-Worksheet.htm", "mw-worksheet"));
    templates.insert(templateData::midweekSlipTemplate, new templateData(templateData::midweekSlipTemplate, "slipTemplate", "", "S-89"));
    templates.insert(templateData::combinationTemplate, new templateData(templateData::combinationTemplate, "combinationTemplate", "COMBO_1.htm", "combo"));
    templates.insert(templateData::publicMeetingTemplate, new templateData(templateData::publicMeetingTemplate, "publicmeetingTemplate", "WE-Schedule_1.htm", "we-schedule"));
    templates.insert(templateData::outgoingScheduleTemplate, new templateData(templateData::outgoingScheduleTemplate, "outgoingSpeakersTemplate", "WE-OutgoingSchedule_1.htm", "we-outgoingschedule"));
    templates.insert(templateData::outgoingSlipTemplate, new templateData(templateData::outgoingSlipTemplate, "outgoingSpeakersSlipTemplate", "WE-OutgoingSlips.htm", "we-outgoingslips"));
    templates.insert(templateData::territoryAssignmentRecordTemplate, new templateData(templateData::territoryAssignmentRecordTemplate, "territoryAssignmentRecordTemplate", "TR-AssignmentRecord.htm", "tr-"));
    templates.insert(templateData::territoryCardTemplate, new templateData(templateData::territoryCardTemplate, "territoryCardTemplate", "TC-Map_OSM.htm", "tc-"));
    templates.insert(templateData::territoryMapCardTemplate, new templateData(templateData::territoryMapCardTemplate, "territoryMapCardTemplate", "", "S-12"));
    templates.insert(templateData::hospitalityTemplate, new templateData(templateData::hospitalityTemplate, "hospitalityTemplate", "WE-CallList", "we-call"));
    currentTemplateData = nullptr;

    ui->btnAdditionalOptions->setChecked(true);
    ui->grpAdditionalOptions->hide();

    fillCustomPaperSizes();

    // button menu for pdf and csv
    QMenu *m = new QMenu(this);
    QAction *apdf = new QAction("PDF File", this);
    connect(apdf, &QAction::triggered, this, &printui::toolButtonPdf_clicked);
    m->addAction(apdf);
    QAction *ahtm = new QAction("HTML File", this);
    connect(ahtm, &QAction::triggered, this, &printui::toolButtonHtml_clicked);
    m->addAction(ahtm);
    QAction *aodf = new QAction("ODF File", this);
    connect(aodf, &QAction::triggered, this, &printui::toolButtonOdf_clicked);
    m->addAction(aodf);
    ui->toolButton->setMenu(m);

    // temp work-around
    individualSlips = sql->getIntSetting("printIndividualSlips", 0);

    // update preview when radio button changed
    connect(ui->radioButtonSchool, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButtonProgram, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButtonTaskOrder, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->checkBoxAssistantSlip, &QCheckBox::clicked, this, &printui::radioButtonClicked);
    connect(ui->checkBoxBlank, &QCheckBox::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButtonWorkSheet, &QRadioButton::clicked, this, &printui::radioButtonClicked);

    connect(ui->radioButtonCombination, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->chk_Comb_LMM, &QCheckBox::clicked, this, &printui::radioButtonClicked);
    connect(ui->chk_Comb_PublicMeeting, &QCheckBox::clicked, this, &printui::radioButtonClicked);
    connect(ui->chk_Comb_PublicMeetingOut, &QCheckBox::clicked, this, &printui::radioButtonClicked);

    connect(ui->radioButtonPublicMeeting, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_PublicMeetingProgram, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_PublicMeetingWorkSheet, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_OutgoingSpeakers, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_OutgoingSpeakersSlips, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButtonTerritories, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_TerritoryRecord, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_TerritoryCard, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_TerritoryMapCard, &QRadioButton::clicked, this, &printui::radioButtonClicked);
    connect(ui->radioButton_Hospitality, &QRadioButton::clicked, this, &printui::radioButtonClicked);

    connect(ui->radioButtonFieldMinistry, &QRadioButton::clicked, this, &printui::radioButtonClicked);

    connect(ui->previewPdf, &PrintPreview::textSizeDecrease, this, &printui::decreaseTextSize);
    connect(ui->previewPdf, &PrintPreview::textSizeIncrease, this, &printui::increaseTextSize);
    connect(ui->previewPdf, &PrintPreview::textSizeReset, this, &printui::resetTextSize);
    connect(ui->previewPdf, &PrintPreview::previewReady, this, &printui::clearBusy);

    connect(ui->previewPdf, &PrintPreview::mapSizeDecrease, this, &printui::decreaseTextSize);
    connect(ui->previewPdf, &PrintPreview::mapSizeIncrease, this, &printui::increaseTextSize);
    connect(ui->previewPdf, &PrintPreview::mapSizeReset, this, &printui::resetTextSize);

    school s;
    schoolday = s.getSchoolDay();
    publicmeetingday = myCongregation.getPublicmeeting(fromDate).getMeetingday();

    ui->checkBoxBlank->setVisible(false);
    ui->checkBoxAssistantSlip->setVisible(false);
    fromDate = date.addDays(1 - date.day());
    fromDate = fromDate.addDays(1 - fromDate.dayOfWeek());
    if (fromDate.month() != date.month())
        fromDate = fromDate.addDays(7);
    thruDate = date.addMonths(1);
    thruDate = thruDate.addDays(-thruDate.day());
    thruDate = thruDate.addDays(1 - thruDate.dayOfWeek());

    ui->grpAdditionalOptions->setVisible(false);
    ui->editDateRangeFrom->setDate(fromDate);
    ui->editDateRangeThru->setDate(thruDate);

    ui->radioButtonFieldMinistry->setVisible(false);

    ui->labelTerritoryNumber->setVisible(false);
    ui->lineEditTerritoryNumber->setVisible(false);
    QRegExp rx("([1-9]\\d*,[ ]?)*[1-9]\\d*");
    QValidator *validator = new QRegExpValidator(rx, this);
    ui->lineEditTerritoryNumber->setValidator(validator);

    // page margins
    QString pm = sql->getSetting("template_pagemargins");
    if (pm == "") {
        pmlist.setLeft(15);
        pmlist.setTop(20);
        pmlist.setRight(15);
        pmlist.setBottom(20);
    } else {
        QStringList temp(pm.split(","));
        pmlist.setLeft(temp[0].toDouble());
        pmlist.setTop(temp[1].toDouble());
        pmlist.setRight(temp[2].toDouble());
        pmlist.setBottom(temp[3].toDouble());
    }
    // page size

    int porient = sql->getSetting("template_paperorientation").toInt();
    if (porient == 0) {
        po = QPrinter::Portrait;
    } else {
        po = QPrinter::Landscape;
    }

    QSettings settings;
    int svalue = settings.value("printui/weeks", 0).toInt();
    qDebug() << QVariant(svalue).toString();
    //ui->spinBox->setValue(svalue);

    // default value for font size factory
    //textSizeFactory = settings.value("printui/textsize",1).toReal();

    // default value for font size factory on assignment slips
    textSizeFactorySlips = settings.value("printui/textsizeslips", 1).toReal();

    mPage = nullptr;

    applyAuthorizationRules();

    isLoaded = true;
    printRequest();
}

printui::~printui()
{
    if (tmpHtmlFile.exists())
        tmpHtmlFile.remove();
    delete mPage;
    delete ui;
}

void printui::setCloud(cloud_controller *c)
{
    this->cloud = c;
    ui->checkBoxQRCode->setEnabled(cloud->logged());
    connect(cloud, &cloud_controller::loggedChanged, this, [=](bool logged) {
        ui->checkBoxQRCode->setEnabled(logged);
        if (!logged)
            ui->checkBoxQRCode->setChecked(false);
    });
}

void printui::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void printui::resizeEvent(QResizeEvent *)
{
    resetZooming();
}

void printui::printRequest()
{
    // part of attempt to fix blank preview issue
    if (!isLoaded)
        return;
    setBusy();

    ui->previewPdf->pdfLoadingStarted();

    currentTemplateData = nullptr;
    //acsv->setEnabled(false);
    if (ui->radioButtonSchool->isChecked()) {
        if (ui->radioButtonTaskOrder->isChecked()) {
            currentTemplateData = templates[templateData::midweekSlipTemplate];
            if (currentTemplateData->optionValue("counsel-name") == "")
                currentTemplateData->setOptionValue("counsel-name", "yes", true, true);
            pickTemplateEditor();
            this->printLMMAssignmentSlipsPreview();
        } else if (ui->radioButtonProgram->isChecked()) {
            currentTemplateData = templates[templateData::midweekScheduleTemplate];
            pickTemplateEditor();
            this->printLMMSchedule();
            //acsv->setEnabled(true);
        } else if (ui->radioButtonWorkSheet->isChecked()) {
            currentTemplateData = templates[templateData::midweekWorksheetTemplate];
            pickTemplateEditor();
            this->printLMMWorkSheet();
            //acsv->setEnabled(true);
        }
    } else if (ui->radioButtonCombination->isChecked()) {
        currentTemplateData = templates[templateData::combinationTemplate];
        pickTemplateEditor();
        this->printLMMCombination();
    } else if (ui->radioButtonPublicMeeting->isChecked()) {
        if (ui->radioButton_PublicMeetingProgram->isChecked()) {
            currentTemplateData = templates[templateData::publicMeetingTemplate];
            pickTemplateEditor();
            this->printPublicMeetingSchedule();
        } else if (ui->radioButton_PublicMeetingWorkSheet->isChecked()) {
            currentTemplateData = templates[templateData::publicMeetingWorksheet];
            pickTemplateEditor();
            this->printPublicMeetingWorkSheet();
        } else if (ui->radioButton_OutgoingSpeakers->isChecked()) {
            currentTemplateData = templates[templateData::outgoingScheduleTemplate];
            pickTemplateEditor();
            this->printOutgoingSpeakersList();
        } else if (ui->radioButton_OutgoingSpeakersSlips->isChecked()) {
            currentTemplateData = templates[templateData::outgoingSlipTemplate];
            pickTemplateEditor();
            this->printOutgoingSpeakersList(true);
        } else if (ui->radioButton_Hospitality->isChecked()) {
            currentTemplateData = templates[templateData::hospitalityTemplate];
            pickTemplateEditor();
            this->printPublicMeetingSchedule();
        }
    } else if (ui->radioButtonTerritories->isChecked()) {
        if (ui->radioButton_TerritoryRecord->isChecked()) {
            currentTemplateData = templates[templateData::territoryAssignmentRecordTemplate];
            pickTemplateEditor();
            this->printTerritoryAssignmentRecord();
        } else if (ui->radioButton_TerritoryCard->isChecked()) {
            currentTemplateData = templates[templateData::territoryCardTemplate];
            pickTemplateEditor();
            this->printTerritoryCard();
        } else if (ui->radioButton_TerritoryMapCard->isChecked()) {
            currentTemplateData = templates[templateData::territoryMapCardTemplate];
            pickTemplateEditor();
            this->printTerritoryMapCardPreview();
        }
    }

    // reset zooming
    resetZooming();
}

void printui::pickTemplateEditor()
{
    NoUIEvents++;
    ui->cboTemplate->clear();
    if (currentTemplateData) {
        int idx = 0;
        for (QString file : templateList) {
            if (
                    file.contains(currentTemplateData->fileFilter, Qt::CaseInsensitive) && (currentTemplateData->notFileFilter.isEmpty() || !file.contains(currentTemplateData->notFileFilter, Qt::CaseInsensitive))) {
                ui->cboTemplate->insertItem(idx, file);
                if (file.compare(currentTemplateData->getTemplateName()) == 0)
                    ui->cboTemplate->setCurrentIndex(idx);
                idx++;
            }
        }
        if (ui->cboTemplate->currentIndex() < 0 && idx > 0)
            ui->cboTemplate->setCurrentIndex(0);
        ui->cboPaperSize->setCurrentText(currentTemplateData->paperSize);
        showCurrentTemplateOptions();
    }
    NoUIEvents--;
}

void printui::fillCustomPaperSizes()
{
    paperSizes.clear();
    paperSizes.append("A4");
    paperSizes.append("Letter");
    paperSizes.append("Legal");
    paperSizes.append("Tabloid");
    paperSizes.append("A6");
    for (templateData *t : templates) {
        if (!paperSizes.contains(t->paperSize))
            paperSizes.append(t->paperSize);
    }
    paperSizes.append(tr("Custom...", "pick custom paper size"));
    NoUIEvents++;
    ui->cboPaperSize->clear();
    ui->cboPaperSize->addItems(paperSizes);
    NoUIEvents--;
}

void printui::on_toolButtonCopy_clicked()
{
    // copy text to clipboard
    QTextEdit *copypastetext = new QTextEdit();
    QTextDocument *d = new QTextDocument(copypastetext);
    d->setHtml(activeHtml);
    copypastetext->setDocument(d);

    copypastetext->selectAll();
    copypastetext->copy();
    QMessageBox::information(this, "", tr("Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)"));
    delete copypastetext;
}

void printui::toolButtonPdf_clicked()
{
    setBusy();
    // save pdf file
    bool debugBoxes(QGuiApplication::keyboardModifiers() & Qt::ControlModifier);
    bool findBoxNames(QGuiApplication::keyboardModifiers() & Qt::ShiftModifier);
    QSettings settings;
    QString exportpath = QFileDialog::getSaveFileName(this, tr("Save file"),
                                                      settings.value("printui/pdf/path", QDir::homePath()).toString(),
                                                      "PDF (*.pdf)");
    if (exportpath != "") {
        pr->setOutputFormat(QPrinter::PdfFormat);
        pr->setOutputFileName(exportpath);
        QFileInfo fileInfo(exportpath);
        settings.setValue("printui/pdf/path", fileInfo.dir().path());

        bool doingSlips((ui->radioButtonSchool->isChecked() && ui->radioButtonTaskOrder->isChecked()) || (ui->radioButtonTerritories->isChecked() && ui->radioButton_TerritoryMapCard->isChecked()));
        if (doingSlips) {
            int method(ui->cboTemplate->currentText().endsWith("pdf", Qt::CaseInsensitive) ? 1 : 0);
            if (method == 0) {
                if (ui->radioButtonTaskOrder->isChecked())
                    printLMMAssignmentSlips_NoFormFields(debugBoxes, findBoxNames, true);
                else
                    printTerritoryMapCard(findBoxNames, true);
            } else {
                if (ui->radioButtonTaskOrder->isChecked()) {
                    slipWorkingPath = exportpath;
                    int lastPeriod = slipWorkingPath.lastIndexOf(".");
                    if (lastPeriod > 0)
                        slipWorkingPath = slipWorkingPath.left(lastPeriod);
                    if (ui->radioButtonTaskOrder->isChecked())
                        printLMMAssignmentSlips(debugBoxes, findBoxNames);
                } else
                    mPage->printToPdf(exportpath, pr->pageLayout());
            }
        } else {
            if (ui->checkBoxQRCode->isChecked())
                uploadSharedFile();
            mPage->printToPdf(exportpath, pr->pageLayout());
        }
    }
    clearBusy();
    QMessageBox::information(this, "", exportpath + " " + tr("file created") + (sharedLink.isEmpty() ? "" : QString("<br>"
                                                                                                                    "<br>"
                                                                                                                    "Shared link: <a href='%1'>%1</a>")
                                                                                                                    .arg(sharedLink)));
    sharedLink = "";
}

void printui::toolButtonHtml_clicked()
{
    // save html file
    QSettings settings;
    QString exportpath = QFileDialog::getSaveFileName(this, tr("Save file"),
                                                      settings.value("printui/html/path", QDir::homePath()).toString(),
                                                      "HTML (*.html)");
    if (exportpath == "")
        return;

    setBusy();
    if (ui->checkBoxQRCode->isChecked() && ((ui->radioButtonSchool->isChecked() && ui->radioButtonProgram->isChecked()) || (ui->radioButtonPublicMeeting->isChecked() && ui->radioButton_PublicMeetingProgram->isChecked()) || ui->radioButtonCombination->isChecked())) {
        uploadSharedFile();
    }
    mPage->save(exportpath, QWebEngineDownloadItem::SingleHtmlSaveFormat);

    QFileInfo fileInfo(exportpath);
    settings.setValue("printui/html/path", fileInfo.dir().path());
    clearBusy();
    QMessageBox::information(this, "", exportpath + " " + tr("file created") + (sharedLink.isEmpty() ? "" : QString("<br>"
                                                                                                                    "<br>"
                                                                                                                    "Shared link: <a href='%1'>%1</a>")
                                                                                                                    .arg(sharedLink)));
    sharedLink = "";
}

void printui::on_toolButtonPrint_clicked()
{
    setBusy();
    if (ui->radioButtonSchool->isChecked() && ui->radioButtonTaskOrder->isChecked() && ui->cboTemplate->currentText().endsWith("pdf", Qt::CaseInsensitive)) {
        // LMM assignment slip -> create PDF file
        bool debugBoxes(QGuiApplication::keyboardModifiers() & Qt::ControlModifier);
        bool findBoxNames(QGuiApplication::keyboardModifiers() & Qt::ShiftModifier);
        if (individualSlips) {
            QSettings settings;
            QString exportpath = QFileDialog::getSaveFileName(this, tr("Save file"),
                                                              settings.value("printui/pdf/path", QDir::homePath()).toString(),
                                                              "PDF (*.pdf)");
            if (exportpath != "") {
                slipWorkingPath = exportpath;
                int lastPeriod = slipWorkingPath.lastIndexOf(".");
                if (lastPeriod > 0)
                    slipWorkingPath = slipWorkingPath.left(lastPeriod);
                printLMMAssignmentSlips(debugBoxes, findBoxNames);
            }
        } else {
            slipWorkingPath = QDir::tempPath() + "/slip";
            printLMMAssignmentSlips(debugBoxes, findBoxNames);
        }
    } else {
        // print to native printer
        pr->setOutputFormat(QPrinter::NativeFormat);
        QPrintDialog printDialog(pr, this);
        printDialog.setOption(QAbstractPrintDialog::PrintShowPageSize, true);
        if (printDialog.exec() == QDialog::Accepted) {
            if (ui->radioButtonSchool->isChecked() && ui->radioButtonTaskOrder->isChecked()) {
                printLMMAssignmentSlips_NoFormFields(false, false, true);
            } else {
                if (ui->radioButtonTerritories->isChecked() && ui->radioButton_TerritoryMapCard->isChecked()
                    && !ui->cboTemplate->currentText().endsWith("pdf", Qt::CaseInsensitive)) {
                    printTerritoryMapCard(false, true);
                } else {
                    if (ui->checkBoxQRCode->isChecked())
                        uploadSharedFile();
                    QEventLoop event;
                    mPage->print(pr, [&event](bool ok) { Q_UNUSED(ok); event.quit(); });
                    event.exec();
                }
            }
            // This is a workaround to reset page orientation after printing.
            QPrinterInfo info(*pr);
            delete pr;
            pr = new QPrinter(info);
        }
    }
    clearBusy();
    if (!sharedLink.isEmpty()) {
        QMessageBox::information(this, "", QString("Shared link: <a href='%1'>%1</a>").arg(sharedLink));
        sharedLink = "";
    }
}

void printui::toolButtonOdf_clicked()
{
    // open document format file

    QSettings settings;
    qDebug() << settings.value("printui/odf/path").toString();
    QString exportpath = QFileDialog::getSaveFileName(this, tr("Save file"),
                                                      settings.value("printui/odf/path", QDir::homePath() + "/export.odt").toString(),
                                                      "Open Document Format (*.odt)");
    if (exportpath == "")
        return;

    QTextDocumentWriter *w = new QTextDocumentWriter(exportpath, "odf");

    QFile file(exportpath);
    if (QFile::exists(file.fileName())) {
        QFile::remove(file.fileName());
    }

    QTextDocument *d = new QTextDocument();
    d->setHtml(activeHtml);
    bool ok = w->write(d);

    QFileInfo info(exportpath);
    if (ok)
        settings.setValue("printui/odf/path", info.dir().path());
    QMessageBox::information(this, "", exportpath + " " + tr("file created"));
}

void printui::radioButtonClicked()
{
    printRequest();
}

void printui::printPublicMeetingWorkSheet()
{
    fillWeekList(publicmeetingday - 1);
    qDebug() << weekList[0]->week.toString(Qt::ISODate);

    int id = sql->getLanguageDefaultId();
    sql_items esitelmat = sql->selectSql("SELECT * FROM publictalks WHERE lang_id = " + QVariant(id).toString() + " ORDER BY theme_number");
    QString html;
    html.append(
            "<html>"
            "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
            "<title>Weekend Meeting worksheet</title>"
            "<style>"
            "table { page-break-inside:auto }"
            "tr    { page-break-inside:avoid; page-break-after:auto }"
            "thead { display:table-header-group }"
            "tfoot { display:table-footer-group }"
            "</style>"
            "</head>"
            "<body>");
    html.append("<p align=center>" + sql->getSetting("print_we_title", tr("Weekend Meeting")) + "</p>");
    html.append("<table border=1 cellpadding=2 cellspacing=0 width=100%>");
    for (unsigned int i = 0; i < esitelmat.size(); i++) {

        html.append("<tr><td>");
        html.append(esitelmat[i].value("theme_number").toString());
        html.append("  ");
        html.append(esitelmat[i].value("theme_name").toString());
        QString aihe_id = esitelmat[i].value("id").toString();
        sql_items pvm = sql->selectSql("SELECT date, theme_id FROM publicmeeting WHERE theme_id = " + aihe_id + " ORDER BY date");
        html.append("</td><td>");
        for (unsigned int j = 0; j < pvm.size(); j++) {
            QDate date = pvm[j].value("date").toDate();
            html.append(date.addDays(publicmeetingday - 1).toString(Qt::DefaultLocaleShortDate));
            html.append(" ");
            html.append("</tr>");
        }
    }
    html.append("</body></html>");

    setHTML(html);
    activeHtml = html;
}

void printui::applyAuthorizationRules()
{
    // midweek meeting
    if (!ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingSchedule) && !ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingAssignmentSlips) && !ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingSchedule)) {
        ui->radioButtonSchool->setHidden(true);
        ui->mwTemplateTypesWidget->setHidden(true);
        ui->radioButtonCombination->setHidden(true);
        ui->combinationTemplateTypesWidget->setHidden(true);
    } else {
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingSchedule)) {
            ui->radioButtonProgram->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingAssignmentSlips)) {
            ui->radioButtonTaskOrder->setHidden(true);
            ui->checkBoxAssistantSlip->setHidden(true);
            ui->checkBoxBlank->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintMidweekMeetingWorksheets)) {
            ui->radioButtonWorkSheet->setHidden(true);
        }
    }

    // weekend meeting
    if (!ac->user()->hasPermission(Permission::Rule::CanPrintWeekendMeetingSchedule) && !ac->user()->hasPermission(Permission::Rule::CanPrintWeekendMeetingWorksheets) && !ac->user()->hasPermission(Permission::Rule::CanPrintSpeakersSchedule) && !ac->user()->hasPermission(Permission::Rule::CanPrintSpeakersAssignments) && !ac->user()->hasPermission(Permission::Rule::CanPrintHospitality) && !ac->user()->hasPermission(Permission::Rule::CanPrintPublicTalkList)) {
        ui->radioButtonPublicMeeting->setHidden(true);
        ui->weTemplateTypesWidget->setHidden(true);
        ui->radioButtonCombination->setHidden(true);
        ui->combinationTemplateTypesWidget->setHidden(true);
    } else {
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintWeekendMeetingSchedule)) {
            ui->radioButton_PublicMeetingProgram->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintWeekendMeetingWorksheets)) {
            ui->radioButton_PublicMeetingWorkSheet->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintSpeakersSchedule)) {
            ui->radioButton_OutgoingSpeakers->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintSpeakersAssignments)) {
            ui->radioButton_OutgoingSpeakersSlips->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintHospitality)) {
            ui->radioButton_Hospitality->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintPublicTalkList)) {
            ui->radioButton_TalksList->setHidden(true);
        }
    }

    // territory
    if (!ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryRecord) && !ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryMapCard) && !ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryMapAndAddressSheets)) {
        ui->radioButtonTerritories->setHidden(true);
        ui->territoryTemplateTypesWidget->setHidden(true);
    } else {
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryRecord)) {
            ui->radioButton_TerritoryRecord->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryMapCard)) {
            ui->radioButton_TerritoryMapCard->setHidden(true);
        }
        if (!ac->user()->hasPermission(Permission::Rule::CanPrintTerritoryMapAndAddressSheets)) {
            ui->radioButton_TerritoryCard->setHidden(true);
        }
    }
}

void printui::printLMMWorkSheet()
{
    pr->setResolution(300);

    QString str = getTemplate();
    str = initPrinter(str);
    str = ifthen.tokenize(str);
    str = fillTemplateBlockLMMOverall(str);
    fillWeekList(0);

    QPair<QString, QString> sec = templateGetSection(str, "!REPEAT_START!", "!REPEAT_END!", "lmm");

    QString basepage = sec.first;
    QString section = sec.second;

    school s;
    int classcnt(s.getClassesQty());
    QString abc("ABC");
    QString iii("III");
    bool isFirst(true);

    QString weekContext = "";

    for (weekInfo *w : weekList) {
        currentWeek = w;
        //QDate tempdate(w->week);
        for (int c = 1; c <= classcnt; c++) {
            QString classLetter(abc.mid(c - 1, 1));
            QString classNumeral(iii.left(c));
            QString oneweek(section);
            oneweek = oneweek.replace(QRegExp("!(GW3|FM[1-9])(_SPEAKER!)"), "!\\1_SPEAKER_" + classLetter + "!");
            oneweek = oneweek.replace("_ASSISTANT!", "_ASSISTANT_" + classLetter + "!");
            oneweek = oneweek.replace("_NOTES!", "_NOTES_" + classLetter + "!");
            oneweek = oneweek.replace("_EX_DONE!", "_" + classLetter + "_EX_DONE!");
            oneweek = oneweek.replace("_STUDYNAME!", "_" + classLetter + "_STUDYNAME!");
            oneweek = oneweek.replace("_STUDY!", "_" + classLetter + "_STUDY!");
            oneweek = oneweek.replace("!CLASS!", tr("Class ") + " " + classNumeral);
            oneweek = oneweek.replace("!CLASS_SCHEDULED!", "!CLASS_" + classLetter + "_SCHEDULED!");

            oneweek = fillTemplateBlockLMM(oneweek, true);
            if (oneweek.length() > 0) {
                if (isFirst)
                    isFirst = false;
                else
                    weekContext.append(addCssPageBreak());
                weekContext.append(oneweek);
            }
        }
    }
    basepage.replace("<div class=\"lmm\"></div>", "<div>" + weekContext + "</div>");
    setHTML(ifthen.compute(basepage));
}

void printui::printLMMSchedule()
{
    pr->setResolution(300);
    QString str = getTemplate();
    str = initPrinter(str);
    str = ifthen.tokenize(str);
    str = fillTemplateBlockLMMOverall(str);
    fillWeekList(0);

    // titles for all meetings
    str.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    str.replace("!CONGREGATION!", tr("Congregation"));
    str.replace("!CONGREGATION_NAME!", myCongregation.name);
    str.replace("!PT_TITLE!", tr("Public Talk"));
    str.replace("!WT_TITLE!", tr("Watchtower Study"));
    str.replace("!TREASURES_TITLE!", tr("Treasures from God's Word"));
    str.replace("!APPLY_TITLE!", tr("Apply Yourself to the Field Ministry"));
    str.replace("!LIVING_TITLE!", tr("Living as Christians"));

    QPair<QString, QString> sec = templateGetSection(str, "!REPEAT_START!", "!REPEAT_END!", "lmm");

    QString basepage = sec.first;
    QString section = sec.second;

    // Fill schedule for each week
    QString weekContext = "";
    for (weekInfo *w : weekList) {
        currentWeek = w;
        //QDate tempdate(w->week);
        QString oneweek(section);
        oneweek = fillTemplateBlockLMM(oneweek, false);
        weekContext.append(oneweek);
    }
    basepage.replace("<div class=\"lmm\"></div>", "<div class=\"lmm\">" + weekContext + "</div>");
    // Add QR Code
    basepage = addQrCode(basepage);

    setHTML(ifthen.compute(basepage));
}

void printui::printLMMAssignmentSlips_NoFormFields(bool debugBoxes, bool forceRescan, bool usePrinter)
{
    qDebug() << "printLMMAssignmentSlips_NoFormFields";
    QPagedPaintDevice *pdevice = nullptr;
    QByteArray byteArray;
    QPainter *painter(nullptr);
    QBuffer buffer;

    //qDebug() << "font";
    QFont f;
    QFont ckFont;
    // font size adjusting
    f.setPixelSize(static_cast<int>(std::round(40 * textSizeFactorySlips)));
    ckFont.setPixelSize(static_cast<int>(std::round(40 * textSizeFactorySlips)));
    f.setFamily("Arial");
    ckFont.setFamily("Arial");
    ckFont.setBold(true);

    QString templateSlipPath = getTemplate(true);
    if (!QFile(templateSlipPath).exists()) {
        if (templateSlipPath == "")
            templateSlipPath = tr("Slip Template");

        buffer.setBuffer(&byteArray);
        buffer.open(QIODevice::WriteOnly);
        pdevice = new QPdfWriter(&buffer);
        painter = new QPainter(pdevice);
        painter->setWindow(0, 0, 2000, 2000);
        painter->setFont(f);
        painter->drawText(50, 50, templateSlipPath + " not found");
        painter->end();
        ui->previewPdf->setPdf(byteArray);
        delete painter;
        return;
    }
    bool onePerPage = !templateSlipPath.contains("4up.", Qt::CaseInsensitive);
    bool showCounselNames = currentTemplateData->optionValue("counsel-name") == "yes";

    SlipScanner ss(this, templateSlipPath, forceRescan);

    customsize = QSize(ss.pageWidth, ss.pageHeight);
    if (usePrinter) {
        if (onePerPage) {
            pr->setPaperSize(QPrinter::A6);
        } else {
            if (currentTemplateData->customPaperSize.width() == 0.0) {
                pr->setPaperSize(currentTemplateData->standardPaperSize);
            } else {
                pr->setPaperSize(currentTemplateData->customPaperSize, currentTemplateData->customPaperUnit);
            }
        }
        pr->setOrientation(QPrinter::Portrait);
        pr->setFullPage(true);
        pr->setPageMargins(0, 0, 0, 0, QPrinter::Millimeter);
        pdevice = pr;
    } else {
        buffer.setBuffer(&byteArray);
        buffer.open(QIODevice::WriteOnly);
        pdevice = new QPdfWriter(&buffer);
    }
    painter = new QPainter(pdevice);
    painter->setWindow(0, 0, customsize.width(), customsize.height());

#ifdef Q_OS_WIN
    // windows fix
    double xTransform = ((pr->pageRect().x() * QVariant(customsize.width()).toDouble()) / pr->paperRect().width());
    double yTransform = ((pr->pageRect().y() * QVariant(customsize.height()).toDouble()) / pr->paperRect().height());
    //qDebug() << "page rect:" << pr->pageRect();
    //qDebug() << "paper rect:" << pr->paperRect();
    //qDebug() << "transform:" << xTransform << yTransform;
    painter->translate(xTransform * -1, yTransform * -1);
#endif

    painter->setFont(f);
    QFontMetrics fm = painter->fontMetrics();
    int textboxOffset(-fm.height());
    int textboxHeight(static_cast<int>(std::round(fm.height() * 2.1)));
#ifdef Q_OS_WIN
    QString checkmark = "√";
#else
    QString checkmark = "\u2713";
#endif

    LMM_Meeting *mtg = new LMM_Meeting();
    int slipnum(0);
    int maxslipsperpage(onePerPage ? 1 : 4);
    bool is2019Slip(false);
    bool wasErr(false);

    if (ss.needFinalized) {
        // try for the pre-2019 box count since it's larger
        ss.FinalizeBoxes(11);
        bool tryFor10(false);

        if (ss.checkBoxes.length() == 11 * maxslipsperpage && ss.textBoxes.length() == 5 * maxslipsperpage) {
            // pre-2019 slip
        } else if (ss.checkBoxes.length() == 10 * maxslipsperpage && ss.textBoxes.length() == 4 * maxslipsperpage) {
        } else {
            tryFor10 = true;
        }

        if (!tryFor10) {
            // if the layout isn't known, try for 10 checkboxes
            tryFor10 = ss.layout != 703 && ss.layout != 704 && ss.layout != 803;
        }

        if (tryFor10)
            ss.FinalizeBoxes(10);
    }

    if (ss.checkBoxes.length() == 11 * maxslipsperpage && ss.textBoxes.length() == 5 * maxslipsperpage) {
        // pre-2019 slip
    } else if (ss.checkBoxes.length() == 10 * maxslipsperpage && ss.textBoxes.length() == 4 * maxslipsperpage) {
        is2019Slip = true;
    } else {
        painter->drawText(100, 100, "Box count for this template is incorrect");
        painter->drawText(100, 200, "Expected " + QString::number(4 * maxslipsperpage) + " text boxes but have " + QString::number(ss.textBoxes.length()));
        painter->drawText(100, 300, "Expected " + QString::number(10 * maxslipsperpage) + " check boxes but have " + QString::number(ss.checkBoxes.length()));
        wasErr = true;
    }

    if (debugBoxes) {
        int id(1);
        for (SlipScanner::region *box : ss.textBoxes) {
            painter->drawText(box->x1, box->y2, "Box #" + QString::number(id++));
            painter->drawRect(box->x1, box->y1, box->width, box->height);
        }
        id = 1;
        for (SlipScanner::region *box : ss.checkBoxes) {
            //qDebug() << "checkbox" << id << box->x1 << box->y1 << box->half << box->quadrant;
            painter->setFont(ckFont);
            drawCheckbox(painter, box->x1, box->y2);
            painter->setFont(f);
            painter->drawText(box->x1 - box->width * 3, box->y2, QString::number(id++));
            painter->drawText(box->x1 + box->width * 1, box->y2, QString::number(box->width) + "x" + QString::number(box->height));
            painter->drawRect(box->x1, box->y1, box->width, box->height);
        }
        wasErr = true;
    }

    if (wasErr) {
        painter->end();
        ui->previewPdf->setPdf(byteArray);
        delete painter;
        return;
    }

    int needNewPage(1); // 0=none, 1=need image, 2=need page

    QList<SlipScanner::region *> br;
    QList<SlipScanner::region *> init;
    QList<SlipScanner::region *> rv1;
    QList<SlipScanner::region *> rv2;
    QList<SlipScanner::region *> rv3;
    QList<SlipScanner::region *> bs;
    QList<SlipScanner::region *> talk;
    QList<SlipScanner::region *> other;
    QList<SlipScanner::region *> main;
    QList<SlipScanner::region *> aux1;
    QList<SlipScanner::region *> aux2;
    QList<QRectF *> nm;
    QList<QRectF *> astn;
    QList<QRectF *> dt;
    QList<QRectF *> cp;
    QList<QRectF *> oasgn;

    //qDebug() << "fill lists";
    //qDebug() << ss.checkBoxes.length();
    //qDebug() << ss.textBoxes.length();
    int cknum(0);
    int txtnum(0);
    SlipScanner::region *box;
    for (int snum = 0; snum < maxslipsperpage; snum++) {
        switch (ss.layout) { // layout is "boxes in first column" * 100 + "boxes in second column"
        case 703:
            br.append(ss.checkBoxes[cknum++]);
            init.append(ss.checkBoxes[cknum++]);
            rv1.append(ss.checkBoxes[cknum++]);
            rv2.append(ss.checkBoxes[cknum++]);
            main.append(ss.checkBoxes[cknum++]);
            aux1.append(ss.checkBoxes[cknum++]);
            aux2.append(ss.checkBoxes[cknum++]);
            bs.append(ss.checkBoxes[cknum++]);
            talk.append(ss.checkBoxes[cknum++]);
            other.append(ss.checkBoxes[cknum++]);
            break;
        case 704:
            br.append(ss.checkBoxes[cknum++]);
            init.append(ss.checkBoxes[cknum++]);
            rv1.append(ss.checkBoxes[cknum++]);
            rv2.append(ss.checkBoxes[cknum++]);
            main.append(ss.checkBoxes[cknum++]);
            aux1.append(ss.checkBoxes[cknum++]);
            aux2.append(ss.checkBoxes[cknum++]);
            rv3.append(ss.checkBoxes[cknum++]);
            bs.append(ss.checkBoxes[cknum++]);
            talk.append(ss.checkBoxes[cknum++]);
            other.append(ss.checkBoxes[cknum++]);
            break;
        case 803:
            br.append(ss.checkBoxes[cknum++]);
            init.append(ss.checkBoxes[cknum++]);
            rv1.append(ss.checkBoxes[cknum++]);
            rv2.append(ss.checkBoxes[cknum++]);
            rv3.append(ss.checkBoxes[cknum++]);
            bs.append(ss.checkBoxes[cknum++]);
            talk.append(ss.checkBoxes[cknum++]);
            other.append(ss.checkBoxes[cknum++]);
            main.append(ss.checkBoxes[cknum++]);
            aux1.append(ss.checkBoxes[cknum++]);
            aux2.append(ss.checkBoxes[cknum++]);
            break;
        default:
            QMessageBox::warning(this, "Unknown Slip", QObject::tr("You have a new kind of slip. Please enter a request on the forum so the app can be updated.  Mention code %1").replace("%1", QVariant(ss.layout).toString()));
            wasErr = true;
        }

        box = ss.textBoxes[txtnum++];
        nm.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));

        box = ss.textBoxes[txtnum++];
        astn.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));

        box = ss.textBoxes[txtnum++];
        dt.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));

        if (!is2019Slip) {
            box = ss.textBoxes[txtnum++];
            cp.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
        }

        box = ss.textBoxes[txtnum++];
        oasgn.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
    }

    //qDebug() << "br" << br.length();
    //qDebug() << "init" << init.length();
    //qDebug() << "rv" << rv.length();
    //qDebug() << "bs" << bs.length();
    //qDebug() << "other" << other.length();
    //qDebug() << "main" << main.length();
    //qDebug() << "aux1" << aux1.length();
    //qDebug() << "aux2" << aux2.length();
    //qDebug() << "nm" << nm.length();
    //qDebug() << "astn" << astn.length();
    //qDebug() << "dt" << dt.length();
    //qDebug() << "cp" << cp.length();
    //qDebug() << "oasgn" << oasgn.length();

    //qDebug() << "fillweeklist";
    school s;
    fillWeekList(0);
    bool onlyAssigned(ui->checkBoxBlank->isChecked());
    bool printAssistant(ui->checkBoxAssistantSlip->isChecked());

    QHash<int, QList<SlipScanner::region *>> checkboxLookup;
    checkboxLookup.insert(LMM_Schedule::TalkType_BibleReading, br);
    checkboxLookup.insert(LMM_Schedule::TalkType_InitialCall, init);
    checkboxLookup.insert(LMM_Schedule::TalkType_ReturnVisit1, rv1);
    checkboxLookup.insert(LMM_Schedule::TalkType_ReturnVisit2, rv2);
    checkboxLookup.insert(LMM_Schedule::TalkType_ReturnVisit3, rv3);
    checkboxLookup.insert(LMM_Schedule::TalkType_BibleStudy, bs);
    checkboxLookup.insert(LMM_Schedule::TalkType_StudentTalk, talk);

    //qDebug() << "main loop";
    if (!wasErr) {
        for (weekInfo *w : weekList) {
            currentWeek = w;
            QDate tempdate(w->week);
            if (currentWeek->week.year() > 2018 && mtg->loadMeeting(tempdate)) {
                QList<LMM_Assignment *> prog = mtg->getAssignments();
                QList<int> ckUsed;
                for (LMM_Assignment *asgn : prog) {
                    for (int spk_asst = 0; spk_asst < 2; spk_asst++) {

                        if (!asgn->canCounsel())
                            continue;
                        LMM_Assignment_ex *studenttalk = qobject_cast<LMM_Assignment_ex *>(asgn);
                        if ((onlyAssigned && !studenttalk->speaker()) || (spk_asst && !(printAssistant && studenttalk->assistant())))
                            continue;

                        if (needNewPage) {
                            if (needNewPage > 1)
                                pdevice->newPage();
                            painter->drawImage(0, 0, ss.img);
                            needNewPage = 0;
                            slipnum = 0;
                        }

                        int sdate = c.getMeetingDay(tempdate, ccongregation::tms);
                        QDate date = tempdate.addDays(sdate - 1);
                        // we'll print the date after we (possibly) add study number
                        QString fullDateText(date.toString(Qt::DefaultLocaleShortDate));
                        qDebug() << "  date" << date;
                        //qDebug() << "  draw date";
                        //qDebug() << slipnum;
                        //qDebug() << dt[slipnum]->x1;

                        //qDebug() << "  draw speakers";
                        //qDebug() << "  slip " << slipnum << studenttalk->speaker()->fullname();

                        if (studenttalk->speaker())
                            painter->drawText(*nm[slipnum], studenttalk->speaker()->fullname());
                        if (studenttalk->assistant())
                            painter->drawText(*astn[slipnum], studenttalk->assistant()->fullname());

                        //qDebug() << "  draw other";
                        if (spk_asst && printAssistant && studenttalk->assistant()) {
                            painter->setFont(ckFont);
                            drawCheckbox(painter, other[slipnum]->x1, other[slipnum]->y2);
                            painter->setFont(f);
                            painter->drawText(*oasgn[slipnum], tr("Assistant"));
                        } else if (studenttalk->speaker()) {
                            QString txt;
                            if (tempdate.year() < 2019) {
                                QString unknowncounselpoint;
                                schoolStudentStudy *study = studenttalk->getCounselPoint(unknowncounselpoint);

                                txt = QVariant(study->getNumber()).toString();
                                if (showCounselNames)
                                    txt += "-" + study->getName();
                                if (!unknowncounselpoint.isEmpty())
                                    txt = QString("%1 (%2)").arg(unknowncounselpoint, txt);
                            } else if (is2019Slip) {
                                fullDateText += "  (" + QString(tr("Study %1", "Text for study point on slip")).arg(QVariant(studenttalk->study_number()).toString()) + ")";
                            } else {
                                txt = QVariant(studenttalk->study_number()).toString() + " " + studenttalk->study_name();
                            }
                            if (!txt.isEmpty()) {
                                painter->drawText(*cp[slipnum], txt);
                            }
                        }

                        painter->drawText(*dt[slipnum], fullDateText);

                        //qDebug() << "  draw ck";
                        if (is2019Slip && studenttalk->talkId() == LMM_Schedule::TalkType_ReturnVisit3) {
                            // no longer used
                        } else {
                            SlipScanner::region *ck;
                            ck = checkboxLookup[studenttalk->talkId()][slipnum];
                            painter->setFont(ckFont);
                            drawCheckbox(painter, ck->x1, ck->y2);
                            painter->setFont(f);
                            int ckKey(studenttalk->talkId() * 100 + studenttalk->classnumber() * 10 + studenttalk->sequence());
                            if (ckUsed.contains(ckKey)) {
                                painter->setFont(ckFont);
                                for (int i = 0; i < 10; i++)
                                    drawCheckbox(painter, other[slipnum]->x1, other[slipnum]->y2);
                                painter->setFont(f);
                                if (!spk_asst) {
                                    painter->drawText(*oasgn[slipnum], tr("2nd talk", "When printing slips: if the first talk is not 'Return Visit'"));
                                    /*
                                    switch (studenttalk->talkId()) {
                                    case LMM_Schedule::TalkType_InitialCall:
                                        oasgn[slipnum]->setValue(tr("1st talk", "When printing slips: if the first talk is not 'Inital Call'"));
                                        break;
                                    case LMM_Schedule::TalkType_ReturnVisit:
                                        oasgn[slipnum]->setValue(tr("2nd talk", "When printing slips: if the first talk is not 'Return Visit'"));
                                        break;
                                    case LMM_Schedule::TalkType_BibleStudy:
                                        oasgn[slipnum]->setValue(tr("3rd talk", "When printing slips: if the first talk is not 'Bible Study'"));
                                        break;
                                    }
                                    */
                                }
                            } else if (!spk_asst)
                                ckUsed.append(ckKey);

                            //qDebug() << " class";
                            switch (studenttalk->classnumber()) {
                            case 1:
                                drawCheckbox(painter, main[slipnum]->x1, main[slipnum]->y2);
                                break;
                            case 2:
                                drawCheckbox(painter, aux1[slipnum]->x1, aux1[slipnum]->y2);
                                break;
                            case 3:
                                drawCheckbox(painter, aux2[slipnum]->x1, aux2[slipnum]->y2);
                                break;
                            }
                        }

                        slipnum++;
                        if (slipnum >= maxslipsperpage)
                            needNewPage = 2;
                    }
                }
            }
        }
    }

    //qDebug() << "finish up";
    painter->end();
    if (!usePrinter) {
        ui->previewPdf->setPdf(byteArray);
        delete pdevice;
    }
    if (buffer.isOpen())
        buffer.close();
    delete painter;
}

void printui::drawCheckbox(QPainter *painter, int x, int y)
{
#ifdef Q_OS_WIN
    QString checkmark = "√";
#else
    QString checkmark = "\u2713";
#endif
    for (int i = -2; i < 3; i++)
        painter->drawText(x + i, y, checkmark);
}

void printui::printLMMAssignmentSlipsPreview()
{
    if (ui->cboTemplate->currentText().endsWith("pdf", Qt::CaseInsensitive))
        setHTML("<HTML><BODY><h1><center>" + tr("Click the print button to preview assignment slips") + "<center></h1><h2><center>" + tr("Print button is found at the bottom right corner") + "</center></h2></BODY></HTML>");
    else
        printLMMAssignmentSlips_NoFormFields(
                QGuiApplication::keyboardModifiers() & Qt::ControlModifier,
                QGuiApplication::keyboardModifiers() & Qt::ShiftModifier);
}

void printui::saveSlipBoxFields(QString templateSlipPath)
{
    QString pdftk = "";
#if defined(Q_OS_MACOS)
    pdftk = qApp->applicationDirPath() + "/../pdftk/bin/pdftk";
#elif defined(Q_OS_LINUX)
    QProcess process;
    process.start("which pdftk");
    process.waitForFinished(-1);
    pdftk = QString(process.readAllStandardOutput()).trimmed();
#elif defined(Q_OS_WIN)
    pdftk = qApp->applicationDirPath() + "/pdftk.exe";
#endif
    if (!QFile(pdftk).exists()) {
        QMessageBox::warning(this, "", pdftk + " not found!");
        return;
    }
#if defined(Q_OS_WIN)
    pdftk = "\"" + pdftk + "\"";
#endif

    bool onePerPage;
    QString slipLanguage;
    {
        int lStart(templateSlipPath.indexOf("S-89-"));
        int lEnd(templateSlipPath.indexOf("4up", lStart));
        if (lEnd < 0)
            lEnd = templateSlipPath.indexOf(".pdf", lStart);
        if (lStart < 0 || lEnd < 0)
            slipLanguage = "e"; // something didn't work right...hope for the best by using English
        else
            slipLanguage = templateSlipPath.mid(lStart + 5, lEnd - lStart - 6).toLower();
        onePerPage = templateSlipPath.indexOf("4up", lEnd) < 0;
    }

    QStringList args;
    args << templateSlipPath << "dump_data_fields";
    QProcess go;
    go.start(pdftk, args);
    go.waitForFinished();
    QString results(go.readAllStandardOutput());

    QList<int> ckNames;
    QList<int> txtNames;
    /*
    QFile fResults(results);
    fResults.open(QIODevice::ReadOnly | QIODevice::Text);
    */
    QTextStream stream(&results);
    QRegularExpression rx("(?<datatype>.+):\\s*(?<base>[^\\d]+)(?<num>\\d+)");
    while (!stream.atEnd()) {
        QString ln(stream.readLine());
        QRegularExpressionMatch m(rx.match(ln));
        if (m.hasMatch() && m.captured("datatype") == "FieldName") {
            if (m.captured("base").contains("Check"))
                ckNames.append(m.captured("num").toInt());
            else
                txtNames.append(m.captured("num").toInt());
        }
    }
    if (ckNames.count() == 0 || txtNames.count() == 0) {
        QMessageBox::warning(this, "", tr("Unable to find information in this template: ") + templateSlipPath);
        return;
    }
    std::sort(ckNames.begin(), ckNames.end());
    std::sort(txtNames.begin(), txtNames.end());
    QString settingNamePattern("slip%1_%2_%3");
    sql->saveSetting(settingNamePattern.arg(onePerPage ? "1" : "4", slipLanguage, "ckboxnames"), intListJoin(ckNames, ";"));
    sql->saveSetting(settingNamePattern.arg(onePerPage ? "1" : "4", slipLanguage, "boxnames"), intListJoin(txtNames, ";"));
}

bool printui::generateSlipPdfToJpg(QString pdfpath, QString jpgpath)
{
    QProgressDialog progressdialog(QString(tr("Converting %1 to JPG file")).arg(pdfpath), "", 0, 0);
    progressdialog.setCancelButton(nullptr);
    progressdialog.setWindowModality(Qt::ApplicationModal);
    progressdialog.show();
    ccloud cl;
    bool ret = cl.convertPdfToJpg(pdfpath, jpgpath);
    progressdialog.close();
    return ret;
}

QString printui::intListJoin(QList<int> list, QString delim)
{
    QString ret;
    bool isFirst(true);
    for (int n : list) {
        if (isFirst)
            isFirst = false;
        else
            ret.append(delim);
        ret.append(QString::number(n));
    }
    return ret;
}

void printui::printLMMAssignmentSlips(bool debugBoxes, bool findBoxNames)
{

    QString pdftk = "";
#if defined(Q_OS_MACOS)
    pdftk = qApp->applicationDirPath() + "/../pdftk/bin/pdftk";
#elif defined(Q_OS_LINUX)
    QProcess process;
    process.start("which pdftk");
    process.waitForFinished(-1);
    pdftk = QString(process.readAllStandardOutput()).trimmed();
#elif defined(Q_OS_WIN)
    pdftk = qApp->applicationDirPath() + "/pdftk.exe";
#endif
    if (!QFile(pdftk).exists()) {
        QMessageBox::warning(this, "", pdftk + " not found!");
        return;
    }
#if defined(Q_OS_WIN)
    pdftk = "\"" + pdftk + "\"";
#endif
    QString templateSlipPath = getTemplate(true);
    if (!QFile(templateSlipPath).exists()) {
        if (templateSlipPath == "")
            templateSlipPath = tr("Slip Template");
        QMessageBox::warning(this, "", templateSlipPath + " not found!");
        return;
    }
    bool onePerPage = !templateSlipPath.contains("4up.", Qt::CaseInsensitive);
    QString slipLanguage;
    {
        int lStart(templateSlipPath.indexOf("S-89-"));
        int lEnd(templateSlipPath.indexOf("4up", lStart));
        if (lEnd < 0)
            lEnd = templateSlipPath.indexOf(".pdf", lStart);
        if (lStart < 0 || lEnd < 0)
            slipLanguage = "e"; // something didn't work right...hope for the best by using English
        else
            slipLanguage = templateSlipPath.mid(lStart + 5, lEnd - lStart - 6).toLower();
    }

    LMM_Meeting *mtg = new LMM_Meeting();
    int slipnum(0);
    int slippagenum(1);
    QStringList pdfs;
    QStringList deleteMe;
    int maxslipsperpage(onePerPage ? 1 : 4);
    QString slipBoxBase("slip" + QString::number(maxslipsperpage) + "_" + slipLanguage + "_");
    if (findBoxNames || sql->getSetting(slipBoxBase + "ckboxnames", "new") == "new")
        saveSlipBoxFields(templateSlipPath);

    QString compiledNames(sql->getSetting(slipBoxBase + "ckboxnames", "1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44"));
    bool fullBoxName(compiledNames.startsWith("~"));
    QString ckPrefix(fullBoxName ? "" : "Check Box");
    if (fullBoxName)
        compiledNames = compiledNames.mid(1);
    QStringList cknumbers(compiledNames.split(';'));

    compiledNames = sql->getSetting(slipBoxBase + "boxnames", "1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20");
    fullBoxName = compiledNames.startsWith("~");
    QString txtPrefix(fullBoxName ? "" : "Text");
    if (fullBoxName)
        compiledNames = compiledNames.mid(1);
    QStringList txtnumbers(compiledNames.split(';'));
    pdfform pdf;
    QList<pdfform::checkbox *> br;
    QList<pdfform::checkbox *> init;
    QList<pdfform::checkbox *> rv1;
    QList<pdfform::checkbox *> rv2;
    QList<pdfform::checkbox *> rv3;
    QList<pdfform::checkbox *> bs;
    QList<pdfform::checkbox *> talk;
    QList<pdfform::checkbox *> other;
    QList<pdfform::checkbox *> main;
    QList<pdfform::checkbox *> aux1;
    QList<pdfform::checkbox *> aux2;
    QList<pdfform::textbox *> nm;
    QList<pdfform::textbox *> astn;
    QList<pdfform::textbox *> dt;
    QList<pdfform::textbox *> cp;
    QList<pdfform::textbox *> oasgn;

    if (debugBoxes) {

        if (onePerPage) {
            for (int i = 0; i < 8; i++) {
                other.append(new pdfform::checkbox(ckPrefix + cknumbers[i]));
                pdf.append(other[i]);
            }
            for (int i = 0; i < 5; i++) {
                oasgn.append(new pdfform::textbox(txtPrefix + txtnumbers[i]));
                pdf.append(oasgn[i]);
            }

            // textboxes
            for (int i = 0; i < 5; i++) {
                oasgn[i]->setValue("# " + QString::number(i + 1) + ": " + oasgn[i]->Name());
            }
            finalizeSlip(pdftk, templateSlipPath, &pdf, &slippagenum, &pdfs, &deleteMe);

            // clear
            for (int i = 0; i < 5; i++) {
                oasgn[i]->setValue("");
            }

            // checkboxes
            for (int i = 0; i < 8; i++) {
                oasgn[0]->setValue(other[i]->Name());
                for (int x = 0; x < 8; x++) {
                    other[x]->setValue(x == i);
                }

                finalizeSlip(pdftk, templateSlipPath, &pdf, &slippagenum, &pdfs, &deleteMe);
            }
        } else {
            for (int i = 0; i < 32; i++) {
                other.append(new pdfform::checkbox(ckPrefix + cknumbers[i]));
                pdf.append(other[i]);
            }
            for (int i = 0; i < 20; i++) {
                oasgn.append(new pdfform::textbox(txtPrefix + txtnumbers[i]));
                pdf.append(oasgn[i]);
            }

            // textboxes
            for (int i = 0; i < 20; i++) {
                oasgn[i]->setValue("# " + QString::number(i + 1) + ": " + oasgn[i]->Name());
            }
            finalizeSlip(pdftk, templateSlipPath, &pdf, &slippagenum, &pdfs, &deleteMe);

            // clear
            for (int i = 0; i < 20; i++) {
                oasgn[i]->setValue("");
            }

            // checkboxes
            for (int i = 0; i < 32; i++) {
                oasgn[0]->setValue(other[i]->Name());
                for (int x = 0; x < 32; x++) {
                    other[x]->setValue(x == i);
                }

                finalizeSlip(pdftk, templateSlipPath, &pdf, &slippagenum, &pdfs, &deleteMe);
            }
        }

    } else {

        int cknum(0);
        int txtnum(0);
        for (int snum = 0; snum < maxslipsperpage; snum++) {
            br.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(br[snum]);
            init.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(init[snum]);
            rv1.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(rv1[snum]);
            rv2.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(rv2[snum]);
            rv3.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(rv3[snum]);
            bs.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(bs[snum]);
            talk.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(talk[snum]);
            other.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(other[snum]);
            main.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(main[snum]);
            aux1.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(aux1[snum]);
            aux2.append(new pdfform::checkbox(ckPrefix + cknumbers[cknum++]));
            pdf.append(aux2[snum]);
            nm.append(new pdfform::textbox(txtPrefix + txtnumbers[txtnum++]));
            pdf.append(nm[snum]);
            astn.append(new pdfform::textbox(txtPrefix + txtnumbers[txtnum++]));
            pdf.append(astn[snum]);
            dt.append(new pdfform::textbox(txtPrefix + txtnumbers[txtnum++]));
            pdf.append(dt[snum]);
            cp.append(new pdfform::textbox(txtPrefix + txtnumbers[txtnum++]));
            pdf.append(cp[snum]);
            oasgn.append(new pdfform::textbox(txtPrefix + txtnumbers[txtnum++]));
            pdf.append(oasgn[snum]);
        }

        school s;
        fillWeekList(0);
        bool onlyAssigned(ui->checkBoxBlank->isChecked());
        bool printAssistant(ui->checkBoxAssistantSlip->isChecked());

        QHash<int, QList<pdfform::checkbox *>> checkboxLookup;
        checkboxLookup.insert(LMM_Schedule::TalkType_BibleReading, br);
        checkboxLookup.insert(LMM_Schedule::TalkType_InitialCall, init);
        checkboxLookup.insert(LMM_Schedule::TalkType_ReturnVisit1, rv1);
        checkboxLookup.insert(LMM_Schedule::TalkType_ReturnVisit2, rv2);
        checkboxLookup.insert(LMM_Schedule::TalkType_ReturnVisit3, rv3);
        checkboxLookup.insert(LMM_Schedule::TalkType_BibleStudy, bs);
        checkboxLookup.insert(LMM_Schedule::TalkType_StudentTalk, talk);

        for (weekInfo *w : weekList) {
            currentWeek = w;
            QDate tempdate(w->week);
            if (mtg->loadMeeting(tempdate)) {
                QList<LMM_Assignment *> prog = mtg->getAssignments();
                QList<int> ckUsed;
                for (LMM_Assignment *asgn : prog) {
                    for (int spk_asst = 0; spk_asst < 2; spk_asst++) {
                        if (!asgn->canCounsel())
                            continue;
                        LMM_Assignment_ex *studenttalk = qobject_cast<LMM_Assignment_ex *>(asgn);
                        if (onlyAssigned && !studenttalk->speaker())
                            continue;

                        int sdate = c.getMeetingDay(tempdate, ccongregation::tms);
                        QDate date = tempdate.addDays(sdate - 1);
                        dt[slipnum]->setValue(date.toString(Qt::DefaultLocaleShortDate));

                        if (studenttalk->speaker())
                            nm[slipnum]->setValue(studenttalk->speaker()->fullname());
                        if (studenttalk->assistant())
                            astn[slipnum]->setValue(studenttalk->assistant()->fullname());
                        else
                            astn[slipnum]->setValue("");

                        if (spk_asst) {
                            if (printAssistant && studenttalk->assistant()) {
                                other[slipnum]->setValue(true);
                                oasgn[slipnum]->setValue(tr("Assistant"));
                            } else
                                continue;
                        } else if (studenttalk->speaker()) {
                            if (tempdate.year() < 2019) {
                                QString unknowncounselpoint;
                                schoolStudentStudy *study = studenttalk->getCounselPoint(unknowncounselpoint);
                                cp[slipnum]->setValue(QVariant(study->getNumber()).toString() + "-" + study->getName());
                                if (!unknowncounselpoint.isEmpty())
                                    cp[slipnum]->setValue(QString("%1 (%2)").arg(unknowncounselpoint, cp[slipnum]->getValue()));
                            } else {
                                cp[slipnum]->setValue(QVariant(studenttalk->study_number()).toString() + " " + studenttalk->study_name());
                            }
                        }

                        checkboxLookup[studenttalk->talkId()][slipnum]->setValue(true);
                        int ckKey(studenttalk->talkId() * 100 + studenttalk->classnumber() * 10 + studenttalk->sequence());
                        if (ckUsed.contains(ckKey)) {
                            other[slipnum]->setValue(true);
                            if (!spk_asst) {
                                oasgn[slipnum]->setValue(tr("2nd talk", "When printing slips: if the first talk is not 'Return Visit'"));
                                /*
                                switch (studenttalk->talkId()) {
                                case LMM_Schedule::TalkType_InitialCall:
                                    oasgn[slipnum]->setValue(tr("1st talk", "When printing slips: if the first talk is not 'Inital Call'"));
                                    break;
                                case LMM_Schedule::TalkType_ReturnVisit:
                                    oasgn[slipnum]->setValue(tr("2nd talk", "When printing slips: if the first talk is not 'Return Visit'"));
                                    break;
                                case LMM_Schedule::TalkType_BibleStudy:
                                    oasgn[slipnum]->setValue(tr("3rd talk", "When printing slips: if the first talk is not 'Bible Study'"));
                                    break;
                                }
                                */
                            }
                        } else if (!spk_asst)
                            ckUsed.append(ckKey);

                        switch (studenttalk->classnumber()) {
                        case 1:
                            main[slipnum]->setValue(true);
                            break;
                        case 2:
                            aux1[slipnum]->setValue(true);
                            break;
                        case 3:
                            aux2[slipnum]->setValue(true);
                            break;
                        }

                        slipnum++;
                        if (slipnum >= maxslipsperpage) {
                            finalizeSlip(pdftk, templateSlipPath, &pdf, &slippagenum, &pdfs, &deleteMe);
                            slipnum = 0;

                            for (int snum = 0; snum < maxslipsperpage; snum++) {
                                dt[snum]->setValue("");
                                nm[snum]->setValue("");
                                astn[snum]->setValue("");
                                cp[snum]->setValue("");
                                oasgn[snum]->setValue("");
                                br[snum]->setValue(false);
                                init[snum]->setValue(false);
                                bs[snum]->setValue(false);
                                rv1[snum]->setValue(false);
                                rv2[snum]->setValue(false);
                                rv3[snum]->setValue(false);
                                talk[snum]->setValue(false);
                                other[snum]->setValue(false);
                                main[snum]->setValue(false);
                                aux1[snum]->setValue(false);
                                aux2[snum]->setValue(false);
                            }
                        }
                    }
                }
            }
        }

        if (slipnum > 0) {
            // clear any partially created slip
            dt[slipnum]->setValue("");
            nm[slipnum]->setValue("");
            astn[slipnum]->setValue("");
            cp[slipnum]->setValue("");
            oasgn[slipnum]->setValue("");
            br[slipnum]->setValue(false);
            init[slipnum]->setValue(false);
            bs[slipnum]->setValue(false);
            rv1[slipnum]->setValue(false);
            rv2[slipnum]->setValue(false);
            rv3[slipnum]->setValue(false);
            talk[slipnum]->setValue(false);
            other[slipnum]->setValue(false);
            main[slipnum]->setValue(false);
            aux1[slipnum]->setValue(false);
            aux2[slipnum]->setValue(false);

            finalizeSlip(pdftk, templateSlipPath, &pdf, &slippagenum, &pdfs, &deleteMe);
        }
    }

    if (slippagenum > 0) {
        QString destFile(slipWorkingPath + ".pdf");
        if (!individualSlips) {
            QStringList args;
            args << pdfs << "cat"
                 << "output" << destFile;
            qDebug() << pdftk << args;
            QProcess::execute(pdftk, args);
        }
        for (QString file : deleteMe) {
            QFile::remove(file);
        }
        if (individualSlips) {
            QMessageBox::information(this, "", tr("Slips created at %1*.pdf").arg(slipWorkingPath));
        } else {
            QDesktopServices::openUrl(QUrl::fromLocalFile(destFile));
        }
    }
}

void printui::finalizeSlip(QString pdftk, QString templateSlipPath, pdfform *pdf, int *slippagenum, QStringList *pdfs, QStringList *deleteMe)
{
    QString results(pdf->fdfvalue());

    QString slipnum("0" + QString::number((*slippagenum)++));
    QString slipBase(slipWorkingPath + slipnum.right(2));
    QString slipfdf(slipBase + ".fdf");
    QString slippdf(slipBase + ".pdf");
    QFile outputFile(slipfdf);
    outputFile.open(QIODevice::WriteOnly);
    if (!outputFile.isOpen()) {
        return;
    }
    QTextStream outStream(&outputFile);
    outStream.setCodec("UTF-8");
    outStream << results;
    outputFile.close();

    QStringList args;
    args << templateSlipPath << "fill_form" << slipfdf << "output" << slippdf << "need_appearances";
    qDebug() << pdftk << args;
    QProcess::execute(pdftk, args);
    pdfs->append(slippdf);
    deleteMe->append(slipfdf);
    if (!individualSlips)
        deleteMe->append(slippdf);
}

void printui::printLMMCombination()
{
    if (
            !ui->chk_Comb_LMM->isChecked() && !ui->chk_Comb_PublicMeeting->isChecked() && !ui->chk_Comb_PublicMeetingOut->isChecked()) {
        setHTML("<HTML><BODY><h1>" + tr("Select at least one option") + "</h1></BODY></HTML>");
        return;
    }

    pr->setResolution(300);
    QString str = getTemplate();
    str = initPrinter(str);
    str = ifthen.tokenize(str);

    // titles for all meetings
    str.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    str.replace("!CONGREGATION!", tr("Congregation"));
    str.replace("!CONGREGATION_NAME!", myCongregation.name);
    str.replace("!CHAIRMAN!", tr("Chairman"));
    str.replace("!COUNSELOR!", tr("Counselor"));
    str.replace("!READER!", tr("Reader"));
    str.replace("!CONDUCTOR!", tr("Conductor"));
    str.replace("!SPEAKER!", tr("Speaker", "Public talk speaker"));
    str.replace("!PM!", tr("Public Meeting"));
    str.replace("!PT!", tr("Public Talk"));
    str.replace("!WT!", tr("Watchtower Study"));
    str.replace("!MIDWEEK!", tr("Midweek Meeting"));
    str.replace("!LMM!", tr("Christian Life and Ministry Meeting"));
    str.replace("!LM!", tr("Christian Life and Ministry Meeting"));

    fillWeekList(0);
    qDebug() << weekList[0]->week.toString(Qt::ISODate);

    QPair<QString, QString> p1 = templateGetSection(str, "!REPEAT_START!", "!REPEAT_END!", "repeat_class");
    QString all_without_repeat = p1.first;
    p1 = templateGetSection(p1.second, "!MIDWEEK_START!", "!MIDWEEK_END!", "mwk_class");
    QString mwk_section = fillTemplateBlockLMMOverall(p1.second);
    p1 = templateGetSection(p1.first, "!WEEKEND_START!", "!WEEKEND_END!", "pm_class");
    QString pm_section = p1.second;
    p1 = templateGetSection(p1.first, "!PTOUT_START!", "!PTOUT_END!", "ptout_class");
    QString ptout_section = p1.second;
    QString repeat_section = p1.first;

    all_without_repeat.replace("!TITLE!", tr("Combined Schedule"));
    repeat_section.replace("!TITLE!", tr("Combined Schedule"));
    pm_section.replace("!TITLE!", sql->getSetting("print_we_title", tr("Weekend Meeting")));
    ptout_section.replace("!TITLE!", tr("Outgoing Speakers"));

    QString weekContext = "";
    ptCounter = 0;

    for (weekInfo *w : weekList) {
        currentWeek = w;
        //QDate tempdate(w->week);
        ptCounter = 1;

        QString oneweek(repeat_section);
        oneweek = replaceDateTag(oneweek, "WEEKSTARTING", w->week, tr("Week Starting %1", "%1 is Monday of the week"));
        oneweek = fillCommonItems(oneweek, c.noMeeting(currentWeek->week), c.isException(currentWeek->week));

        // Midweek
        if (ui->chk_Comb_LMM->isChecked()) {
            QString sectionCopy = QString(mwk_section);
            oneweek.replace("<div class=\"mwk_class\"></div>",
                            "<div>" + fillTemplateBlockLMM(sectionCopy, false) + "</div>");
        }

        // public meeting
        if (ui->chk_Comb_PublicMeeting->isChecked()) {
            QString sectionCopy = QString(pm_section);
            oneweek.replace("<div class=\"pm_class\"></div>",
                            "<div>" + fillTemplateBlockPM(sectionCopy) + "</div>");
        }

        oneweek.remove("!NO_MEETING!");

        if (ui->chk_Comb_PublicMeetingOut->isChecked()) {
            oneweek.replace("<div class=\"ptout_class\"></div>",
                            "<div>" + fillTemplateBlockPTOUT(QString(ptout_section)) + "</div>");
        }
        weekContext.append(oneweek);
    }

    all_without_repeat.replace("<div class=\"repeat_class\"></div>", "<div class=\"repeat_class\">" + weekContext + "</div>");
    // Add QR Code
    all_without_repeat = addQrCode(all_without_repeat);

    setHTML(ifthen.compute(all_without_repeat));
    //setHTML(ifthen.compute(webFrame->toHtml()));
}

void printui::printPublicMeetingSchedule()
{
    pr->setResolution(300);
    QString str = getTemplate();
    str = initPrinter(str);
    str = ifthen.tokenize(str);

    fillWeekList(publicmeetingday - 1);

    str.replace("!TITLE!", sql->getSetting("print_we_title", tr("Weekend Meeting")));
    str.replace("!PM!", tr("Public Meeting"));

    // titles for all meetings
    str.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    str.replace("!CONGREGATION!", tr("Congregation"));
    str.replace("!CONGREGATION_NAME!", myCongregation.name);
    str.replace("!CONDUCTOR!", tr("Conductor"));
    str.replace("!CHAIRMAN!", tr("Chairman"));
    str.replace("!SPEAKER!", tr("Speaker", "Public talk speaker"));
    str.replace("!READER!", tr("Reader"));
    str.replace("!PT!", tr("Public Talk"));
    str.replace("!WT!", tr("Watchtower Study"));
    str.replace("!CBS!", tr("Congregation Bible Study"));
    str.replace("!LM!", tr("Christian Life and Ministry Meeting"));
    str.replace("!DATE_TITLE!", tr("Date"));
    str.replace("!PHONE!", tr("Phone", "Phone number title"));
    str.replace("!HOST!", tr("Host", "Host for incoming public speaker"));

    QPair<QString, QString> sec = templateGetSection(str, "!REPEAT_START!", "!REPEAT_END!", "publicmeeting");
    QString basepage = sec.first;
    QString section = sec.second;

    // titles
    basepage.replace("!PT_SPEAKER!", tr("Speaker"));
    basepage.replace("!PT_CHAIRMAN!", tr("Chairman"));

    basepage.replace("!READER!", tr("Reader"));
    basepage.replace("!WT_READER!", tr("WT Reader"));

    basepage.replace("!DATE_TITLE!", tr("Date"));
    basepage.replace("!PT_THEME_TITLE!", tr("Theme"));

    QString weekContext = "";

    for (weekInfo *w : weekList) {
        currentWeek = w;
        QString oneweek(section);
        oneweek = fillTemplateBlockPM(oneweek);
        weekContext.append(oneweek);
    }

    basepage.replace("<div class=\"publicmeeting\"></div>", "<div>" + weekContext + "</div>");
    // Add QR Code
    basepage = addQrCode(basepage);

    setHTML(ifthen.compute(basepage));
}

void printui::printOutgoingSpeakersList(bool slips)
{
    pr->setResolution(300);

    QString str = "";
    if (slips) {
        str = getTemplate();
    } else {
        str = getTemplate();
    }
    str = initPrinter(str);
    str = ifthen.tokenize(str);

    fillWeekList(publicmeetingday - 1);
    QDate firstdate(weekList[0]->week);
    qDebug() << firstdate.toString(Qt::ISODate);

    QString basepage = "";
    QString pagesec = "";
    QString ptsec = "";
    if (slips) {
        QPair<QString, QString> sec = templateGetSection(str, "!PTOUT_START!", "!PTOUT_END!", "ptout_class");
        basepage = fillTemplateBlockPTOUTTitle(sec.first);
        sec = templateGetSection(sec.second, "!REPEAT_START!", "!REPEAT_END!", "repeat_class");
        pagesec = sec.first;
        ptsec = sec.second;
    } else {
        QPair<QString, QString> sec = templateGetSection(str, "!PTOUT_START!", "!PTOUT_END!", "ptout");
        basepage = fillTemplateBlockPTOUTTitle(sec.first);
        ptsec = sec.second;
    }

    // titles for all meetings
    basepage.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    basepage.replace("!CONGREGATION!", tr("Congregation"));
    basepage.replace("!CONGREGATION_NAME!", myCongregation.name);
    basepage.replace("!PT_TITLE!", tr("Public Talk"));
    basepage.replace("!WT_TITLE!", tr("Watchtower Study"));
    basepage.replace("!CBS_TITLE!", tr("Congregation Bible Study"));
    basepage.replace("!TMS_TITLE!", tr("Theocratic Ministry School"));
    basepage.replace("!SM_TITLE!", tr("Service Meeting"));

    QString weekContext = "";

    if (slips) {
        // print slips for each speaker
        cpersons cp_class;
        // get speakers in own congregation
        QList<person *> speakers = cp_class.getAllPersons(2);
        bool firstpage = true;
        for (person *speaker : speakers) { // loop each speaker
            QString filledsection = "";
            QDate lastdate = weekList[weekList.length() - 1]->week;
            QString filledpage = fillTemplateBlockPTOUTTitle(QString(pagesec), speaker->fullname());
            filledsection = fillTemplateBlockPTOUTSlips(speaker, firstdate, lastdate, QString(ptsec));

            if (filledsection != "") {
                if (!firstpage)
                    filledpage = addCssPageBreak() + filledpage;
                filledpage.replace("<div class=\"repeat_class\"></div>",
                                   "<div>" + filledsection + "</div>");
                weekContext.append(filledpage);
                firstpage = false;
            }
        }
        qDeleteAll(speakers);
        basepage.replace("<div class=\"ptout_class\"></div>", "<div>" + weekContext + "</div>");
    } else {
        // print overview list for bulleting board
        int lastMonth(-1);
        ptCounter = 0;
        for (weekInfo *w : weekList) {
            currentWeek = w;
            //QDate tempdate(w->week);
            QString oneweek(ptsec);

            if (w->mtgDate.month() != lastMonth) {
                lastMonth = w->mtgDate.month();
                ptCounter = 1;
            }

            oneweek = fillTemplateBlockPTOUT(oneweek); // fill a one week
            if (oneweek != "") {
                weekContext.append(oneweek);
            }
        }
        basepage.replace("<div class=\"ptout\"></div>", "<div>" + weekContext + "</div>");
    }
    setHTML(ifthen.compute(basepage));
}

void printui::printTerritoryAssignmentRecord()
{
    pr->setResolution(300);
    QString str = getTemplate();
    str = initPrinter(str);
    str = ifthen.tokenize(str);

    bool waitForLoadedEvent = currentTemplateData->metaValue("tb-send-loaded-event") == "yes";

    // titles
    str = fillTemplateBlockTerritoryOverall(str);
    str.replace("!TITLE!", tr("Territory Assignment Record"));
    str.replace("!TITLE_TERRITORYCOVERAGE!", tr("Territory Coverage"));
    str.replace("!TOTALNUMBER_TERRITORY!", tr("Total number of territories"));
    str.replace("!WORKED_LT6M!", tr("< 6 months", "territory worked"));
    str.replace("!WORKED_6TO12M!", tr("6 to 12 months", "territory worked"));
    str.replace("!WORKED_GT12M!", tr("> 12 months ago", "territory worked"));
    str.replace("!AVERAGE_PER_YEAR!", tr("Average per year", "Number of times territory has been worked per year on average"));

    sql_items territories = sql->selectSql("SELECT t.*,tc.city FROM territories t"
                                           " LEFT JOIN territory_city tc ON t.city_id = tc.id"
                                           " ORDER BY territory_number");

    str.replace("!TR_TOTALNUMBER_TERRITORY!", QString::number(territories.size()));

    QString basepage = "";
    QString pagesec = "";
    QString tr_sec = "";
    QString adr_sec = "";
    QPair<QString, QString> sec = templateGetSection(str, "!TERRITORY_START!", "!TERRITORY_END!", "territory_class");
    basepage = sec.first;
    sec = templateGetSection(sec.second, "!REPEAT_START!", "!REPEAT_END!", "repeat_class");
    tr_sec = sec.second;
    sec = templateGetSection(sec.first, "!ADDRESS_START!", "!ADDRESS_END!", "address_class");
    pagesec = sec.first;
    adr_sec = sec.second;

    cpersons cp;
    QString pages = "";

    for (unsigned int i = 0; i < territories.size(); i++) {
        QString filledpage(pagesec);

        filledpage.replace("!TR_TERRITORY_NUMBER!", territories[i].value("territory_number").toString());
        filledpage.replace("!TR_LOCALITY!", territories[i].value("locality").toString());
        filledpage.replace("!TR_TERRITORY_TYPE!", territories[i].value("type_name").toString());
        filledpage.replace("!TR_CITY!", territories[i].value("city").toString());
        filledpage.replace("!TR_DATE_LAST_WORKED!", territories[i].value("lastworked_date").toString());
        filledpage.replace("!TR_TERRITORY_REMARK!", territories[i].value("remark").toString());
        filledpage.replace("!TR_TERRITORY_GEOMETRY!", territories[i].value("wkt_geometry").toString());
        person *personAssignedTo = cp.getPerson(territories[i].value("person_id").toInt());
        QString assignedTo("");
        if (personAssignedTo)
            assignedTo = personAssignedTo->fullname();
        filledpage.replace("!TR_ASSIGNED_TO!", assignedTo);

        sql_items assignments = sql->selectSql("SELECT * FROM territoryassignments WHERE territory_id = " + territories[i].value("id").toString() + " ORDER BY checkedout_date");
        const unsigned int evaluatedYears = 5;
        double calcTable[evaluatedYears][6];
        double xSum = 0;
        double ySum = 0;
        for (unsigned int j = 0; j < evaluatedYears; j++) {
            calcTable[j][0] = j;
            calcTable[j][1] = 0;
            calcTable[j][2] = 0;
            calcTable[j][3] = 0;
            calcTable[j][4] = 0;
            calcTable[j][5] = 0;
        }

        QString filledsection = "";
        for (unsigned int j = 0; j < assignments.size(); j++) {
            QString currentSection(tr_sec);
            currentSection = fillCommonItems(currentSection, false, ccongregation::none);

            person *publisher = cp.getPerson(assignments[j].value("person_id").toInt());
            currentSection.replace("!TR_PUBLISHER!", publisher->fullname());

            QDate checkedOut_date = assignments[j].value("checkedout_date").toDate();
            currentSection = replaceDateTag(currentSection, "TR_DATE_CHECKED_OUT", checkedOut_date);

            QDate checkedBackIn_date = assignments[j].value("checkedbackin_date").toDate();
            currentSection = replaceDateTag(currentSection, "TR_DATE_CHECKED_BACK_IN", checkedBackIn_date);

            unsigned int x = evaluatedYears + 1;
            if (checkedBackIn_date.isValid())
                x = evaluatedYears - uint(checkedBackIn_date.daysTo(QDate::currentDate()) / 365) - 1;
            if (x < evaluatedYears)
                calcTable[x][1] += 1;

            filledsection.append(currentSection);
        }
        for (unsigned int j = 0; j < evaluatedYears; j++) {
            xSum += calcTable[j][0];
            ySum += calcTable[j][1];
        }

        double deltaX = xSum / evaluatedYears;
        double deltaY = ySum / evaluatedYears;
        double bc = 0, bd = 0;
        for (unsigned int j = 0; j < evaluatedYears; j++) {
            calcTable[j][2] = calcTable[j][0] - deltaX;
            calcTable[j][3] = calcTable[j][1] - deltaY;
            calcTable[j][4] = calcTable[j][2] * calcTable[j][3];
            calcTable[j][5] = calcTable[j][2] * calcTable[j][2];
            bc += calcTable[j][4];
            bd += calcTable[j][5];
        }
        double b = bd == 0.0 ? 0.0 : bc / bd;
        double a = deltaY - b * deltaX;
        double averagePerYear = a + b * (evaluatedYears - 1);
        filledpage.replace("!TR_AVERAGE_PER_YEAR!", QString::number(averagePerYear));

        if (filledsection != "")
            filledpage.replace("<div class=\"repeat_class\"></div>", "<div class=\"repeat_class\">" + filledsection + "</div>");

        sql_items addresses = sql->selectSql("SELECT * FROM territoryaddresses WHERE territory_id = " + territories[i].value("id").toString() + " ORDER BY street, housenumber, name");
        filledsection = "";
        for (unsigned int j = 0; j < addresses.size(); j++) {
            QString currentSection(adr_sec);
            currentSection = fillCommonItems(currentSection, false, ccongregation::none);

            currentSection.replace("!TA_COUNTRY!", addresses[j].value("country").toString());
            currentSection.replace("!TA_STATE!", addresses[j].value("state").toString());
            currentSection.replace("!TA_COUNTY!", addresses[j].value("county").toString());
            currentSection.replace("!TA_CITY!", addresses[j].value("city").toString());
            currentSection.replace("!TA_DISTRICT!", addresses[j].value("district").toString());
            currentSection.replace("!TA_STREET!", addresses[j].value("street").toString());
            currentSection.replace("!TA_HOUSENUMBER!", addresses[j].value("housenumber").toString());
            currentSection.replace("!TA_POSTALCODE!", addresses[j].value("postalcode").toString());
            currentSection.replace("!TA_POINT_GEOMETRY!", addresses[j].value("wkt_geometry").toString());
            currentSection.replace("!TA_NAME!", addresses[j].value("name").toString());
            currentSection.replace("!TA_ADDRESSTYPE_NUMBER!", addresses[j].value("addresstype_number").toString());
            currentSection.replace("!TA_ADDRESSTYPE_NAME!", addresses[j].value("addresstype_name").toString());
            currentSection.replace("!TA_ADDRESSTYPE_COLOR!", addresses[j].value("color").toString());

            filledsection.append(currentSection);
        }

        if (filledsection != "")
            filledpage.replace("<div class=\"address_class\"></div>", "<div class=\"address_class\">" + filledsection + "</div>");

        pages.append(filledpage);
    }

    basepage.replace("<div class=\"territory_class\"></div>", "<div class=\"territory_class\">" + pages + "</div>");
    setHTML(ifthen.compute(basepage), waitForLoadedEvent);
}

void printui::printTerritoryCard()
{
    pr->setResolution(300);
    QString str = getTemplate();
    str = initPrinter(str);
    str = ifthen.tokenize(str);

    bool waitForLoadedEvent = currentTemplateData->metaValue("tb-send-loaded-event") == "yes";

    // titles
    str = fillTemplateBlockTerritoryOverall(str);
    str.replace("!TITLE!", tr("Territory Map Card", "Title tag for a S-12 or similar card"));
    str.replace("!TITLE_MAP!", tr("Territory Map", "Title tag for a sheet with a territory map"));
    str.replace("!TITLE_ADDRESSES!", tr("Address List", "Title tag for a sheet with a territory's address list"));
    str.replace("!TITLE_MAP_ADDRESSES!", tr("Territory Map with Address List", "Title tag for a sheet with a territory's map and address list"));
    str.replace("!TITLE_STREETS!", tr("Street List", "Title tag for a sheet with a territory's street list"));
    str.replace("!TITLE_MAP_STREETS!", tr("Territory Map with Street List", "Title tag for a sheet with a territory's map and street list"));
    str.replace("!TITLE_DNC!", tr("Do-Not-Call List", "Title tag for a sheet with a territory's Do-Not-Call list"));

    QString basepage = "";
    QString pagesec = "";
    QString tr_sec = "";
    QString ad_sec = "";
    QString st_sec = "";
    QPair<QString, QString> sec = templateGetSection(str, "!TERRITORY_START!", "!TERRITORY_END!", "territory_class");
    basepage = sec.first;
    tr_sec = sec.second;

    sql_items territories = sql->selectSql("SELECT t.*,tc.city FROM territories t"
                                           " LEFT JOIN territory_city tc ON t.city_id = tc.id"
                                           " WHERE territory_number in ("
                                           + territoryNumberList + ")");

    cpersons cp;
    QString pages = "";

    for (unsigned int i = 0; i < territories.size(); i++) {
        QString filledpage(tr_sec);

        filledpage.replace("!TR_TERRITORY_NUMBER!", territories[i].value("territory_number").toString());
        filledpage.replace("!TR_LOCALITY!", territories[i].value("locality").toString());
        filledpage.replace("!TR_TERRITORY_TYPE!", territories[i].value("type_name").toString());
        filledpage.replace("!TR_CITY!", territories[i].value("city").toString());
        filledpage.replace("!TR_DATE_LAST_WORKED!", territories[i].value("lastworked_date").toString());
        filledpage.replace("!TR_TERRITORY_REMARK!", territories[i].value("remark").toString());
        filledpage.replace("!TR_TERRITORY_GEOMETRY!", territories[i].value("wkt_geometry").toString());
        person *personAssignedTo = cp.getPerson(territories[i].value("person_id").toInt());
        QString assignedTo("");
        if (personAssignedTo)
            assignedTo = personAssignedTo->fullname();
        filledpage.replace("!TR_ASSIGNED_TO!", assignedTo);

        sec = templateGetSection(filledpage, "!REPEAT_START!", "!REPEAT_END!", "repeat_class");
        filledpage = sec.first;
        ad_sec = sec.second;

        sql_items addresses = sql->selectSql("SELECT * FROM territoryaddresses WHERE territory_id = " + territories[i].value("id").toString() + " ORDER BY city, street, housenumber, name");
        QString filledsection = "";
        for (unsigned int j = 0; j < addresses.size(); j++) {
            QString currentSection(ad_sec);
            currentSection = fillCommonItems(currentSection, false, ccongregation::none);

            currentSection.replace("!TA_COUNTRY!", addresses[j].value("country").toString());
            currentSection.replace("!TA_STATE!", addresses[j].value("state").toString());
            currentSection.replace("!TA_COUNTY!", addresses[j].value("county").toString());
            currentSection.replace("!TA_CITY!", addresses[j].value("city").toString());
            currentSection.replace("!TA_DISTRICT!", addresses[j].value("district").toString());
            currentSection.replace("!TA_STREET!", addresses[j].value("street").toString());
            currentSection.replace("!TA_HOUSENUMBER!", addresses[j].value("housenumber").toString());
            currentSection.replace("!TA_POSTALCODE!", addresses[j].value("postalcode").toString());
            currentSection.replace("!TA_POINT_GEOMETRY!", addresses[j].value("wkt_geometry").toString());
            currentSection.replace("!TA_NAME!", addresses[j].value("name").toString());
            currentSection.replace("!TA_ADDRESSTYPE_NUMBER!", addresses[j].value("addresstype_number").toString());
            currentSection.replace("!TA_ADDRESSTYPE_NAME!", addresses[j].value("addresstype_name").toString());
            currentSection.replace("!TA_ADDRESSTYPE_COLOR!", addresses[j].value("color").toString());

            filledsection.append(currentSection);
        }
        if (filledsection != "")
            filledpage.replace("<div class=\"repeat_class\"></div>", "<div class=\"repeat_class\">" + filledsection + "</div>");

        sec = templateGetSection(filledpage, "!STREET_START!", "!STREET_END!", "street_class");
        filledpage = sec.first;
        st_sec = sec.second;

        sql_items streets = sql->selectSql("SELECT * FROM territorystreets WHERE territory_id = " + territories[i].value("id").toString() + " ORDER BY street_name, from_number");
        filledsection = "";
        for (unsigned int j = 0; j < streets.size(); j++) {
            QString currentSection(st_sec);
            currentSection = fillCommonItems(currentSection, false, ccongregation::none);

            currentSection.replace("!TS_STREET!", streets[j].value("street_name").toString());
            currentSection.replace("!TS_FROM_NUMBER!", streets[j].value("from_number").toString());
            currentSection.replace("!TS_TO_NUMBER!", streets[j].value("to_number").toString());
            currentSection.replace("!TS_QUANTITY!", streets[j].value("quantity").toString());
            currentSection.replace("!TS_LINE_GEOMETRY!", streets[j].value("wkt_geometry").toString());
            currentSection.replace("!TS_STREETTYPE_NAME!", streets[j].value("streettype_name").toString());
            currentSection.replace("!TS_STREETTYPE_COLOR!", streets[j].value("color").toString());

            filledsection.append(currentSection);
        }
        if (filledsection != "")
            filledpage.replace("<div class=\"street_class\"></div>", "<div class=\"street_class\">" + filledsection + "</div>");

        pages.append(filledpage);
    }

    basepage.replace("<div class=\"territory_class\"></div>", "<div class=\"territory_class\">" + pages + "</div>");
    setHTML(ifthen.compute(basepage), waitForLoadedEvent);
}

void printui::printTerritoryMapCardPreview()
{
    if (ui->cboTemplate->currentText().endsWith("pdf", Qt::CaseInsensitive))
        setHTML("<HTML><BODY><h1><center>" + tr("This Territory Map Card could not be converted to a JPG file") + "<center></h1></BODY></HTML>");
    else
        printTerritoryMapCard(
                QGuiApplication::keyboardModifiers() & Qt::ControlModifier,
                QGuiApplication::keyboardModifiers() & Qt::ShiftModifier);
}

QString printui::encodePointValue(double value)
{
    bool isNegative = value < 0;

    // Take the decimal value and multiply it by 1e5, rounding the result
    int intValue = qRound(value * 1e5);

    // Left-shift the binary value one bit
    intValue <<= 1;

    // If the original decimal value is negative, invert this encoding
    intValue = isNegative ? ~intValue : intValue;

    // Break the binary value out into 5-bit chunks (starting from the right hand side)
    // Place the 5-bit chunks into reverse order
    int chunks[6];
    chunks[0] = intValue & 0x1F;
    chunks[1] = (intValue & 0x3E0) >> 5;
    chunks[2] = (intValue & 0x7C00) >> 10;
    chunks[3] = (intValue & 0xF8000) >> 15;
    chunks[4] = (intValue & 0x1F00000) >> 20;
    chunks[5] = (intValue & 0x3E000000) >> 25;

    QString result("");
    unsigned int lastChunk = 0;
    for (unsigned int i = 5; i > 0; i--) {
        if (chunks[i] != 0) {
            lastChunk = i;
            break;
        }
    }

    for (unsigned int i = 0; i <= lastChunk; i++) {
        // OR each value with 0x20 if another bit chunk follows
        if (i < lastChunk)
            chunks[i] = chunks[i] | 0x20;

        // Add 63 to each value
        chunks[i] += 63;

        // Convert each value to its ASCII equivalent
        result += QChar(chunks[i]).toLatin1();
    }
    return result;
}

void printui::printTerritoryMapCard(bool forceRescan, bool usePrinter)
{
    //qDebug() << "printTerritoryMapCard";
    QPagedPaintDevice *pdevice = nullptr;
    QByteArray byteArray;
    QPainter *painter(nullptr);
    QBuffer buffer;
    QString pages = "";

    //qDebug() << "font";
    QFont f;
    QFont ckFont;
    // font size adjusting
    f.setPixelSize(int(40 * textSizeFactorySlips));
    ckFont.setPixelSize(int(40 * textSizeFactorySlips));
    f.setFamily("Arial");
    ckFont.setFamily("Arial");
    ckFont.setBold(true);

    QString templateSlipPath = getTemplate(true);
    if (!QFile(templateSlipPath).exists()) {
        if (templateSlipPath == "")
            templateSlipPath = tr("Template");

        buffer.setBuffer(&byteArray);
        buffer.open(QIODevice::WriteOnly);
        pdevice = new QPdfWriter(&buffer);
        painter = new QPainter(pdevice);
        painter->setWindow(0, 0, 2000, 2000);
        painter->setFont(f);
        painter->drawText(50, 50, templateSlipPath + " not found");
        painter->end();
        ui->previewPdf->setPdf(byteArray);
        delete painter;

        return;
    }
    bool onePerPage = !templateSlipPath.contains(QRegExp("4[-]?up.", Qt::CaseInsensitive));

    SlipScanner ss(this, templateSlipPath, forceRescan);
    if (ss.needFinalized)
        ss.FinalizeBoxes(0);

    customsize = QSize(ss.pageWidth, ss.pageHeight);
    if (usePrinter) {
        if (onePerPage) {
            pr->setPaperSize(QPrinter::A6);
        } else {
            if (!currentTemplateData->customPaperSize.isValid()) {
                pr->setPaperSize(currentTemplateData->standardPaperSize);
            } else {
                pr->setPaperSize(currentTemplateData->customPaperSize, currentTemplateData->customPaperUnit);
            }
        }
        pr->setOrientation(QPrinter::Landscape);
        pr->setFullPage(true);
        pr->setPageMargins(0, 0, 0, 0, QPrinter::Millimeter);
        pdevice = pr;
    } else {
        buffer.setBuffer(&byteArray);
        buffer.open(QIODevice::WriteOnly);
        pdevice = new QPdfWriter(&buffer);
        pdevice->setPageOrientation(QPageLayout::Landscape);
    }
    painter = new QPainter(pdevice);
    painter->setWindow(0, 0, customsize.width(), customsize.height());

#ifdef Q_OS_WIN
    // windows fix
    double xTransform = ((pr->pageRect().x() * QVariant(customsize.width()).toDouble()) / pr->paperRect().width());
    double yTransform = ((pr->pageRect().y() * QVariant(customsize.height()).toDouble()) / pr->paperRect().height());
    //qDebug() << "page rect:" << pr->pageRect();
    //qDebug() << "paper rect:" << pr->paperRect();
    //qDebug() << "transform:" << xTransform << yTransform;
    painter->translate(xTransform * -1, yTransform * -1);
#endif

    painter->setFont(f);
    QFontMetrics fm = painter->fontMetrics();
    int textboxOffset(-fm.height());
    int textboxHeight(int(fm.height() * 2.1));
    int slipnum(0);
    int maxslipsperpage(onePerPage ? 1 : 4);

    int needNewPage(1); // 0=none, 1=need image, 2=need page

    QList<QRectF *> localityRegions;
    QList<QRectF *> territoryNumberRegions;
    QList<QRectF *> mapRegions;

    int txtnum(0);
    SlipScanner::region *box1;
    SlipScanner::region *box2;

    int pagecx = (ss.pageWidth / 2);
    int slipWidth = ss.pageWidth;
    if (!onePerPage && ss.textBoxes.count() == 8)
        slipWidth = ss.textBoxes[2]->x1 - ss.textBoxes[0]->x1;

    for (int snum = 0; snum < maxslipsperpage; snum++) {
        // depending on the language, the position of the locality and terr.no. is different
        // the longer textbox should be locality
        box1 = ss.textBoxes[txtnum++];
        box2 = ss.textBoxes[txtnum++];
        if (box1->width > box2->width) {
            localityRegions.append(new QRectF(box1->x1, box1->y2 + textboxOffset, box1->width, textboxHeight));
            territoryNumberRegions.append(new QRectF(box2->x1, box2->y2 + textboxOffset, box2->width, textboxHeight));
        } else {
            localityRegions.append(new QRectF(box2->x1, box2->y2 + textboxOffset, box2->width, textboxHeight));
            territoryNumberRegions.append(new QRectF(box1->x1, box1->y2 + textboxOffset, box1->width, textboxHeight));
        }

        int cx = pagecx;
        if (!onePerPage)
            cx = pagecx - slipWidth / 2 + (snum % 2) * slipWidth;

        mapRegions.append(new QRectF(cx - (725 + mapSizeFactory),
                                     ss.blankAreas[snum]->y1 + textboxHeight / 4,
                                     (1450 + mapSizeFactory * 2),
                                     ss.blankAreas[snum]->height));
    }

    sql_items territories = sql->selectSql("SELECT t.*,tc.city FROM territories t"
                                           " LEFT JOIN territory_city tc ON t.city_id = tc.id"
                                           " WHERE territory_number in ("
                                           + territoryNumberList + ")");

    for (unsigned int i = 0; i < territories.size(); i++) {
        if (needNewPage) {
            if (needNewPage > 1)
                pdevice->newPage();
            painter->drawImage(0, 0, ss.img);
            needNewPage = 0;
            slipnum = 0;
        }

        painter->drawText(*localityRegions[slipnum], Qt::AlignHCenter, territories[i].value("locality").toString());
        painter->drawText(*territoryNumberRegions[slipnum], Qt::AlignHCenter, territories[i].value("territory_number").toString());

        QSettings settings;
        int geoServiceProvider = settings.value("geo_service_provider/default", 0).toInt();
        QString googleAPIKey = settings.value("geo_service_provider/google_api_key", "").toString();
        googleAPIKey = googleAPIKey.isEmpty() ? "" : "&key=" + googleAPIKey;
        QString hereAppId = settings.value("geo_service_provider/here_app_id", "").toString();
        QString hereAppCode = settings.value("geo_service_provider/here_app_code", "").toString();
        hereAppId = hereAppId.isEmpty() ? "" : "&app_id=" + hereAppId;
        hereAppCode = hereAppCode.isEmpty() ? "" : "&app_code=" + hereAppCode;

        switch (geoServiceProvider) {
        case 1: {
            // Google
            break;
        }
        case 2: {
            // HERE geocoding service
            break;
        }
        default:
            // OSM
            geoServiceProvider = 2;
            if (!googleAPIKey.isEmpty())
                geoServiceProvider = 1;
            if (!hereAppId.isEmpty() && !hereAppCode.isEmpty())
                geoServiceProvider = 2;
            break;
        }

        // generate path parameter of territory boundary
        QString wktGeometry = territories[i].value("wkt_geometry").toString();

        QString geoPath;
        QRegularExpression regExp("((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))\\s*((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))");
        QRegularExpression polygonRegex("\\(.*?\\)"); //g, polygonMatch;
        QRegularExpression multiPolygonRegex("\\(\\(.*?\\)\\)"); //g, multiPolygonMatch;
        int outerPolygonIndex = 0;
        QRegularExpressionMatchIterator iMultiPolygon = multiPolygonRegex.globalMatch(wktGeometry);
        while (iMultiPolygon.hasNext()) {
            QRegularExpressionMatch multiPolygonMatch = iMultiPolygon.next();
            QString multiPolygon = multiPolygonMatch.captured(0);
            QRegularExpressionMatchIterator iPolygon = polygonRegex.globalMatch(multiPolygon);

            int innerPolygonIndex = 0;
            while (iPolygon.hasNext()) {
                QRegularExpressionMatch polygonMatch = iPolygon.next();
                QString polygon = polygonMatch.captured(0);

                int i = 0;
                QRegularExpressionMatchIterator iMatch = regExp.globalMatch(polygon);
                QString separator("");
                QString lastLatLon("");
                double lastLatitude(0);
                double lastLongitude(0);
                while (iMatch.hasNext()) {
                    i += 1;

                    QRegularExpressionMatch match = iMatch.next();
                    // round to 4 decimal places
                    double longitude = qRound(match.captured(1).toDouble() * 1e4) / 1e4;
                    double latitude = qRound(match.captured(2).toDouble() * 1e4) / 1e4;

                    QString encLatLon("");
                    double deltaLatitude = i == 1 ? latitude : latitude - lastLatitude;
                    double deltaLongitude = i == 1 ? longitude : longitude - lastLongitude;
                    if (i != 1 && deltaLatitude == 0.0 && deltaLongitude == 0.0)
                        continue;
                    switch (geoServiceProvider) {
                    case 1: {
                        // Google
                        if (i == 1)
                            geoPath += "&path=color:0xff00ff|enc:";
                        encLatLon = encodePointValue(deltaLatitude);
                        encLatLon += encodePointValue(deltaLongitude);
                        geoPath += encLatLon;
                        break;
                    }
                    case 2: {
                        // HERE geocoding service
                        if (i == 1)
                            geoPath += QString("&lc%1=ff00ff&a%1=").arg(QVariant(outerPolygonIndex).toString());
                        geoPath += separator;
                        encLatLon = QVariant(latitude).toString();
                        encLatLon += ",";
                        encLatLon += QVariant(longitude).toString();
                        geoPath += encLatLon;
                        separator = ",";
                        break;
                    }
                    }

                    lastLatitude = latitude;
                    lastLongitude = longitude;
                }
                innerPolygonIndex += 1;
            }
            outerPolygonIndex += 1;
        }

        // request google staticmap image
        QNetworkAccessManager manager;
        QEventLoop q;
        QTimer tT;

        tT.setSingleShot(true);
        connect(&tT, SIGNAL(timeout()), &q, SLOT(quit()));
        connect(&manager, SIGNAL(finished(QNetworkReply *)),
                &q, SLOT(quit()));

        QNetworkRequest request;
        int mapWidth = int(mapRegions[slipnum]->width());
        int mapHeight = int(mapRegions[slipnum]->height());
        // Free maximum allowable value: 640x640
        if (mapWidth > 640) {
            mapHeight = int(640.0 / mapWidth * mapHeight);
            mapWidth = 640;
        }

        switch (geoServiceProvider) {
        case 1: {
            // Google
            request.setUrl(QUrl("https://maps.googleapis.com/maps/api/staticmap?scale=2&size=" + QString::number(mapWidth) + "x" + QString::number(mapHeight) + geoPath + googleAPIKey));
            break;
        }
        case 2: {
            // HERE geocoding service
            request.setUrl(QUrl("https://image.maps.api.here.com/mia/1.6/region?ppi=320&h=" + QString::number(mapHeight) + "&w=" + QString::number(mapWidth) + hereAppId + hereAppCode + geoPath));
            break;
        }
        }
        QNetworkReply *reply = manager.get(request);

        tT.start(15000); // 15s timeout
        q.exec();

        QByteArray responseData;
        if (tT.isActive()) {
            // download complete
            tT.stop();
            responseData = reply->readAll();

            if (reply->error() != QNetworkReply::NoError) {
                reply->close();
                QString errorMessage = QString::fromUtf8(responseData);
                if (errorMessage.isEmpty())
                    errorMessage = reply->errorString();
                errorMessage = QTextDocumentFragment::fromHtml(errorMessage).toPlainText();
                painter->drawText(*mapRegions[slipnum], errorMessage);
            } else {
                QImage image;
                image.loadFromData(responseData);
                painter->drawImage(*mapRegions[slipnum], image);
            }
        } else {
            // timeout
            painter->drawText(*mapRegions[slipnum], reply->errorString());
        }

        slipnum++;
        if (slipnum >= maxslipsperpage)
            needNewPage = 2;
    }

    //qDebug() << "finish up";
    painter->end();
    if (!usePrinter) {
        ui->previewPdf->setPdf(byteArray);
        delete pdevice;
    }
    if (buffer.isOpen())
        buffer.close();
    delete painter;
}

QString printui::fillTemplateBlockLMMOverall(QString context)
{
    context.replace("!TITLE!", sql->getSetting("print_mw_title", tr("Midweek Meeting")));
    context.replace("!WORKSHEET!", tr("Worksheet"));
    context.replace("!OPENING!", tr("Opening Comments", "See Workbook"));
    context.replace("!CONCLUSION!", tr("Review Followed by Preview of Next Week", "See Workbook"));
    context.replace("!SONG!", tr("Song"));
    context.replace("!PRAYER!", tr("Prayer"));
    context.replace("!CHAIRMAN!", tr("Chairman"));
    context.replace("!COUNSELOR!", tr("Counselor"));
    context.replace("!CONDUCTOR!", tr("Conductor"));
    context.replace("!READER!", tr("Reader"));
    context.replace("!LM!", tr("Christian Life and Ministry Meeting"));
    context.replace("!GW!", tr("Treasures from God's Word"));
    context.replace("!FM!", tr("Apply Yourself to the Field Ministry"));
    context.replace("!CL!", tr("Living as Christians"));
    context.replace("!CBS!", tr("Congregation Bible Study"));
    context.replace("!CO!", tr("Circuit Overseer"));
    context.replace("!NO_MEETING!", tr("No regular meeting"));
    context.replace("!ASSISTANT!", tr("Assistant", "Assistant to student"));
    context.replace("!STUDY!", tr("Study", "Counsel point"));
    context.replace("!THEME!", tr("Theme", "Talk Theme description from workbook"));
    context.replace("!SOURCE!", tr("Source", "Source information from workbook"));
    context.replace("!EXERCISES!", tr("Exercises"));
    context.replace("!TIMING!", tr("Timing", "Assignment completed in time?"));
    context.replace("!NOTES!", tr("Notes"));
    context.replace(QRegExp("!NEXT_STUDY[A-Z|_]*!"), tr("Next study"));
    context.replace("!SETTING!", tr("Setting", "for sisters assignment"));
    context.replace("!SCHOOL1!", tr("Class") + " I");
    context.replace("!SCHOOL2!", tr("Class") + " II");
    context.replace("!SCHOOL3!", tr("Class") + " III");
    context.replace("!ACC!", tr("Auxiliary Classroom Counselor", "See S-140"));
    context.replace("!AC!", tr("Auxiliary Classroom", "See S-140"));
    context.replace("!MH!", tr("Main Hall", "See S-140"));
    context.replace("!MH_SHORT!", tr("MH", "Abbreviation for 'Main Hall'"));
    context.replace("!AUX_SHORT!", tr("A", "Abbreviation for 'Auxiliary Classroom'"));
    context.replace("!STUDENT!", tr("Student", "See S-140"));
    context.replace("!RPA!", tr("Review/Preview/Announcements", "See S-140"));
    context.replace("!OC_TITLE!", sql->getSetting("print_mw_oc_title", tr("Opening Comments", "Customizable title for OC notes")));
    context.replace("!RPA_TITLE!", sql->getSetting("print_mw_rpa_title", tr("Review/Preview/Announcements", "Customizable title for RPA notes")));
    context.replace("!TODAY!", tr("Today"));
    context.replace("!NEXT_WEEK!", tr("Next week"));

    context.replace("!SECTION_TITLES!", currentTemplateData->optionValue("section-titles", "yes") == "yes" ? "yes" : "");

    return context;
}

QString printui::fillTemplateBlockLMM(QString context, bool worksheet)
{

    QDate date(currentWeek->week);
    school s;
    bool showSongTitles = currentTemplateData->optionValue("song-titles") == "yes";
    bool showWorkbookNumber = currentTemplateData->optionValue("mwb-number") == "yes";
    bool useDuration = currentTemplateData->optionValue("duration") == "duration";

    ccongregation::exceptions ex = c.isException(date);
    int md = c.getMeetingDay(date, ccongregation::tms);

    QTime starttime = QTime::fromString(myCongregation.time_meeting1, "hh:mm");

    QDate d;
    if (ex == ccongregation::circuit_overseer_visit) {
        d = date.addDays(md - 1);
    } else if (ex == ccongregation::other) {
        if (md == 0) {
            // no meeting
            d = date.addDays(schoolday - 1);
        } else {
            d = date.addDays(md - 1);
        }
    } else {
        d = date.addDays(schoolday - 1);
    }
    context = replaceDateTag(context, "DATE", d);
    bool nomeeting(md == 0);
    context = fillCommonItems(context, nomeeting, ex);
    if (worksheet && nomeeting)
        return "";

    context = replaceTimeTag(context, "LM_STARTTIME", starttime);

    LMM_Meeting *mtg = new LMM_Meeting();
    if (!mtg->loadMeeting(date, true))
        return context;
    QList<LMM_Assignment *> prog = mtg->getAssignments();

    QString abcd = "ABCD";
#ifdef Q_OS_WIN
    QString checkmark = "√";
#else
    QString checkmark = "\u2713";
#endif
    if (useDuration)
        context.replace("!DURATION!", "Yes");
    else
        context.replace("!DURATION!", "");
    if (ex == ccongregation::convention || md == 0) {
        /* Since we now have printing tags, we will skip this

        // no meeting because of convention or other reason
        exceptionText = mtg->bibleReading() + "\n";
        exceptionText += c.getExceptionText(date);
        // TODO: mene loppuun
        if (worksheet){
            context.replace("!BIBLE_READING!",exceptionText);
        }

        */
        context.replace("!LM_SOURCE!", mtg->bibleReading());
    } else {
        context.replace("!LM_SOURCE!", mtg->bibleReading());
        context.replace("!LM_CHAIRMAN!", mtg->chairman() ? mtg->chairman()->fullname() : "");
        context.replace("!COUNSELOR_A1!", mtg->counselor2() ? mtg->counselor2()->fullname() : "");
        context.replace("!COUNSELOR_A2!", mtg->counselor3() ? mtg->counselor3()->fullname() : "");

        context.replace("!CLASS_A_SCHEDULED!", mtg->chairman() ? "Yes" : "");
        context.replace("!CLASS_B_SCHEDULED!", mtg->counselor2() ? "Yes" : "");
        context.replace("!CLASS_C_SCHEDULED!", mtg->counselor3() ? "Yes" : "");

        context.replace("!SONG1_NO!", mtg->songBeginning() > 0 ? QVariant(mtg->songBeginning()).toString() : "");
        context.replace("!SONG2_NO!", mtg->songMiddle() > 0 ? QVariant(mtg->songMiddle()).toString() : "");
        context.replace("!SONG3_NO!", mtg->songEnd() > 0 ? QVariant(mtg->songEnd()).toString() : "");
        if (showSongTitles) {
            context.replace("!SONG1_NAME!", mtg->songBeginningTitle());
            context.replace("!SONG2_NAME!", mtg->songMiddleTitle());
            context.replace("!SONG3_NAME!", mtg->songEndTitle());
        } else {
            context.replace("!SONG1_NAME!", "");
            context.replace("!SONG2_NAME!", "");
            context.replace("!SONG3_NAME!", "");
        }
        context = replaceTimeTag(context, "SONG1_STARTTIME", starttime);
        context = replaceTimeTag(context, "SONG2_STARTTIME", starttime.addSecs(60 * 47));
        context = replaceTimeTag(context, "SONG3_STARTTIME", starttime.addSecs(60 * 100));
        context.replace("!PRAYER1_NAME!", mtg->prayerBeginning() ? mtg->prayerBeginning()->fullname() : "");
        context.replace("!PRAYER2_NAME!", mtg->prayerEnd() ? mtg->prayerEnd()->fullname() : "");

        context = replaceTimeTag(context, "OC_STARTTIME", starttime.addSecs(60 * 5));
        context.replace("!OC_CONTENT!", mtg->openingComments().replace("\n", "<br>"));
        context = replaceTimeTag(context, "RPA_STARTTIME", starttime.addSecs(60 * ((ex == ccongregation::circuit_overseer_visit) ? 67 : 97)));
        context.replace("!RPA_CONTENT!", mtg->closingComments().replace("\n", "<br>"));

        int fmID(0);
        context.replace("!FM0_THEME!", "");
        int elapsedMinutes = 8;
        for (LMM_Assignment *asgn : prog) {
            QString basetagname = "";
            QString lblspkrname = "";
            QString spkrname = "";
            QString lblasstname = "";
            QString asstname = "";
            QString lblsource = "";
            QString source(asgn->source());
            QString lbltheme = "";
            QString theme(asgn->theme());
            QString lblnote = "";
            QString note(asgn->note());
            QString time = "";
            QString lbltime = "";
            QString lblstarttime = "";
            LMM_Assignment_ex *studenttalk(nullptr);
            LMM_Assignment_ex *cbs(nullptr);
            int currentDuration(asgn->time());

            if (asgn->canCounsel())
                studenttalk = qobject_cast<LMM_Assignment_ex *>(asgn);
            else if (worksheet && d.year() < 2018) {
                if (asgn->talkId() == LMM_Schedule::TalkType_SampleConversationVideo) {
                    int len = asgn->theme().length();
                    context.replace("!PREPARE_PRESENTATIONS!", (len > 1) ? "Yes" : "");
                }
                // now worksheets only have student info
                // Chairman can have the normal schedule with him for other talks.
                continue;
            }
            if (asgn->talkId() == LMM_Schedule::TalkType_CBS)
                cbs = qobject_cast<LMM_Assignment_ex *>(asgn);

            if (asgn->speaker())
                spkrname = asgn->speaker()->fullname();
            if (studenttalk != nullptr && studenttalk->assistant())
                asstname = studenttalk->assistant()->fullname();
            else if (cbs && cbs->assistant())
                asstname = cbs->assistant()->fullname();

            switch (asgn->talkId()) {
            case LMM_Schedule::TalkType_Treasures:
                basetagname = "GW1";
                break;
            case LMM_Schedule::TalkType_Digging:
                basetagname = "GW2";
                currentDuration += 1;
                break;
            case LMM_Schedule::TalkType_BibleReading:
                basetagname = "GW3";
                currentDuration += 1;
                break;
            case LMM_Schedule::TalkType_SampleConversationVideo:
            case LMM_Schedule::TalkType_ApplyYourself:
            case LMM_Schedule::TalkType_InitialCall:
            case LMM_Schedule::TalkType_ReturnVisit1:
            case LMM_Schedule::TalkType_ReturnVisit2:
            case LMM_Schedule::TalkType_ReturnVisit3:
            case LMM_Schedule::TalkType_BibleStudy:
            case LMM_Schedule::TalkType_StudentTalk:
                if (asgn->theme().isEmpty()) {
                    basetagname = "Unused";
                } else {
                    if (asgn->classnumber() == 1) {
                        fmID++;
                        if (asgn->talkId() != LMM_Schedule::TalkType_SampleConversationVideo && asgn->talkId() != LMM_Schedule::TalkType_ApplyYourself)
                            currentDuration += 1;
                    }
                    basetagname = "FM" + QVariant(fmID).toString();
                }
                break;
            case LMM_Schedule::TalkType_LivingTalk1:
                elapsedMinutes = 52;
                basetagname = "CL1";
                break;
            case LMM_Schedule::TalkType_LivingTalk2:
                basetagname = "CL2";
                break;
            case LMM_Schedule::TalkType_LivingTalk3:
                basetagname = "CL3";
                break;
            case LMM_Schedule::TalkType_CBS:
                basetagname = "CBS";
                break;
            case LMM_Schedule::TalkType_COTalk:
                elapsedMinutes += 3; // RPA
                basetagname = "CO";
                break;
            }
            QString classLetter(abcd.mid(asgn->classnumber() - 1, 1));
            lbltheme = "!" + basetagname + "_THEME!";
            lblsource = "!" + basetagname + "_SOURCE!";
            lblspkrname =
                    "!" + basetagname + (cbs ? "_CONDUCTOR" : "_SPEAKER") + (asgn->canMultiSchool() ? "_" + classLetter : "") + "!";
            lblasstname =
                    "!" + basetagname + (cbs ? "_READER" : "_ASSISTANT") + (asgn->canMultiSchool() ? "_" + classLetter : "") + "!";
            lblnote =
                    "!" + basetagname + "_NOTES" + (asgn->canMultiSchool() ? "_" + classLetter : "") + "!";
            lbltime = "!" + basetagname + "_TIME!";
            //! need to convert "time" from duration to actual TOD
            time = QString::number(asgn->time());
            lblstarttime = "!" + basetagname + "_STARTTIME!";

            /*
            qDebug() << "-------------------TAGS";
            qDebug() << "lbltheme" << lbltheme;
            qDebug() << "theme" << lbltheme;
            qDebug() << "lblsource" << lblsource;
            qDebug() << "source" << source;
            qDebug() << "lblspkrname" << lblspkrname;
            qDebug() << "spkrname" << spkrname;
            qDebug() << "lblasstname" << lblasstname;
            qDebug() << "asstname" << asstname;
            qDebug() << "lbltime" << lbltime;
            qDebug() << "time" << time;
            */

            context.replace(lbltheme, theme);
            context.replace(lblsource, source);
            context.replace(lblspkrname, spkrname);
            context.replace(lblasstname, asstname);
            context.replace(lblnote, note);
            context.replace(lbltime, time);
            context = replaceTimeTag(context, basetagname + "_STARTTIME", starttime.addSecs(60 * elapsedMinutes));
            if (asgn->classnumber() == 1)
                elapsedMinutes += currentDuration;
            if (asgn->canMultiSchool())
                context.replace(QString("!%1_SAMPLEVIDEO!").arg(basetagname), asgn->talkId() == LMM_Schedule::TalkType_SampleConversationVideo ? "Yes" : "");

            // work sheet
            QString studyname = "";
            QString studynumber = "";
            QString exercises = "";

            if (studenttalk) {
                context.replace(QString("!%1_TH_NO!").arg(basetagname), QVariant(asgn->study_number()).toString());
                context.replace(QString("!%1_TH_NAME!").arg(basetagname), asgn->study_name());

                if (asgn->date().year() > 2018) {
                    exercises = "";
                    studynumber = QVariant(asgn->study_number()).toString();
                    QString unknowncounselpoint;
                    schoolStudentStudy *study = studenttalk->getCounselPoint(unknowncounselpoint);
                    if (study)
                        studyname = study->getName();
                } else {
                    context.replace(QString("!%1_SETTING!").arg(basetagname), studenttalk->setting());
                    // student's study
                    if (studenttalk->speaker()) {
                        QString unknowncounselpoint;
                        schoolStudentStudy *study = studenttalk->getCounselPoint(unknowncounselpoint);
                        if (study) {
                            if (study->getExercise())
                                exercises = checkmark;
                            studynumber = QVariant(study->getNumber()).toString();
                            studyname = study->getName();
                            if (!unknowncounselpoint.isEmpty())
                                studyname = QString("%1 (%2)").arg(unknowncounselpoint, studyname);
                        }
                    }
                }
                context.replace(QString("!%1_SPEAKER_%2_EX_DONE!").arg(basetagname, classLetter), exercises);
                context.replace(QString("!%1_SPEAKER_%2_STUDYNAME!").arg(basetagname, classLetter), studyname);
                context.replace(QString("!%1_SPEAKER_%2_STUDY!").arg(basetagname, classLetter), studynumber);
            }
            context.replace("!NOTES!", asgn->note());

            // other school
            if (context.contains("!OTHER_SCHOOLS!")) {
                if (s.getClassesQty() == 1) {
                    context.remove("!OTHER_SCHOOLS!");
                    context.remove("!OTHER_SCHOOLS_NAMES!");
                } else {
                    context.replace("!OTHER_SCHOOLS!", tr("Other schools"));
                    QString otherschoolnames = "";
                    for (int schools2 = 2; schools2 < s.getClassesQty() + 1; schools2++) {
                        otherschoolnames.append(tr("Class"));
                        if (schools2 == 2) {
                            otherschoolnames.append(" II");
                        } else if (schools2 == 3) {
                            otherschoolnames.append(" III");
                        } else {
                            otherschoolnames.append(QVariant(schools2).toString());
                        }
                        otherschoolnames.append(": <br/>");
                        bool isFirst(true);
                        for (LMM_Assignment *otherasgn : prog) {
                            if (otherasgn->classnumber() != schools2)
                                continue;
                            if (isFirst)
                                isFirst = false;
                            else
                                otherschoolnames.append("<br/>");
                            LMM_Assignment_ex *otherstudenttalk(qobject_cast<LMM_Assignment_ex *>(otherasgn));
                            otherschoolnames.append(otherstudenttalk->talkName() + " ");
                            if (otherstudenttalk->speaker())
                                otherschoolnames.append(otherasgn->speaker()->fullname());
                            if (otherstudenttalk->assistant())
                                otherschoolnames.append(" / " + otherstudenttalk->assistant()->fullname());
                        }
                    }
                    context.replace("!OTHER_SCHOOLS_NAMES!", otherschoolnames);
                }
            }
        }
    }

    // next week
    if (context.contains("_NW!")) {
        QSharedPointer<LMM_Meeting> nw_mtg(new LMM_Meeting());
        if (nw_mtg->loadMeeting(date.addDays(7), true)) {
            context.replace("!LM_SOURCE_NW!", nw_mtg->bibleReading());
            QList<LMM_Assignment *> prog = nw_mtg->getAssignments();
            int fmID(0);
            for (LMM_Assignment *asgn : prog) {
                QString basetagname = "";
                switch (asgn->talkId()) {
                case LMM_Schedule::TalkType_Treasures:
                    basetagname = "GW1";
                    break;
                case LMM_Schedule::TalkType_Digging:
                    basetagname = "GW2";
                    break;
                case LMM_Schedule::TalkType_BibleReading:
                    basetagname = "GW3";
                    break;
                case LMM_Schedule::TalkType_SampleConversationVideo:
                case LMM_Schedule::TalkType_ApplyYourself:
                case LMM_Schedule::TalkType_InitialCall:
                case LMM_Schedule::TalkType_ReturnVisit1:
                case LMM_Schedule::TalkType_ReturnVisit2:
                case LMM_Schedule::TalkType_ReturnVisit3:
                case LMM_Schedule::TalkType_BibleStudy:
                case LMM_Schedule::TalkType_StudentTalk:
                    if (asgn->theme().isEmpty()) {
                        continue;
                    } else {
                        if (asgn->classnumber() == 1)
                            fmID++;
                        basetagname = "FM" + QVariant(fmID).toString();
                    }
                    break;
                case LMM_Schedule::TalkType_LivingTalk1:
                    basetagname = "CL1";
                    break;
                case LMM_Schedule::TalkType_LivingTalk2:
                    basetagname = "CL2";
                    break;
                case LMM_Schedule::TalkType_LivingTalk3:
                    basetagname = "CL3";
                    break;
                case LMM_Schedule::TalkType_CBS:
                    basetagname = "CBS";
                    break;
                case LMM_Schedule::TalkType_COTalk:
                    basetagname = "CO";
                    break;
                }
                QString lbltheme = "!" + basetagname + "_THEME_NW!";
                context.replace(lbltheme, asgn->theme());
                if (asgn->canCounsel()) {
                    QString classLetter(abcd.mid(asgn->classnumber() - 1, 1));
                    QString lblspkrname = "!" + basetagname + "_SPEAKER_" + classLetter + "_NW!";
                    QString spkrname = "";
                    if (asgn->speaker())
                        spkrname = asgn->speaker()->fullname();
                    context.replace(lblspkrname, spkrname);
                }
            }
        }
        context.replace("!CO_THEME_NW!", "");
        context.replace("!CBS_THEME_NW!", "");
    }

    // week after next week
    // TODO

    context.replace("!MWB_COLOR!", mtg->monthlyColor().name());
    context.replace("!MWB_RGB!", mtg->monthlyRGBColor());
    if (showWorkbookNumber) {
        context.replace("!MWB_NO!", QVariant(mtg->date().month()).toString());
    } else {
        context.replace("!MWB_NO!", "");
    }
    context.remove(QRegExp("!SCHOOL[1-3]!"));
    context.remove(QRegExp("!H_[A-Z|a-z|_]*!"));
    context.remove(QRegExp("!NO[1-3]_[A-Z|a-z|_]*!"));
    context.remove(QRegExp("!NO[1-3]!"));
    context.remove(QRegExp("!OTHER_[A-Z|a-z|_]*!"));
    context.remove(QRegExp("!FM[0-4]_[A-Z|a-z|_]*!"));

    delete mtg;

    return context;
}

QString printui::fillTemplateBlockPM(QString context)
{
    QSharedPointer<cpublictalks> cpt(new cpublictalks());
    QSharedPointer<cptmeeting> meeting(cpt->getMeeting(currentWeek->week));
    ccongregation::exceptions ex = c.isException(currentWeek->week);
    int md = c.getMeetingDay(currentWeek->week, ccongregation::pm);

    bool showSongTitles = currentTemplateData->optionValue("song-titles") == "yes";
    bool showWatchtowerNumber = currentTemplateData->optionValue("wt-number") == "yes";
    bool useDuration = currentTemplateData->optionValue("duration") == "duration";

    context = replaceDateTag(context, "DATE", currentWeek->week.addDays(md == 0 ? publicmeetingday - 1 : md - 1));
    context = fillCommonItems(context, md == 0, ex);

    // title
    context.replace("!PT!", tr("Public Talk"));
    context.replace("!PT_TIME!", "30");
    context.replace("!WT!", tr("Watchtower Study"));
    context.replace("!CO!", tr("Circuit Overseer"));
    context.replace("!WT_TIME!", ex == ccongregation::circuit_overseer_visit ? "30" : "60");
    context.replace("!PM_START_TIME!", myCongregation.getPublicmeeting(currentWeek->week).getMeetingtime());

    // prayer title
    context.replace("!PRAYER!", tr("Prayer"));

    // song title
    context.replace("!SONG!", tr("Song"));

    if (useDuration)
        context.replace("!DURATION!", "Yes");
    else
        context.replace("!DURATION!", "");

    // pt song
    if (meeting->song_talk > 0)
        context = replaceIntTag(context, "SONG1_NO", meeting->song_talk);
    else
        context.replace("!SONG1_NO!", "");
    context.replace("!SONG1_NAME!", showSongTitles ? meeting->getSong1Title() : "");

    // wt song1
    if (meeting->song_wt_start > 0)
        context = replaceIntTag(context, "SONG2_NO", meeting->song_wt_start);
    else
        context.replace("!SONG2_NO!", "");
    context.replace("!SONG2_NAME!", showSongTitles ? meeting->getSong2Title() : "");

    // wt song2
    if (meeting->song_wt_end > 0)
        context = replaceIntTag(context, "SONG3_NO", meeting->song_wt_end);
    else
        context.replace("!SONG3_NO!", "");
    context.replace("!SONG3_NAME!", showSongTitles ? meeting->getSong3Title() : "");

    // theme
    context.replace("!PT_THEME!", meeting->theme.theme);
    context = replaceIntTag(context, "PT_NUMBER", meeting->theme.number);

    QRegExp rx("!PT_GRP_THEME_[A-Z|a-z]*!");
    int grppos = rx.indexIn(context);
    if (grppos > -1) {
        QStringList grpTexts = rx.capturedTexts();
        context.replace(rx, meeting->theme.themeInLanguage(sql->getLanguageId(grpTexts.at(0).right(3).left(2).toLower()), currentWeek->week));
    }

    context.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    context.replace("!CONGREGATION!", tr("Congregation"));
    context.replace("!CONGREGATION_NAME!", myCongregation.name);

    // speaker
    context.replace("!PT_SPEAKER!", meeting->speaker() ? meeting->speaker()->fullname() : "");
    context.replace("!PT_SPEAKER_PHONE!", meeting->speaker() ? meeting->speaker()->phone() : "");
    context.replace("!PT_SPEAKER_MOBILE!", meeting->speaker() ? meeting->speaker()->mobile() : "");
    sql_item congregationInfo;
    if (meeting->speaker())
        congregationInfo = meeting->speaker()->congregationInfo()[0];
    context.replace("!PT_SPEAKER_CONGREGATION!", meeting->speaker() ? congregationInfo.value("name").toString() : "");
    context.replace("!PT_SPEAKER_CONGREGATION_ADDRESS!", meeting->speaker() ? congregationInfo.value("address").toString() : "");
    context.replace("!PT_SPEAKER_CONGREGATION_CIRCUIT!", meeting->speaker() ? congregationInfo.value("circuit").toString() : "");
    context.replace("!PT_SPEAKER_CONGREGATION_INFO!", meeting->speaker() ? congregationInfo.value("info").toString() : "");

    // hospitality
    context.replace("!PT_SPEAKER_HOST!", meeting->getHospitalityhost() ? meeting->getHospitalityhost()->fullname() : "");

    // chairman
    context.replace("!PM_CHAIRMAN!", meeting->chairman() ? meeting->chairman()->fullname() : "");

    // reader
    context.replace("!WT_READER!", meeting->wtReader() ? meeting->wtReader()->fullname() : "");

    // wt details
    context.replace("!WT_ARTICLE!", meeting->wtsource);
    context.replace("!WT_COLOR!", meeting->getWtIssueColor().name());
    if (showWatchtowerNumber) {
        context.replace("!WT_NO!", meeting->wtissue);
    } else {
        context.replace("!WT_NO!", "");
    }
    context.replace("!WT_THEME!", meeting->wttheme);

    // wt conductor
    context.replace("!WT_CONDUCTOR_TITLE!", tr("Watchtower Conductor"));
    context.replace("!WT_CONDUCTOR!", meeting->wtConductor() ? meeting->wtConductor()->fullname() : "");

    context.replace("!NO_MEETING!", tr("No regular meeting"));
    context.replace("!CO_THEME!", meeting->getFinalTalk());
    context.replace("!CO_TIME!", QVariant(ex == ccongregation::circuit_overseer_visit ? 30 : 0).toString());

    context.remove("!PT_TITLE!");
    context.remove("!PT_THEME!");
    context.remove("!PT_NO!");
    context.remove("!PT_SPEAKER!");
    context.remove("!PT_SPEAKER_PHONE!");
    context.remove("!PT_SPEAKER_CONGREGATION!");
    context.remove("!PT_CHAIRMAN!");
    context.remove("!PT_CHAIRMAN_NAME!");
    context.remove("!PT_CONDUCTOR_NAME!");
    context.remove("!WT_ARTICLE");
    context.remove("!WT_THEME!");
    context.remove("!WT_CONDUCTOR_TITLE!");
    context.remove("!WT_READER!");
    context.remove("!WT_READER_NAME!");
    return QString(context);
}

// Fill outgoing speakers to template section
QString printui::fillTemplateBlockPTOUT(QString context)
{
    QString newcontext = "";
    cpublictalks cpt;
    QList<cpoutgoing *> out = cpt.getOutgoingSpeakers(currentWeek->week);

    bool didOne(false);
    foreach (cpoutgoing *o, out) {
        person *speaker = o->getSpeaker();
        if (!speaker)
            continue;
        didOne = true;

        QString currentSection(context);
        currentSection = fillCommonItems(currentSection, false, c.isException(currentWeek->week));
        currentSection = replaceIntTag(currentSection, "PTCOUNTER", ptCounter++);
        currentSection = replaceDateTag(currentSection, "DATE", currentWeek->week.addDays(o->getCongregation().getPublicmeeting(currentWeek->week).getMeetingday() - 1));
        currentSection.replace("!PT_SPEAKER!", o->getSpeaker()->fullname());
        currentSection.replace("!PT_NO!", QVariant(o->getTheme().number).toString());
        currentSection.replace("!PT_THEME!", o->getTheme().theme);
        currentSection.replace("!PT_CONGREGATION!", o->getCongregation().name);
        currentSection.replace("!CONGREGATION_ADDRESS!", o->getCongregation().address);
        currentSection.replace("!PM_START_TIME!", o->getCongregation().getPublicmeeting(currentWeek->week).getMeetingtime());

        currentSection = fillTemplateBlockPTOUTTitle(currentSection);

        newcontext.append(currentSection);
    }
    if (!didOne) {
        QString currentSection(context);
        currentSection = fillCommonItems(currentSection, false, c.isException(currentWeek->week));
        currentSection = replaceDateTag(currentSection, "DATE", currentWeek->week.addDays(myCongregation.getPublicmeeting(currentWeek->week).getMeetingday() - 1));
        currentSection.replace("!PT_SPEAKER!", "");
        currentSection.replace("!PT_NO!", "");
        currentSection.replace("!PT_THEME!", "");
        currentSection.replace("!CONGREGATION!", "");
        currentSection.replace("!CONGREGATION_ADDRESS!", "");
        currentSection.replace("!PM_START_TIME!", "");

        currentSection = fillTemplateBlockPTOUTTitle(currentSection);

        newcontext.append(currentSection);
    }
    return newcontext;
}

QString printui::fillTemplateBlockPTOUTSlips(person *speaker, QDate fromdate, QDate todate, QString context)
{
    QString newcontext = "";

    // todo: APC, July 2017. Think we should use QSharedPointer like this:
    // QSharedPointer<cpublictalks::cptmeeting> cm(new cpublictalks::cptmeeting());
    cpublictalks cp;
    QList<cpoutgoing *> out = cp.getOutgoingBySpeaker(speaker->id(), fromdate, todate);

    foreach (cpoutgoing *o, out) {
        QString currentSection(context);
        currentSection = fillCommonItems(currentSection, false, ccongregation::none);
        // temporary fix for public meeting day
        currentSection = replaceDateTag(currentSection, "DATE", o->date());
        currentSection.replace("!PT_SPEAKER!", o->getSpeaker()->fullname());
        currentSection = replaceIntTag(currentSection, "PT_NO", o->getTheme().number);
        currentSection.replace("!PT_THEME!", o->getTheme().theme);
        currentSection.replace("!PT_CONGREGATION!", o->getCongregation().name);
        currentSection.replace("!CONGREGATION_ADDRESS!", o->getCongregation().address);
        currentSection.replace("!PM_START_TIME!", o->time());

        currentSection = fillTemplateBlockPTOUTTitle(currentSection);

        newcontext.append(currentSection);
    }
    return newcontext;
}

///
/// \brief printui::fillTemplateBlockPTOUTTitle Fill header in outgoing speaker schedule or slip
/// \param context The content of HTML section
/// \param speakername The optional parameter for defining name of speaker
/// \return Filled HTML content
///
QString printui::fillTemplateBlockPTOUTTitle(QString context, QString speakername)
{
    context.replace("!TALK!", tr("Talk"));
    context.replace("!TITLE!", tr("Outgoing Speakers"));
    context.replace("!DATE_TITLE!", tr("Date"));
    context.replace("!SPEAKER!", tr("Speaker"));
    context.replace("!PT_THEME_TITLE!", tr("Theme"));
    context.replace("!PT_NO_TITLE!", tr("Theme Number"));
    context.replace("!CONGREGATION!", tr("Congregation"));
    context.replace("!TIME!", tr("Start Time"));

    context.replace("!DISCLAIMER!", tr("NOTE: Dear brother, in spite of careful database-maintenance, "
                                       "sometimes times or addresses might be out of date. "
                                       "So, please verify by looking those up via JW.ORG. Thank you!"));

    if (speakername != "")
        context.replace("!PT_SPEAKER!", speakername);
    return context;
}

QString printui::fillTemplateBlockTerritoryOverall(QString context)
{
    context.replace("!ADDRESS!", tr("Address"));
    context.replace("!ADDRESSES!", tr("Addresses", "Addresses included in the territory"));
    context.replace("!ADDRESSTYPE!", tr("Address type"));
    context.replace("!ASSIGNED_TO!", tr("Assigned to"));
    context.replace("!CONGREGATION!", tr("Congregation"));
    context.replace("!CONGREGATION_NAME!", myCongregation.name);
    context.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    context.replace("!DATE_CHECKED_OUT!", tr("Date checked out"));
    context.replace("!DATE_CHECKED_BACK_IN!", tr("Date checked back in"));
    context.replace("!DATE_LAST_WORKED!", tr("Date last worked"));
    context.replace("!DATE_TITLE!", tr("Date"));
    context.replace("!CITY!", tr("City"));
    context.replace("!COUNTRY!", tr("Country", "Short name of country"));
    context.replace("!COUNTY!", tr("County", "Name of administrative area level 2"));
    context.replace("!DISTRICT!", tr("District", "Sublocality, first-order civil entity below a locality"));
    context.replace("!FROM_NUMBER!", tr("From", "From number; in number range"));

    QSettings settings;
    QString googleAPIkey = settings.value("geo_service_provider/google_api_key", "").toString();
    context.replace("!GOOGLE_API_KEY!", googleAPIkey.isEmpty() ? "" : googleAPIkey);
    QString hereAppId = settings.value("geo_service_provider/here_app_id", "").toString();
    context.replace("!HERE_APP_ID!", hereAppId.isEmpty() ? "" : hereAppId);
    QString hereAppCode = settings.value("geo_service_provider/here_app_code", "").toString();
    context.replace("!HERE_APP_CODE!", hereAppCode.isEmpty() ? "" : hereAppCode);

    context.replace("!HOUSENUMBER!", tr("No.", "House or street number"));
    context.replace("!LOCALITY!", tr("Locality"));
    context.replace("!MAP!", tr("Map", "Map of a territory"));
    context.replace("!NAME!", tr("Name", "Name of person or building"));
    context.replace("!POSTALCODE!", tr("Postalcode", "Mail code, ZIP"));
    context.replace("!PUBLISHER!", tr("Name of publisher"));
    context.replace("!REMARK!", tr("Remark"));
    context.replace("!STATE!", tr("State", "Short name of administrative area level 1"));
    context.replace("!STREET!", tr("Street", "Streetname"));
    context.replace("!TERRITORY!", tr("Territory"));
    context.replace("!TERRITORY_NUMBER!", tr("Terr. No."));
    context.replace("!TERRITORY_TYPE!", tr("Territory type"));
    context.replace("!TO_NUMBER!", tr("To", "To number; in number range"));
    context.replace("!TYPE!", tr("Type", "Type of something"));
    context.replace("!SUM!", tr("Sum", "Total amount"));
    return context;
}

QString printui::fillCommonItems(QString context, bool nomeeting, ccongregation::exceptions ex)
{
    context = fillDate(context, ccongregation::pm, "PM_DATE");
    context = fillExceptionDate(context, "EXCEPTION_DATE");
    context.replace("!EXCEPTION!", c.getStandardExceptionText(currentWeek->week, false));
    context.replace("!NO_MEETING_EXCEPTION!", nomeeting ? "Yes" : "");
    context.replace("!CO_VISIT!", ex == ccongregation::circuit_overseer_visit ? "Yes" : "");
    context.replace("!CO_NAME!", sql->getSetting("circuitoverseer"));
    context.replace("!CO_TITLE!", tr("Service Talk"));
    context.replace("!CONVENTION!", ex == ccongregation::convention ? "Yes" : "");
    context.replace("!WEEKS!", QVariant(currentWeek->weeks).toString());
    context = replaceIntTag(context, "FULLWEEKNUMBER", currentWeek->fullweeknum);
    context = replaceIntTag(context, "WEEKNUMBER", currentWeek->weeknum);
    context.replace("!NEWMONTH!", currentWeek->newmonth ? "Yes" : "");
    context.replace("!ENDMONTH!", currentWeek->endmonth ? "Yes" : "");
    context.replace("!ISFIRST!", currentWeek->isFirst ? "Yes" : "");
    context.replace("!ISLAST!", currentWeek->isLast ? "Yes" : "");
    context.replace("!BEGIN!", tr("Begins at", "Used in print template, example 'Begins at 11:00'"));
    return context;
}

QString printui::fillDate(QString context, ccongregation::meetings meetingtype, QString VarName)
{
    QDate dt;
    int day(c.getMeetingDay(currentWeek->week, meetingtype));
    if (day == 0)
        dt = QDate();
    else
        dt = currentWeek->week.addDays(day - 1);
    context = replaceDateTag(context, VarName, dt);
    return context;
}

QString printui::fillExceptionDate(QString context, QString tag)
{
    QDate d1, d2;
    QString fulltag("!" + tag);
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        fulltag += "([^!]*)!";
        c.getExceptionDates(currentWeek->week, d1, d2);
        QLocale loc;
        QString defaultFormat = loc.dateFormat(QLocale::ShortFormat);
        int pos = 0;
        QRegExp rx(fulltag);
        while ((pos = rx.indexIn(context)) > -1) {
            if (d1.isNull()) {
                context = context.left(pos) + context.mid(pos + rx.matchedLength());
            } else {
                QString format = rx.cap(2).isEmpty() ? defaultFormat : rx.cap(1).trimmed();
                QString exceptionText = d1.toString(format);
                if (!d2.isNull() && d1 != d2)
                    exceptionText += "-" + d2.toString(format);
                context = context.left(pos) + exceptionText + context.mid(pos + rx.matchedLength());
            }
        }
    }
    return context;
}

QString printui::replaceDateTag(QString context, QString tag, QDate date, QString wrapperText)
{
    int pos = 0;
    QString fulltag("!" + tag + " ");
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        fulltag = fulltag + "([^!]+)!";
        QRegExp rx(fulltag);
        while ((pos = rx.indexIn(context)) > -1) {
            if (date.isNull())
                context = context.left(pos) + context.mid(pos + rx.matchedLength());
            else
                context = context.left(pos) + wrapperText.arg(date.toString(rx.cap(1))) + context.mid(pos + rx.matchedLength());
        }
    }
    fulltag = "!" + tag + "!";
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        if (date.isNull())
            context.replace(fulltag, "");
        else
            context.replace(fulltag, wrapperText.arg(date.toString(Qt::DefaultLocaleShortDate)));
    }
    return context;
}

QString printui::replaceTimeTag(QString context, QString tag, QTime time)
{
    int pos = 0;
    QString fulltag("!" + tag + " ");
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        fulltag = fulltag + "([^!]+)!";
        QRegExp rx(fulltag);
        while ((pos = rx.indexIn(context)) > -1) {
            if (time.isNull())
                context = context.left(pos) + context.mid(pos + rx.matchedLength());
            else
                context = context.left(pos) + time.toString(rx.cap(1)) + context.mid(pos + rx.matchedLength());
        }
    }
    fulltag = "!" + tag + "!";
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        if (time.isNull())
            context.replace(fulltag, "");
        else
            context.replace(fulltag, time.toString(Qt::DefaultLocaleShortDate));
    }
    return context;
}

QString printui::replaceIntTag(QString context, QString tag, int number)
{
    int pos = 0;
    QString fulltag("!" + tag + " ");
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        fulltag = fulltag + "([^!]+)!";
        QRegExp rx(fulltag);
        while ((pos = rx.indexIn(context)) > -1) {
            // must be a padding number
            int padding(rx.cap(1).toInt());
            context = context.left(pos) + QString("%1").arg(number, padding, 10, QChar('0')) + context.mid(pos + rx.matchedLength());
        }
    }
    fulltag = "!" + tag + "!";
    if (context.indexOf(fulltag, 0, Qt::CaseInsensitive) > -1) {
        context.replace(fulltag, QVariant(number).toString());
    }
    return context;
}

///
/// \brief printui::templateGetSection Get template section from HTML
/// \param context The full content of HTML
/// \param start Start tag name of section eg. !REPEAT_START!
/// \param end End tag name of section eg. !REPEAT_END!
/// \param classname The css classname for div element to replaces section found
/// \return QPair where first QString is content without section and second is only section that found
///
QPair<QString, QString> printui::templateGetSection(QString context, const QString start, const QString end,
                                                    const QString classname)
{
    QRegularExpression exp(QString("<p>%1[\\s\\S]*?%2</p>").arg(start, end), QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatch match;
    QString section = "";
    if (context.indexOf(exp, 0, &match) > -1) {
        context = context.replace(exp, QString("<div class=\"%1\"></div>").arg(classname));
        section = match.captured().remove(QRegularExpression(QString("<p>%1</p>|<p>%2</p>").arg(start, end)));
    }
    return qMakePair(context, section);
}

QString printui::addQrCode(QString context)
{
    qDebug() << "adding QR code";
    QString qrcodeTag = "";
    QString sharedLinkTag = "!SHARED_LINK!";
    QRegExp rx("!QRCODE_[0-9]*!");
    if (rx.indexIn(context) > -1)
        qrcodeTag = rx.capturedTexts().first();
    if (ui->checkBoxQRCode->isChecked()) {
        if (qrcodeTag.isEmpty()) {
            qrcodeTag = "!QRCODE_100!";
            context.append("<div align=\"center\" style=\"margin:20px; padding:10px; border:1px solid  black\">" + qrcodeTag + "<br>" + sharedLinkTag + "</div>");
        }
        QString size = qrcodeTag.mid(qrcodeTag.indexOf("_") + 1, qrcodeTag.length() - (qrcodeTag.indexOf("_") + 2));
        context.replace(qrcodeTag, QString("<img src=\"%1\" height=\"%2\" width=\"%2\">").arg(sharedLink.isEmpty() ? QString("data:image/svg+xml;utf8,"
                                                                                                                             "%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width"
                                                                                                                             "%3D%22100%22%20height%3D%22100%22%3E%3Crect%20x%3D%225%22%20y%3D%225%22%20"
                                                                                                                             "width%3D%2290%22%20height%3D%2290%22%20rx%3D%2220%22%20fill%3D%22none%22%20"
                                                                                                                             "stroke%3D%22black%22%20stroke-width%3D%225%22%20stroke-dasharray"
                                                                                                                             "%3D%2210%22%2F%3E%3C%2Fsvg%3E")
                                                                                                                   : QString("https://api.theocbase.net/api.php?qrcode&data=%1").arg(sharedLink),
                                                                                              size));
        context.replace(sharedLinkTag, QString("<a href=\"%1\">%1</a>").arg(sharedLink.isEmpty() ? "https://dropbox.com/..." : sharedLink));
    } else {
        context.replace(qrcodeTag, "");
        context.replace(sharedLinkTag, "");
    }
    return context;
}

QString printui::addHtmlImage(QString path, QString tagname)
{
    QFile f(path);
    if (!f.exists()) {
        qDebug() << "image path does not found";
        return "";
    }
    f.open(QIODevice::ReadOnly);
    QByteArray bytearray = f.readAll();
    f.close();
    QString size = tagname.mid(tagname.indexOf("_") + 1, tagname.length() - (tagname.indexOf("_") + 2));
    QString imgtag = QString("<img src=data:image/png;base64,%1 height=\"%2\" width=\"%2\">").arg(QString::fromUtf8(bytearray.toBase64()), size);
    qDebug() << imgtag;
    return imgtag;
}

QString printui::addCssPageBreak()
{
    return ("<div style=\"page-break-after:always\"></div>");
}

QString printui::uploadSharedFile()
{
    QProgressDialog progressdialog("Uploading shared file...", "", 0, 0);
    progressdialog.setCancelButton(nullptr);
    progressdialog.setWindowModality(Qt::ApplicationModal);
    progressdialog.show();
    QFileInfo tInfo("/Apps/TheocBase/public/" + currentTemplateData->getTemplateName());
    QString dropboxPath = tInfo.path() + "/" + tInfo.completeBaseName() + ".pdf";
    QDateTime dt = cloud->authentication()->upload(bContent, dropboxPath);
    if (dt.isValid()) {
        sharedLink = cloud->authentication()->getSharedLink(dropboxPath);
        qDebug() << "shared link" << sharedLink;
        printRequest();
        QEventLoop loop;
        connect(ui->previewPdf, &PrintPreview::previewReady, &loop, &QEventLoop::quit);
        loop.exec();
    }
    progressdialog.close();
    return sharedLink;
}

void printui::handleCurrentTemplateMeta(QString html)
{
    currentTemplateData->loadMeta(html);
    showCurrentTemplateOptions();
}

void printui::showCurrentTemplateOptions()
{
    bool hasSongTitle(currentTemplateData->optionValue("song-titles").length() > 0);
    bool hasWorkbookNumber(currentTemplateData->optionValue("mwb-number").length() > 0);
    bool hasWatchtowerNumber(currentTemplateData->optionValue("wt-number").length() > 0);
    bool hasDuration(currentTemplateData->optionValue("duration").length() > 0);
    bool hasCounselName(currentTemplateData->optionValue("counsel-name").length() > 0);
    bool hasSectionTitle(currentTemplateData->optionValue("section-titles").length() > 0);

    NoUIEvents++;
    ui->ckOptionShowSongTitles->setVisible(hasSongTitle);
    ui->ckOptionShowSongTitles->setChecked(currentTemplateData->optionValue("song-titles") == "yes");
    ui->ckOptionShowWorkbookNumber->setVisible(hasWorkbookNumber);
    ui->ckOptionShowWorkbookNumber->setChecked(currentTemplateData->optionValue("mwb-number") == "yes");
    ui->ckOptionShowWatchtowerNumber->setVisible(hasWatchtowerNumber);
    ui->ckOptionShowWatchtowerNumber->setChecked(currentTemplateData->optionValue("wt-number") == "yes");
    ui->ckOptionIncludeCounselText->setVisible(hasCounselName);
    ui->ckOptionIncludeCounselText->setChecked(currentTemplateData->optionValue("counsel-name") == "yes");
    ui->rbOptionShowTime->setVisible(hasDuration);
    ui->rbOptionShowDuration->setVisible(hasDuration);
    if (currentTemplateData->optionValue("duration") == "duration")
        ui->rbOptionShowDuration->setChecked(true);
    else
        ui->rbOptionShowTime->setChecked(true);
    ui->ckOptionSectionTitles->setVisible(hasSectionTitle);
    ui->ckOptionSectionTitles->setChecked(currentTemplateData->optionValue("section-titles") == "yes");
    ui->txtMWTitle->setText(sql->getSetting("print_mw_title", tr("Midweek Meeting")));
    ui->txtWETitle->setText(sql->getSetting("print_we_title", tr("Weekend Meeting")));

    bool worksheet = ui->radioButtonSchool->isChecked() && ui->radioButtonWorkSheet->isChecked();
    ui->lblMW_OC->setVisible(worksheet);
    ui->lblMW_RPA->setVisible(worksheet);
    ui->txtMW_OC_Title->setVisible(worksheet);
    ui->txtMW_RPA_Title->setVisible(worksheet);
    ui->txtMW_OC_Title->setText(sql->getSetting("print_mw_oc_title", tr("Opening Comments")));
    ui->txtMW_RPA_Title->setText(sql->getSetting("print_mw_rpa_title", tr("Closing Comments")));

    NoUIEvents--;
}

QPrinter::Orientation printui::getOrientation()
{
    if (currentTemplateData->metaValue("tb-orientation") == "")
        return po;
    return currentTemplateData->metaValue("tb-orientation").compare("Landscape", Qt::CaseInsensitive) == 0 ? QPrinter::Landscape : QPrinter::Portrait;
}

QMarginsF printui::getMargins()
{
    if (currentTemplateData->metaValue("tb-margin") == "")
        return pmlist;
    QStringList margins = currentTemplateData->metaValue("tb-margin").split(';');
    QMarginsF ret;
    if (margins.length() > 3) {
        ret.setLeft(margins[0].toDouble());
        ret.setTop(margins[1].toDouble());
        ret.setRight(margins[2].toDouble());
        ret.setBottom(margins[3].toDouble());
    } else if (margins.length() > 1) {
        ret.setLeft(margins[0].toDouble());
        ret.setTop(margins[1].toDouble());
        ret.setRight(margins[0].toDouble());
        ret.setBottom(margins[1].toDouble());
    } else {
        ret.setLeft(margins[0].toDouble());
        ret.setTop(margins[0].toDouble());
        ret.setRight(margins[0].toDouble());
        ret.setBottom(margins[0].toDouble());
    }
    return ret;
}

QString printui::initPrinter(QString html)
{
    // use full page printing because margins are set manually
    pr->setFullPage(true);
    if (currentTemplateData->customPaperSize.width() == 0.0) {
        pr->setPaperSize(currentTemplateData->standardPaperSize);
    } else {
        pr->setPaperSize(currentTemplateData->customPaperSize, currentTemplateData->customPaperUnit);
    }
    pr->setOrientation(getOrientation());

    if (pr->setPageMargins(getMargins()) == 0) {
        // show message for not accepting margin request?
    }

    QSizeF size(pr->pageSizeMM());
    html.replace("!PAPERWIDTH!", QString::number(size.width()));
    html.replace("!PAPERHEIGHT!", QString::number(size.height()));
    return html;
}

QString printui::getTemplate(bool returnFileName)
{
    QString selTemplate(ui->cboTemplate->currentText());
    if (!selTemplate.isEmpty() && !QFile::exists(selTemplate)) {
        // try again with path
        selTemplate = templatePath2 + selTemplate;
        if (!QFile::exists(selTemplate))
            selTemplate = templatePath1 + ui->cboTemplate->currentText();
        if (!QFile::exists(selTemplate))
            return "";
    }
    if (returnFileName)
        return selTemplate;

    if (!QFile::exists(selTemplate)) {
        return "<HTML><BODY><h3>" + tr("Template not found", "printing template not found") + "</h3><p>Path: " + selTemplate + "</p></BODY></HTML>"; //<p>!REPEAT_START!</p><p>!REPEAT_END!</p>
    }

    QFile file(selTemplate);
    if (!file.open(QFile::ReadOnly)) {
        return "<HTML><BODY><h3>" + tr("Can't read file", "cannot read printing template") + "</h3><p>Path: " + selTemplate + "</p></BODY></HTML>"; //<p>!REPEAT_START!</p><p>!REPEAT_END!</p>
    }
    QByteArray data = file.readAll();
    file.close();

    QTextCodec *codec = Qt::codecForHtml(data);
    QString str = codec->toUnicode(data);

    currentTemplateData->loadMeta(str);
    handleCurrentTemplateMeta(str);
    return str;
}

void printui::setHTML(QString html, bool waitForLoadedEvent)
{
    if (mPage)
        delete mPage;
    mPage = new QWebEnginePage(this);
    mWebView->setPage(mPage);
    mPage->settings()->globalSettings()->setAttribute(QWebEngineSettings::WebAttribute::LocalContentCanAccessFileUrls, true);
    mPage->settings()->globalSettings()->setAttribute(QWebEngineSettings::WebAttribute::LocalContentCanAccessRemoteUrls, true); //
    mPage->settings()->globalSettings()->setAttribute(QWebEngineSettings::WebAttribute::AllowRunningInsecureContent, true);

    if (!waitForLoadedEvent)
        connect(mPage, &QWebEnginePage::loadFinished, [=](bool ok) {
            qDebug() << "Page loaded" << ok;
            mPage->printToPdf([this](const QByteArray &result) {
                bContent = result;
                qDebug() << "print pdf ready";
                ui->previewPdf->setPdf(result);
            },
                              pr->pageLayout());
        });
    else {
        channel = new QWebChannel(mPage);
        mPage->setWebChannel(channel);
        channel->registerObject(QString("printChannel"), &printChannel);

        QMetaObject::Connection *const connection = new QMetaObject::Connection;
        *connection = connect(&printChannel, &PrintChannel::loadFinished, [=](QString msg) {
            QObject::disconnect(*connection);
            delete connection;
            qDebug() << "Page loaded event" << msg;
            mPage->printToPdf([this](const QByteArray &result) {
                bContent = result;
                qDebug() << "print pdf ready";
                ui->previewPdf->setPdf(result);
            },
                              pr->pageLayout());
        });
    }

    // load html code from temporary file because setHtml has 2MB limit
    tmpHtmlFile.setFileTemplate(QDir::tempPath() + QDir::separator() + "TheocBase_XXXXXX.html");
    if (tmpHtmlFile.exists())
        tmpHtmlFile.remove();
    tmpHtmlFile.setAutoRemove(false);
    if (tmpHtmlFile.open()) {
        QByteArray tempContent;
        tempContent.append(html);
        tmpHtmlFile.write(tempContent);
        tmpHtmlFile.close();

        QUrl qurl = QUrl::fromLocalFile(tmpHtmlFile.fileName());
        mPage->load(qurl);
    }

    activeHtml = html;
    //    // font size
    //    webFrame->setTextSizeMultiplier(textSizeFactory);
}

void printui::resetZooming()
{
    // resize event -> reset preview zooming to 'FitInView'
    //pv->fitInView();
    // update zoom factory in print preview because it depens of window size
    //defaultZoomFactor = pv->zoomFactor();
    //ui->zoomSlider->setValue(100);
}

void printui::fillWeekList(int offsetFromMonday)
{
    int selectedWeeks(QVariant((thruDate.toJulianDay() - fromDate.toJulianDay()) / 7 + 1).toInt());
    weekInfo *previous(nullptr);
    weekList.clear();
    QDate tempdate = fromDate;
    QDate mtgDate = fromDate.addDays(offsetFromMonday);
    for (int weeks = 0; weeks < selectedWeeks; weeks++) {
        weekInfo *w = new weekInfo(tempdate, mtgDate);
        if (previous) {
            w->newmonth = w->mtgDate.year() != previous->mtgDate.year() || w->mtgDate.month() != previous->mtgDate.month();
            if (w->newmonth) {
                fillWeekList_SetWeeks(previous);
                previous->endmonth = true;
                w->weeknum = 1;
            } else {
                w->weeknum = previous->weeknum + 1;
            }
        } else {
            w->weeknum = 1;
            w->isFirst = true;
            w->newmonth = true;
        }
        w->fullweeknum = weeks + 1;
        weekList.append(w);
        previous = w;
        tempdate = tempdate.addDays(7);
        mtgDate = mtgDate.addDays(7);
    }
    fillWeekList_SetWeeks(previous);
    previous = weekList[weekList.length() - 1];
    previous->isLast = true;
    previous->endmonth = true;
}

void printui::fillWeekList_SetWeeks(weekInfo *previous)
{
    if (previous) {
        for (weekInfo *i : weekList) {
            if (i->weeks == 0)
                i->weeks = previous->weeknum;
        }
    }
}

void printui::on_checkBoxQRCode_clicked(bool checked)
{
    if (checked) {
        ccloud cld;
        cld.generateQRCode("school");
        cld.generateQRCode("combination");
        cld.generateQRCode("publicmeeting");
    }
    printRequest();
}

void printui::increaseTextSize()
{
    // increase text size
    if (ui->radioButtonSchool->isChecked() && ui->radioButtonTaskOrder->isChecked()) {
        textSizeFactorySlips += 0.1;
        printRequest();
    } else if (ui->radioButtonTerritories->isChecked()) {
        mapSizeFactory += 1;
        printRequest();
    } else {
        mPage->runJavaScript("document.documentElement.style.fontSize = (parseInt(window.getComputedStyle"
                             "(document.documentElement).getPropertyValue('font-size') )+1).toString()+'px'",
                             [this](const QVariant &v) {
                                 Q_UNUSED(v);
                                 mPage->printToPdf([this](const QByteArray &result) {
                                     ui->previewPdf->setPdf(result);
                                 },
                                                   pr->pageLayout());
                             });
    }
}

void printui::decreaseTextSize()
{
    // decrease font size
    if (ui->radioButtonSchool->isChecked() && ui->radioButtonTaskOrder->isChecked()) {
        textSizeFactorySlips -= 0.1;
        printRequest();
    } else if (ui->radioButtonTerritories->isChecked()) {
        mapSizeFactory -= 1;
        printRequest();
    } else {
        mPage->runJavaScript("document.documentElement.style.fontSize = (parseInt(window.getComputedStyle"
                             "(document.documentElement).getPropertyValue('font-size') )-1).toString()+'px'",
                             [this](const QVariant &v) {
                                 Q_UNUSED(v);
                                 mPage->printToPdf([this](const QByteArray &result) {
                                     ui->previewPdf->setPdf(result);
                                 },
                                                   pr->pageLayout());
                             });
    }
}

printui::weekInfo::weekInfo(QDate week, QDate mtgDate)
    : week(week), mtgDate(mtgDate), weeks(0), fullweeknum(0), weeknum(0), newmonth(false), endmonth(false), isFirst(0), isLast(0)
{
}

void printui::on_editDateRangeFrom_dateChanged(const QDate &date)
{
    static int noUI(0);
    if (noUI > 0)
        return;
    noUI++;

    fromDate = date.addDays(1 - date.dayOfWeek());
    if (fromDate.month() != date.month())
        fromDate = fromDate.addDays(7);
    if (fromDate != date)
        ui->editDateRangeFrom->setDate(fromDate);
    if (fromDate > thruDate) {
        thruDate = fromDate.addMonths(1);
        thruDate = thruDate.addDays(-thruDate.day());
        thruDate = thruDate.addDays(1 - thruDate.dayOfWeek());
        ui->editDateRangeThru->setDate(thruDate);
    }
    publicmeetingday = myCongregation.getPublicmeeting(fromDate).getMeetingday();
    printRequest();

    noUI--;
}

void printui::on_editDateRangeThru_dateChanged(const QDate &date)
{
    static int noUI(0);
    if (noUI > 0)
        return;
    noUI++;

    thruDate = date.addDays(1 - date.dayOfWeek());
    if (fromDate > thruDate)
        thruDate = fromDate;
    if (thruDate != date)
        ui->editDateRangeThru->setDate(thruDate);
    printRequest();

    noUI--;
}

void printui::on_lineEditTerritoryNumber_returnPressed()
{
    territoryNumberList = ui->lineEditTerritoryNumber->text();
    if (territoryNumberList.endsWith(","))
        territoryNumberList.remove(territoryNumberList.size() - 1, 1);
    printRequest();
}

void printui::resetTextSize()
{
    textSizeFactorySlips = 1;
    textSizeFactory = 0;
    mapSizeFactory = 1;
    printRequest();
}

void printui::on_cboTemplate_activated(const QString &arg)
{
    if (NoUIEvents)
        return;

    if (currentTemplateData) {
        currentTemplateData->setTemplateName(arg);
        printRequest();
    }
}

void printui::on_cboPaperSize_activated(int index)
{
    if (NoUIEvents)
        return;

    if (index == ui->cboPaperSize->count() - 1) {
        bool ok(false);
        QString size = QInputDialog::getText(this, tr("New Custom Paper Size", "title of dialog box"), tr("Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm"), QLineEdit::Normal, "", &ok);
        if (ok && !size.isEmpty()) {
            QString msg = currentTemplateData->setPaperSize(size);
            if (msg.isEmpty()) {
                ok = true;
                fillCustomPaperSizes();
            } else {
                QMessageBox::information(this, "", tr("Invalid entry, sorry."));
            }
        }
        ui->cboPaperSize->setCurrentText(currentTemplateData->paperSize);
        if (!ok)
            return;
    } else {
        currentTemplateData->setPaperSize(ui->cboPaperSize->currentText());
    }
    radioButtonClicked();
}

printui::templateData::templateData(templateTypes templateType, QString settingName, QString currentTemplate, QString fileFilter, QString notFileFilter)
{
    sql = &Singleton<sql_class>::Instance();
    this->templateType = templateType;
    this->settingName = settingName;
    this->templateName = currentTemplate;
    this->fileFilter = fileFilter;
    this->notFileFilter = notFileFilter;
    if (currentTemplate.isEmpty())
        this->templateName = sql->getSetting(settingName);
    setPaperSize();
    metaLoaded = false;
}

printui::templateData::templateTypes printui::templateData::getTemplateType()
{
    return templateType;
}

QString printui::templateData::getTemplateName()
{
    return templateName;
}

void printui::templateData::setTemplateName(QString name)
{
    templateName = name;
    sql->saveSetting(settingName, templateName);
    metaLoaded = false;
}

QString printui::templateData::setPaperSize(QString size)
{
    static QRegularExpression rx("(\\d+[.]*\\d*)\\s*(\"|in|mm)\\s*x\\s*(\\d+[.]*\\d*)\\s*(\"|in|mm)");
    QString setting(settingName + "_papersize");
    bool loading(size.isEmpty());
    if (loading)
        size = sql->getSetting(setting);
    QRegularExpressionMatch m = rx.match(size);
    if (m.hasMatch()) {
        if (m.captured(2) != m.captured(4))
            return tr("Width unit does not match height unit", "while asking for custom paper size");
        customPaperSize.setWidth(m.captured(1).toDouble());
        customPaperSize.setHeight(m.captured(3).toDouble());
        if (m.captured(2) == "mm")
            customPaperUnit = QPrinter::Millimeter;
        else
            customPaperUnit = QPrinter::Inch;
    } else if (size == "A4") {
        standardPaperSize = QPrinter::A4;
        customPaperSize.setWidth(0);
    } else if (size == "Letter") {
        standardPaperSize = QPrinter::Letter;
        customPaperSize.setWidth(0);
    } else if (size == "Legal") {
        standardPaperSize = QPrinter::Legal;
        customPaperSize.setWidth(0);
    } else if (size == "Tabloid") {
        standardPaperSize = QPrinter::Tabloid;
        customPaperSize.setWidth(0);
    } else if (size == "A6") {
        standardPaperSize = QPrinter::A6;
        customPaperSize.setWidth(0);
    } else {
        standardPaperSize = QPrinter::A4;
        customPaperSize.setWidth(0);
        size = "A4";
        if (!loading) {
            paperSize = size;
            return tr("Invalid entry, sorry.", "while asking for custom paper size");
        }
    }
    paperSize = size;
    if (!loading)
        sql->saveSetting(setting, size);
    return "";
}

void printui::templateData::loadMeta(QString html)
{
    if (!metaLoaded) {
        options.clear();
        meta.clear();
        static QRegularExpression rxMeta("<meta name=\"([^\"]+)\" content=\"([^\"]+)\">");
        QRegularExpressionMatchIterator i = rxMeta.globalMatch(html);
        while (i.hasNext()) {
            QRegularExpressionMatch m = i.next();
            QString key = m.captured(1);
            if (key.startsWith("tb-option-")) {
                key = key.mid(10);
                options.insert(key, m.captured(2));
                QString userValue = sql->getSetting(templateName + ":" + key, "!n/a!");
                if (userValue != "!n/a!")
                    options[key] = userValue;
            } else
                meta.insert(m.captured(1), m.captured(2));
        }
        metaLoaded = true;
    }
}

QString printui::templateData::metaValue(QString name, QString defaultValue)
{
    if (meta.contains(name))
        return meta[name];
    return defaultValue;
}

QString printui::templateData::optionValue(QString name, QString defaultValue)
{
    if (options.contains(name))
        return options[name];
    return defaultValue;
}

void printui::templateData::setOptionValue(QString name, QString value, bool okAdd, bool persist)
{
    if (options.contains(name))
        options[name] = value;
    else {
        if (!okAdd)
            return;
        options.insert(name, value);
    }
    if (persist)
        sql->saveSetting(templateName + ":" + name, value);
}

void printui::on_ckOptionShowSongTitles_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionShowSongTitles->isVisible())
        return;
    NoUIEvents++;
    currentTemplateData->setOptionValue("song-titles", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionShowWorkbookNumber_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionShowWorkbookNumber->isVisible())
        return;
    NoUIEvents++;
    currentTemplateData->setOptionValue("mwb-number", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionShowWatchtowerNumber_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionShowWatchtowerNumber->isVisible())
        return;
    NoUIEvents++;
    currentTemplateData->setOptionValue("wt-number", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionIncludeCounselText_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionIncludeCounselText->isVisible())
        return;
    NoUIEvents++;
    currentTemplateData->setOptionValue("counsel-name", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_rbOptionShowTime_clicked(bool checked)
{
    if (NoUIEvents || !checked || !ui->rbOptionShowTime->isVisible())
        return;
    NoUIEvents++;
    currentTemplateData->setOptionValue("duration", "time", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_rbOptionShowDuration_clicked(bool checked)
{
    if (NoUIEvents || !checked || !ui->rbOptionShowDuration->isVisible())
        return;
    NoUIEvents++;
    currentTemplateData->setOptionValue("duration", "duration", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_ckOptionSectionTitles_clicked(bool checked)
{
    if (NoUIEvents || !ui->ckOptionSectionTitles->isVisible())
        return;
    NoUIEvents++;
    currentTemplateData->setOptionValue("section-titles", checked ? "yes" : "no", false, true);
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_btnAdditionalOptions_clicked(bool checked)
{
    ui->grpAdditionalOptions->setVisible(!checked);
}

void printui::on_txtMWTitle_editingFinished()
{
    if (NoUIEvents || !ui->txtMWTitle->isVisible() || ui->txtMWTitle->text().isEmpty())
        return;
    NoUIEvents++;
    sql->saveSetting("print_mw_title", ui->txtMWTitle->text());
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_txtWETitle_editingFinished()
{
    if (NoUIEvents || !ui->txtWETitle->isVisible() || ui->txtWETitle->text().isEmpty())
        return;
    NoUIEvents++;
    sql->saveSetting("print_we_title", ui->txtWETitle->text());
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_txtMW_OC_Title_editingFinished()
{
    if (NoUIEvents || !ui->txtMW_OC_Title->isVisible() || ui->txtMW_OC_Title->text().isEmpty())
        return;
    NoUIEvents++;
    sql->saveSetting("print_mw_oc_title", ui->txtMW_OC_Title->text());
    radioButtonClicked();
    NoUIEvents--;
}

void printui::on_txtMW_RPA_Title_editingFinished()
{
    if (NoUIEvents || !ui->txtMW_RPA_Title->isVisible() || ui->txtMW_RPA_Title->text().isEmpty())
        return;
    NoUIEvents++;
    sql->saveSetting("print_mw_rpa_title", ui->txtMW_RPA_Title->text());
    radioButtonClicked();
    NoUIEvents--;
}

void printui::setBusy()
{
    this->setCursor(Qt::WaitCursor);
    ui->frameSidebar->setEnabled(false);
}

void printui::clearBusy()
{
    this->setCursor(Qt::ArrowCursor);
    ui->frameSidebar->setEnabled(true);
}
