/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "school_setting.h"

school_setting::school_setting()
    : m_number(-1)
    , m_onlybrothers(false)
    , sql(&Singleton<sql_class>::Instance())
{

}

int school_setting::getNumber()
{
    return m_number;
}

void school_setting::setNumber(int number)
{
    m_number = number;
}

QString school_setting::getName()
{
    if (m_name == "" && m_number > 0){
        // Get name from database
        std::vector<sql_item> s = sql->selectSql("SELECT name FROM school_settings WHERE number = " + QVariant(getNumber()).toString());
        if (!s.empty()){
            m_name = s[0].value("name").toString();
        }
    }
    return m_name;
}

void school_setting::setName(QString name)
{
    m_name = name;
}

bool school_setting::getOnlyBrothers()
{
    return m_onlybrothers;
}

void school_setting::setOnlyBrothers(bool brothers)
{
    m_onlybrothers = brothers;
}
