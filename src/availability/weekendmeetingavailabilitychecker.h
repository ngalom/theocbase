#ifndef WEEKENDMEETINGAVAILABILITYCHECKER_H
#define WEEKENDMEETINGAVAILABILITYCHECKER_H

#ifndef AVAILABILITYCHECKER_H
    #include "availabilitychecker.h"
#endif

namespace tbAvailability
{

// Use WeekendMeetingAvailabilityChecker to gather information about availability of
// bros and sisters for parts on a meeting.
//
// Example Usage:
//
//  WeekendMeetingAvailabilityChecker checker(meetingDate, weekCommencingDate);
//  Availability a = checker.Get();
//
//  for(int n=0; n<a.Count(); ++n)
//  {
//      AvailabilityItem *ai = a.GetItem(index);
//      qDebug() << ai->DisplayName;
//      qDebug() << ai->OnHoliday? "On holiday" : "";
//      qDebug() << ai->OutsideSpeaker? "Talk outside" : "";
//      qDebug() << ai->HasRole(person::WtCondoctor)? "Can be WT conductor" : "Can't be WT conductor";
//      qDebug() << "Last time reading = " << GetDateLastAssigned(person::WtReader).toString(Qt::SystemLocaleShortDate);
//      qDebug() << ai->HasAssignmentsOtherThan(person::WtReader, weekCommencingDate)? "Yes" : "No";
//  }
//

class WeekendMeetingAvailabilityChecker : public AvailabilityChecker
{
private:
    virtual QString GenerateAssignedPersonsSql(QString dataSourceId);
    virtual void PopulateAssignments(QString dataSourceId, AvailabilityItem &result, const sql_items &assignedPersons);

public:
    WeekendMeetingAvailabilityChecker(const QDate &meetingDate, const QDate &weekCommencingDate);

    Availability GetPublicSpeaker(int congregationId = 0, int talkThemeId = 0);
    Availability GetLocalPublicSpeaker(int talkThemeId = 0);
};

class HostpitalityChecker : public AvailabilityChecker
{
public:
    HostpitalityChecker(const QDate &meetingDate, const QDate &weekCommencingDate);
    Availability GetHospitality();
private:
    virtual QString GenerateAssignedPersonsSql(QString dataSourceId);
    virtual void PopulateAssignments(QString dataSourceId, AvailabilityItem &result, const sql_items &assignedPersons);
};

} //namespace tbAvailability

#endif // WEEKENDMEETINGAVAILABILITYCHECKER_H
