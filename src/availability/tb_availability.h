#ifndef AVAILABILITY_H
#define AVAILABILITY_H

#ifndef QSTRING_H
    #include <QString>
#endif

#ifndef _VECTOR_
    #include <vector>
#endif

#ifndef _SET_
    #include <set>
#endif

#ifndef CPERSONS_H
    #include "../cpersons.h"
#endif

namespace tbAvailability
{

typedef std::set<person::UseFor> SetOfRoles;
typedef std::map<QDate, SetOfRoles> AssignmentsByDate;
typedef std::map<QDate, SetOfRoles>::iterator AssignmentsByDateI;

// AvailabilityItem
//=================

struct AvailabilityItem
{
    int Id;
    QString FirstName;
    QString LastName;
    QString DisplayName;
    QString CongregationName;
    QString Circuit;
    bool OnHoliday;
    bool OutsideSpeaker;
    SetOfRoles Roles;
    AssignmentsByDate AssignmentsAtHome;

    AvailabilityItem();

    void AddHomeAssignment(QDate weekCommencingDate, person::UseFor assignment);
    bool operator<(const AvailabilityItem &rhs) const;
    bool HasRole(person::UseFor role);
    bool HasAssignmentsOtherThan(person::UseFor assignment, QDate weekCommencingDate);
    QDate GetDateLastAssigned(person::UseFor assignment);
};

// Availability
//=============

class Availability
{
private:
    std::vector<AvailabilityItem> items_;

public:
    void Add(AvailabilityItem &item);
    void Sort();
    void Filter(person::UseFor role);
    void FilterOnPersonIds(std::vector<int> personIds);
    int Count();
    AvailabilityItem *GetItem(int index);
};

} // namespace tbAvailability


#endif // AVAILABILITY_H
