#include "mapSettings.h"

MapSettings::MapSettings(QObject *parent)
    : QObject(parent)
{
}

double MapSettings::markerScale()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    return QVariant(sql->getSetting("territory_map_markerscale", "0.5")).toDouble();
}
