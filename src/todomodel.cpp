/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "todomodel.h"

TodoModel::TodoModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

TodoModel::~TodoModel()
{
    qDeleteAll(_list);
    _list.clear();
}

int TodoModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _list.empty() ? 0 : _list.count();
}

int TodoModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 6;
}

QHash<int, QByteArray> TodoModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[IdRole] = "id";
    items[SpeakerRole] = "speaker";
    items[CongregationRole] = "congregation";
    items[DateRole] = "date";
    items[ThemeRole] = "theme";
    items[NotesRole] = "notes";
    items[IsIncomingRole] = "isIncoming";
    return items;
}

QVariant TodoModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case IdRole:
        return _list[index.row()]->getId();
    case DateRole:
        return QDateTime(_list[index.row()]->getDate());
    case SpeakerRole:
        return _list[index.row()]->getSpeaker();
    case CongregationRole:
        return _list[index.row()]->getCongregation();
    case ThemeRole:
        return _list[index.row()]->getTheme();
    case NotesRole:
        return _list[index.row()]->getNotes();
    case IsIncomingRole:
        return _list[index.row()]->getIsIncoming();
    default:
        return QVariant();
    }
}

bool TodoModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid()) {
        QDateTime d;
        switch(role)
        {
        case DateRole:
            d = value.toDateTime();            
            _list[index.row()]->setDate(d.offsetFromUtc() < 0 ? d.toUTC().date() : d.date());
            break;
        case SpeakerRole:
            _list[index.row()]->setSpeaker(value.toString());
            break;
        case CongregationRole:
            _list[index.row()]->setCongregation(value.toString());
            break;
        case ThemeRole:
            _list[index.row()]->setTheme(value.toString());
            break;
        case NotesRole:
            _list[index.row()]->setNotes(value.toString());
            break;
        default:
            return false;
        }       
        emit dataChanged(index, index, { role });
        return true;
    }
    return false;
}

void TodoModel::loadList()
{
    beginResetModel();
    qDeleteAll(_list);
    _list.clear();
    _list = todo::getList();
    endResetModel();    
    emit modelChanged();
}

int TodoModel::addRow(const bool inComing)
{
    todo *newTodo = new todo(inComing);    
    newTodo->setDate(QDate::currentDate());
    newTodo->save();
    int newIndex = 0;
    if (inComing) {
        int i = 0;
        for (i = 0; i < _list.size(); i++) {
            if (!_list[i]->getIsIncoming()) {
                newIndex = i;
                break;
            }
        }
    } else {
        newIndex = rowCount();
    }            
    beginInsertRows(QModelIndex(), newIndex, newIndex);
    _list.insert(newIndex, newTodo);
    endInsertRows();
    emit modelChanged();
    return newIndex;
}

void TodoModel::removeRow(const int rowIndex)
{
    beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
    todo *selTodo = _list[rowIndex];
    selTodo->deleteme();
    delete selTodo;
    _list.removeAt(rowIndex);
    endRemoveRows();
    emit modelChanged();
}

void TodoModel::saveRow(const int rowIndex)
{
    _list[rowIndex]->save();
}

void TodoModel::editRow(const int row, const QDate date, const QString speaker, const QString congregation, const QString notes)
{
    beginResetModel();
    _list[row]->setDate(date);
    _list[row]->setSpeaker(speaker);
    _list[row]->setCongregation(congregation);
    _list[row]->setNotes(notes);
    _list[row]->save();
    endResetModel();
    emit modelChanged();
}

QVariantMap TodoModel::get(int row) const
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

QString TodoModel::moveToSchedule(const int row)
{    
    QString returnValue = _list[row]->moveToSchedule();
    if (returnValue.isEmpty()) {
        removeRow(row);
        emit modelChanged();
    }
    return returnValue;
}
