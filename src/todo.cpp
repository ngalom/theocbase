/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "todo.h"

QList<todo*> todo::getList()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QList<todo*> results;
    sql_items items = sql->selectSql("select * from publictalks_todolist where active order by inout, id");
    for(unsigned int i = 0; i < items.size(); i++) {
        sql_item item = items[i];
        todo *t = new todo(item.value("id").toInt(), item.value("inout").toString() == "I");
        t->setId(item.value("id").toInt());
        t->setDate(item.value("date").toDate());
        t->setSpeaker(item.value("speaker").toString());
        t->setCongregation(item.value("congregation").toString());
        t->setTheme(item.value("theme").toString());
        t->setNotes(item.value("notes").toString());
        results.append(t);
    }
    return results;
}

todo::todo(bool isIncoming) :
    id(-1),
    isIncoming(isIncoming)
{
    sql = &Singleton<sql_class>::Instance();
    sql_item s;
    s.insert("inout", isIncoming ? "I" : "O");
    id = sql->insertSql("publictalks_todolist", &s, "id");
}

todo::todo(int id, bool isIncoming) :
    id(id),
    isIncoming(isIncoming),
    _congregationId(0),
    _speaker(nullptr),
    _themeId(0)
{
    sql = &Singleton<sql_class>::Instance();
}

bool todo::getIsIncoming() const
{
    return isIncoming;
}

int todo::getId() const
{
    return id;
}

void todo::setId(int value)
{
    id = value;
}

QDate todo::getDate() const
{
    return _date;
}

void todo::setDate(const QDate &value)
{
    _date = value;
}

QString todo::getSpeaker() const
{
    return speaker;
}

void todo::setSpeaker(const QString &value)
{
    speaker = value;
}

QString todo::getCongregation() const
{
    return congregation;
}

void todo::setCongregation(const QString &value)
{
    congregation = value;
}

QString todo::getTheme() const
{
    return theme;
}

void todo::setTheme(const QString &value)
{
    theme = value;
}

QString todo::getNotes() const
{
    return notes;
}

void todo::setNotes(const QString &value)
{
    notes = value;
}

QDate todo::get_date() const
{
    return _date;
}

int todo::get_congregationId() const
{
    return _congregationId;
}

person *todo::get_speaker()
{
    return _speaker.data();
}

int todo::get_themeId()
{
    return _themeId;
}

void todo::deleteme()
{
    sql_item s;
    s.insert("active",0);
    sql->updateSql("publictalks_todolist","id",QString::number(id),&s);
    id = -1;
}

void todo::save()
{
    sql_item s;
    s.insert("inout", isIncoming ? "I" : "O");
    s.insert("date", _date);
    s.insert("speaker", speaker);
    s.insert("congregation", congregation);
    s.insert("theme", theme);
    s.insert("notes", notes);
    s.insert("active", 1);
    sql->updateSql("publictalks_todolist", "id", QVariant(id).toString(), &s);
}

QString todo::moveToSchedule()
{
    QString errs;
    if (!_date.isValid()) {
        if (!errs.isEmpty())
            errs.append(", ");
        errs.append(QObject::tr("Date", "The todo list date cell is in error"));
    } else {
        // set to Monday
        _date = _date.addDays(1 - _date.dayOfWeek());
    }

    _congregationId = 0;
    ccongregation c;
    if (!congregation.isEmpty()) {
        _congregationId = c.getCongregationIDByPartialName(congregation);
    }

    if (isIncoming)
        _speaker.reset(cpersons::getPerson(speaker, "partial", _congregationId));
    else
        _speaker.reset(cpersons::getPerson(speaker, "partial", 0));
    if (!_speaker) {
        if (!errs.isEmpty())
            errs.append(", ");
        errs.append(QObject::tr("Speaker", "The todo list Speaker cell is in error"));
    } else if (isIncoming){
        _congregationId = _speaker->congregationid();
    }
    if (_congregationId == 0) {
        if (!errs.isEmpty())
            errs.append(", ");
        errs.append(QObject::tr("Congregation", "The todo list Congregation cell is in error"));
    }

    _themeId = 0;
    cpublictalks cp;    
    QStandardItemModel *model = cp.getThemesTable(theme,
                get_speaker(), QDate::currentDate(), false);
    if (model->rowCount() != 1) {
        if (!errs.isEmpty())
            errs.append(", ");
        errs.append(QObject::tr("Theme (could be a theme this speaker does not give)", "The todo list Theme cell is in error"));
    } else {
         _themeId = model->index(0,0).data(0).toInt();
    }

    if (!errs.isEmpty())
        return errs;

    cptmeeting *talk = cp.getMeeting(_date);
    if (isIncoming) {
        // check for existing scheduled talk
        if (talk->speaker()) {
            errs = QObject::tr("Date Already Scheduled");
            return errs;
        }

        talk->setSpeaker(get_speaker());
        talk->theme = cp.getThemeById(_themeId);
    } else {
        // check for existing scheduled talk in same congregation
        QList<cpoutgoing *> outgoing = cp.getOutgoingSpeakers(_date);
        foreach (cpoutgoing *ot, outgoing) {
            if (ot->getCongregation().id == _congregationId) {
                errs = QObject::tr("Date Already Scheduled");
                return errs;
            }
        }

        cp.addOutgoingSpeaker(_date, _speaker->id(), _themeId, _congregationId);
    }
    talk->save();
    deleteme();
    return "";
}
