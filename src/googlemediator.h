#ifndef GOOGLEMEDIATOR_H
#define GOOGLEMEDIATOR_H

#include <QObject>
#include <QDialog>
#include <QHBoxLayout>
#include <QWebEngineView>
#include <QNetworkAccessManager>
#include <QOAuth2AuthorizationCodeFlow>
#include <QOAuthHttpServerReplyHandler>
#include <QNetworkReply>
#include <QSettings>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDesktopServices>
#include <QTimer>
#include "simplecrypt.h"

class googleMediator : public QObject
{
    Q_OBJECT

public:

    enum Tasks {
        AquireToken,
        Get,
        Post,
        PostWithJsonWrapper
    };

    enum States {        
        State_OK,
        State_MissingClientID,
        State_MissingClientSecret,
        State_Authorizing,
        State_AuthorizationFailed,
        State_NeedCode,
        State_NeedTokenRefresh,
        State_ReplyError,
        State_JsonError
    };

    enum Scopes {
        Profile,
        GmailReadonly,
        GmailCompose,
        GmailModify
    };


    googleMediator(QObject *parent, QString ClientID, QString ClientSecret);
    ~googleMediator();
    States GetUserAuthorization();
    void setRefreshToken(QString token);
    QString getRefreshToken();
    States SendRequest(Tasks Task, QString Url, QByteArray *PostData);
    States getState();
    QString getStateString();
    bool getCurrentJsonValid();
    QJsonObject *getCurrentJson();
    QString getCurrentNetworkReplyError();
    QString getCurrentJsonErr();

signals:
    void statusChanged(QAbstractOAuth::Status status);

private:
    States _SendRequest(Tasks Task, QString Url, QByteArray *PostData);
    void setState();

    QOAuth2AuthorizationCodeFlow *googleAuth;
    QNetworkAccessManager manager;
    QNetworkReply *networkReply;
    States state;
    QString clientID;
    QString clientSecret;
    QString refreshToken;
    QString accessToken;
    QDateTime tokenExpiration;
    QString currentNetworkReplyErr;
    QJsonParseError currentJsonErr;
    QJsonDocument currentJsonDoc;
    bool currentJsonValid;
    QJsonObject currentJson;
};

#endif // GOOGLEMEDIATOR_H
