#include "lmmworksheetregex.h"
#include "ui_lmmworksheetregex.h"
#include "QDebug"

lmmWorksheetRegEx::lmmWorksheetRegEx(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::lmmWorksheetRegEx),
    import(nullptr)
{
    ui->setupUi(this);
    sql = &Singleton<sql_class>::Instance();
    okFont = ui->lblParts->font();
    errFont = okFont;
    errFont.setBold(true);
}

lmmWorksheetRegEx::~lmmWorksheetRegEx()
{
    delete ui;
    if (import)
        delete import;
}

void lmmWorksheetRegEx::on_txtEPUB_editingFinished()
{
    qDebug() << "importEPUB";

    if (import)
        delete import;
    import = new importlmmworkbook(ui->txtEPUB->text().replace("\\", "/"), ui->ckAddPDFLanguage->isChecked());

    if (import->prepared)
        importEPUB(true);
    // else show error somewhere
}

void lmmWorksheetRegEx::importEPUB(bool loadRegEx)
{
    if (!loadRegEx)
        copyRegexToImport();

    tocModel = new QStringListModel(this);
    QString results = import->Import();
    if (!results.isEmpty()) {
        ui->lblParts->setFont(errFont);
        ui->lblParts->setStyleSheet("color:rgb(128, 0, 0)");
        ui->lblParts->setText("Parts (" + results + ")");
    } else {
        ui->lblParts->setFont(okFont);
        ui->lblParts->setStyleSheet("");
        ui->lblParts->setText("Parts");
    }
    QStringList sl(import->tocEntries);
    tocModel->setStringList(sl);
    ui->lstTOC->setModel(tocModel);

    if (loadRegEx) {
        ui->lblLanguage->setText("Language: " + import->epb.language);
        ui->rxSong->setText(import->regexes["song"]);
        ui->rxDate1->setText(import->regexes["date1"]);
        ui->rxDate2->setText(import->regexes["date2"]);
        ui->rxTiming->setText(import->regexes["timing"]);
        ui->txtA5->setText(import->regexes["study"]);
        if (import->regexes.contains("assignment1")) {
            ui->txtA1->setText(import->regexes["assignment1"]);
        } else {
            ui->txtA1->setText("");
        }
        if (import->regexes.contains("assignment2")) {
            ui->txtA2->setText(import->regexes["assignment2"]);
        } else {
            ui->txtA2->setText("");
        }
        if (import->regexes.contains("assignment3")) {
            ui->txtA3->setText(import->regexes["assignment3"]);
        } else {
            ui->txtA3->setText("");
        }
        if (import->regexes.contains("assignment4")) {
            ui->txtA4->setText(import->regexes["assignment4"]);
        } else {
            ui->txtA4->setText("");
        }
    }
    qDebug() << "importEPUB Finished";
}

void lmmWorksheetRegEx::copyRegexToImport()
{
    qDebug() << "copyRegexToImport";
    import->regexes.clear();
    import->meetingResults.clear();

    import->regexes.insert("song", ui->rxSong->text());
    import->regexes.insert("date1", ui->rxDate1->text());
    import->regexes.insert("date2", ui->rxDate2->text());
    import->regexes.insert("timing", ui->rxTiming->text());
    import->regexes.insert("study", ui->txtA5->text());
    if (!ui->txtA1->text().isEmpty()) {
        import->regexes.insert("assignment1", ui->txtA1->text());
    }
    if (!ui->txtA2->text().isEmpty()) {
        import->regexes.insert("assignment2", ui->txtA2->text());
    }
    if (!ui->txtA3->text().isEmpty()) {
        import->regexes.insert("assignment3", ui->txtA3->text());
    }
    if (!ui->txtA4->text().isEmpty()) {
        import->regexes.insert("assignment4", ui->txtA4->text());
    }
    qDebug() << "copyRegexToImport";
}

void lmmWorksheetRegEx::on_lstTOC_clicked(const QModelIndex &index)
{
    int idx(index.row());
    if (index.row()>-1 && idx < import->tocEntries.length()) {
        if (QGuiApplication::keyboardModifiers() & Qt::ControlModifier) {
            QString url(import->epb.oebpsPath + "/" + import->tocEntriesHTML[idx]);
            QDesktopServices::openUrl(url);
        } else {
            QClipboard *clipboard = QApplication::clipboard();
            clipboard->setText(import->tocEntries[idx]);
            ImportDate(false);
        }
    }
}

void lmmWorksheetRegEx::on_lstParts_clicked(const QModelIndex &index)
{
    int idx(index.row());
    if (index.row()>-1 && idx < import->meetingResults.length()) {
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(import->meetingResults[idx]);
    }
}


void lmmWorksheetRegEx::ImportDate(bool reloadRex)
{
    if (reloadRex)
        copyRegexToImport();
    int idx(ui->lstTOC->currentIndex().row());
    if (idx > -1 && idx < import->tocEntriesHTML.length()) {
        QDate* dt(import->tocEntriesDate[idx]);
        if (dt) {
            QString href(import->tocEntriesHTML[idx]);
            import->ImportDate(*import->tocEntriesDate[idx], href, nullptr);

            mtgModel = new QStringListModel(this);
            QStringList sl(import->meetingResults);
            ui->lblParts->setText("Parts (" + QVariant(import->meetingResults.length()).toString() + " found)");
            mtgModel->setStringList(sl);
            ui->lstParts->setModel(mtgModel);
        }
    }
}

void lmmWorksheetRegEx::on_ckAddPDFLanguage_clicked()
{
    importEPUB(true);
}

void lmmWorksheetRegEx::on_rxDate1_editingFinished()
{
    importEPUB();
}

void lmmWorksheetRegEx::on_rxDate2_editingFinished()
{
    importEPUB();
}

void lmmWorksheetRegEx::on_rxSong_editingFinished()
{
    importEPUB();
}
void lmmWorksheetRegEx::on_rxTiming_editingFinished()
{
    ImportDate(true);
}

void lmmWorksheetRegEx::on_txtA1_editingFinished()
{
    ImportDate(true);
}


void lmmWorksheetRegEx::on_txtA2_editingFinished()
{
    ImportDate(true);
}

void lmmWorksheetRegEx::on_txtA3_editingFinished()
{
    ImportDate(true);
}

void lmmWorksheetRegEx::on_txtA4_editingFinished()
{
    ImportDate(true);
}

void lmmWorksheetRegEx::on_txtA5_editingFinished()
{
    ImportDate(true);
}


void lmmWorksheetRegEx::on_btnToSQL_clicked()
{    
    QString clip;
    addToClipboardAndRun(&clip, "delete from lmm_workbookregex where lang = '" + import->epb.language + "'");
    addToClipboardAndRun(&clip, "insert into lmm_workbookregex (lang, key, value) select '" + import->epb.language + "', 'date1', '" + sql->EscapeQuotes(ui->rxDate1->text()) + "'");
    if (!ui->rxDate2->text().isEmpty())
        addToClipboardAndRun(&clip, "insert into lmm_workbookregex (lang, key, value) select '" + import->epb.language + "', 'date2', '" + sql->EscapeQuotes(ui->rxDate2->text()) + "'");
    addToClipboardAndRun(&clip, "insert into lmm_workbookregex (lang, key, value) select '" + import->epb.language + "', 'song', '" + sql->EscapeQuotes(ui->rxSong->text()) + "'");
    addToClipboardAndRun(&clip, "insert into lmm_workbookregex (lang, key, value) select '" + import->epb.language + "', 'timing', '" + sql->EscapeQuotes(ui->rxTiming->text()) + "'");
    addToClipboardAndRun(&clip, "insert into lmm_workbookregex (lang, key, value) select '" + import->epb.language + "', 'study', '" + sql->EscapeQuotes(ui->txtA5->text()) + "'");
    if (import->regexes.contains("assignment1"))
        addToClipboardAndRun(&clip, "insert into lmm_workbookregex (lang, key, value) select '" + import->epb.language + "', 'assignment1', '" + sql->EscapeQuotes(import->regexes["assignment1"]) + "'");
    if (import->regexes.contains("assignment2"))
        addToClipboardAndRun(&clip, "insert into lmm_workbookregex (lang, key, value) select '" + import->epb.language + "', 'assignment2', '" + sql->EscapeQuotes(import->regexes["assignment2"]) + "'");
    if (import->regexes.contains("assignment3"))
        addToClipboardAndRun(&clip, "insert into lmm_workbookregex (lang, key, value) select '" + import->epb.language + "', 'assignment3', '" + sql->EscapeQuotes(import->regexes["assignment3"]) + "'");
    if (import->regexes.contains("assignment4"))
        addToClipboardAndRun(&clip, "insert into lmm_workbookregex (lang, key, value) select '" + import->epb.language + "', 'assignment4', '" + sql->EscapeQuotes(import->regexes["assignment4"]) + "'");

    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(clip);
}

void lmmWorksheetRegEx::addToClipboardAndRun(QString *clipboard, QString cmd) {
    clipboard->append(cmd);
    clipboard->append("\r\n");
    sql->execSql(cmd);
}
