import QtQuick 2.0
import QtQuick.Layouts 1.1

Item {
    id: validationTextInput

    property bool isUntouched: false
    property bool isTextValid: true
    property bool isReadOnly: false
    property string placeholderText: qsTr("Text")
    property alias textInput: textInput
    property alias text: textInput.text
    property alias validator: textInput.validator
    property alias color: textInput.color
    property alias horizontalAlignment: textInput.horizontalAlignment

    signal selectedRowChanged(var row)

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    Rectangle {
        id: invalidTextRect

        anchors.fill: parent
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHLeft | Qt.AlignVCenter

        border.width: 1
        border.color: "red"
        opacity: 1
        color: "transparent"

        states: [
            State {
                name: "Untouched"; when: isUntouched
                PropertyChanges { target: invalidTextRect; opacity: 0 }
            },
            State {
                name: "ValidText"; when: !isUntouched && isTextValid
                PropertyChanges { target: invalidTextRect; opacity: 0 }
            },
            State {
                name: "InvalidText"; when: !isUntouched && !isTextValid
                PropertyChanges { target: invalidTextRect; opacity: 1 }
            }]

            transitions:[
                Transition {
                    from: "ValidText"
                    to: "InvalidText"
                    animations: invalidDataAnimation
                },
                Transition {
                    from: "Untouched"
                    to: "InvalidText"
                    animations: invalidDataAnimation
                }]
    }

    Text {
        id: placeholderTextBox
        anchors.fill: parent
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter

        color: myPalette.mid
        renderType: Text.NativeRendering
        text: placeholderText
        elide: Text.ElideRight

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                isEditing = true
                if (!styleData.selected)
                    selectedRowChanged(styleData.row)
            }
        }
    }

    TextInput {
        id: textInput

        anchors.fill: parent
        layer.enabled: true
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHLeft | Qt.AlignVCenter
        verticalAlignment: TextInput.AlignVCenter
        horizontalAlignment: TextInput.AlignLeft
        anchors.leftMargin: 5

        readOnly: isReadOnly
        renderType: Text.NativeRendering
        selectByMouse: true

        onTextChanged: {
            placeholderTextBox.text = text === "" ? placeholderText : "";
        }

        onActiveFocusChanged: {
            if (!activeFocus)
                parent.isUntouched = false;
            else
                if (!styleData.selected)
                    selectedRowChanged(styleData.row)
        }
    }
}
