import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Window 2.1
import QtQml.Models 2.2
import QtQuick.Dialogs 1.2
import QtPositioning 5.8
import QtLocation 5.8

Item {
    id: item1
    property alias okButton: okButton
    property alias cancelButton: cancelButton
    property alias searchButton: searchButton
    property alias locationsModel: locationsModel
    property alias searchAddress: searchAddress
    property alias locationsTableView: locationsTableView
    property alias countryTextField: countryTextField
    property alias stateTextField: stateTextField
    property alias cityTextField: cityTextField
    property alias districtTextField: districtTextField
    property alias streetTextField: streetTextField
    property alias postalCodeTextField: postalCodeTextField
    property alias houseNumberTextField: houseNumberTextField
    property bool isEditAddressMode: false

    anchors.fill: parent

    ListModel {
        id: locationsModel
    }

    Address {
        id: searchAddress
        country: countryTextField.text
        state: stateTextField.text
        city: cityTextField.text
        district: districtTextField.text
        street: isEditAddressMode ? houseNumberTextField.text + " " + streetTextField.text : streetTextField.text
        postalCode: postalCodeTextField.text
    }

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        spacing: 10

        GroupBox {
            id: groupBox
            title: qsTr("Address")

            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            GridLayout {
                id: gridLayout1
                Layout.fillHeight: true
                Layout.fillWidth: true
                columnSpacing: 10
                rowSpacing: 10
                anchors.rightMargin: 5
                anchors.leftMargin: 5
                anchors.bottomMargin: 5
                anchors.topMargin: 10
                anchors.fill: parent
                rows: 5
                columns: 4

                Label {
                    id: countryLabel
                    text: qsTr("Country:")
                }

                TextField {
                    id: countryTextField
                    Layout.fillWidth: true
                }

                Label {
                    id: stateLabel
                    text: qsTr("State:", "Administrative area level 1")
                }

                TextField {
                    id: stateTextField
                    Layout.fillWidth: true
                }

                Label {
                    id: cityLabel
                    text: qsTr("City:", "Locality")
                }

                TextField {
                    id: cityTextField
                    Layout.fillWidth: true
                }

                Label {
                    id: districtLabel
                    text: qsTr("District:", "Sublocality, first-order civil entity below a locality")
                }

                TextField {
                    id: districtTextField
                    Layout.fillWidth: true
                }

                Label {
                    id: streetLabel
                    text: qsTr("Street:")
                }

                TextField {
                    id: streetTextField
                    Layout.fillWidth: true
                }

                Label {
                    id: postalCodeLabel
                    text: qsTr("Postalcode:")
                }

                TextField {
                    id: postalCodeTextField
                    Layout.fillWidth: true
                }

                Label {
                    id: houseNumberLabel
                    text: qsTr("No.:")
                    visible:isEditAddressMode
                }

                TextField {
                    id: houseNumberTextField
                    Layout.fillWidth: true
                    visible:isEditAddressMode
                }

                Button {
                    id: searchButton
                    text: qsTr("Search", "Search address")
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.columnSpan: 4
                }

                TableView {
                    id: locationsTableView
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Layout.columnSpan: 4

                    model: locationsModel

                    TableViewColumn {
                        title: qsTr("Country", "Short name of country")
                        width: 60
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.country
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    TableViewColumn {
                        title: qsTr("State",
                                    "Short name of administrative area level 1")
                        width: 60
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.state
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    TableViewColumn {
                        title: qsTr("County",
                                    "Name of administrative area level 2")
                        width: 100
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.county
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    TableViewColumn {
                        title: qsTr("City", "Locality")
                        width: 130
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.city
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    TableViewColumn {
                        title: qsTr("District", "Sublocality, first-order civil entity below a locality")
                        width: 120
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.district
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    TableViewColumn {
                        title: qsTr("Street", "Streetname")
                        width: 230
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.street
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    TableViewColumn {
                        title: qsTr("No.", "House or street number")
                        width: 50
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.houseNumber
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    TableViewColumn {
                        title: qsTr("Postal code", "Mail code, ZIP")
                        width: 80
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.postalCode
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    TableViewColumn {
                        title: qsTr("Latitude")
                        width: 80
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.latitude
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    TableViewColumn {
                        title: qsTr("Longitude")
                        width: 80
                        visible: true

                        delegate: Item {
                            Text {
                                anchors.left: parent.left
                                width: parent.width
                                text: model.longitude
                                renderType: Text.NativeRendering
                                elide: Text.ElideRight
                                color: myPalette.text
                            }
                        }
                    }

                    rowDelegate: Rectangle {
                        id: tableViewRowDelegate
                        height: 25
                        color: styleData.selected ? myPalette.highlight : (styleData.alternate ? myPalette.alternateBase : myPalette.base)
                    }
                }
            }
        }

        RowLayout {
            id: rowLayout
            height: 33
            Layout.alignment: Qt.AlignRight | Qt.AlignBottom

            Button {
                id: okButton
                text: qsTr("OK")
            }

            Button {
                id: cancelButton
                text: qsTr("Cancel")
            }
        }
    }
}
