/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.1
import QtQuick.Window 2.1
import QtQuick.Dialogs 1.1
import net.theocbase 1.0

Item {
    id: studentAssignmentDialog
    property string title: "Student Assignment"
    width: 300
    height: 550
    property string returnValue: ""
    property LMM_Assignment currentAssignment

    function saveStudy()
    {
        if (currentAssignment.speaker){
            if (currentAssignment.volunteer) {
                myController.saveStudy(checkExerice.checked,
                                       false, comboCurrentStudy.currentIndex)
            } else {
                var currentstudy = myController.currentStudy
                if (currentstudy && currentstudy.id < 1){
                    currentstudy.number = comboCurrentStudy.currentIndex
                }
                myController.saveStudy(checkExerice.checked,
                                       checkCompleted.checked,
                                       comboNextStudy.currentIndex)
            }
        }
    }

    AssignmentController { id: myController }

    onCurrentAssignmentChanged: {
        if (!currentAssignment) return
        myController.assignment = currentAssignment
        if (!currentAssignment.volunteer)
            comboVolunteer.currentIndex = -1        
    }
    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        Label {
            text: qsTr("Theme")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            id: labelTheme
            text: currentAssignment ? currentAssignment.theme : ""
            topPadding: 8
            font.pointSize: 12
            wrapMode: Text.WordWrap
            Layout.fillWidth: true
            font.bold: true
        }

        Label {
            text: qsTr("Source")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            Layout.fillWidth: true
            height: 100
            id: textArea
            text: currentAssignment ? currentAssignment.source : ""
            font.pointSize: 11
            readOnly: true
            font.italic: false
            wrapMode: Text.WordWrap
        }

        Label {
            text: qsTr("Student")
            font.capitalization: Font.AllUppercase
        }
        ComboBoxTable {
            currentText: currentAssignment && currentAssignment.speaker ? currentAssignment.speaker.fullname : ""
            Layout.fillWidth: true
            enabled: !checkCompleted.checked
            column3.title: labelTheme.text //qsTr("Selected", "Dropdown column title")
            column4.title: qsTr("All", "Dropdown column title")
            onBeforeMenuShown: {
                //                        if (typeof model === "undefined")
                model = currentAssignment.getSpeakerList()
                rowTooltip = "?"
                column2.resizeToContents()
            }
            onRowSelected: {
                currentAssignment.speaker = id < 1 ? null : myController.getPublisherById(id)
                currentAssignment.save()
            }
            onCurrentTextChanged: {
                if (currentText.length > 2 && currentAssignment.date.getFullYear() < 2019){
                    // get studies
                    comboCurrentStudy.model = myController.getStudies()
                    comboNextStudy.model = myController.getStudies()
                    // get active study
                    console.log(currentAssignment.speaker ? currentAssignment.speaker.fullname : "No speaker")
                    var study = myController.currentStudy
                    console.log("active study " + study.name + " " + study.number + " id:" + study.id)
                    comboCurrentStudy.currentIndex = study.number
                    comboCurrentStudy.enabled = (study.id < 1)
                    checkExerice.checked = study.exercise
                    console.log("Next study " + myController.nextStudy)
                    comboNextStudy.currentIndex = currentAssignment.completed ? myController.nextStudy : study.number
                }
            }
            onTooltipRequest: {
                if (model.get(row).id === "undefined")
                    return;
                var userid = model.get(row).id
                rowTooltip = myController.getHistoryTooltip(userid)
            }
        }

        Label {
            text: qsTr("Assistant")
            visible: currentAssignment && currentAssignment.canHaveAssistant
            font.capitalization: Font.AllUppercase
        }
        ComboBoxTable {
            id: comboAssistant
            currentText: currentAssignment && currentAssignment.assistant ? currentAssignment.assistant.fullname : ""
            Layout.fillWidth: true
            visible: currentAssignment && currentAssignment.canHaveAssistant
            enabled: currentAssignment && currentAssignment.speaker  && !checkCompleted.checked
            column3.title: qsTr("With Student", "Dropdown column title")
            column4.title: qsTr("All", "Dropdown column title")
            onBeforeMenuShown: {
                //if (typeof model === "undefined")
                model = currentAssignment.getAssistantList()
                rowTooltip = "?"
                column2.resizeToContents()
            }
            onRowSelected: {
                currentAssignment.assistant = id < 1 ? null : myController.getPublisherById(id)
                if (id > 0 && currentAssignment.assistant.gender !== currentAssignment.speaker.gender &&
                        (currentAssignment.talkId === LMM_Schedule.TalkType_ReturnVisit1 ||
                         currentAssignment.talkId === LMM_Schedule.TalkType_ReturnVisit2 ||
                         currentAssignment.talkId === LMM_Schedule.TalkType_ReturnVisit3 ||
                         currentAssignment.talkId === LMM_Schedule.TalkType_BibleStudy)){
                    message.text = qsTr('The assistant should not be someone of the opposite sex.')
                    message.visible = true
                } else {
                    currentAssignment.save()
                }
            }
            onTooltipRequest:  {
                if (model.get(row).id === "undefined")
                    return;
                var userid = model.get(row).id
                rowTooltip = myController.getHistoryTooltip(userid)
            }

            MessageDialog {
                id: message
                standardButtons: StandardButton.Ok
                icon: StandardIcon.Warning
                modality: Qt.ApplicationModal
            }
        }

        Label {
            text: qsTr("Study point")
            visible: currentAssignment.date.getFullYear() >= 2019
            font.capitalization: Font.AllUppercase
        }
        TextField {
            Layout.fillWidth: true
            background.width: width
            text: currentAssignment.studyNumber + " " + currentAssignment.studyName
            visible: currentAssignment.date.getFullYear() >= 2019
            enabled: false
        }

        // result
        GridLayout {            
            columns: 2            

            Label {
                text: qsTr("Result")
                font.capitalization: Font.AllUppercase
            }

            CheckBox {
                id: checkCompleted
                text: qsTr("Completed")
                Layout.columnSpan: 2
                enabled: currentAssignment && currentAssignment.speaker
                checked: currentAssignment && currentAssignment.completed ? true : false
                onClicked: {
                    if (currentAssignment.completed !== checked) {
                        currentAssignment.completed = checked
                        currentAssignment.save()
                         if (currentAssignment.date.getFullYear < 2019)
                             saveStudy()
                    }
                    comboVolunteer.currentIndex = -1
                }
            }

            CheckBox {
                id: checkVolunteer
                text: qsTr("Volunteer")
                visible: checkCompleted.checked
                checked: currentAssignment && currentAssignment.volunteer ? true : false
                onCheckedChanged: {
                    if (!checked){
                        comboVolunteer.currentIndex = -1
                        currentAssignment.volunteer = null
                        currentAssignment.save()
                    }else{
                        comboVolunteer.currentIndex = currentAssignment.volunteer ?
                                    comboVolunteer.model.find(currentAssignment.volunteer.fullname,2) : -1
                        if (currentAssignment.date.getFullYear() < 2019)
                            comboNextStudy.currentIndex = comboCurrentStudy.currentIndex
                    }
                }
            }
            ComboBox {
                id: comboVolunteer
                Layout.fillWidth: true
                visible: checkCompleted.checked
                enabled: checkVolunteer.checked
                displayText: currentAssignment && currentAssignment.volunteer ? currentAssignment.volunteer.fullname : ""
                textRole: "name"
                model: currentAssignment ? currentAssignment.getSpeakerList() : null
                onActivated: {
                    var id = model.get(index).id
                    if (typeof id === "undefined")
                        currentIndex = -1
                    currentAssignment.volunteer = id < 1 ? null : myController.getPublisherById(id)
                    currentAssignment.save()
                    if (currentAssignment.date.getFullYear() < 2019)
                        saveStudy()                    
                }
            }

            Label { text: qsTr("Timing") }
            TextField {
                //placeholderText: "Timing"
                id: textfieldTiming
                Layout.fillWidth: true
                background.width: width
                enabled: checkCompleted.checked
                text: currentAssignment ? currentAssignment.timing : ""
                onEditingFinished: {
                    if (currentAssignment.timing !== text) {
                        currentAssignment.timing = text
                        currentAssignment.save()
                    }
                }
            }

            Label {
                text: qsTr("Current Study")
                wrapMode: Text.WordWrap
                elide: Text.ElideRight
                Layout.maximumWidth: 120
                visible: currentAssignment.date.getFullYear() < 2019
            }
            ComboBox {
                id: comboCurrentStudy
                Layout.fillWidth: true
                enabled: false
                model: ListModel { ListElement { text: "" }}
                visible: currentAssignment.date.getFullYear() < 2019
            }

            CheckBox {
                id: checkExerice
                text: qsTr("Exercise Completed")
                Layout.columnSpan: 2
                enabled: currentAssignment && currentAssignment.completed && !currentAssignment.volunteer
                visible: currentAssignment.date.getFullYear() < 2019
                onClicked: {
                    saveStudy()
                }
            }

            Label {
                text: qsTr("Next Study")
                visible: comboNextStudy.visible && currentAssignment.date.getFullYear < 2019
                elide: Text.ElideRight
                wrapMode: Text.WordWrap
                Layout.maximumWidth: 120

            }
            ComboBox {
                id: comboNextStudy
                visible: checkCompleted.checked && currentAssignment.date.getFullYear < 2019
                enabled: checkExerice.checked && !currentAssignment.volunteer
                Layout.fillWidth: true
                model: ListModel { ListElement { text: "" }}
                onActivated: {
                    comboNextStudy.currentIndex = index
                    saveStudy()
                }
            }
        }

        Label {
            text: qsTr("Note")
            font.capitalization: Font.AllUppercase
        }
        ScrollView {
            Layout.fillWidth: true
            Layout.preferredHeight: 100
            clip: true

            TextArea{
                id: texteditNote
                wrapMode: Text.WordWrap
                selectByMouse: true
                text: currentAssignment ? currentAssignment.note : ""
                font.pointSize: 11
                onEditingFinished: {
                    if (currentAssignment.note != text)
                        currentAssignment.note = text
                        currentAssignment.save()
                }
            }
        }
        Item { Layout.fillHeight: true }
    }
}

