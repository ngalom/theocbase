/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

Item {
    id: outgoingSpeakerEdit
    anchors.fill: parent

    property string title: "Outgoing Speaker Edit"
    property OutgoingSpeakersModel outModel
    property int modelIndex: -1
    property int speakerId: -1
    property int themeId: -1
    property int congId: -1
    width: 500
    height: 700

    onOutModelChanged: {
        if (modelIndex >= 0) {
            speakerId = outModel.get(modelIndex).speakerId
            congId = outModel.get(modelIndex).congregationId
            themeId = outModel.get(modelIndex).themeId
        }
    }

    onModelIndexChanged: {
        if (outModel == null)
            return
        speakerId = outModel.get(modelIndex).speakerId
        congId = outModel.get(modelIndex).congregationId
        themeId = outModel.get(modelIndex).themeId
        }

    PublicMeetingController { id: controller }

    function saveChanges() {
        if (modelIndex < 0) {
            console.error("model index is negative!!!")
            return
        }
        outModel.editRow(modelIndex, speakerId, themeId, congId)
    }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 10

        Label {
            text: qsTr("Speaker")
            font.capitalization: Font.AllUppercase
        }
        ComboBoxTable {
            id: comboSpeaker
            Layout.fillWidth: true
            column2.width: comboSpeaker.width - 30 // name
            column3.visible: false // date
            column4.visible: false // ?
            column5.width: 30      // availablity icon
            currentText: speakerId > 0 ? CPersons.getPerson(speakerId).fullname : ""

            onBeforeMenuShown: {
                model = controller.speakerListLocal()
            }
            onRowSelected: {
                console.log(id)
                speakerId = id
                saveChanges()
            }
        }

        // Theme
        Label {
            text: qsTr("Theme")
            font.capitalization: Font.AllUppercase
        }

        ComboBoxTable {
            id: comboTheme
            Layout.fillWidth: true
            currentText: (outModel.get(modelIndex).themeNo > 0 ? outModel.get(modelIndex).themeNo + " " : "") +
                         outModel.get(modelIndex).theme
            column2.width: comboTheme.width // theme number and title
            column3.visible: false // date
            column4.visible: false
            column5.visible: false
            enabled: comboSpeaker.currentText != ""

            onBeforeMenuShown: {
                model = controller.themeList(speakerId)
            }
            onRowSelected: {
                console.log(id)
                themeId = id
                saveChanges()
            }
        }

        // Congregations
        Label {
            text: qsTr("Congregation")
            font.capitalization: Font.AllUppercase
        }
        ComboBoxTable {
            id: comboCongregation
            Layout.fillWidth: true
            column2.width: comboCongregation.width // congregation name
            column3.visible: false
            column4.visible: false
            column5.visible: false
            currentText: congId > 0 ? outModel.get(modelIndex).congregation : ""

            onBeforeMenuShown: {
                model = controller.congregationList()
            }
            onRowSelected: {
                console.log(id)
                congId = id
                saveChanges()
                // change congId to refresh congregation info
                congId = -1
                congId = id
            }
        }

        Label {
            text: qsTr("Info")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            Layout.fillWidth: true
            selectByMouse: true
            wrapMode: Text.WordWrap
            readOnly: true
            text: congId > 0 ? outModel.get(modelIndex).congregationInfo : ""
        }

        Label {
            text: qsTr("Address")
            font.capitalization: Font.AllUppercase
        }
        TextArea{
            id: textAddress
            Layout.fillWidth: true
            selectByMouse: true
            wrapMode: Text.WordWrap
            readOnly: true
            text: congId > 0 ? outModel.get(modelIndex).congregationAddress : ""
        }

        Label {
            text: qsTr("Meeting day and time")
            font.capitalization: Font.AllUppercase
        }

        RowLayout {
            TextField {
                text: congId > 0 ? Qt.locale().dayName(outModel.get(modelIndex).date.getDay(), Locale.LongFormat) : ""
                Layout.fillWidth: true
            }
            TextField {
                text: congId > 0 ? outModel.get(modelIndex).time : ""
                Layout.fillWidth: true
            }
        }

        Item {
            Layout.fillHeight: true
        }
    }
}

