/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.1
import QtQuick.Window 2.1
import net.theocbase 1.0

Item {
    id: assignmentDialog
    property string title: "Assignment Dialog"
    width: 300
    height: 400
    property string returnValue: ""
    property LMM_Assignment currentAssignment

    AssignmentController { id: myController }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 10
        Label {
            text: qsTr("Theme")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            id: textTheme
            text: currentAssignment ? currentAssignment.theme : ""
            wrapMode: Text.WordWrap
            Layout.fillWidth: true
            font.bold: true
            readOnly: !(currentAssignment && currentAssignment.talkId == LMM_Schedule.TalkType_COTalk)
            onEditingFinished: {
                // save theme if CO service talk
                if (currentAssignment.talkId === LMM_Schedule.TalkType_COTalk && currentAssignment.theme != text) {
                    currentAssignment.theme = text
                    currentAssignment.save()
                }
            }
        }

        Label {
            text: qsTr("Source")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            Layout.fillWidth: true
            height: 100
            id: textareaSource
            text: currentAssignment ? currentAssignment.source : ""
            font.pointSize: 11
            readOnly: true
            font.italic: false
            wrapMode: Text.WordWrap
        }

        Label {
            text: currentAssignment && currentAssignment.talkId === 1 ? qsTr("Chairman") : qsTr("Speaker")
            font.capitalization: Font.AllUppercase
        }
        ComboBoxTable {
            id: comboSpeaker
            currentText: currentAssignment ? currentAssignment.speakerFullName : ""
            Layout.fillWidth: true
            enabled: currentAssignment &&
                     currentAssignment.talkId != LMM_Schedule.TalkType_COTalk &&
                     currentAssignment.talkId != LMM_Schedule.TalkType_SampleConversationVideo &&
                     currentAssignment.talkId != LMM_Schedule.TalkType_ApplyYourself
            column3.title: qsTr("Selected", "Dropdown column title")
            column4.title: qsTr("All", "Dropdown column title")
            onBeforeMenuShown: {
                //                      if (typeof model === "undefined")
                model = currentAssignment.getSpeakerList()
                rowTooltip: "?"
                column2.resizeToContents()
            }
            onRowSelected: {
                currentAssignment.speaker = id < 1 ? null : myController.getPublisherById(id)
                currentAssignment.save()
            }
            onTooltipRequest: {
                if (model.get(row).id === "undefined")
                    return;
                var userid = model.get(row).id
                rowTooltip = myController.getHistoryTooltip(userid)
            }
        }
        Label {
            text: qsTr("Note")
            font.capitalization: Font.AllUppercase
        }

        ScrollView {
            id: view
            Layout.fillWidth: true
            Layout.preferredHeight: 100
            clip: true

            TextArea{
                id: texteditNote
                text: currentAssignment ? currentAssignment.note : ""
                font.pointSize: 11
                selectByMouse: true
                wrapMode: Text.WordWrap       
                onEditingFinished: {
                    if (currentAssignment.note != text) {
                        currentAssignment.note = text
                        currentAssignment.save()
                    }
                }
            }
        }

        Item { Layout.fillHeight: true }
    }
}
