import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import net.theocbase 1.0

Rectangle {
    y: 300
    height: 300
    Layout.fillWidth: true
    property date currentdate
    property LMM_Assignment currentAssignment
    property int talkid
    property int classnumber
    LMM_Meeting{ id: myMeeting }
    onCurrentdateChanged: {
       myMeeting.loadMeeting(currentdate)
    }
    onTalkidChanged: {
        currentAssignment = myMeeting.getAssignment(talkid,classnumber)
    }

    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            Text { text: talkid }
            Text { text: classnumber }
            Button {
                text: "Speakers"
                enabled: talkid > 0
                onClicked: {
                    if (currentAssignment === null) return
                    tableView.model = currentAssignment.getSpeakerList()
                }
            }
            Button {
                text: "Assistants"
                enabled: (talkid === LMM_Schedule.TalkType_InitialCall || talkid === LMM_Schedule.TalkType_ReturnVisit || talkid === LMM_Schedule.TalkType_BibleStudyOrTalk)
                onClicked: {
                    if (currentAssignment === null) return
                    tableView.model = currentAssignment.getAssistantList()
                }
            }
            Button {
                text: "CBS Readers"
                enabled: talkid === LMM_Schedule.TalkType_CBS
                onClicked: {
                    if (currentAssignment === null) return
                    tableView.model = currentAssignment.getReaderList()
                }
            }
            Button {
                text: "Chairmans"
                onClicked: {
                    tableView.model = myMeeting.getChairmanList()
                }
            }
            Button {
                text: "Prayers"
                onClicked: {
                    tableView.model = myMeeting.getPrayerList()
                }
            }
        }


        TableView {
            id: tableView
            Layout.fillWidth: true
            Layout.fillHeight: true
            focus: true
            sortIndicatorVisible: true

            onSortIndicatorColumnChanged: model.sort(sortIndicatorColumn, sortIndicatorOrder)
            onSortIndicatorOrderChanged: model.sort(sortIndicatorColumn, sortIndicatorOrder)

            selectionMode: SelectionMode.SingleSelection

            rowDelegate: Rectangle {
                height: 30
                color: styleData.selected ? "#116CD6" : (styleData.alternate? "#eee" : "#fff")
            }
            itemDelegate: Text {
                anchors.fill: parent
                anchors.leftMargin: 5
                verticalAlignment: Text.AlignVCenter
                color: styleData.selected ? "white" : styleData.textColor
                text: styleData.value ? styleData.value : ""
            }

            TableViewColumn {
                role: "id"
                title: "ID"
                width: 10
                visible: false
            }

            TableViewColumn {
                role: "name"
                title: "Name"
                width: 100
            }
            TableViewColumn {
                role: "date"
                title: "Date"
                width: 70
            }
            TableViewColumn {
                role: "date2"
                title: "Date2"
                width: 70
            }
            TableViewColumn {
                role: "icon"
                title: "Icon"
                width: 30
                delegate: Item {
                    width: 30
                    height: 30
                    Image {
                        source: typeof styleData.value === "undefined" ? "" : styleData.value
                        width: 25
                        height: 25
                        anchors.centerIn: parent
                    }
                }
            }
        }
    }
}

