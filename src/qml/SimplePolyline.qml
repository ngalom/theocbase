import QtQuick 2.5
import QtLocation 5.6
import QtGraphicalEffects 1.0
import QtPositioning 5.8
import QtQuick.Controls 1.4
import net.theocbase 1.0

MapPolyline {
    property alias mainPolyline: mainPolyline
    property int lineWidth: 1

    id: mainPolyline
    line.width: lineWidth
    line.color: 'red'
}
