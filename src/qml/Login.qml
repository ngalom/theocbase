import QtQuick 2.9
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import net.theocbase 1.0

Rectangle {
    property alias username: textFieldUser.text
    property alias password: textFieldPsw.text
    visible: true

    signal dialogClosed()
    width: 400
    height: 300
    color: "#30496f"

    Connections {
        target: oauth
        onStatusChanged: {
            console.log("MyOAuth status changed: " + status)
            column.enabled = (status === 0 || status === 2);
            switch(status)
            {
            case 0: textStatus.text = "Not authenticated"; break;
            case 1: case 3 : textStatus.text = "Please wait..."; break;
            case 2: textStatus.text = "Authenticated"; break;
            default: textStatus.text = "";
            }
        }
        onGranted: {
            console.log("MyOAuth granted");
            //close()
        }
    }
    FontLoader {
        id: openSansFont
        source: "qrc:/fonts/OpenSans-Regular.ttf"
    }
    ColumnLayout {
        id: column
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.rightMargin: 75
        anchors.leftMargin: 75
        spacing: 5
        anchors.fill: parent

        Item { Layout.fillHeight: true }
        Label {
            id: label
            color: "#ffffff"
            text: "theocbase cloud"
            font.family: openSansFont.name
            font.bold: true
            font.pointSize: 28
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        TextField {
            id: textFieldUser
            Layout.fillWidth: true
            placeholderText: qsTr("Username or Email")
            selectByMouse: true
            text: ""
        }

        TextField {
            id: textFieldPsw
            Layout.fillWidth: true
            placeholderText: qsTr("Password")
            selectByMouse: true
            echoMode: TextInput.Password
            text: ""
        }

        Button {
            id: button
            Layout.fillWidth: true
            text: qsTr("Login")
            enabled: textFieldUser.text.length > 2 && textFieldPsw.text.length > 2
            onClicked: {                

                oauth.setCredentials(textFieldUser.text, textFieldPsw.text)
                oauth.grant()

            }
        }

        Label {
            id: textStatus
            color: "red"
            text: ""
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }
        Item { Layout.minimumHeight: 10 }
        Label {
            color: mouseAreaForgot.pressed ? "lightgrey" : "darkgrey"
            text: qsTr("Forgot Password")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            MouseArea {
                id: mouseAreaForgot
                anchors.fill: parent
                onClicked: {
                    Qt.openUrlExternally("https://www.theocbase.net/register/reset.php")
                }
            }
        }
        Item { Layout.minimumHeight: 20 }
        Label {
            color: mouseAreaCreate.pressed ? "grey" : "white"
            font.bold: true
            text: qsTr("Create Account") + " >"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            MouseArea {
                id: mouseAreaCreate
                anchors.fill: parent
                onClicked: {
                    Qt.openUrlExternally("https://www.theocbase.net/register")
                }
            }
        }
        Item { Layout.fillHeight: true }

    }

}
