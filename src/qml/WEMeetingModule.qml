/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import net.theocbase 1.0

Rectangle {
    radius: 5
    height: 310 + finalTalkRow.height
    Layout.preferredHeight: height
    property int fontsize: defaultfontsize
    property bool editpossible: true
    property CPTMeeting _cptMeeting
    property string lastEditPage: ""
    property bool lastStartSong: null
    property bool shiftPressed: false
    color: "white"

    signal showEditPanel(var name, var args)
    signal movedTodoList()

    function loadSchedule(date){
        pmController.date = date
        _cptMeeting = pmController.meeting
        if (lastEditPage !== "") {
            if (lastStartSong === null)
                showEditPanel(lastEditPage, { "meeting" : _cptMeeting })
            else
                showEditPanel(lastEditPage, { "meeting" : _cptMeeting, "startSong" : lastStartSong })
        }
    }    
    function reload() {
        pmController.reload()
        _cptMeeting = pmController.meeting
    }

    onShowEditPanel: {
        lastEditPage = name
        if (typeof(args.startSong) == "undefined")
            lastStartSong = null
        else
            lastStartSong = args.startSong
    }

    PublicMeetingController { id: pmController }
    WTImport { id: wtImport }    
    FileDialog {
        id: fileDialog
        nameFilters: [ "Epub files (*.epub)" /*, "JWpub files (*.jwpub)" */ ]
        folder: shortcuts.desktop
        onAccepted: {
            console.log("File selected: " + fileUrls[0])
            var msg = wtImport.importFile(fileUrls[0])
            msgbox.text = msg
            msgbox.open()

            reload()
        }
    }

    focus: true
    Keys.onPressed: { if (event.key === Qt.Key_Shift) shiftPressed = true }
    Keys.onReleased: { if (event.key === Qt.Key_Shift) shiftPressed = false }

    MessageDialog {
        id: msgbox
        icon: MessageDialog.Warning
    }

    // public talk
    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 0

        Text {
            text: "○○● " + Qt.locale().dayName(wkDate.getDay(), Locale.LongFormat) + ", " +
                  Qt.formatDate(wkDate, Qt.SystemLocaleShortDate) + " | " + qsTr("Weekend Meeting").toUpperCase()
            font.pointSize: 12
            color: "#254f8e"
            Layout.preferredHeight: 30
            Layout.fillWidth: true
            elide: Text.ElideRight
        }

        ScheduleRowItem {
            Layout.preferredHeight: 30
            themeText.text: _cptMeeting ?
                                (_cptMeeting.songTalk < 1 ?
                                     qsTr("Song and Prayer") :
                                     qsTr("Song %1 and Prayer").arg(_cptMeeting.songTalk)) : ""
            themeText.color: "grey"
            timeText.color: "#2f4870"
            timeText.font.pointSize: 12
            timeText.text: "\u266B"
            nameText1.text: _cptMeeting ? _cptMeeting.chairman ? _cptMeeting.chairman.fullname : "" : ""
            timebox.color: "transparent"
            enabled: editpossible
            clickable: true
            editable: canEditWeekendMeetingSchedule
            onClicked: {
                showEditPanel("WEMeetingChairmanPanel.qml", { "meeting" : _cptMeeting })
            }
        }

        Rectangle {
            height: 80
            Layout.fillWidth: true
            color: "#ebedf1"
            ColumnLayout {
                anchors.fill: parent
                spacing: 0
                RowLayout {
                    Rectangle {
                        height: 40
                        width: 40
                        color: "#2f4870"
                        Image {
                            source: "qrc:///icons/pt.svg"
                            anchors.fill: parent
                            anchors.margins: 5
                        }
                    }
                    Text {
                        height: 40
                        text: qsTr("PUBLIC TALK")
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        font.pointSize: 16
                        color: "#2f4870"
                    }
                }
                ScheduleRowItem {
                    timeText.text: "30"
                    timebox.color: "#2f4870"
                    themeText.text: _cptMeeting ? _cptMeeting.themeNumber > 0 ? _cptMeeting.theme +
                                                                       " (" + _cptMeeting.themeNumber + ")" : "" : ""
                    nameText1.text: _cptMeeting ? _cptMeeting.speaker ? _cptMeeting.speaker.fullname +
                                                                        " (" + _cptMeeting.speaker.congregationName + ")" : "" : ""
                    clickable: editpossible
                    editable: canEditWeekendMeetingSchedule
                    onClicked: showEditPanel("PublicTalkPanel.qml", { "meeting" : _cptMeeting })
                    button2icon: "qrc:/icons/move_to_todo.svg"
                    button2tooltip: qsTr("Send to To Do List")
                    button2.onClicked: {
                        if (pmController.moveToTodo())
                            movedTodoList()
                    }
                    button3icon: "qrc:/icons/move_to_week.svg"
                    button3tooltip: qsTr("Move to different week")
                    button3.onClicked: {                        
                        calPopup.selectedDate = wkDate
                        calPopup.x = width - 80 - calPopup.width
                        calPopup.y = 0
                        calPopup.open()
                    }

                    button4icon: "qrc:/icons/delete.svg"
                    button4tooltip: qsTr("Clear Public Talk selections")
                    button4.onClicked: {
                        // clear public talk theme and speaker                        
                        _cptMeeting.speaker = null
                        _cptMeeting.setTheme(0)
                        _cptMeeting.save()
                    }

                    MessageDialog {
                        id: tmpMsg
                        text: "Function is not implemented!!"
                        icon: MessageDialog.Warning
                    }
                    CalendarPopup {
                        id: calPopup
                        onClosed: {
                            if (pmController.moveTo(selectedDate)) {
                                movedTodoList()
                            }
                        }
                    }
                }
            }
        }

        // watchtower study
        Rectangle {
            height: 110
            Layout.fillWidth: true
            Layout.topMargin: 10
            color: "#eef0ee"
            ColumnLayout {
                anchors.fill: parent
                spacing: 0
                RowLayout {
                        Rectangle {
                            width: 40
                            height: 40
                            color: "#4d654d"
                            Image {
                                anchors.fill: parent
                                source: "qrc:///icons/wt.svg"
                                anchors.margins: 5
                            }
                        }
                        Text {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            text: qsTr("WATCHTOWER STUDY")
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                            font.pointSize: 16
                            color: "#4d654d"
                        }
                    }
                ScheduleRowItem {
                    Layout.preferredHeight: 30
                    themeText.text: qsTr("Song %1").arg(_cptMeeting ?
                                                            (_cptMeeting.songWtStart < 1 ? "" : _cptMeeting.songWtStart) : "")
                    themeText.color: "grey"
                    timeText.font.pointSize: 12
                    timeText.color: "#4d654d"
                    timeText.text: "\u266B"
                    timebox.color: "transparent"
                    enabled: editpossible
                    clickable: true
                    editable: canEditWeekendMeetingSchedule
                    onClicked: showEditPanel("WatchtowerSongPanel.qml", { "meeting" : _cptMeeting, "startSong" : true } )
                }

                ScheduleRowItem {
                    timeText.text: _cptMeeting ? _cptMeeting.wtTime : "60"
                    timebox.color: "#4d654d"
                    themeText.text: _cptMeeting ? _cptMeeting.wtTheme : ""
                    Label {
                        anchors.left: parent.left
                        anchors.leftMargin: 50
                        anchors.verticalCenter: parent.verticalCenter
                        text: "<html><a href='#'>" + qsTr("Import WT...") + "</a></html>"
                        font.pointSize: parent.themeText.font.pointSize
                        visible: canEditWeekendMeetingSchedule && editpossible && parent.themeText.text == "" &&
                                 _cptMeeting && _cptMeeting.date > new Date("2019-03-03")
                        onLinkActivated: {
                            if (shiftPressed)
                                wtImport.exportAssistFiles()
                            else
                                fileDialog.open()
                            shiftPressed = false
                        }
                        DropArea {
                            anchors.fill: parent
                            onEntered: {
                                if (!drag.hasUrls || !drag.urls[0].endsWith(".epub"))
                                    drag.accepted = false
                            }
                            onDropped: {
                                console.log("Dropped " + drop.urls)                                
                                if (drop.urls.length > 0) {
                                    var msg = wtImport.importFile(drop.urls[0])
                                    msgbox.text = msg
                                    msgbox.open()
                                    reload()
                                }
                            }
                        }
                    }
                    nameText1.text: _cptMeeting ? _cptMeeting.wtConductor ? _cptMeeting.wtConductor.fullname +  " (" + qsTr("Conductor") + ")" : "" : ""
                    nameText2.text: _cptMeeting ? _cptMeeting.wtReader ? _cptMeeting.wtReader.fullname + " (" + qsTr("Reader") + ")" : "" : ""
                    clickable: editpossible
                    editable: canEditWeekendMeetingSchedule
                    onClicked: {
                        showEditPanel("WatchtowerStudyPanel.qml", { "meeting" : _cptMeeting })                        
                    }                    
                }

            }
        }

        // final talk
        ScheduleRowItem {
            id: finalTalkRow
            visible: _cptMeeting && _cptMeeting.finalTalk !== ""
            timeText.text: "30"
            timebox.color: "black"
            themeText.text: _cptMeeting ? _cptMeeting.finalTalk : ""
            nameText1.text: _cptMeeting ? _cptMeeting.finalTalkSpeakerName : ""
            editable: canEditWeekendMeetingSchedule
            onClicked: {
                if (!canEditWeekendMeetingSchedule)
                    return
                showEditPanel("WEMeetingFinalTalkPanel.qml", { "meeting" : _cptMeeting } )
                lastEditPage = "WEMeetingChairmanPanel.qml"
            }
        }
        // song
        ScheduleRowItem {
            Layout.preferredHeight: 30
            themeText.text: _cptMeeting ?
                                (_cptMeeting.songWtEnd < 1 ? qsTr("Song and Prayer") :
                                                             qsTr("Song %1 and Prayer").arg(_cptMeeting.songWtEnd)) : ""
            themeText.color: "grey"
            timeText.font.pointSize: 12
            timeText.color: "#4d654d"
            timeText.text: "\u266B"
            timebox.color: "transparent"
            enabled: editpossible
            clickable: true
            editable: canEditWeekendMeetingSchedule
            onClicked: {
                if (!canEditWeekendMeetingSchedule)
                    return
                showEditPanel("WatchtowerSongPanel.qml", { "meeting" : _cptMeeting, "startSong" : false })
            }
        }
        Item { Layout.fillHeight: true }
    }
}
