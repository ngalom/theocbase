<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="no">
<context>
    <name>AssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Taler</translation>
    </message>
    <message>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>Valgte</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Alle</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Notat</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>AssignmentPanel</name>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="40"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="60"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="75"/>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="75"/>
        <source>Speaker</source>
        <translation>Taler</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="86"/>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>Valgt</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="87"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="106"/>
        <source>Note</source>
        <translation>OBS</translation>
    </message>
</context>
<context>
    <name>CBSDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>Valgte</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Alle</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Notat</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>CBSPanel</name>
    <message>
        <location filename="../qml/CBSPanel.qml" line="39"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="51"/>
        <source>Source</source>
        <translation>Stopp</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="66"/>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="72"/>
        <source>CBS conductor</source>
        <comment>Dropdown column title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="73"/>
        <source>Any CL assignment</source>
        <comment>Dropdown column title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>Valgt</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="86"/>
        <source>Reader</source>
        <translation>Leser</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="105"/>
        <source>Note</source>
        <translation>OBS</translation>
    </message>
</context>
<context>
    <name>ComboBoxTable</name>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="189"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="195"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
</context>
<context>
    <name>CongregationMap</name>
    <message>
        <location filename="../qml/CongregationMap.qml" line="68"/>
        <source>Display congregation address</source>
        <comment>Display marker at the location of the congregation on the map</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DropboxSettings</name>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="76"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="214"/>
        <source>Last synchronized: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="218"/>
        <source>Synchronize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="230"/>
        <source>Delete Cloud Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpViewer</name>
    <message>
        <location filename="../helpviewer.cpp" line="81"/>
        <source>TheocBase Help</source>
        <translation>TheocBase Hjelp</translation>
    </message>
    <message>
        <location filename="../helpviewer.cpp" line="82"/>
        <source>Unable to launch the help viewer (%1)</source>
        <translation>Kunne ikke start hjelp-siden (%1)</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Notater</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>LMMNotesPanel</name>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="42"/>
        <source>Notes</source>
        <translation>Notat</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment</name>
    <message>
        <location filename="../lmm_assignment.cpp" line="180"/>
        <source>Please find below details of your assignment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="181"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="182"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="183"/>
        <source>Assignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="184"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="185"/>
        <source>Source material</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMM_AssignmentContoller</name>
    <message>
        <location filename="../lmm_assignmentcontoller.cpp" line="54"/>
        <source>Do not assign the next study</source>
        <translation>Ikke tildel neste leksjon</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment_ex</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="179"/>
        <source>Please find below details of your assignment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="180"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="181"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="184"/>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="186"/>
        <source>Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="188"/>
        <source>Assignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="191"/>
        <source>Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="193"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="194"/>
        <source>Source material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="238"/>
        <source>See your be book</source>
        <comment>Counsel point is not known yet. See your &apos;Ministry School&apos; book.</comment>
        <translation>Se i skoleboken din</translation>
    </message>
</context>
<context>
    <name>LMM_Meeting</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="607"/>
        <source>Enter source material here</source>
        <translation>Legg inn kildematerialet her</translation>
    </message>
</context>
<context>
    <name>LMM_Schedule</name>
    <message>
        <location filename="../lmm_schedule.cpp" line="97"/>
        <location filename="../lmm_schedule.cpp" line="122"/>
        <source>Chairman</source>
        <comment>talk title</comment>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="98"/>
        <location filename="../lmm_schedule.cpp" line="123"/>
        <source>Treasures From God’s Word</source>
        <comment>talk title</comment>
        <translation>Skatter fra Guds Ord</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="99"/>
        <location filename="../lmm_schedule.cpp" line="124"/>
        <source>Digging for Spiritual Gems</source>
        <comment>talk title</comment>
        <translation>Let etter åndelige perler</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="100"/>
        <location filename="../lmm_schedule.cpp" line="125"/>
        <source>Bible Reading</source>
        <comment>talk title</comment>
        <translation>Bibellesning</translation>
    </message>
    <message>
        <source>Prepare This Month’s Presentations</source>
        <comment>talk title</comment>
        <translation>Forbered månedens presentasjoner</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="103"/>
        <location filename="../lmm_schedule.cpp" line="128"/>
        <source>Initial Call</source>
        <comment>talk title</comment>
        <translation>Førstegangsbesøk</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <comment>talk title</comment>
        <translation>Gjenbesøk</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="101"/>
        <location filename="../lmm_schedule.cpp" line="126"/>
        <source>Sample Conversation Video</source>
        <comment>talk title</comment>
        <translation>Eksempelvideo</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="104"/>
        <location filename="../lmm_schedule.cpp" line="129"/>
        <source>First Return Visit</source>
        <comment>talk title</comment>
        <translation>Første gjenbesøk</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="105"/>
        <location filename="../lmm_schedule.cpp" line="130"/>
        <source>Second Return Visit</source>
        <comment>talk title</comment>
        <translation>Andre gjenbesøk</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="106"/>
        <location filename="../lmm_schedule.cpp" line="131"/>
        <source>Third Return Visit</source>
        <comment>talk title</comment>
        <translation>Tredje gjenbesøk</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="107"/>
        <location filename="../lmm_schedule.cpp" line="132"/>
        <source>Bible Study</source>
        <comment>talk title</comment>
        <translation>Bibelstudium</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="108"/>
        <location filename="../lmm_schedule.cpp" line="133"/>
        <source>Talk</source>
        <comment>talk title</comment>
        <translation>Tale</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="109"/>
        <location filename="../lmm_schedule.cpp" line="134"/>
        <source>Living as Christians Talk 1</source>
        <comment>talk title</comment>
        <translation>VÅRT LIV SOM KRISTNE Tale 1</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="110"/>
        <location filename="../lmm_schedule.cpp" line="135"/>
        <source>Living as Christians Talk 2</source>
        <comment>talk title</comment>
        <translation>VÅRT LIV SOM KRISTNE Tale 2</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="111"/>
        <location filename="../lmm_schedule.cpp" line="136"/>
        <source>Living as Christians Talk 3</source>
        <comment>talk title</comment>
        <translation>VÅRT LIV SOM KRISTNE Tale 3</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="112"/>
        <location filename="../lmm_schedule.cpp" line="137"/>
        <source>Congregation Bible Study</source>
        <comment>talk title</comment>
        <translation>Menighetsbibelstudiet</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="113"/>
        <location filename="../lmm_schedule.cpp" line="138"/>
        <source>Circuit Overseer&#x27;s Talk</source>
        <comment>talk title</comment>
        <translation>Kretstilsynsmannens tjenestetale</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="127"/>
        <source>Apply Yourself to Reading and Teaching</source>
        <comment>talk title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LifeMinistryMeetingSchedule</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>SKATTER FRA GUDS ORD</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>OPPLÆRING I FORKYNNELSEN</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VÅRT LIV SOM KRISTNE</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Importer plan...</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Veileder</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Sang %1 og Bønn</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Innledende kommentarer</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Repetisjon og oversikt over neste ukes møte</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Sang %1</translation>
    </message>
    <message>
        <source>Main hall</source>
        <translation>Hovedsal</translation>
    </message>
    <message>
        <source>Auxiliary classroom 1</source>
        <translation>B-sal</translation>
    </message>
    <message>
        <source>Auxiliary classroom 2</source>
        <translation>C-sal</translation>
    </message>
    <message>
        <source>Main Class</source>
        <translation>Hovedsal</translation>
    </message>
    <message>
        <source>Second Class</source>
        <translation>B-Sal</translation>
    </message>
    <message>
        <source>Third class</source>
        <translation>C-Sal</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/Login.qml" line="61"/>
        <source>Username or Email</source>
        <translation>Brukernavn eller E-post</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="69"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="78"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="97"/>
        <source>Forgot Password</source>
        <translation>Glemt passord</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="111"/>
        <source>Create Account</source>
        <translation>Lag konto</translation>
    </message>
</context>
<context>
    <name>LoginForm.ui</name>
    <message>
        <source>TheocBase</source>
        <translation>TheocBase</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Innlogging</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../main.cpp" line="62"/>
        <source>Services</source>
        <translation>Tjenester</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Hide %1</source>
        <translation>Skjul %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>Hide Others</source>
        <translation>Skjul Andre</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="65"/>
        <source>Show All</source>
        <translation>Vis alle</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Preferences...</source>
        <translation>Innstillinger...</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="67"/>
        <source>Quit %1</source>
        <translation>Avslutt %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="68"/>
        <source>About %1</source>
        <translation>Om: %1</translation>
    </message>
</context>
<context>
    <name>MWMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="76"/>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="95"/>
        <source>Auxiliary Classroom Counselor II</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="115"/>
        <source>Auxiliary Classroom Counselor III</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWMeetingModule</name>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="217"/>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>SKATTER FRA GUDS ORD</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="225"/>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>OPPLÆRING I FORKYNNELSEN</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="230"/>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VÅRT LIV SOM KRISTNE</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="243"/>
        <source>Midweek Meeting</source>
        <translation>Midtukemøtet</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="255"/>
        <source>Import Schedule...</source>
        <translation>Importer plan...</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="303"/>
        <location filename="../qml/MWMeetingModule.qml" line="504"/>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="304"/>
        <location filename="../qml/MWMeetingModule.qml" line="505"/>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="305"/>
        <location filename="../qml/MWMeetingModule.qml" line="506"/>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="314"/>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="314"/>
        <source>Counselor</source>
        <translation>Veileder</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="340"/>
        <location filename="../qml/MWMeetingModule.qml" line="392"/>
        <source>Song %1 and Prayer</source>
        <translation>Sang %1 og Bønn</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="355"/>
        <source>Opening Comments</source>
        <translation>Innledende kommentarer</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="374"/>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Repetisjon/Kort oversikt/Opplysninger</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="469"/>
        <source>Song %1</source>
        <translation>Sang %1</translation>
    </message>
    <message>
        <source>Main hall</source>
        <translation>Hovedsal</translation>
    </message>
    <message>
        <source>Auxiliary classroom 1</source>
        <translation>B-sal</translation>
    </message>
    <message>
        <source>Auxiliary classroom 2</source>
        <translation>C-sal</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="490"/>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="492"/>
        <source>Reader</source>
        <translation>Leser</translation>
    </message>
</context>
<context>
    <name>MWMeetingPrayerPanel</name>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="44"/>
        <source>Prayer</source>
        <translation>Bønn</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>(No meeting)</source>
        <translation>(Ikke møte)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="418"/>
        <source>No meeting</source>
        <translation>Ikke møte</translation>
    </message>
    <message>
        <source>Nothing to display</source>
        <translation>Ikke noe å vise</translation>
    </message>
    <message>
        <source>Prayer:</source>
        <translation>Bønn:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="617"/>
        <source>Conductor:</source>
        <translation>Studieleder:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>Reader:</source>
        <translation>Oppleser:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="467"/>
        <source>Copyright</source>
        <translation>Opphavsrett</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="475"/>
        <source>Qt libraries licensed under the GPL.</source>
        <translation>Qt biblioteker lisensiert under GPL.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="467"/>
        <source>TheocBase Team</source>
        <translation>TheocBase Team</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="278"/>
        <source>Last synchronized</source>
        <translation>Sist synkronisert</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="470"/>
        <source>Licensed under GPLv3.</source>
        <translation>Lisensiert under GPLv3.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Versions of Qt libraries </source>
        <translation>Versjoner av Qt bibliotek </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="520"/>
        <location filename="../mainwindow.cpp" line="1345"/>
        <source>TheocBase data exchange</source>
        <translation>TheocBase datautveksling</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="522"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="522"/>
        <source>Hello</source>
        <translation>Hei</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="522"/>
        <source>Best Regards</source>
        <translation>Med vennlig hilsen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="556"/>
        <source>New update available. Do you want to install?</source>
        <translation>Ny oppdatering tilgjengelig. Vil du installere?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="560"/>
        <source>No new update available</source>
        <translation>Ingen oppdatering tilgjengelig</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="616"/>
        <source>Enter e-mail address.</source>
        <translation>Skriv inn e-postaddresse.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="595"/>
        <source>Save file</source>
        <translation>Lagre fil</translation>
    </message>
    <message>
        <source>TheocBase cloud synchronizing...</source>
        <translation>TheocBase Skyen Synkroniserer...</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>De samme endringene finnes både lokalt og i skyen (%1 rader). Vil du beholde de lokale endringene?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1717"/>
        <source>Select ePub file</source>
        <translation>Velg ePub-fil</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1630"/>
        <source>Send e-mail reminders?</source>
        <translation>Send påminnelse via e-post?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1664"/>
        <source>Updates available...</source>
        <translation>Oppdatering er tilgjengelig...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="644"/>
        <location filename="../mainwindow.cpp" line="678"/>
        <location filename="../mainwindow.cpp" line="1248"/>
        <source>Error sending e-mail</source>
        <translation>Feil ved sending av e-post</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="392"/>
        <source>WEEK STARTING %1</source>
        <translation>UKEN FRA %1</translation>
    </message>
    <message>
        <source>min</source>
        <comment>Abbreviation of minutes</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="576"/>
        <source>Exporting outgoing speakers not ready yet, sorry.</source>
        <translation>Eksportering av foredragsholdere eksternt er ikke klar ennå, beklager.</translation>
    </message>
    <message>
        <source>Exporting speakers to iCal not ready yet, sorry.</source>
        <translation>Eksportering av foredragsholdere til iCal-format er ikke klart enda, beklager.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="580"/>
        <source>Emailing iCal is not ready yet, sorry.</source>
        <translation>Å sende e-post med iCal-fil er ikke klart enda, beklager.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="584"/>
        <source>Exporting study history to iCal is not supported</source>
        <translation>Eksportering av leksjonhistorikk i iCal-formatet støttes ikke</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="599"/>
        <source>Save folder</source>
        <translation>Lagre folder</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="680"/>
        <location filename="../mainwindow.cpp" line="1250"/>
        <source>E-mail sent successfully</source>
        <translation>E-post sendt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="683"/>
        <source>Saved successfully</source>
        <translation>Lagring vellykket</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <location filename="../mainwindow.cpp" line="748"/>
        <source>Counselor-Class II</source>
        <translation>Veileder Klasse 2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="744"/>
        <location filename="../mainwindow.cpp" line="749"/>
        <source>Counselor-Class III</source>
        <translation>Veileder Klasse 3</translation>
    </message>
    <message>
        <source>Opening Prayer</source>
        <translation>Innledende bønn</translation>
    </message>
    <message>
        <source>Concluding Prayer</source>
        <translation>Avsluttende bønn</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="764"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>Partner for %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="831"/>
        <location filename="../mainwindow.cpp" line="1046"/>
        <location filename="../mainwindow.cpp" line="1071"/>
        <source>Kingdom Hall</source>
        <translation>Rikets Sal</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="742"/>
        <location filename="../mainwindow.cpp" line="747"/>
        <location filename="../mainwindow.cpp" line="837"/>
        <location filename="../mainwindow.cpp" line="848"/>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="771"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>Oppleser for Menighetsbibelstudiet</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="756"/>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Source</source>
        <comment>short for Source material</comment>
        <translation>Kilde</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="745"/>
        <location filename="../mainwindow.cpp" line="750"/>
        <location filename="../mainwindow.cpp" line="795"/>
        <location filename="../mainwindow.cpp" line="797"/>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="756"/>
        <source>Timing</source>
        <translation>Tidtakning</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="837"/>
        <location filename="../mainwindow.cpp" line="1002"/>
        <source>Public Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="837"/>
        <source>Watchtower Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="854"/>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="859"/>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="861"/>
        <source>Watchtower reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1107"/>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want to keep the local changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1148"/>
        <source>The cloud data has now been deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1152"/>
        <source>Synchronize</source>
        <translation>Synkroniser</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1154"/>
        <source>Sign Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1164"/>
        <source>The cloud data has been deleted. Your local data will be replaced. Continue?</source>
        <translation>Data i nettskyen har blitt slettet. Det vil nå bli erstattet med lokale data. Fortsette?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1259"/>
        <source>Open file</source>
        <translation>Åpne fil</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1286"/>
        <source>Open directory</source>
        <translation>Åpne katalog</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1317"/>
        <source>Import Error</source>
        <translation>Importfeil</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1317"/>
        <source>Could not import from Ta1ks. Files are missing:</source>
        <translation>Kunne ikke importere fra Ta1ks. Filer som mangler:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1332"/>
        <source>Save unsaved data?</source>
        <translation>Lagre ulagrede data?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1341"/>
        <source>Import file?</source>
        <translation>Importere fil?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="691"/>
        <source>School assignment</source>
        <translation>Oppgavetildeling</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Congregation Bible Study:</source>
        <translation>Menighetsbibelstudiet:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="327"/>
        <source>Theocratic Ministry School:</source>
        <translation>Den Teokratiske Tjenesteskolen:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="358"/>
        <source>Service Meeting:</source>
        <translation>Tjenestemøtet:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="806"/>
        <source>Congregation Bible Study</source>
        <translation>Menighetsbibelstudiet</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="833"/>
        <source>Theocratic Ministry School</source>
        <translation>Den Teokratiske Tjenesteskolen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="860"/>
        <source>Service Meeting</source>
        <translation>Tjenestemøtet</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1103"/>
        <location filename="../mainwindow.ui" line="1427"/>
        <source>Export</source>
        <translation>Eksporter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1177"/>
        <location filename="../mainwindow.ui" line="1383"/>
        <source>E-mail</source>
        <translation>E-post</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1406"/>
        <source>Recipient:</source>
        <translation>Mottaker:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1392"/>
        <source>Subject: </source>
        <translation>Emne:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1303"/>
        <source>Public talks</source>
        <translation>Offentlige foredrag</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1457"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1491"/>
        <source>info</source>
        <translation>Informasjon</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2066"/>
        <source>Data exhange</source>
        <translation>Data utveksling</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2128"/>
        <source>TheocBase Cloud</source>
        <translation>TheocBase Skyen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1650"/>
        <location filename="../mainwindow.ui" line="1683"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1882"/>
        <source>Timeline</source>
        <translation>Tidslinje</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1943"/>
        <source>Print...</source>
        <translation>Skriv ut...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1946"/>
        <source>Print</source>
        <translation>Skriv ut</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1955"/>
        <source>Settings...</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1967"/>
        <source>Publishers...</source>
        <translation>Forkynnere...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1246"/>
        <location filename="../mainwindow.ui" line="1970"/>
        <source>Publishers</source>
        <translation>Forkynnere</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2036"/>
        <source>Speakers...</source>
        <translation>Foredragsholdere...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1283"/>
        <location filename="../mainwindow.ui" line="2039"/>
        <source>Speakers</source>
        <translation>Foredragsholdere</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2063"/>
        <source>Data exhange...</source>
        <translation>Data utveksling...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2075"/>
        <source>TheocBase help...</source>
        <translation>TheocBase hjelp...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2090"/>
        <source>History</source>
        <translation>Historikk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2102"/>
        <location filename="../mainwindow.ui" line="2105"/>
        <source>Full Screen</source>
        <translation>Full skjerm</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2110"/>
        <source>Startup Screen</source>
        <translation>Startside</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2119"/>
        <source>Reminders...</source>
        <translation>Påminnelser...</translation>
    </message>
    <message>
        <source>&lt;--</source>
        <translation>&lt;--</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="568"/>
        <source>Theme:</source>
        <translation>Tema:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="573"/>
        <source>Speaker:</source>
        <translation>Foredragsholder:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="578"/>
        <source>Chairman:</source>
        <translation>Ordstyrer:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="583"/>
        <source>Watchtower Study:</source>
        <translation>Vakttårnstudiet:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="529"/>
        <source>Data exchange</source>
        <translation>Utveksling av data</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1114"/>
        <source>Export Format</source>
        <translation>Eksporteringsformat</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1120"/>
        <source>For sending data to another user</source>
        <translation>For å sende data til en annen bruker</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1139"/>
        <source>For easy import to Calendar programs</source>
        <translation>For enkel importering til Kalenderprogrammer</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1171"/>
        <source>Export Method</source>
        <translation>Metode for eksportering</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1190"/>
        <source>Save to File</source>
        <translation>Lagre til fil</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1213"/>
        <source>Events grouped by date</source>
        <translation>Hendelser sortert etter dato</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1220"/>
        <source>All day events</source>
        <translation>Heldagshendelser</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1236"/>
        <source>Midweek Meeting</source>
        <translation>Midtukemøtet</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1259"/>
        <source>Study History</source>
        <translation>Historikk leksjoner</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1310"/>
        <source>Outgoing Talks</source>
        <translation>Foredrag eksternt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1321"/>
        <source>Date Range</source>
        <translation>Dato perioder</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1328"/>
        <source>Previous Weeks</source>
        <translation>Tidligere uker</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1342"/>
        <source>From Date</source>
        <translation>Fra dato</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1359"/>
        <source>Thru Date</source>
        <translation>Til og med dato</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1836"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1842"/>
        <source>Tools</source>
        <translation>Verktøy</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1855"/>
        <source>Help</source>
        <translation>Hjelp</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1931"/>
        <source>Today</source>
        <translation>I dag</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1979"/>
        <source>Exit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1991"/>
        <source>Report bug...</source>
        <translation>Rapporter feil...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2003"/>
        <source>Send feedback...</source>
        <translation>Send tilbakemelding...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2021"/>
        <source>About TheocBase...</source>
        <translation>Om TheocBase...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2048"/>
        <source>Check updates...</source>
        <translation>Se etter oppdateringer...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2078"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2146"/>
        <source>Territories...</source>
        <translation>Distrikter...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2149"/>
        <source>Territories</source>
        <translation>Distrikt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1910"/>
        <source>Back</source>
        <translation>Tilbake</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1922"/>
        <source>Next</source>
        <translation>Neste</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2137"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2012"/>
        <source>TheocBase website</source>
        <translation>TheocBase hjemmeside</translation>
    </message>
    <message>
        <source>(Circuit overseer visit)</source>
        <translation>(Kretstilsynsmannens besøk)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="414"/>
        <source>Convention week (no meeting) </source>
        <translation>Stevneuke (ikke møte)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="534"/>
        <source>Public Talk:</source>
        <translation>Offentlig Foredrag:</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerEdit</name>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="70"/>
        <source>Speaker</source>
        <translation>Taler</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="94"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="121"/>
        <source>Congregation</source>
        <translation>Menighet</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="147"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="159"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="172"/>
        <source>Meeting day and time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModel</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="163"/>
        <source>From %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModule</name>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="109"/>
        <source>Outgoing speakers</source>
        <translation>Foredragsholdere eksternt</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/OutgoingSpeakersModule.qml" line="193"/>
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="194"/>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Permission</name>
    <message>
        <source>Edit Settings</source>
        <comment>Access Control</comment>
        <translation>Endre innstillinger</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule</name>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Sang %1 og Bønn</translation>
    </message>
    <message>
        <source>Song and Prayer</source>
        <translation>Sang og Bønn</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>OFFENTLIG FOREDRAG</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>VAKTTÅRNSTUDIET</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Sang %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="48"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="72"/>
        <source>Congregation</source>
        <translation>Menighet</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="94"/>
        <source>Speaker</source>
        <translation>Taler</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="128"/>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="140"/>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="152"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="164"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="177"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="70"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="71"/>
        <source>No</source>
        <translation>Nei</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>&amp;No</source>
        <translation>&amp;Nei</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>Cancel</source>
        <translation>Slett</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Slett</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>Save</source>
        <translation>Lagre</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>&amp;Save</source>
        <translation>&amp;Lagre</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="77"/>
        <source>Open</source>
        <translation>Åpne</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="159"/>
        <source>Wrong username and/or password</source>
        <translation>Feil brukernavn og/eller passord</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="267"/>
        <source>Database not found!</source>
        <translation>Database ikke funnet!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="270"/>
        <source>Choose database</source>
        <translation>Velg database</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="270"/>
        <source>SQLite files (*.sqlite)</source>
        <translation>SQLite filer (*.sqlite)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="294"/>
        <source>Database restoring failed</source>
        <translation>Gjenoppretting av database mislyktes</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="325"/>
        <location filename="../mainwindow.cpp" line="224"/>
        <source>Save changes?</source>
        <translation>Lagre endringer?</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="263"/>
        <source>Database copied to </source>
        <translation>Database kopiert til</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="37"/>
        <source>Database file not found! Searching path =</source>
        <translation>Database fil ikke funnet! Søker sti =</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="56"/>
        <source>Database Error</source>
        <translation>Database feil</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="706"/>
        <source>This version of the application (%1) is older than the database (%2). There is a strong probability that error messages will popup and changes may not be saved correctly. Please download and install the latest version for best results.</source>
        <translation>Denne versjonen av programmet (%1) er eldre enn databasen (%2). Det er stor sannsynlighet for at feil vil oppstå og endringer blir kanskje ikke lagret. Vennligst installér nyeste versjon for best resultat.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="345"/>
        <source>Circuit</source>
        <translation>Krets</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="748"/>
        <source>Database updated</source>
        <translation>Database oppdatert</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="314"/>
        <location filename="../ccongregation.cpp" line="340"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>Kretstilsynsmannens besøk</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="316"/>
        <location filename="../ccongregation.cpp" line="321"/>
        <source>%1 (No meeting)</source>
        <comment>no meeting exception type</comment>
        <translation>%1 (Ikke møte)</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="316"/>
        <location filename="../ccongregation.cpp" line="342"/>
        <source>Convention week</source>
        <translation>Stevneuke</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="318"/>
        <location filename="../ccongregation.cpp" line="344"/>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="349"/>
        <source>Review</source>
        <comment>Theocratic ministry school review</comment>
        <translation>Repetisjon</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="56"/>
        <location filename="../cpublictalks.cpp" line="341"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="57"/>
        <location filename="../cpublictalks.cpp" line="260"/>
        <location filename="../cpublictalks.cpp" line="342"/>
        <source>Last</source>
        <translation>Siste</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="257"/>
        <location filename="../school.cpp" line="424"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="258"/>
        <location filename="../school.cpp" line="425"/>
        <source>Highlights</source>
        <translation>Viktige punkter fra ukens Bibellesning</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="262"/>
        <location filename="../school.cpp" line="429"/>
        <source>WT Reader</source>
        <translation>VT Oppleser</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="264"/>
        <location filename="../school.cpp" line="431"/>
        <source>Selected</source>
        <translation>Valgte</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="323"/>
        <location filename="../school.cpp" line="486"/>
        <source>Assignment already has been made</source>
        <translation>Oppgaven er allerede tildelt</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="327"/>
        <location filename="../school.cpp" line="490"/>
        <source>Assigned student has other meeting parts</source>
        <translation>Eleven har andre oppgaver på møtet</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="331"/>
        <location filename="../school.cpp" line="494"/>
        <source>Student unavailable</source>
        <translation>Elev utilgjengelig</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="335"/>
        <source>Student part of family</source>
        <translation>Elev er i familie med</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="434"/>
        <source>Recently together</source>
        <translation>Nylig oppgave sammen</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="498"/>
        <source>Family member used in another TMS assignment</source>
        <translation>Familiemedlem brukt i andre elevoppgaver</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="67"/>
        <source>I/O</source>
        <comment>Incoming/Outgoing</comment>
        <translation>I/U</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="72"/>
        <source>Notes</source>
        <translation>Notater</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="564"/>
        <source>Congregation Name</source>
        <translation>Menighetens navn:</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="273"/>
        <location filename="../historytable.cpp" line="322"/>
        <location filename="../school.cpp" line="854"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="322"/>
        <source>CBS</source>
        <translation>MBS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="322"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>SL</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="325"/>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="325"/>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="468"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="469"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="208"/>
        <source>Assignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>date</source>
        <translation>dato</translation>
    </message>
    <message>
        <source>no</source>
        <translation>Nr.</translation>
    </message>
    <message>
        <source>material</source>
        <translation>Kildestoff</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="470"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="209"/>
        <source>Note</source>
        <translation>Merknad</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="471"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="210"/>
        <source>Time</source>
        <translation>Tidsbruk</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <location filename="../school.cpp" line="263"/>
        <location filename="../school.cpp" line="423"/>
        <location filename="../school.cpp" line="430"/>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <source>Student</source>
        <translation>Elev</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="265"/>
        <source>Bible highlights:</source>
        <translation>Viktige punkter fra ukens bibellesing:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="268"/>
        <source>No. 1:</source>
        <translation>Nr. 1:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="271"/>
        <source>No. 2:</source>
        <translation>Nr. 2:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="274"/>
        <source>No. 3:</source>
        <translation>Nr. 3:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="277"/>
        <source>Reader:</source>
        <translation>Oppleser:</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="816"/>
        <location filename="../generatexml.cpp" line="107"/>
        <source>Default language not selected!</source>
        <translation>Standard språk ikke valgt!</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="937"/>
        <source>The header row of CSV file is not valid.</source>
        <translation>Header rad i CSV-filen er ikke korrekt</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="296"/>
        <source>Confirm password!</source>
        <translation>Bekreft passord!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2249"/>
        <source>A user with the same E-mail address has already been added.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2455"/>
        <source>All talks have been added to this week</source>
        <translation>Alle talene er blitt lagt til for denne uken</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="259"/>
        <location filename="../mainwindow.cpp" line="1003"/>
        <location filename="../publictalkedit.cpp" line="71"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Import Error</source>
        <translation>Import feil</translation>
    </message>
    <message>
        <source>Unable to open database</source>
        <translation>Databasen kunne ikke åpnes</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import Complete</source>
        <translation>Importering fullført</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="192"/>
        <source>Speaker</source>
        <comment>The todo list Speaker cell is in error</comment>
        <translation>Foredragsholder</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="199"/>
        <source>Congregation</source>
        <comment>The todo list Congregation cell is in error</comment>
        <translation>Menighet</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="209"/>
        <source>Theme (could be a theme this speaker does not give)</source>
        <comment>The todo list Theme cell is in error</comment>
        <translation>Tema (kan være at foredragsholderen ikke holder dette temaet)</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="221"/>
        <location filename="../todo.cpp" line="232"/>
        <source>Date Already Scheduled</source>
        <translation>Dato allerede planlagt</translation>
    </message>
    <message>
        <source>File reading failed</source>
        <translation>Feil ved lesing av fil</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="85"/>
        <source>Unable to read new Workbook format</source>
        <translation>Ikke i stand til å lese nytt Arbeidshefte-format</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="87"/>
        <source>Database not set to handle language &#x27;%1&#x27;</source>
        <translation>Database ikke konfigurert til å håndtere språket %1</translation>
    </message>
    <message>
        <source>Nothing imported (no month names recognized)</source>
        <translation>Ingenting ble importert (ingen måneder ble gjenkjent)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="89"/>
        <source>Unable to find year/month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="90"/>
        <source>Nothing imported (no dates recognized)</source>
        <translation>Ingenting importert (Ingen datoer funnet)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="94"/>
        <location filename="../wtimport.cpp" line="31"/>
        <location filename="../wtimport.cpp" line="32"/>
        <source>Imported %1 weeks from %2 thru %3</source>
        <translation>Importert %1 uker fra %2 til %3</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="397"/>
        <source>Please select the Talk Names to match the names we found in the workbook</source>
        <translation>Vennligst velg navn på oppdrag som samsvarer med navnene vi fant i arbeidsheftet</translation>
    </message>
    <message>
        <source>Imported weeks from %1 thru %2</source>
        <translation>Importerte ukene fra %1 til %2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="7"/>
        <source>Ch</source>
        <comment>history table: abbreviation for the &apos;chairman&apos; of the Christian Life and Ministry Meeting</comment>
        <translation>OS</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="9"/>
        <source>H</source>
        <comment>history table: abbreviation for the &apos;highlights&apos;</comment>
        <translation>SK</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="11"/>
        <source>D</source>
        <comment>history table: abbreviation for &apos;Diging spiritual gem&apos;</comment>
        <translation>ÅP</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="13"/>
        <source>#R</source>
        <comment>history table: abbreviation for the &apos;reader&apos;</comment>
        <translation>#BL</translation>
    </message>
    <message>
        <source>#M</source>
        <comment>history table: abbreviation for &apos;Prepare your monthly presentation&apos;</comment>
        <translation>#FP</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="15"/>
        <source>#V</source>
        <comment>history table: abbreviation for &apos;prepare video presentation&apos;</comment>
        <translation>#V</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="17"/>
        <source>#A</source>
        <comment>history table: abbreviation for &apos;Apply yourself to Reading and Teaching&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="20"/>
        <source>♢1</source>
        <comment>history table: abbreviation for assignment 1, assistant/householder of &apos;Initial Visit&apos;</comment>
        <translation>♢1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="21"/>
        <source>#1</source>
        <comment>history table: abbreviation for assignment 1, &apos;Initial Visit&apos;</comment>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="26"/>
        <source>♢2</source>
        <comment>history table: abbreviation for assignment 2, assistant/householder of &apos;Return Visit&apos;</comment>
        <translation>♢2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="27"/>
        <source>#2</source>
        <comment>history table: abbreviation for assignment 2, &apos;Return Visit&apos;</comment>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="31"/>
        <source>♢3</source>
        <comment>history table: abbreviation for assignment 3, assistant/householder of &apos;Bible Study&apos;</comment>
        <translation>♢3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="32"/>
        <source>#3</source>
        <comment>history table: abbreviation for assignment 3, &apos;Bible Study&apos;</comment>
        <translation>#3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="34"/>
        <source>CL1</source>
        <comment>history table: abbreviation for cristian life, talk 1</comment>
        <translation>KL1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="36"/>
        <source>CL2</source>
        <comment>history table: abbreviation for cristian life, talk 2</comment>
        <translation>KL2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="38"/>
        <source>CL3</source>
        <comment>history table: abbreviation for cristian life, talk 3</comment>
        <translation>KL3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="41"/>
        <source>BS-R</source>
        <comment>history table: abbreviation for cristian life, Bible Study Reader</comment>
        <translation>MBS-L</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="42"/>
        <source>BS</source>
        <comment>history table: abbreviation for cristian life, Bible Study</comment>
        <translation>MBS</translation>
    </message>
    <message>
        <location filename="../slipscanner.cpp" line="28"/>
        <source>One-time Scanning of New Slip</source>
        <translation>Engangsscanning av den nye oppdragslappen (S-89)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="820"/>
        <source>All weekend meetings for</source>
        <comment>Filename prefix for weekend meetings iCal export</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="832"/>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="940"/>
        <source>All outgoing talks for</source>
        <comment>File name prefix for outgoing talks iCal export</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="956"/>
        <location filename="../mainwindow.cpp" line="967"/>
        <source>Outgoing Talks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1072"/>
        <source>Midweek Meeting</source>
        <translation>Midtukemøtet</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="467"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="206"/>
        <location filename="../publictalkedit.cpp" line="68"/>
        <location filename="../schoolreminder.cpp" line="182"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <source>Material</source>
        <comment>Source material</comment>
        <translation>Kildestoff</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="473"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="212"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>Med Elev</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="166"/>
        <source>Sister</source>
        <translation>Søster</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="167"/>
        <source>Brother</source>
        <translation>Bror</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="171"/>
        <source>Dear %1 %2</source>
        <translation>Kjære %1 %2</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="172"/>
        <source>Please find below details of your upcoming assignment:</source>
        <translation>Her er opplysningene du trenger til det kommende elevoppdraget ditt:</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="231"/>
        <source>Regards</source>
        <translation>Vennlig hilsen</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="970"/>
        <source>You have a new kind of slip. Please enter a request on the forum so the app can be updated.  Mention code %1</source>
        <translation>Du har en ny type oppdragslapp. Vennligst legg inn et ønske på forumet, så applikasjonen kan bli oppdatert. Nevn kode %1</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="203"/>
        <source>Publisher</source>
        <comment>Roles and access control</comment>
        <translation>Forkynner</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="96"/>
        <source>View midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="98"/>
        <source>Edit midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="100"/>
        <source>View midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="102"/>
        <source>Edit midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="104"/>
        <source>Send midweek meeting reminders</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="106"/>
        <source>Print midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="108"/>
        <source>Print midweek meeting assignment slips</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="110"/>
        <source>Print midweek meeting worksheets</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="112"/>
        <source>View weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="114"/>
        <source>Edit weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="116"/>
        <source>View weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="118"/>
        <source>Edit weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="120"/>
        <source>View public talk list</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="122"/>
        <source>Edit public talk list</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="124"/>
        <source>Schedule hospitality</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="126"/>
        <source>Print weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="128"/>
        <source>Print weekend meeting worksheets</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="130"/>
        <source>Print speakers schedule</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="132"/>
        <source>Print speakers assignments</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="134"/>
        <source>Print hospitality</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="136"/>
        <source>Print public talk list</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="146"/>
        <source>View public speakers</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="148"/>
        <source>Edit public speakers</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="138"/>
        <source>View publishers</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="140"/>
        <source>Edit publishers</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="142"/>
        <source>View student data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="144"/>
        <source>Edit student data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="150"/>
        <source>View privileges</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="152"/>
        <source>Edit privileges</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="154"/>
        <source>View midweek meeting talk history</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="156"/>
        <source>View availabilities</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="158"/>
        <source>Edit availabilities</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="160"/>
        <source>View permissions</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="162"/>
        <source>Edit permissions</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="164"/>
        <source>View territories</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="166"/>
        <source>Edit territories</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="168"/>
        <source>Print territory record</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="170"/>
        <source>Print territory map card</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="172"/>
        <source>Print territory map and address sheets</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="174"/>
        <source>View territory settings</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="176"/>
        <source>Edit territory settings</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="178"/>
        <source>View territory assignments</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="180"/>
        <source>View territory addresses</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="182"/>
        <source>View congregation settings</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="184"/>
        <source>Edit congregation settings</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="186"/>
        <source>View special events</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="188"/>
        <source>Edit special events</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="190"/>
        <source>View song list</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="192"/>
        <source>Edit song list</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="194"/>
        <source>Delete cloud data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="205"/>
        <source>Elder</source>
        <comment>Roles and access control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="207"/>
        <source>LMM Chairman</source>
        <comment>Roles and access control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="209"/>
        <source>LMM Overseer</source>
        <comment>Roles and access control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="211"/>
        <source>Public Talk Coordinator</source>
        <comment>Roles and access control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="213"/>
        <source>Territory Servant</source>
        <comment>Roles and access control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="215"/>
        <source>Secretary</source>
        <comment>Roles and access control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="217"/>
        <source>Service Overseer</source>
        <comment>Roles and access control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="219"/>
        <source>Coordinator of BOE</source>
        <comment>Roles and access control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="221"/>
        <source>Administrator</source>
        <comment>Roles and access control</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.ui" line="174"/>
        <location filename="../settings.ui" line="177"/>
        <location filename="../settings.ui" line="1610"/>
        <source>Exceptions</source>
        <translation>Unntakelser</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="204"/>
        <location filename="../settings.ui" line="207"/>
        <source>Public Talks</source>
        <translation>Offentlige foredrag</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="773"/>
        <source>Custom templates folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="917"/>
        <source>Open database location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="976"/>
        <source>Backup database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1010"/>
        <source>Restore database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1454"/>
        <source>Names display order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1521"/>
        <source>Color palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1466"/>
        <source>By last name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1478"/>
        <source>By first name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1533"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1545"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2497"/>
        <source>Public talks maintenance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2589"/>
        <source>Schedule hospitality for public speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3105"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3111"/>
        <source>Street types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3361"/>
        <source>Map marker scale:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3387"/>
        <source>Geo Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3395"/>
        <source>Google:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3405"/>
        <location filename="../settings.ui" line="3408"/>
        <source>API Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3415"/>
        <source>Here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3422"/>
        <source>Default:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3432"/>
        <source>OpenStreetMap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3437"/>
        <source>Google</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3442"/>
        <source>Here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3457"/>
        <location filename="../settings.ui" line="3460"/>
        <source>App Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3467"/>
        <location filename="../settings.ui" line="3470"/>
        <source>App Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3506"/>
        <source>Send E-Mail Reminders</source>
        <translation>Send påminnelser via e-post</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3756"/>
        <source>Send reminders when closing TheocBase</source>
        <translation>Send påminnelser når TheocBase lukkes</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3528"/>
        <source>E-Mail Options</source>
        <translation>E-post innstillinger</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3562"/>
        <source>Sender&#x27;s e-mail</source>
        <translation>Avsenders e-post</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3572"/>
        <source>Sender&#x27;s name</source>
        <translation>Avsenders navn</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3666"/>
        <source>Test Connection</source>
        <translation>Test Forbindelse</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="706"/>
        <source>Printing</source>
        <translation>Utskrifter</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3796"/>
        <source>Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3812"/>
        <source>E-mail:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3865"/>
        <source>Rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="159"/>
        <location filename="../settings.ui" line="162"/>
        <source>General</source>
        <translation>Generelt</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="839"/>
        <source>Backup</source>
        <translation>Sikkerhetskopi</translation>
    </message>
    <message>
        <source>Database backup</source>
        <translation>Sikkerhetskopier database</translation>
    </message>
    <message>
        <source>Restore backup database</source>
        <translation>Gjenopprett sikkerhetskopi av databasen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="468"/>
        <source>Current congregation</source>
        <translation>Aktuell Menighet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1302"/>
        <source>User interface</source>
        <translation>Brukergrensesnitt</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1386"/>
        <source>User interface language</source>
        <translation>Språkvalg for brukergrensesnittet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1063"/>
        <source>Security</source>
        <translation>Sikkerhet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1242"/>
        <source>Enable password</source>
        <translation>Aktiver passord</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1267"/>
        <source>Enable database encryption</source>
        <translation>Slå på databasekryptering</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1168"/>
        <source>Confirm</source>
        <translation>Bekreft</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="189"/>
        <location filename="../settings.ui" line="192"/>
        <location filename="../settings.ui" line="1820"/>
        <source>Life and Ministry Meeting</source>
        <translation>Tjenestemøtet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2013"/>
        <source>Remove Duplicates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2020"/>
        <source>Meeting Items</source>
        <translation>Møtedeler</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2119"/>
        <source>Schedule</source>
        <comment>Name of tab to edit Midweek Meeting schedule</comment>
        <translation>Skoleplan</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2599"/>
        <source>Hide discontinued</source>
        <translation>Skjul utgåtte</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2781"/>
        <source>Add songs</source>
        <translation>Legg til sanger</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="219"/>
        <location filename="../settings.ui" line="2787"/>
        <source>Songs</source>
        <translation>Sanger</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2859"/>
        <location filename="../settings.ui" line="2865"/>
        <source>Add song one at a time</source>
        <translation>Legg til en sang av gangen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2880"/>
        <source>Song number</source>
        <translation>Sangnummer</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2887"/>
        <source>Song title</source>
        <translation>Sangtittel</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2961"/>
        <source>Cities</source>
        <translation>Byer</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3033"/>
        <source>Territory types</source>
        <translation>Typer distrikt</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3210"/>
        <source>Addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3216"/>
        <source>Address types</source>
        <translation>Type adresse</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3324"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scale:</source>
        <translation>Skala:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="246"/>
        <location filename="../settings.cpp" line="2263"/>
        <source>Access Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3222"/>
        <source>Type number:</source>
        <translation>Nummertype:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3117"/>
        <location filename="../settings.ui" line="3232"/>
        <location filename="../settings.ui" line="3802"/>
        <source>Name:</source>
        <translation>Navn:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3186"/>
        <location filename="../settings.ui" line="3242"/>
        <source>Color:</source>
        <translation>Farge:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3127"/>
        <location filename="../settings.ui" line="3252"/>
        <source>#0000ff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3332"/>
        <source>Default address type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;a href=&quot;#&quot;&gt;Forgot Password&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;#&quot;&gt;Glemt passord&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Delete Cloud Data</source>
        <translation>Slett data i nettskyen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="228"/>
        <source>Territories</source>
        <translation>Distrikt</translation>
    </message>
    <message>
        <source>TheocBase Cloud</source>
        <translation>TheocBase Skyen</translation>
    </message>
    <message>
        <source>Open Database Location</source>
        <translation>Åpne databasemappen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="386"/>
        <location filename="../settings.cpp" line="1101"/>
        <source>Congregation</source>
        <translation>Menighet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="624"/>
        <source>Weekend Meeting</source>
        <translation>Helgemøtet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="435"/>
        <source>Current Circuit Overseer</source>
        <translation>Nåværende kretstilsynsmann</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="668"/>
        <source>Click to edit</source>
        <translation>Klikk for å redigere</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1111"/>
        <source>Username</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1132"/>
        <location filename="../settings.ui" line="3673"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="529"/>
        <location filename="../settings.ui" line="1838"/>
        <location filename="../settings.ui" line="1902"/>
        <source>Mo</source>
        <translation>Man</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="534"/>
        <location filename="../settings.ui" line="1843"/>
        <location filename="../settings.ui" line="1907"/>
        <source>Tu</source>
        <translation>Tir</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="539"/>
        <location filename="../settings.ui" line="1848"/>
        <location filename="../settings.ui" line="1912"/>
        <source>We</source>
        <translation>Ons</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="544"/>
        <location filename="../settings.ui" line="1853"/>
        <location filename="../settings.ui" line="1917"/>
        <source>Th</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="549"/>
        <location filename="../settings.ui" line="1858"/>
        <location filename="../settings.ui" line="1922"/>
        <source>Fr</source>
        <translation>Fre</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="554"/>
        <location filename="../settings.ui" line="1863"/>
        <location filename="../settings.ui" line="1927"/>
        <source>Sa</source>
        <translation>Lør</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="559"/>
        <location filename="../settings.ui" line="1868"/>
        <location filename="../settings.ui" line="1932"/>
        <source>Su</source>
        <translation>Søn</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1881"/>
        <source>Public Talk and Watchtower study</source>
        <translation>Helgemøtet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1754"/>
        <location filename="../settings.ui" line="1783"/>
        <location filename="../settings.ui" line="2210"/>
        <location filename="../settings.ui" line="2271"/>
        <location filename="../settings.ui" line="2321"/>
        <location filename="../settings.ui" line="2360"/>
        <location filename="../settings.ui" line="2420"/>
        <location filename="../settings.ui" line="2443"/>
        <location filename="../settings.ui" line="2530"/>
        <location filename="../settings.ui" line="2730"/>
        <location filename="../settings.ui" line="2808"/>
        <location filename="../settings.ui" line="2831"/>
        <location filename="../settings.ui" line="2906"/>
        <location filename="../settings.ui" line="2982"/>
        <location filename="../settings.ui" line="3008"/>
        <location filename="../settings.ui" line="3054"/>
        <location filename="../settings.ui" line="3080"/>
        <location filename="../settings.ui" line="3149"/>
        <location filename="../settings.ui" line="3175"/>
        <location filename="../settings.ui" line="3271"/>
        <location filename="../settings.ui" line="3297"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Custom Templates Folder</source>
        <translation>Mappen for egne utskrifts-maler</translation>
    </message>
    <message>
        <source>iCal Export</source>
        <translation>Eksporter fra iCal</translation>
    </message>
    <message>
        <source>Events grouped by date</source>
        <translation>Sortert etter dato</translation>
    </message>
    <message>
        <source>All day events</source>
        <translation>Heldagshendelser</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1652"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>Kretstilsynsmannens besøk</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1657"/>
        <source>Convention</source>
        <translation>Stevne</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1662"/>
        <source>Memorial</source>
        <translation>Minnehøytiden</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1667"/>
        <source>Zone overseer&#x27;s talk</source>
        <translation>Tale av en representant for hovedkontoret</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1672"/>
        <source>Other exception</source>
        <translation>Andre unntakelser</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1685"/>
        <location filename="../settings.cpp" line="608"/>
        <source>Start date</source>
        <translation>Start dato</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1716"/>
        <location filename="../settings.cpp" line="609"/>
        <source>End date</source>
        <translation>Slutt dato</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1833"/>
        <location filename="../settings.ui" line="1897"/>
        <source>No meeting</source>
        <translation>Ikke møte</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1799"/>
        <source>Description</source>
        <translation>Beskrivelse</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2053"/>
        <source>Import Christian Life and Ministry Meeting Workbook</source>
        <translation>Importer arbeidshefte for Tjenestemøtet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1981"/>
        <source>Main</source>
        <comment>Main Midweek meeting tab</comment>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2132"/>
        <source>Year</source>
        <translation>År</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="486"/>
        <location filename="../settings.ui" line="2170"/>
        <source>Midweek Meeting</source>
        <translation>Midtukemøtet</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Errors&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Feil&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2297"/>
        <source>Midweek Meeting Schedule for selected Meeting above</source>
        <translation>Planen for Midtukemøtet den valgte uken finner du over</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3584"/>
        <source>Custom settings</source>
        <translation>Bruk tilpassede innstillinger</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3705"/>
        <source>Authorize</source>
        <translation>Gi tilgang</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3715"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2788"/>
        <source>Synchronize</source>
        <translation>Synkroniser</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Brukernavn eller E-post</translation>
    </message>
    <message>
        <source>&lt;a href=&quot;http://register.theocbase.net/index.php&quot;&gt;Create an account&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://register.theocbase.net/index.php&quot;&gt;Lag en konto&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="237"/>
        <source>Reminders</source>
        <translation>Påminnelser</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3932"/>
        <source>Run a file provided by the Help Desk</source>
        <translation>Kjør en fil med hjelp fra Help Desk</translation>
    </message>
    <message>
        <source>Run Special Command</source>
        <translation>Kjør Spesiell Kommando</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1987"/>
        <source>Number of classes</source>
        <translation>Antall skoler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2741"/>
        <source>Studies</source>
        <translation>Leksjoner</translation>
    </message>
    <message>
        <source>Add subjects</source>
        <translation>Legg til emner</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2613"/>
        <location filename="../settings.ui" line="2619"/>
        <source>Add subject one at a time</source>
        <translation>Legg til ett emne av gangen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2653"/>
        <source>Public talk number</source>
        <translation>Foredragsnummer</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2660"/>
        <source>Public talk subject</source>
        <translation>Foredragstema</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2670"/>
        <location filename="../settings.ui" line="2917"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2702"/>
        <source>Add congregations and speakers</source>
        <translation>Legg til menigheter og foredragsholdere</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="828"/>
        <location filename="../settings.cpp" line="1103"/>
        <location filename="../settings.cpp" line="1171"/>
        <source>Number</source>
        <translation>Nummer</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2558"/>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="556"/>
        <source>Select a backup file</source>
        <translation>Velg en backup fil</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="934"/>
        <location filename="../settings.cpp" line="1256"/>
        <location filename="../settings.cpp" line="1553"/>
        <location filename="../settings.cpp" line="1621"/>
        <location filename="../settings.cpp" line="1705"/>
        <location filename="../settings.cpp" line="1801"/>
        <source>Remove selected row?</source>
        <translation>Fjern valgte rad?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1032"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>Et offentlig foredrag med det samme nummeret er allerede lagret!
Vil du slette den forrige oppføringen?

Planlagte foredrag vil bli flyttet til Oppgavelisten.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1172"/>
        <source>Title</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1272"/>
        <source>Song number missing</source>
        <translation>Sangnummer mangler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1275"/>
        <source>Song title missing</source>
        <translation>Sangtittel mangler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1285"/>
        <source>Song is already saved!</source>
        <translation>Sangen er allerede lagret!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1294"/>
        <source>Song added to database</source>
        <translation>Sang lagt til databasen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1331"/>
        <source>City</source>
        <translation>By</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1367"/>
        <location filename="../settings.cpp" line="1451"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1517"/>
        <source>City name missing</source>
        <translation>Bynavn mangler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1526"/>
        <source>City is already saved!</source>
        <translation>Byen er allerede lagret!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1534"/>
        <source>City added to database</source>
        <translation>By lagt til i databasen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1585"/>
        <source>Territory type name missing</source>
        <translation>Distriktstype mangler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1594"/>
        <source>Territory type is already saved!</source>
        <translation>Distriktstype er allerede lagret!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1602"/>
        <source>Territory type added to database</source>
        <translation>Distriktstype lagt til i databasen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1671"/>
        <source>Name of the street type is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1678"/>
        <source>Street type is already saved!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1686"/>
        <source>Street type added to database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2026"/>
        <source>This is no longer an option. Please request help in the forum if this is needed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2263"/>
        <source>Remove permissions for the selected user?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2495"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="829"/>
        <location filename="../settings.cpp" line="2557"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="831"/>
        <source>Released on</source>
        <comment>Release date of the public talk outline</comment>
        <translation>Utgitt</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="832"/>
        <source>Discontinued on</source>
        <comment>Date after which the public talk outline should no longer be used</comment>
        <translation>Utgått</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="985"/>
        <source>Discontinuing this talk will move talks scheduled with this outline to the To Do List.

</source>
        <translation>Ved å fjerne dette foredraget vil foredrag som er planlagt med dette temaet bli flyttet til Oppgavelisten.

</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1104"/>
        <source>Revision</source>
        <translation>Revidert</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1408"/>
        <location filename="../settings.cpp" line="1452"/>
        <location filename="../settings.cpp" line="2140"/>
        <location filename="../settings.cpp" line="2196"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1409"/>
        <location filename="../settings.cpp" line="1453"/>
        <source>Color</source>
        <translation>Farge</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1758"/>
        <source>Number of address type is missing</source>
        <translation>Tallkoden for adressetype mangler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1762"/>
        <source>Name of address type is missing</source>
        <translation>Navn på adressetype mangler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1771"/>
        <source>Address type is already saved!</source>
        <translation>Adressetype er allerede lagret!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1781"/>
        <source>Address type added to database</source>
        <translation>Adressetype lagt til i databasen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1879"/>
        <source>Error sending e-mail</source>
        <translation>Feil ved sending av e-post</translation>
    </message>
    <message>
        <source>Sign In</source>
        <translation>Logg inn</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2790"/>
        <source>Sign Out</source>
        <translation>Logg ut</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Innlogging feilet</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1931"/>
        <source>Authorized. (Re-authorization only needed if you are getting error messages when sending mail.)</source>
        <translation>Autorisert. (Re-autorisasjon er bare nødvendig hvis du får feilmelding ved sending av e-post)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1933"/>
        <source>Click the button to authorize theocbase to send email on your behalf</source>
        <translation>Klikk for å godkjenne at TheocBase kan sende e-post på dine vegne</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2002"/>
        <location filename="../settings.cpp" line="2683"/>
        <source>Select ePub file</source>
        <translation>Velg ePub-fil</translation>
    </message>
    <message>
        <source>TMS History Copied (up to 2 years of assignments)</source>
        <translation>Historikk fra TTS kopiert (inntil 2 år tilbake)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2031"/>
        <source>Warning: Make sure this file comes from a trusted source. Continue?</source>
        <translation>Advarsel: Vær sikker på at filen kommer fra en pålitelig kilde. Fortsette?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2035"/>
        <source>Command File</source>
        <translation>Kommando Fil</translation>
    </message>
    <message>
        <source>Last synchronized</source>
        <translation>Sist synkronisert</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2416"/>
        <location filename="../settings.cpp" line="2466"/>
        <source>Meeting</source>
        <translation>Møtet</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2416"/>
        <source>Remove the whole meeting? (Use only to remove invalid data from database)</source>
        <translation>Slette hele møtet? (Brukes kun for å slette feil data fra databasen)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2458"/>
        <source>Enter source material here</source>
        <translation>Legg inn kildematerialet her</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2466"/>
        <source>Remove this talk? (Use only to remove invalid data from database)</source>
        <translation>Slette denne talen? (Brukes kun for å slette feil data fra databasen)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2496"/>
        <source>Bible Reading</source>
        <translation>Bibellesning</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2497"/>
        <source>Song 1</source>
        <translation>Sang 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2498"/>
        <source>Song 2</source>
        <translation>Sang 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2499"/>
        <source>Song 3</source>
        <translation>Sang 3</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2556"/>
        <source>Meeting Item</source>
        <translation>Møtedel</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2559"/>
        <source>Timing</source>
        <translation>Varighet</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2703"/>
        <source>Study Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2704"/>
        <source>Study Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2771"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>Er du sikker på at du vil slette data i nettskyen permanent?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2784"/>
        <source>The cloud data has now been deleted.</source>
        <translation>Data i nettskyen har blitt slettet.</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="833"/>
        <location filename="../settings.cpp" line="1105"/>
        <location filename="../settings.cpp" line="1173"/>
        <location filename="../settings.cpp" line="1332"/>
        <location filename="../settings.cpp" line="1368"/>
        <location filename="../settings.cpp" line="1454"/>
        <source>Language id</source>
        <translation>Språk ID</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1004"/>
        <source>Public talk number missing</source>
        <translation>Foredragsnummer mangler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1007"/>
        <source>Public talk subject missing</source>
        <translation>Foredragstema mangler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1028"/>
        <source>Public talk is already saved!</source>
        <translation>Foredraget er allerede lagret!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1062"/>
        <source>Public talk added to database</source>
        <translation>Offentlig foredrag er lagt til i databasen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1069"/>
        <location filename="../settings.cpp" line="1301"/>
        <location filename="../settings.cpp" line="1540"/>
        <location filename="../settings.cpp" line="1608"/>
        <location filename="../settings.cpp" line="1692"/>
        <location filename="../settings.cpp" line="1788"/>
        <source>Adding failed</source>
        <translation>Tillegging feilet</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="607"/>
        <source>Exception</source>
        <translation>Unntak</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="610"/>
        <source>Meeting 1</source>
        <translation>Møte 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="611"/>
        <source>Meeting 2</source>
        <translation>Møte 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="574"/>
        <source>Database restored. The program will be restarted.</source>
        <translation>Database gjenopprettet. Programmet blir nå restartet.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2742"/>
        <source>Remove ALL studies? (Use only to remove invalid data from database)</source>
        <translation>Fjern ALLE leksjoner? (Brukes kun for å fjerne feil data fra databasen)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1100"/>
        <source>Speaker</source>
        <translation>Taler</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1099"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1102"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="538"/>
        <source>Save database</source>
        <translation>Lagre databasen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="546"/>
        <source>Database backuped</source>
        <translation>Database gjenopprettet</translation>
    </message>
</context>
<context>
    <name>SmtpClient</name>
    <message>
        <location filename="../smtp/smtpclient.cpp" line="392"/>
        <source>Sender&#x27;s name</source>
        <translation>Avsenders navn</translation>
    </message>
    <message>
        <location filename="../smtp/smtpclient.cpp" line="397"/>
        <source>Sender&#x27;s email</source>
        <translation>Avsenders e-post</translation>
    </message>
</context>
<context>
    <name>StartSliderPage1.ui</name>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="13"/>
        <source>Welcome to theocbase</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StudentAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Elev</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Alle</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <source>With Student</source>
        <comment>Dropdown column title</comment>
        <translation>Med Elev</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Medhjelperen skal ikke være en av motsatt kjønn.</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation>Situasjon</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Fullført</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Frivillig</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tid</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Nåværende leksjon</translation>
    </message>
    <message>
        <source>Exercise Completed</source>
        <translation>Øvelse utført</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Neste leksjon</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Notat</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="65"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="79"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="94"/>
        <source>Student</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="102"/>
        <location filename="../qml/StudentAssignmentPanel.qml" line="149"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="138"/>
        <source>Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="148"/>
        <source>With Student</source>
        <comment>Dropdown column title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="163"/>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="185"/>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="202"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="208"/>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="225"/>
        <source>Volunteer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="260"/>
        <source>Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="277"/>
        <source>Current Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="293"/>
        <source>Exercise Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="303"/>
        <source>Next Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="324"/>
        <source>Note</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryAddAddressForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="53"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="115"/>
        <source>Street:</source>
        <translation>Gate:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="125"/>
        <source>Postalcode:</source>
        <translation>Postnummer:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="147"/>
        <source>Search</source>
        <comment>Search address</comment>
        <translation>Søk</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="162"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="179"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Fylke</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="197"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>Kommune</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="75"/>
        <source>Country:</source>
        <translation>Land:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="85"/>
        <source>State:</source>
        <comment>Administrative area level 1</comment>
        <translation>Fylke:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="95"/>
        <source>City:</source>
        <comment>Locality</comment>
        <translation>By:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="105"/>
        <source>District:</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Område:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="135"/>
        <source>No.:</source>
        <translation>Nr.:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="215"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>By</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="232"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Område</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="249"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>Gate</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="266"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>Nr.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="283"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>Postnummer</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="300"/>
        <source>Latitude</source>
        <translation>Breddegrad</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="317"/>
        <source>Longitude</source>
        <translation>Lengdegrad</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="349"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="354"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>TerritoryAddStreetForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="32"/>
        <source>Select the streets to be added to the territory:</source>
        <comment>Add street names to territoy</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="53"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="74"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="79"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="156"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="197"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="256"/>
        <source>Undefined [%1]</source>
        <comment>Undefined territory address type</comment>
        <translation>Udefinert [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="299"/>
        <source>Edit address</source>
        <translation>Rediger adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="347"/>
        <source>Add address</source>
        <comment>Add address to territory</comment>
        <translation>Legg til adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="348"/>
        <source>Please select an address in the list of search results.</source>
        <comment>Add address to territory</comment>
        <translation>Vennligst velg en adresse blant søketreffet.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="407"/>
        <location filename="../qml/TerritoryAddressList.qml" line="417"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>Søk i adresser</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Final Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="408"/>
        <source>No address found.</source>
        <comment>Add or edit territory address</comment>
        <translation>Ingen adresser funnet.</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="39"/>
        <source>ID</source>
        <translation>Nr.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>Distriktsnummer</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="83"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>Kommune</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="106"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Fylke</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="129"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>Kommune</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="173"/>
        <source>District</source>
        <comment>Sublocality</comment>
        <translation>Område</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="151"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>By</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="196"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>Gate</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="221"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>Nr.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="245"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>Postnummer</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="268"/>
        <source>Geometry</source>
        <comment>Coordinate geometry of the address</comment>
        <translation>Koordinater</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="290"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="300"/>
        <source>Type</source>
        <comment>Type of address</comment>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="325"/>
        <source>Add new address</source>
        <translation>Legg til ny adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="336"/>
        <source>Edit selected address</source>
        <translation>Rediger valgte adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="347"/>
        <source>Remove selected address</source>
        <translation>Fjern valgt adresse</translation>
    </message>
</context>
<context>
    <name>TerritoryImportForm.ui</name>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="78"/>
        <source>Filename:</source>
        <translation>Filnavn</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="133"/>
        <source>Match KML Fields</source>
        <comment>Choose which data fields in kml-file correspond to which territory properties</comment>
        <translation>Match KML felt</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="55"/>
        <source>Territory boundaries</source>
        <comment>Territory data import option</comment>
        <translation>Distriktsgrenser</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="63"/>
        <source>Addresses</source>
        <comment>Territory data import option</comment>
        <translation>Adresser</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="163"/>
        <source>Description:</source>
        <comment>Territory data import KML Field</comment>
        <translation>Beskrivelse:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="180"/>
        <source>Search by &quot;Description&quot; if territory is not found by &quot;Name&quot;</source>
        <translation>Søk i &quot;Beskrivelse&quot; om ingen treff i &quot;Navn&quot;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="195"/>
        <source>Territory No.</source>
        <comment>Territory number</comment>
        <translation>Distriktsnr.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="201"/>
        <source>Locality</source>
        <comment>Territory locality</comment>
        <translation>Lokasjon</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="207"/>
        <source>Remark</source>
        <translation>Anmerkning</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="238"/>
        <source>Match Fields</source>
        <comment>Choose which fields in the import-file correspond to the address data</comment>
        <translation>Match felt</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="255"/>
        <source>Address:</source>
        <comment>Territory address import field</comment>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="266"/>
        <source>Name:</source>
        <comment>Territory address import field</comment>
        <translation>Navn:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="304"/>
        <source>Address type:</source>
        <comment>Address type for territory address import</comment>
        <translation>Adressetype:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="326"/>
        <source>Output filename for failed addresses:</source>
        <comment>Territory address import</comment>
        <translation>Filnavn for mislykkede adresser:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="396"/>
        <source>Import</source>
        <translation>Importér</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="405"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>TerritoryManagement</name>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="132"/>
        <source>Group by:</source>
        <translation>Sorter etter:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="139"/>
        <source>City</source>
        <comment>Group territories by city</comment>
        <translation>By</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="141"/>
        <source>Type</source>
        <comment>Group territories by type of the territory</comment>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="142"/>
        <source>Worked through</source>
        <comment>Group territories by time frame they have been worked through</comment>
        <translation>Gjennomarbeidet</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="242"/>
        <source>Add new territory</source>
        <translation>Legg til distrikt</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="257"/>
        <source>Remove selected territory</source>
        <translation>Fjern valgte distrikt</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="353"/>
        <source>Remark</source>
        <translation>Anmerkning</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="372"/>
        <source>Type:</source>
        <comment>Territory-type that is used to group territories</comment>
        <translation>Type:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="399"/>
        <source>City:</source>
        <comment>Cityname that is used to group territories</comment>
        <translation>By:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="464"/>
        <source>Assignments</source>
        <translation>Tildelinger</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="831"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="870"/>
        <source>Addresses</source>
        <translation>Adresser</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="298"/>
        <source>No.:</source>
        <translation>Nr.:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="309"/>
        <source>Number</source>
        <translation>Nummer</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="320"/>
        <source>Locality:</source>
        <translation>Sted:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="330"/>
        <source>Locality</source>
        <translation>Sted</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="434"/>
        <source>Map</source>
        <comment>Territory map</comment>
        <translation>Kart</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="499"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="513"/>
        <source>Publisher-ID</source>
        <translation>Forkynner-ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="627"/>
        <source>Checked out</source>
        <comment>Date when the publisher obtained the territory</comment>
        <translation>Utlevert</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="633"/>
        <source>Checked back in</source>
        <comment>Date when the territory is returned</comment>
        <translation>Innlevert</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="784"/>
        <source>Add new assignment</source>
        <translation>Legg til nytt oppdrag</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="808"/>
        <source>Remove selected assignment</source>
        <translation>Fjern valgte oppdrag</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="592"/>
        <source>Publisher</source>
        <comment>Dropdown column title</comment>
        <translation>Forkynner</translation>
    </message>
</context>
<context>
    <name>TerritoryMap</name>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="334"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="760"/>
        <source>The new boundary overlaps %n territory(ies):</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="762"/>
        <source>Do you want to assign overlapping areas to the current territory?
Select &#x27;No&#x27; if overlapping areas should remain in their territories and to add only the part, that doesn&#x27;t overlap other territories.</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="796"/>
        <source>Add address to selected territory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="804"/>
        <source>Assign to selected territory</source>
        <comment>Reassign territory address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="812"/>
        <source>Remove address</source>
        <comment>Delete territory address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="902"/>
        <source>%1 of %2 address(es) imported.</source>
        <comment>Territory address import progress</comment>
        <translation>%1 av %2 adresse(r) ble importert.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="917"/>
        <source>Import territory boundaries</source>
        <comment>Territory import dialog</comment>
        <translation>Importer distriktsgrenser</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="927"/>
        <location filename="../qml/TerritoryMap.qml" line="938"/>
        <location filename="../qml/TerritoryMap.qml" line="953"/>
        <location filename="../qml/TerritoryMap.qml" line="962"/>
        <location filename="../qml/TerritoryMap.qml" line="995"/>
        <location filename="../qml/TerritoryMap.qml" line="1004"/>
        <source>Import territory data</source>
        <comment>Territory import dialog</comment>
        <translation>Importér distriktsdata</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="928"/>
        <source>%n territory(ies) imported or updated.</source>
        <comment>Number of territories imported or updated</comment>
        <translation>
            <numerusform>%n distrikt ble importert eller oppdatert.</numerusform>
            <numerusform>%n distrikter ble importert eller oppdatert.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="954"/>
        <source>Please select the address and name fields.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>Vennligst velg adresse- og navnefelt.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="963"/>
        <source>The selected fields should be different.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>De valgte feltene må være forskjellige.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="971"/>
        <source>Import territory addresses</source>
        <comment>Territory import dialog</comment>
        <translation>Importér distriktsadresser</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="972"/>
        <source>The addresses will be added to the current territory. Please select a territory first.</source>
        <comment>Territory address import from file</comment>
        <translation>Adressene vil bli lagt til i et valgt distrikt. Vennligst velg et distrikt først.</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="996"/>
        <source>%n address(es) imported.</source>
        <comment>Number of addresses imported</comment>
        <translation>
            <numerusform>%n adresse ble importert.</numerusform>
            <numerusform>%n adresser ble importert.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1006"/>
        <source>No valid territory selected.</source>
        <comment>Territory boundary import</comment>
        <translation>Ingen gyldige distrikt valgt.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1008"/>
        <source>The import file could not be read.</source>
        <comment>Territory boundary import</comment>
        <translation>Import-filen kunne ikke leses.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1040"/>
        <source>Address</source>
        <comment>Default Address-field for territory address import</comment>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1042"/>
        <source>Name</source>
        <comment>Default Name-field for territory address import</comment>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1071"/>
        <location filename="../qml/TerritoryMap.qml" line="1079"/>
        <source>Open file</source>
        <translation>Åpne fil</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1072"/>
        <location filename="../qml/TerritoryMap.qml" line="1073"/>
        <source>KML files (*.kml)</source>
        <comment>Filedialog pattern</comment>
        <translation>KML filer (*.kml)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1072"/>
        <location filename="../qml/TerritoryMap.qml" line="1080"/>
        <location filename="../qml/TerritoryMap.qml" line="1088"/>
        <source>All files (*)</source>
        <comment>Filedialog pattern</comment>
        <translation>Alle filer (*)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1080"/>
        <location filename="../qml/TerritoryMap.qml" line="1081"/>
        <location filename="../qml/TerritoryMap.qml" line="1088"/>
        <location filename="../qml/TerritoryMap.qml" line="1089"/>
        <source>CSV files (*.csv)</source>
        <comment>Filedialog pattern</comment>
        <translation>CSV-filer (*.csv)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1080"/>
        <location filename="../qml/TerritoryMap.qml" line="1088"/>
        <source>Text files (*.txt)</source>
        <comment>Filedialog pattern</comment>
        <translation>Tekstfiler (*.txt)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1087"/>
        <source>Save file</source>
        <translation>Lagre fil</translation>
    </message>
</context>
<context>
    <name>TerritoryMapForm.ui</name>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="65"/>
        <source>search</source>
        <comment>Search address in territory map</comment>
        <translation>søk</translation>
    </message>
    <message>
        <source>Import boundaries</source>
        <comment>Import territory boundaries from a file</comment>
        <translation>Importér grenser</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="149"/>
        <source>Import data</source>
        <comment>Import territory data from a file</comment>
        <translation>Importér data</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="193"/>
        <source>Switch edit mode</source>
        <comment>Switch edit mode on territory map</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="207"/>
        <source>Create boundary</source>
        <comment>Create a new boundary for the territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="219"/>
        <source>Remove boundary</source>
        <comment>Remove boundary or geometry of the territory</comment>
        <translation>Fjern grense</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="231"/>
        <source>Split territory</source>
        <comment>Cut territory in two parts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="160"/>
        <source>Zoom full</source>
        <comment>Zoom full to display all territories</comment>
        <translation>Full zoom</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="170"/>
        <source>Show/hide territories</source>
        <comment>Show/hide boundaries of territories</comment>
        <translation>Vis/Skjul distrikter</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="181"/>
        <source>Show/hide markers</source>
        <comment>Show/hide address markers of territories</comment>
        <translation>Vis/Skjul markeringer</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetList</name>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="102"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="209"/>
        <source>Undefined [%1]</source>
        <comment>Undefined street type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="269"/>
        <location filename="../qml/TerritoryStreetList.qml" line="280"/>
        <source>Add streets</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="270"/>
        <source>No streets found.</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryStreetListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="38"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="60"/>
        <source>Territory-ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="82"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="91"/>
        <source>From number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="100"/>
        <source>To number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="109"/>
        <source>Quantity</source>
        <comment>Quantity of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="119"/>
        <source>Type</source>
        <comment>Type of street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="128"/>
        <source>Geometry</source>
        <comment>Line geometry of the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="166"/>
        <source>Add new street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="177"/>
        <source>Remove selected street</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryTreeModel</name>
    <message>
        <location filename="../territorymanagement.cpp" line="118"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; 6 måneder</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="119"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 til 12 måneder</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="120"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>&gt; 12 måneder</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="303"/>
        <source>Not worked</source>
        <comment>Group text for territories that are not worked yet.</comment>
        <translation>Ikke tildelt</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="305"/>
        <location filename="../territorymanagement.cpp" line="457"/>
        <location filename="../territorymanagement.cpp" line="468"/>
        <source>Not assigned</source>
        <comment>Value of the field, the territories are grouped by, is empty.</comment>
        <translation>Ikke tildelt</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territory</source>
        <translation>distrikt</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territories</source>
        <translation>distrikter</translation>
    </message>
</context>
<context>
    <name>TodoPanel</name>
    <message>
        <location filename="../qml/TodoPanel.qml" line="114"/>
        <source>To Do List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="209"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Incoming</source>
        <comment>incoming speaker</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Outgoing</source>
        <comment>outgoing speaker</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ValidationTextInput</name>
    <message>
        <location filename="../qml/ValidationTextInput.qml" line="10"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="69"/>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="45"/>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Service Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="66"/>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WEMeetingModule</name>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="97"/>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="109"/>
        <location filename="../qml/WEMeetingModule.qml" line="316"/>
        <source>Song and Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="110"/>
        <location filename="../qml/WEMeetingModule.qml" line="317"/>
        <source>Song %1 and Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="145"/>
        <source>PUBLIC TALK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="163"/>
        <source>Send to To Do List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="169"/>
        <source>Move to different week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="178"/>
        <source>Clear Public Talk selections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="226"/>
        <source>WATCHTOWER STUDY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="235"/>
        <source>Song %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="256"/>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="284"/>
        <source>Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="285"/>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <location filename="../qml/WatchtowerSongPanel.qml" line="25"/>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="46"/>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="68"/>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="80"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="99"/>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="121"/>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>cPersonComboBox</name>
    <message>
        <source>Outside speaker</source>
        <translation>Tilreisende foredragsholder</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="238"/>
        <source>Brother has other meeting parts</source>
        <translation>Broren har andre oppgaver på møtet</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="233"/>
        <source>Person unavailable</source>
        <translation>Person ikke tilgjengelig</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="228"/>
        <source>Outgoing speaker</source>
        <translation>Holder foredrag eksternt</translation>
    </message>
</context>
<context>
    <name>ccongregation</name>
    <message>
        <location filename="../ccongregation.cpp" line="118"/>
        <source>(Missing Record)</source>
        <comment>database is now missing this entry</comment>
        <translation>(Mangler data)</translation>
    </message>
</context>
<context>
    <name>checkupdates</name>
    <message>
        <location filename="../checkupdates.cpp" line="100"/>
        <source>New version available</source>
        <translation>Ny versjon tilgjengelig</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="102"/>
        <source>Download</source>
        <translation>Last ned</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="105"/>
        <source>New version...</source>
        <translation>Ny versjon...</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="116"/>
        <source>No new update available</source>
        <translation>Ingen oppdatering tilgjengelig</translation>
    </message>
</context>
<context>
    <name>csync</name>
    <message>
        <location filename="../csync.cpp" line="112"/>
        <source>File reading failed</source>
        <translation>Kunne ikke lese filen</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="450"/>
        <source>XML file generated in the wrong version.</source>
        <translation>XML-fil generert i feil utgave.</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="517"/>
        <source>Persons - added </source>
        <translation>Personer - lagt til </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="555"/>
        <source>Persons - updated </source>
        <translation>Personer - oppdatert </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="586"/>
        <source>Theocratic ministry school - schedule added </source>
        <translation>Den Teokratiske Tjenesteskolen - plan lagt til </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="588"/>
        <source>Theocratic ministry school - schedule updated </source>
        <translation>Den Teokratiske Tjenesteskolen - plan oppdatert </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="784"/>
        <source>Theocratic Ministry School - Study updated</source>
        <translation>Den Teokratiske Tjenesteskolen - Leksjon oppdatert</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="798"/>
        <source>Theocratic Ministry School - Study added</source>
        <translation>Den Teokratiske Tjenesteskolen - Leksjon lagt til</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="834"/>
        <source>Public talks - theme added </source>
        <translation>Offentlige foredrag - tema lagt til </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="892"/>
        <source>Public Talk and WT - schedule updated </source>
        <translation>Offentlig foredrag og VT-studiet - plan oppdatert </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="898"/>
        <source>Public Talk and WT - schedule added </source>
        <translation>Offentlig foredrag og VT-studiet - plan lagt til </translation>
    </message>
    <message>
        <source>Congregation Bible Study - schedule added </source>
        <translation>Menighetsbibelstudiet - plan lagt til </translation>
    </message>
    <message>
        <source>Congregation Bible Study - schedule updated </source>
        <translation>Menighetsbibelstudiet - plan oppdatert </translation>
    </message>
    <message>
        <source>Service Meeting - schedule added </source>
        <translation>Tjenestemøtet - plan lagt til </translation>
    </message>
    <message>
        <source>Service Meeting - schedule updated </source>
        <translation>Tjenestemøtet - plan oppdatert </translation>
    </message>
</context>
<context>
    <name>cterritories</name>
    <message>
        <location filename="../cterritories.cpp" line="210"/>
        <source>Do not call</source>
        <comment>Territory address type</comment>
        <translation>Ikke besøk</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1512"/>
        <source>Save failed addresses</source>
        <comment>Territory address import</comment>
        <translation>Lagre mislykkede adresser</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1513"/>
        <source>The file is in read only mode</source>
        <comment>Save failed addresses in territory data import</comment>
        <translation>Filen er skrivebeskyttet</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <location filename="../family.cpp" line="124"/>
        <source>Not set</source>
        <translation>Ikke angitt</translation>
    </message>
</context>
<context>
    <name>googleMediator</name>
    <message>
        <location filename="../googlemediator.cpp" line="198"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="200"/>
        <source>OK but JSON not available</source>
        <translation>OK men JSON ikke tigjengelig</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="202"/>
        <source>Authorizing</source>
        <translation>Autoriserer</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="204"/>
        <source>Authorization Failed</source>
        <translation>Autorisering feilet</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="210"/>
        <source>Missing Client ID</source>
        <translation>Mangler Klient ID</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="212"/>
        <source>Missing Client Secret</source>
        <translation>Mangler Klient kode</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="214"/>
        <source>Need Authorization Code</source>
        <translation>Trenger autorisasjonskode</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="216"/>
        <source>Need Token Refresh</source>
        <translation>Må fornye tilgangsautorisering</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <location filename="../historytable.ui" line="196"/>
        <source>Number of weeks after selected date</source>
        <translation>Antall uker etter valgte dato</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="246"/>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Antall uker forkynneren skal utheves etter et oppdrag</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="220"/>
        <source>weeks</source>
        <translation>uker</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="122"/>
        <source>Timeline</source>
        <translation>Tidslinje</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="180"/>
        <source>Number of weeks before selected date</source>
        <translation>Antall uker før valgte dato</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="418"/>
        <source>Publishers</source>
        <comment>History Table (column title)</comment>
        <translation>Forkynnere</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="582"/>
        <location filename="../historytable.cpp" line="589"/>
        <source>CBS</source>
        <translation>MBS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="582"/>
        <location filename="../historytable.cpp" line="684"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="589"/>
        <location filename="../historytable.cpp" line="614"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="611"/>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>SK</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="623"/>
        <source>TMS</source>
        <translation>TTS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="750"/>
        <source>SM</source>
        <translation>TM</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="768"/>
        <source>PT</source>
        <comment>abbreviation of &apos;public talk&apos; for history table</comment>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="774"/>
        <source>Ch</source>
        <comment>abbreviation of public talk &apos;chairman&apos; for history table</comment>
        <translation>OS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="783"/>
        <source>WT-R</source>
        <comment>abbreviation of &apos;watchtower reader&apos; for history table</comment>
        <translation>VT-L</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="999"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1000"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1001"/>
        <source>Speaker</source>
        <translation>Foredragsholder</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1002"/>
        <source>Congregation</source>
        <translation>Menighet</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1003"/>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1004"/>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
</context>
<context>
    <name>importwizard</name>
    <message>
        <location filename="../importwizard.cpp" line="40"/>
        <source>Next &gt;</source>
        <translation>Neste &gt;</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="39"/>
        <source>&lt; Back</source>
        <translation>&lt; Tilbake</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="41"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="67"/>
        <source>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Importering av Den Teokratisk Tjenesteskolens pensum. Kopier hele pensumet fra WTLibrary og lim inn under (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="68"/>
        <source>Check schedule</source>
        <translation>Sjekk skoleplan</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="71"/>
        <source>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Importering av leksjoner. Kopier leksjoner fra WTLibrary og lim inn under (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="72"/>
        <source>Check studies</source>
        <translation>Sjekk leksjoner</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="75"/>
        <source>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Importering av situasjoner. Kopier situasjoner fra WTLibrary og lim inn under (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="76"/>
        <source>Check settings</source>
        <translation>Sjekk situasjoner</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="80"/>
        <source>Check subjects</source>
        <translation>Sjekk emner</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="83"/>
        <source>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>Legg til foredragsholdere og menigheter. Kopier alle data til utklippstavlen og lim inn under (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="84"/>
        <source>Check data</source>
        <translation>Sjekk data</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="87"/>
        <source>Add songs. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>Legg til sanger. Markér og kopier sangnummer+tema og lim inn under (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="88"/>
        <source>Check songs</source>
        <translation>Kontroller sangene</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="100"/>
        <source>No schedule to import.</source>
        <translation>Ingen plan funnet for importering.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="141"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="142"/>
        <source>Please add start date YYYY-MM-DD (eg. 2011-01-03)</source>
        <translation>Vennligst legg til startdato ÅÅÅÅ-MM-DD (f.eks. 2014-01-03)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="149"/>
        <source>The date is not first day of week (Monday)</source>
        <translation>Denne datoen er ikke første dag i uken (Mandag)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="202"/>
        <source>Import songs</source>
        <translation>Importer sanger</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="244"/>
        <source>Only brothers</source>
        <translation>Kun brødre</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="474"/>
        <source>Subject</source>
        <translation>Emne</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="517"/>
        <source>id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="520"/>
        <source>Congregation</source>
        <translation>Menighet</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="523"/>
        <source>Speaker</source>
        <translation>Foredragsholder</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="526"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="529"/>
        <source>Public talks</source>
        <translation>Offentlige foredrag</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="532"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="549"/>
        <location filename="../importwizard.cpp" line="550"/>
        <source>First name</source>
        <translation>Fornavn</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="549"/>
        <location filename="../importwizard.cpp" line="550"/>
        <source>Last name</source>
        <translation>Etternavn</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="178"/>
        <source>Import subjects</source>
        <translation>Importer emner</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="179"/>
        <location filename="../importwizard.cpp" line="203"/>
        <source>Choose language</source>
        <translation>Velg språk</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="38"/>
        <source>Save to database</source>
        <translation>Lagre i databasen</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="79"/>
        <source>Add public talk&#x27;s subjects. Copy themes and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>Legg til Offentlige foredrag. Kopier temaer og lim inn under (Ctrl + V / cmd + V).
Nummer skal være i den første kolonnen og tema i den andre.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="233"/>
        <source>date</source>
        <translation>dato</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="236"/>
        <source>number</source>
        <translation>nummer</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="239"/>
        <source>subject</source>
        <translation>emne</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="242"/>
        <source>material</source>
        <translation>Kildemateriale</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="353"/>
        <source>setting</source>
        <translation>situasjon</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="414"/>
        <source>study</source>
        <translation>leksjon</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="600"/>
        <source>Title</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="746"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>Et offentlig foredrag med det samme nummeret er allerede lagret!
Vil du slette den forrige oppføringen?

Planlagte foredrag vil bli flyttet til Oppgavelisten.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="749"/>
        <source>Previous talk: </source>
        <translation>Forrige foredrag: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="750"/>
        <source>New talk: </source>
        <translation>Nytt foredrag: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="871"/>
        <source>Public talk themes not found. Add themes and try again!</source>
        <translation>Foredragstemaer ikke funnet. Legg til temaer og prøv igjen!</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="898"/>
        <source> rows added</source>
        <translation> rader lagt til</translation>
    </message>
</context>
<context>
    <name>lmmtalktypeedit</name>
    <message>
        <source>Tak Type Editor</source>
        <comment>dialog name</comment>
        <translation>Rediger type oppdrag</translation>
    </message>
    <message>
        <source>Continue</source>
        <comment>dialog button name</comment>
        <translation>Fortsett</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="14"/>
        <source>Talk Name in the Workbook</source>
        <translation>Oppdragets navn i arbeidsheftet</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="16"/>
        <source>Meeting Item</source>
        <translation>Møtedel</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="42"/>
        <source>Unknown</source>
        <comment>Unknown talk name</comment>
        <translation>Ukjent</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.ui" line="14"/>
        <source>Talk Type Editor</source>
        <comment>dialog name</comment>
        <translation>Endre type oppdrag</translation>
    </message>
</context>
<context>
    <name>logindialog</name>
    <message>
        <location filename="../logindialog.ui" line="33"/>
        <source>Username</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../logindialog.ui" line="50"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>WEEK STARTING %1</source>
        <translation>UKE STARTER %1</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Midtukemøtet</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Helgemøtet</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="292"/>
        <source>Select an assignment on the left to edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>personsui</name>
    <message>
        <location filename="../personsui.ui" line="366"/>
        <source>Sister</source>
        <translation>Søster</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="390"/>
        <source>Brother</source>
        <translation>Bror</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="342"/>
        <source>Servant</source>
        <translation>Tjener</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="23"/>
        <source>Publishers</source>
        <translation>Forkynnere</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="782"/>
        <source>General</source>
        <translation>Generelt</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="990"/>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1189"/>
        <source>Public talks</source>
        <translation>Offentlige foredrag</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="929"/>
        <location filename="../personsui.ui" line="1182"/>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1162"/>
        <source>Weekend Meeting</source>
        <translation>Helgemøtet</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1246"/>
        <source>Watchtower reader</source>
        <translation>Lese på Vakttårnstudiet</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="414"/>
        <source>Prayer</source>
        <translation>Bønn</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1120"/>
        <source>Cong. Bible Study reader</source>
        <translation>Lese på Menighetsbibelstudiet</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1275"/>
        <source>Meeting for field ministry</source>
        <translation>Lede frammøter</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="542"/>
        <location filename="../personsui.cpp" line="551"/>
        <source>First name</source>
        <translation>Fornavn</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="642"/>
        <location filename="../personsui.cpp" line="550"/>
        <source>Last name</source>
        <translation>Etternavn</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="581"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="481"/>
        <source>E-mail</source>
        <translation>E-post</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1331"/>
        <source>Assignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1448"/>
        <source>Current Study</source>
        <translation>Nåværende leksjon</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1716"/>
        <location filename="../personsui.ui" line="1739"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1212"/>
        <source>Watchtower Study Conductor</source>
        <translation>Vakttårnstudieleder</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="354"/>
        <source>Family Head</source>
        <translation>Familieoverhode</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="426"/>
        <source>Family member linked to</source>
        <translation>I familie med</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="892"/>
        <source>Midweek Meeting</source>
        <translation>Midtukemøtet</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1066"/>
        <source>Only Auxiliary Classes</source>
        <translation>Kun B-sal</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1039"/>
        <source>Only Main Class</source>
        <translation>Kun Hovedsal</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="330"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="953"/>
        <source>Treasures From God&#x27;s Word</source>
        <translation>SKATTER FRA GUDS ORD</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1004"/>
        <source>Initial Call</source>
        <translation>Førstegangsbesøk</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="967"/>
        <source>Bible Reading</source>
        <translation>Bibellesning</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1073"/>
        <source>All Classes</source>
        <translation>Alle skoler</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="997"/>
        <source>Bible Study</source>
        <translation>Bibelstudium</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1113"/>
        <source>Congregation Bible Study</source>
        <translation>Menighetsbibelstudiet</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1106"/>
        <source>Living as Christians Talks</source>
        <translation>Livet som Kristne Taler</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Forbered Denne Månedens Presentasjoner</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="960"/>
        <source>Digging for Spiritual Gems</source>
        <translation>Let etter Åndelige Perler</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1011"/>
        <source>Return Visit</source>
        <translation>Gjenbesøk</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="307"/>
        <source>Personal Info</source>
        <translation>Personinfo</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="378"/>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="709"/>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="751"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1086"/>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1287"/>
        <source>History</source>
        <translation>Historikk</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1321"/>
        <source>date</source>
        <translation>dato</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1326"/>
        <source>no</source>
        <translation>Nr.</translation>
    </message>
    <message>
        <source>material</source>
        <translation>kilde</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1336"/>
        <source>Note</source>
        <translation>Notat</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1341"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1346"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1355"/>
        <source>Studies</source>
        <translation>Leksjoner</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1420"/>
        <source>Study</source>
        <translation>Leksjon</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1425"/>
        <source>Date assigned</source>
        <translation>Dato tildelt</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1430"/>
        <source>Exercises</source>
        <translation>Øvelser</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1435"/>
        <source>Date completed</source>
        <translation>Dato gjennomført</translation>
    </message>
    <message>
        <source>Settings#S</source>
        <comment>for sisters assignment</comment>
        <translation>Situasjoner</translation>
    </message>
    <message>
        <source>setting</source>
        <translation>innstilling</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1615"/>
        <source>Unavailable</source>
        <translation>Utilgjengelig</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1649"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1654"/>
        <source>End</source>
        <translation>Slutt</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="131"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="181"/>
        <location filename="../personsui.ui" line="1585"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="382"/>
        <source>A person with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>En person med samme navn er allerede oppført: &apos;%1&apos;. Vil du endre navnet?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="601"/>
        <source>Remove student?</source>
        <translation>Fjerne elev?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="578"/>
        <source>Remove student</source>
        <translation>Fjern elev</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="405"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove him as speaker.
Remove him as speaker?</source>
        <translation>%1 er satt opp med offentlige foredrag! Disse foredragene vil
bli flyttet til Oppgavelisten om du fjerner ham som foredragsholder.
Vil du fjerne ham som foredragsholder? </translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="592"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove the student.</source>
        <translation>%1 er satt opp med offentlige foredrag!
Disse foredragene vil bli flyttet til Oppgavelisten om du fjerner eleven.</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="1077"/>
        <source>Remove study</source>
        <translation>Fjern leksjon</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="1098"/>
        <source>Copy to the clipboard</source>
        <translation>Kopier til utklippstavlen</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1101"/>
        <source>Copy</source>
        <translation>Kopier</translation>
    </message>
    <message>
        <source>Assignment slip for assistant</source>
        <translation>Oppdragslapp til partneren</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="235"/>
        <location filename="../printui.ui" line="326"/>
        <source>Schedule</source>
        <translation>Skoleplan</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="14"/>
        <source>Print</source>
        <translation>Skriv ut</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="390"/>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="751"/>
        <source>Midweek Meeting Title</source>
        <translation>Midtukemøtets tittel</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="732"/>
        <source>Weekend Meeting Title</source>
        <translation>Helgemøtets tittel</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Vis klokkeslett</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Via varighet</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="674"/>
        <source>Show Section Titles</source>
        <translation>Vis navn på møtedelene</translation>
    </message>
    <message>
        <source>Generate QR Code
and upload file to TheocBase Cloud</source>
        <translation>Generer QR kode
og last den opp til TheocBase Skyen</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="799"/>
        <source>Review/Preview/Announcements Title</source>
        <comment>See S-140</comment>
        <translation>Repetisjon/Kort oversikt/Opplysninger</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="780"/>
        <source>Opening Comments Title</source>
        <comment>See S-140</comment>
        <translation>Innledende kommentarer</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="258"/>
        <source>Assignment Slips for Assistants</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="361"/>
        <source>Outgoing Speakers Schedules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="377"/>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="403"/>
        <source>Talks List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="545"/>
        <source>Map and Addresses Sheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="681"/>
        <source>Show duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="704"/>
        <source>Show time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="806"/>
        <source>Show Workbook Issue no.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="813"/>
        <source>Show Watchtower Issue no.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="957"/>
        <source>Territory number(s)</source>
        <translation>Distriktsnummer</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="964"/>
        <source>Comma delimited; press Enter to refresh</source>
        <translation>Kommaavgrenset; trykk Enter for å laste på nytt</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="970"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <source>Map and address sheets</source>
        <translation>Kart- og adresseliste</translation>
    </message>
    <message>
        <source>Territory number</source>
        <translation>Distriktsnummer</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="522"/>
        <source>Territory Record</source>
        <translation>Distriktshistorikk</translation>
    </message>
    <message>
        <source>Territory Card</source>
        <translation>Distriktskort</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="859"/>
        <location filename="../printui.cpp" line="2232"/>
        <source>Template</source>
        <translation>Mal</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="885"/>
        <source>Paper Size</source>
        <translation>Papirstørrelse</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="947"/>
        <source>Print From Date</source>
        <translation>Skriv ut fra Dato</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="905"/>
        <source>Print Thru Date</source>
        <translation>Skriv ut til og med dato</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="778"/>
        <location filename="../printui.cpp" line="1302"/>
        <source>Slip Template</source>
        <translation>Mal Oppdragslapp</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1028"/>
        <source>Printing</source>
        <translation>Skriver ut</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1031"/>
        <location filename="../printui.ui" line="1066"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="248"/>
        <source>Assignment Slips</source>
        <translation>Oppdragslapp</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="767"/>
        <source>Share in Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="278"/>
        <location filename="../printui.ui" line="345"/>
        <source>Worksheets</source>
        <translation>Arbeidsark</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="565"/>
        <source>Meetings for field ministry</source>
        <translation>Frammøte til felttjeneste</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="423"/>
        <source>Combination</source>
        <translation>Kombinasjon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2529"/>
        <location filename="../printui.cpp" line="2988"/>
        <source>Prayer</source>
        <translation>Bønn</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1767"/>
        <location filename="../printui.cpp" line="1841"/>
        <location filename="../printui.cpp" line="2538"/>
        <source>Congregation Bible Study</source>
        <translation>Menighetsbibelstudiet</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1842"/>
        <source>Theocratic Ministry School</source>
        <translation>Den Teokratiske Tjenesteskolen</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1843"/>
        <source>Service Meeting</source>
        <translation>Tjenestemøtet</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="632"/>
        <source>Additional Options</source>
        <translation>Tilleggsvalg</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="667"/>
        <source>Show Song Titles</source>
        <translation>Vis sangtitler</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="660"/>
        <source>Show Counsel Text</source>
        <translation>Vis leksjonens tittel</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="730"/>
        <location filename="../printui.cpp" line="1678"/>
        <location filename="../printui.cpp" line="1765"/>
        <location filename="../printui.cpp" line="1839"/>
        <location filename="../printui.cpp" line="2980"/>
        <source>Public Talk</source>
        <translation>Offentlig foredrag</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="474"/>
        <source>Outgoing Speakers Schedule</source>
        <translation>Oversikt over foredragsholdere som besøker annen menighet</translation>
    </message>
    <message>
        <source>Outgoing Speaker Assignments</source>
        <translation>Foredrag som skal holdes i andre menigheter</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="268"/>
        <source>Print assigned only</source>
        <translation>Skriv kun ut tildelte oppgaver</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="385"/>
        <source>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</source>
        <translation>Kopiert til utklippstavlen. Sett inn i tekstbehandlingsprogram (Ctrl+V/Cmd+V)</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="431"/>
        <location filename="../printui.cpp" line="457"/>
        <location filename="../printui.cpp" line="546"/>
        <source>file created</source>
        <translation>fil generert</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2550"/>
        <location filename="../printui.cpp" line="2551"/>
        <location filename="../printui.cpp" line="2552"/>
        <location filename="../printui.cpp" line="2845"/>
        <source>Class</source>
        <translation>Klasse</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="396"/>
        <location filename="../printui.cpp" line="442"/>
        <location filename="../printui.cpp" line="473"/>
        <location filename="../printui.cpp" line="526"/>
        <source>Save file</source>
        <translation>Lagre fil</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2545"/>
        <source>Exercises</source>
        <translation>Øvelser</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="494"/>
        <source>Territories</source>
        <translation>Distrikt</translation>
    </message>
    <message>
        <source>No meeting</source>
        <translation>Ikke møte</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1769"/>
        <location filename="../printui.cpp" line="1784"/>
        <location filename="../printui.cpp" line="3175"/>
        <location filename="../printui.cpp" line="3203"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1700"/>
        <location filename="../printui.cpp" line="3174"/>
        <source>Outgoing Speakers</source>
        <translation>Foredragsholdere eksternt</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3178"/>
        <source>Theme Number</source>
        <translation>Tema Nummer</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="728"/>
        <location filename="../printui.cpp" line="1670"/>
        <location filename="../printui.cpp" line="1759"/>
        <location filename="../printui.cpp" line="1837"/>
        <location filename="../printui.cpp" line="3031"/>
        <location filename="../printui.cpp" line="3179"/>
        <location filename="../printui.cpp" line="3197"/>
        <source>Congregation</source>
        <translation>Menighet</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1674"/>
        <location filename="../printui.cpp" line="1764"/>
        <location filename="../printui.cpp" line="1781"/>
        <location filename="../printui.cpp" line="2533"/>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1672"/>
        <location filename="../printui.cpp" line="1762"/>
        <location filename="../printui.cpp" line="1779"/>
        <location filename="../printui.cpp" line="2530"/>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="731"/>
        <location filename="../printui.cpp" line="1679"/>
        <location filename="../printui.cpp" line="1766"/>
        <location filename="../printui.cpp" line="1840"/>
        <location filename="../printui.cpp" line="2982"/>
        <source>Watchtower Study</source>
        <translation>Vakttårnstudiet</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="700"/>
        <source>Class </source>
        <translation>Skole </translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="732"/>
        <location filename="../printui.cpp" line="2535"/>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Skatter fra Guds Ord</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="733"/>
        <location filename="../printui.cpp" line="2536"/>
        <source>Apply Yourself to the Field Ministry</source>
        <translation>Opplæring i Forkynnelsen</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="734"/>
        <location filename="../printui.cpp" line="2537"/>
        <source>Living as Christians</source>
        <translation>Vårt Liv som Kristne</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1174"/>
        <source>Click the print button to preview assignment slips</source>
        <translation>Klikk på utskriftsknappen for å forhåndsvise oppdragslapper</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1174"/>
        <source>Print button is found at the bottom right corner</source>
        <translation>Utskriftsknapp finner du nederst i høyre hjørne</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1905"/>
        <source>Territory Assignment Record</source>
        <translation>Distriktsfortegnelser</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1906"/>
        <source>Territory Coverage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3194"/>
        <source>Addresses</source>
        <comment>Addresses included in the territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3208"/>
        <source>From</source>
        <comment>From number; in number range</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3220"/>
        <source>Map</source>
        <comment>Map of a territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3227"/>
        <source>Territory</source>
        <translation>Distrikt</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3228"/>
        <source>Terr. No.</source>
        <translation>Distriktnr.</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1907"/>
        <source>Total number of territories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3219"/>
        <source>Locality</source>
        <translation>Sted</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3229"/>
        <source>Territory type</source>
        <translation>Distriktstype</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3223"/>
        <source>Name of publisher</source>
        <translation>Forkynnerens navn</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3200"/>
        <source>Date checked out</source>
        <translation>Dato utlevert</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3201"/>
        <source>Date checked back in</source>
        <translation>Dato innlevert</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3202"/>
        <source>Date last worked</source>
        <translation>Sist gjennomarbeidet</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3196"/>
        <source>Assigned to</source>
        <translation>Tildelt til</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3224"/>
        <source>Remark</source>
        <translation>Anmerkning</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1908"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1909"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1910"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1911"/>
        <source>Average per year</source>
        <comment>Number of times territory has been worked per year on average</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3193"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3205"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3225"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Fylke</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3206"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>Kommune</translation>
    </message>
    <message>
        <source>DistrictSublocality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>City</source>
        <comment>Locality</comment>
        <translation>By</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3207"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Bydel</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3226"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>Gate</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3218"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>Nr.</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3222"/>
        <source>Postalcode</source>
        <comment>Mail code, ZIP</comment>
        <translation>Postnummer</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3221"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3195"/>
        <source>Address type</source>
        <translation>Type adresse</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2157"/>
        <source>This Territory Map Card could not be converted to a JPG file</source>
        <translation>Distriktskartet kunne ikke bli konvertert til en JPG-fil</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1081"/>
        <source>Study %1</source>
        <comment>Text for study point on slip</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1770"/>
        <source>Phone</source>
        <comment>Phone number title</comment>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1771"/>
        <source>Host</source>
        <comment>Host for incoming public speaker</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2054"/>
        <source>Territory Map Card</source>
        <comment>Title tag for a S-12 or similar card</comment>
        <translation>Distriktskort</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2055"/>
        <source>Territory Map</source>
        <comment>Title tag for a sheet with a territory map</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2056"/>
        <source>Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s address list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2057"/>
        <source>Territory Map with Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and address list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2058"/>
        <source>Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2059"/>
        <source>Territory Map with Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2060"/>
        <source>Do-Not-Call List</source>
        <comment>Title tag for a sheet with a territory&apos;s Do-Not-Call list</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="278"/>
        <source>qrc:/qml/CongregationMap.qml</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.cpp" line="2540"/>
        <location filename="../printui.cpp" line="3069"/>
        <source>No regular meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2544"/>
        <source>Source</source>
        <comment>Source information from workbook</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2553"/>
        <source>Auxiliary Classroom Counselor</source>
        <comment>See S-140</comment>
        <translation>Veileder i B-sal</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2554"/>
        <source>Auxiliary Classroom</source>
        <comment>See S-140</comment>
        <translation>B-sal</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2555"/>
        <source>Main Hall</source>
        <comment>See S-140</comment>
        <translation>Hovedsal</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2556"/>
        <source>MH</source>
        <comment>Abbreviation for &apos;Main Hall&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2557"/>
        <source>A</source>
        <comment>Abbreviation for &apos;Auxiliary Classroom&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2558"/>
        <source>Student</source>
        <comment>See S-140</comment>
        <translation>Elev</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2561"/>
        <source>Review/Preview/Announcements</source>
        <comment>Customizable title for RPA notes</comment>
        <translation>Repetisjon/Kort oversikt/Opplysninger</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2562"/>
        <source>Today</source>
        <translation>I dag</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2563"/>
        <source>Next week</source>
        <translation>Neste uke</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3182"/>
        <source>NOTE: Dear brother, in spite of careful database-maintenance, sometimes times or addresses might be out of date. So, please verify by looking those up via JW.ORG. Thank you!</source>
        <translation>OBS: Kjære bror, på tross av nøye vedlikehold av databasen, kan det av og til forekomme at møtetider eller adresser ikke er helt oppdatert. Kontroller at alt stemmer ved å sammenligne med det som står på JW.ORG. Takk!</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1677"/>
        <location filename="../printui.cpp" line="1755"/>
        <source>Public Meeting</source>
        <translation>Offentlig møte</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="368"/>
        <source>Custom...</source>
        <comment>pick custom paper size</comment>
        <translation>Tilpassede...</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="727"/>
        <location filename="../printui.cpp" line="1669"/>
        <location filename="../printui.cpp" line="1758"/>
        <location filename="../printui.cpp" line="1836"/>
        <location filename="../printui.cpp" line="3030"/>
        <location filename="../printui.cpp" line="3199"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>%1 Menighet</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1254"/>
        <source>Converting %1 to JPG file</source>
        <translation>Konverterer til %1 JPG fil</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1659"/>
        <source>Select at least one option</source>
        <translation>Velg minst ett alternativ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="207"/>
        <location filename="../printui.ui" line="461"/>
        <location filename="../printui.cpp" line="1680"/>
        <location filename="../printui.cpp" line="2524"/>
        <location filename="../printui.cpp" line="3488"/>
        <source>Midweek Meeting</source>
        <translation>Midtukemøtet</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1681"/>
        <location filename="../printui.cpp" line="1682"/>
        <location filename="../printui.cpp" line="1768"/>
        <location filename="../printui.cpp" line="2534"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>Tjenestemøtet</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1697"/>
        <location filename="../printui.cpp" line="1698"/>
        <source>Combined Schedule</source>
        <translation>Kombinert plan</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1782"/>
        <source>WT Reader</source>
        <translation>VT oppleser</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1785"/>
        <location filename="../printui.cpp" line="3177"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2525"/>
        <source>Worksheet</source>
        <translation>Arbeidsheftet</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Repetisjon/Kort oversikt/Opplysninger</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2539"/>
        <location filename="../printui.cpp" line="2983"/>
        <source>Circuit Overseer</source>
        <translation>Kretstilsynsmannen</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2541"/>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Study</source>
        <comment>title for study point</comment>
        <translation>Leksjon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2547"/>
        <source>Notes</source>
        <translation>Notater</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3230"/>
        <source>To</source>
        <comment>To number; in number range</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3231"/>
        <source>Type</source>
        <comment>Type of something</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3232"/>
        <source>Sum</source>
        <comment>Total amount</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3244"/>
        <source>Service Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3253"/>
        <source>Begins at</source>
        <comment>Used in print template, example &apos;Begins at 11:00&apos;</comment>
        <translation>Begynner kl.</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3497"/>
        <source>Closing Comments</source>
        <translation>Avsluttende kommentarer</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3575"/>
        <source>Can&#x27;t read file</source>
        <comment>cannot read printing template</comment>
        <translation>Kan ikke lese filen</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3836"/>
        <source>New Custom Paper Size</source>
        <comment>title of dialog box</comment>
        <translation>Ny selvvalgt papirstørrelse</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3836"/>
        <source>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</source>
        <translation>Format: Bredde X Høyde. Bredde og høyde kan være i tommer eller mm. Eksempel 210mm x 297mm</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2549"/>
        <source>Setting</source>
        <comment>for sisters assignment</comment>
        <translation>Situasjon</translation>
    </message>
    <message>
        <source>Print button is found at the bottom left corner</source>
        <translation>Utskriftsknappen er nederst i venstre hjørne</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1242"/>
        <source>Unable to find information in this template: </source>
        <translation>Ikke i stand til å finne informasjon i denne malen: </translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1108"/>
        <location filename="../printui.cpp" line="1525"/>
        <source>2nd talk</source>
        <comment>When printing slips: if the first talk is not &apos;Return Visit&apos;</comment>
        <translation>2. oppdrag</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1620"/>
        <source>Slips created at %1*.pdf</source>
        <translation>Oppdragslapper laget i %1.pdf</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1673"/>
        <location filename="../printui.cpp" line="2531"/>
        <source>Counselor</source>
        <translation>Veileder</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1676"/>
        <location filename="../printui.cpp" line="1763"/>
        <source>Speaker</source>
        <comment>Public talk speaker</comment>
        <translation>Foredragsholder</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="298"/>
        <location filename="../printui.ui" line="448"/>
        <location filename="../printui.cpp" line="574"/>
        <location filename="../printui.cpp" line="1699"/>
        <location filename="../printui.cpp" line="1754"/>
        <location filename="../printui.cpp" line="3489"/>
        <source>Weekend Meeting</source>
        <translation>Helgemøtet</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1711"/>
        <source>Week Starting %1</source>
        <comment>%1 is Monday of the week</comment>
        <translation>Ukestart %1</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3496"/>
        <source>Opening Comments</source>
        <translation>Innledende kommentarer</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2528"/>
        <location filename="../printui.cpp" line="2991"/>
        <source>Song</source>
        <translation>Sang</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3066"/>
        <source>Watchtower Conductor</source>
        <translation>Vakttårnstudieleder</translation>
    </message>
    <message>
        <source>No Meeting</source>
        <translation>Ikke møte</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3173"/>
        <source>Talk</source>
        <translation>Tale</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1675"/>
        <location filename="../printui.cpp" line="1761"/>
        <location filename="../printui.cpp" line="2532"/>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3570"/>
        <source>Template not found</source>
        <comment>printing template not found</comment>
        <translation>Mal ikke funnet</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2842"/>
        <source>Other schools</source>
        <translation>Andre skoler</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tidtakning</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2548"/>
        <source>Next study</source>
        <translation>Neste leksjon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3180"/>
        <source>Start Time</source>
        <translation>Starttid</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3896"/>
        <source>Width unit does not match height unit</source>
        <comment>while asking for custom paper size</comment>
        <translation>Breddevalg passer ikke sammen med høydevalg</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3924"/>
        <source>Invalid entry, sorry.</source>
        <comment>while asking for custom paper size</comment>
        <translation>Ugyldig valg, beklager.</translation>
    </message>
    <message>
        <source>buttonGroup_2</source>
        <translation>buttonGroup_2</translation>
    </message>
    <message>
        <source>buttonGroup</source>
        <translation>buttonGroup</translation>
    </message>
</context>
<context>
    <name>publicmeeting_controller</name>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="202"/>
        <source>From %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="230"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="235"/>
        <source>Swap Talks</source>
        <comment>Button text</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="236"/>
        <source>Cancel</source>
        <comment>Button text</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>publictalkedit</name>
    <message>
        <location filename="../publictalkedit.ui" line="67"/>
        <source>Public Talk</source>
        <translation>Offentlig foredrag</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="257"/>
        <location filename="../publictalkedit.ui" line="592"/>
        <location filename="../publictalkedit.ui" line="681"/>
        <location filename="../publictalkedit.ui" line="881"/>
        <location filename="../publictalkedit.cpp" line="47"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="169"/>
        <location filename="../publictalkedit.ui" line="866"/>
        <location filename="../publictalkedit.cpp" line="48"/>
        <source>Speaker</source>
        <translation>Foredragsholder</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="144"/>
        <location filename="../publictalkedit.ui" line="328"/>
        <location filename="../publictalkedit.cpp" line="50"/>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="232"/>
        <location filename="../publictalkedit.ui" line="617"/>
        <location filename="../publictalkedit.ui" line="642"/>
        <source>Song</source>
        <translation>Sang</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="411"/>
        <source>Watchtower Study</source>
        <translation>Vakttårnstudiet</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="472"/>
        <location filename="../publictalkedit.cpp" line="52"/>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="542"/>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="285"/>
        <location filename="../publictalkedit.ui" line="932"/>
        <source>Move to different week</source>
        <translation>Flytt til en annen uke</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="118"/>
        <location filename="../publictalkedit.ui" line="906"/>
        <source>Send to To Do List</source>
        <translation>Send til Oppgaveliste</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="311"/>
        <source>Clear Public Talk selections</source>
        <comment>Clear=Delete</comment>
        <translation>Fjern valg i Offentlige Foredrag</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="567"/>
        <location filename="../publictalkedit.ui" line="658"/>
        <location filename="../publictalkedit.cpp" line="51"/>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="497"/>
        <location filename="../publictalkedit.ui" line="671"/>
        <source>Service Talk</source>
        <translation>Tjenestetale</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="759"/>
        <source>Outgoing Speakers This Weekend</source>
        <translation>Holder foredrag eksternt denne helgen</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="833"/>
        <source>Add to Outgoing List</source>
        <translation>Legg til i utgående liste</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="876"/>
        <source>Start time</source>
        <translation>Starttid</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="886"/>
        <source>Theme No.</source>
        <translation>Tema nr.</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="979"/>
        <source>To Do List</source>
        <translation>Oppgaveliste</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1019"/>
        <source>Add item to schedule</source>
        <translation>Legg oppføringen til i planen</translation>
    </message>
    <message>
        <source>Add Outgoing To Do item</source>
        <comment>Add OUT item</comment>
        <translation>Legg til utgående oppgave</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="793"/>
        <location filename="../publictalkedit.ui" line="1045"/>
        <source>Remove item</source>
        <translation>Fjern oppføring</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1071"/>
        <source>Add Incoming To Do item</source>
        <translation>Legg til innkommende oppgave</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="207"/>
        <location filename="../publictalkedit.ui" line="871"/>
        <location filename="../publictalkedit.cpp" line="49"/>
        <source>Congregation</source>
        <translation>Menighet</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="89"/>
        <source>Watchtower Study Edition</source>
        <translation>Vakttårnet Studieutgave</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="255"/>
        <location filename="../publictalkedit.cpp" line="290"/>
        <source>From %1; speaker removed</source>
        <comment>From [scheduled date]; speaker removed</comment>
        <translation>Fra %1; foredragsholder fjernet</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="293"/>
        <source>From %1; speaker moved to %2</source>
        <comment>From [scheduled date]; speaker moved to [new congregation]</comment>
        <translation>Fra %1; foredragsholder flyttet til %2</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="339"/>
        <location filename="../publictalkedit.cpp" line="374"/>
        <source>From %1; talk discontinued</source>
        <comment>From [scheduled date]; talk discontinued</comment>
        <translation>Fra %1; foredrag utgått</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="694"/>
        <source>In</source>
        <comment>Incoming</comment>
        <translation>Inn</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="694"/>
        <source>Out</source>
        <comment>Outgoing</comment>
        <translation>Ut</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="816"/>
        <source>The selected speaker has already public talk on this calendar month. Do you want to add?</source>
        <translation>Den valgte taleren har allerede et offentlig foredrag i denne måneden. Vil du legge til?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="944"/>
        <location filename="../publictalkedit.cpp" line="995"/>
        <location filename="../publictalkedit.cpp" line="1035"/>
        <source>The destination date already has a talk scheduled. What to do?</source>
        <translation>Den valgte datoen har allerede oppført et foredrag. Hva vil du gjøre?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="945"/>
        <location filename="../publictalkedit.cpp" line="996"/>
        <source>&amp;Swap Talks</source>
        <translation>&amp;Bytt Foredrag</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="946"/>
        <location filename="../publictalkedit.cpp" line="997"/>
        <location filename="../publictalkedit.cpp" line="1036"/>
        <source>&amp;Move other talk to To Do List</source>
        <translation>&amp;Flytt det andre foredraget til oppgavelisten</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="947"/>
        <location filename="../publictalkedit.cpp" line="998"/>
        <location filename="../publictalkedit.cpp" line="1037"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="964"/>
        <location filename="../publictalkedit.cpp" line="1010"/>
        <location filename="../publictalkedit.cpp" line="1046"/>
        <location filename="../publictalkedit.cpp" line="1055"/>
        <location filename="../publictalkedit.cpp" line="1077"/>
        <location filename="../publictalkedit.cpp" line="1110"/>
        <source>From %1</source>
        <translation>Fra %1</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1033"/>
        <source>Date Already Scheduled</source>
        <translation>Dato allerede planlagt</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1064"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1064"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>Kan ikke tildele denne oppgaven før disse feltene er fikset: %1</translation>
    </message>
</context>
<context>
    <name>reminders</name>
    <message>
        <location filename="../reminders.ui" line="14"/>
        <source>Reminders</source>
        <translation>Påminnelser</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="58"/>
        <source>Date range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="159"/>
        <source>Send selected reminders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="177"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="195"/>
        <source>To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="247"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="298"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="303"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="313"/>
        <source>Subject</source>
        <translation>Emne</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="318"/>
        <source>Message</source>
        <translation>Beskjed</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="308"/>
        <source>E-Mail</source>
        <translation>E-post</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="95"/>
        <source>Email sending...</source>
        <translation>Sender e-post...</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="113"/>
        <source>Error sending e-mail</source>
        <translation>Feil ved sending av e-post</translation>
    </message>
    <message>
        <source>Send</source>
        <comment>Email reminder</comment>
        <translation>Send</translation>
    </message>
    <message>
        <source>Resend</source>
        <comment>Email reminder</comment>
        <translation>Send på nytt</translation>
    </message>
</context>
<context>
    <name>schoolreminder</name>
    <message>
        <location filename="../schoolreminder.cpp" line="57"/>
        <source>Chairman</source>
        <translation>Ordstyrer</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="60"/>
        <source>Counselor-Class II</source>
        <translation>Veileder klasse B</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="63"/>
        <source>Counselor-Class III</source>
        <translation>Veileder klasse C</translation>
    </message>
    <message>
        <source>Opening Prayer</source>
        <translation>Innledende bønn</translation>
    </message>
    <message>
        <source>Concluding Prayer</source>
        <translation>Avsluttende bønn</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="66"/>
        <source>Prayer I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="69"/>
        <source>Prayer II</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="161"/>
        <source>Cancellation - Our Christian Life and Ministry Meeting Assignment</source>
        <translation>Avlysning - Elevoppdrag på tjenestemøtet</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="161"/>
        <source>Our Christian Life and Ministry Meeting Assignment</source>
        <translation>Elevoppdrag på tjenestemøtet</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="181"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="183"/>
        <source>Assignment</source>
        <translation>Oppdrag</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="186"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="188"/>
        <source>Source Material</source>
        <translation>Kildemateriale</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="202"/>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="212"/>
        <location filename="../schoolreminder.cpp" line="214"/>
        <source>Study</source>
        <translation>Leksjon</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="219"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>Oppleser for menighetsbibelstudiet</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="221"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>Partner for %1</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="226"/>
        <source>Cancellation</source>
        <translation>Avlysning</translation>
    </message>
</context>
<context>
    <name>schoolresult</name>
    <message>
        <location filename="../schoolresult.ui" line="23"/>
        <source>Volunteer</source>
        <translation>Frivillig</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="33"/>
        <source>Study</source>
        <translation>Leksjon</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="45"/>
        <source>Exercise Completed</source>
        <translation>Øvelse utført</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="52"/>
        <source>Next study:</source>
        <translation>Neste leksjon:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="69"/>
        <source>Setting:</source>
        <comment>for sisters assignment</comment>
        <translation>Situasjon:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="96"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="130"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="137"/>
        <source>Completed</source>
        <translation>Fullført</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="151"/>
        <source>Timing:</source>
        <translation>Tid:</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="59"/>
        <source>Bible highlights</source>
        <translation>Viktige punkter fra ukens Bibellesing</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="61"/>
        <source>reading</source>
        <translation>lesning</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="65"/>
        <source>Assignment result</source>
        <translation>Oppgave resultat</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="130"/>
        <source>Do not assign the next study</source>
        <translation>Ikke tildel neste leksjon</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="159"/>
        <location filename="../schoolresult.cpp" line="369"/>
        <source>Leave on current study</source>
        <translation>Behold på nåværende leksjon</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="239"/>
        <source>Select setting</source>
        <translation>Velg situasjon</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="246"/>
        <source>Timing</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="247"/>
        <source>The timing is empty. Save?</source>
        <translation>Tid ikke angitt. Lagre?</translation>
    </message>
</context>
<context>
    <name>schoolui</name>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <location filename="../schoolui.cpp" line="36"/>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="41"/>
        <source>Class</source>
        <translation>Klasse</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="111"/>
        <source>Bible highlights:</source>
        <translation>Viktige punkter fra ukens Bibellesing</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="115"/>
        <source>No. 1:</source>
        <translation>Nr 1</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="119"/>
        <source>No. 2:</source>
        <translation>Nr 2</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="123"/>
        <source>No. 3:</source>
        <translation>Nr 3</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="127"/>
        <source>Reader:</source>
        <translation>Oppleser:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="176"/>
        <source>No school</source>
        <translation>Ingen skole</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="329"/>
        <source>Nothing to display</source>
        <translation>Ikke noe å vise</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="495"/>
        <source>No assignment has been made!</source>
        <translation>Ingen oppgaver er tildelt!</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="500"/>
        <source>Please import new school schedule from Watchtower Library (Settings-&gt;Theocratic Ministry School...)</source>
        <translation>Vennligst importer ny skoleplan fra Watchtower Library (Innstillinger-&gt;Den Teokratiske Tjenesteskolen...)</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="542"/>
        <source>Show Details...</source>
        <translation>Vis detaljer...</translation>
    </message>
</context>
<context>
    <name>servicemeetingui</name>
    <message>
        <source>Song</source>
        <translation>Sang</translation>
    </message>
    <message>
        <source>min</source>
        <comment>Abbreviation of minutes</comment>
        <translation>min</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Bønn</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="14"/>
        <source>Congregations and Speakers</source>
        <translation>Menigheter og foredragsholdere</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="400"/>
        <source>Congregation...</source>
        <translation>Menighet...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="371"/>
        <source>Speaker...</source>
        <translation>Foredragsholder...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1332"/>
        <source>Toggle Talks Editable</source>
        <translation>Vis alle tilgjengelige foredrag</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1300"/>
        <source>Add Multiple Talks</source>
        <translation>Legg til flere foredrag</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="95"/>
        <location filename="../speakersui.ui" line="136"/>
        <location filename="../speakersui.ui" line="180"/>
        <location filename="../speakersui.ui" line="246"/>
        <location filename="../speakersui.ui" line="1303"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="494"/>
        <source>Select a Congregation or Speaker</source>
        <translation>Velg en Menighet eller Foredragsholder</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="725"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="792"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="865"/>
        <location filename="../speakersui.cpp" line="171"/>
        <source>Circuit</source>
        <translation>Krets</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="667"/>
        <location filename="../speakersui.ui" line="1685"/>
        <location filename="../speakersui.cpp" line="179"/>
        <source>Congregation</source>
        <translation>Menighet</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="92"/>
        <source>Speakers</source>
        <translation>Foredragsholdere</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="133"/>
        <source>Group by congregation</source>
        <translation>Sortér etter menighet</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="177"/>
        <source>Group by circuit</source>
        <translation>Sortér etter krets</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="221"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="243"/>
        <source>Configure Filter</source>
        <translation>Konfigurer filter</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="882"/>
        <source>Congregation Details</source>
        <translation>Info om menighet</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="894"/>
        <source>Meeting Times</source>
        <translation>Møtetider</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="999"/>
        <location filename="../speakersui.ui" line="1169"/>
        <source>Mo</source>
        <translation>Man</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1004"/>
        <location filename="../speakersui.ui" line="1174"/>
        <source>Tu</source>
        <translation>Tir</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1009"/>
        <location filename="../speakersui.ui" line="1179"/>
        <source>We</source>
        <translation>Ons</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1014"/>
        <location filename="../speakersui.ui" line="1184"/>
        <source>Th</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1019"/>
        <location filename="../speakersui.ui" line="1189"/>
        <source>Fr</source>
        <translation>Fre</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1024"/>
        <location filename="../speakersui.ui" line="1194"/>
        <source>Sa</source>
        <translation>Lør</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1029"/>
        <location filename="../speakersui.ui" line="1199"/>
        <source>Su</source>
        <translation>Søn</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1363"/>
        <source>Personal Info</source>
        <translation>Personinfo</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1786"/>
        <location filename="../speakersui.cpp" line="833"/>
        <source>First Name</source>
        <translation>Fornavn</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1838"/>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1576"/>
        <location filename="../speakersui.cpp" line="834"/>
        <source>Last Name</source>
        <translation>Etternavn</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1540"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1377"/>
        <source>Public Talks</source>
        <translation>Offentlige foredrag</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1646"/>
        <source>E-mail</source>
        <translation>E-post</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1467"/>
        <source>Notes</source>
        <translation>Notater</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="187"/>
        <source>Speaker</source>
        <translation>Foredragsholder</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="361"/>
        <location filename="../speakersui.cpp" line="541"/>
        <source>Undefined</source>
        <translation>Udefinert</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="444"/>
        <location filename="../speakersui.cpp" line="449"/>
        <source>%1 Meeting Day/Time</source>
        <translation>%1 Møte Dag/Tid</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="574"/>
        <source>A speaker with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>En foredragsholder med det samme navnet eksisterer allerede: &apos;%1&apos;. Vil du endre navnet?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="706"/>
        <source>The congregation has speakers!</source>
        <translation>Menigheten har foredragsholdere!</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="711"/>
        <source>Remove the congregation?</source>
        <translation>Fjerne Menigheten?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="757"/>
        <source>Remove the speaker?</source>
        <translation>Fjerne foredragsholderen?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="762"/>
        <source>The speaker is scheduled for talks! These talks will
be moved to the To Do List if you remove the speaker.</source>
        <translation>Foredragsholderen er satt opp med foredrag! Disse foredragene
vil bli flyttet til Oppgavelisten om du fjerner foredragsholderen.</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="826"/>
        <source>Missing Information</source>
        <translation>Manglende opplysninger</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="826"/>
        <source>Select congregation first</source>
        <translation>Velg først menighet</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="859"/>
        <source>New Congregation</source>
        <translation>Ny menighet</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="927"/>
        <source>Add Talks</source>
        <translation>Legg til foredrag</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="927"/>
        <source>Enter talk numbers separated by commas or periods</source>
        <translation>Skriv inn foredragsnummer, skill med komma eller punktum</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="963"/>
        <source>Change congregation to &#x27;%1&#x27;?</source>
        <translation>Endre menigheten til &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="965"/>
        <source>The speaker is scheduled for outgoing talks. These talks will
be moved to the To Do List if you change the congregation.</source>
        <translation>Foredragsholderen er satt opp med foredrag eksternt. Disse foredragene
vil bli flyttet til Oppgavelisten om du endrer menighet.</translation>
    </message>
</context>
<context>
    <name>startup</name>
    <message>
        <location filename="../startup.ui" line="35"/>
        <source>Start Page</source>
        <translation>Startside</translation>
    </message>
</context>
<context>
    <name>study</name>
    <message>
        <source>Song</source>
        <translation>Sang</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Bønn</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleder</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Oppleser</translation>
    </message>
</context>
<context>
    <name>sync_cloud</name>
    <message>
        <location filename="../sync_cloud.cpp" line="240"/>
        <source>Version conflict: The cloud changes have been made with a newer version!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sync_cloud.cpp" line="246"/>
        <source>Version conflict: The cloud data needs to be updated with the same version by an authorized user.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>territorymanagement</name>
    <message>
        <location filename="../territorymanagement.ui" line="14"/>
        <source>Territories</source>
        <translation>Distrikter</translation>
    </message>
</context></TS>