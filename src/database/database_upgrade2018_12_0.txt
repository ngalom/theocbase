﻿DROP VIEW publicmeetinghistory;
#okNextError Any
alter table publicmeeting add hospitality_id int;
CREATE VIEW publicmeetinghistory AS SELECT DISTINCT m.date AS weekof, date(m.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') AS mtg_date, m.theme_id AS theme_id, pt.theme_number, pt.theme_name AS theme, m.speaker_id AS speaker_id, m.chairman_id AS chairman_id, m.wtreader_id AS wtreader_id, m.wt_conductor_id, m.wt_source, m.wt_theme, m.final_talk, m.song_pt, m.song_wt_start, m.song_wt_end, m.hospitality_id, m.id FROM publicmeeting m LEFT JOIN publictalks pt ON m.theme_id = pt.id LEFT JOIN congregationmeetingtimes cmt ON cmt.congregation_id = (SELECT value FROM settings WHERE name = 'congregation_id') AND strftime('%Y', m.date) = cmt.mtg_year;
#renamecolumn persons.phone2 = mobile
#okNextError Any
INSERT INTO languages (id,language,desc,code, uuid) SELECT (SELECT MAX(id)+1 FROM languages),'Guadeloupean Creole','Kréyòl Gwadloup (Guadeloupean Creole)','gcf','08af6161-9ffa-75bc-5308-1f8c2b4ddf34' WHERE NOT EXISTS (SELECT 1 FROM languages WHERE code = 'gcf')
