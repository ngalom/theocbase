DROP TABLE IF EXISTS territory_street
CREATE TABLE territory_street (id INTEGER PRIMARY KEY, territory_id INTEGER, street_name TEXT, from_number TEXT, to_number TEXT, quantity INTEGER, streettype_id INTEGER, wkt_geometry TEXT, time_stamp INTEGER DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT)
DROP TABLE IF EXISTS territory_streettype
CREATE TABLE territory_streettype (id INTEGER PRIMARY KEY, streettype_name TEXT, color TEXT, time_stamp INT DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT)
DROP VIEW IF EXISTS territorystreets
CREATE VIEW territorystreets AS SELECT s.id, s.territory_id, t.territory_number, t.locality, s.street_name, s.from_number, s.to_number, s.quantity, s.streettype_id, st.streettype_name, st.color, s.wkt_geometry FROM territory_street s JOIN territory t ON s.territory_id = t.id LEFT JOIN territory_streettype st ON s.streettype_id = st.id WHERE t.active AND s.active
DELETE FROM geometry_columns WHERE f_table_name = 'territory_street'
DELETE FROM geometry_columns WHERE f_table_name = 'territorystreets'
INSERT INTO geometry_columns (f_table_name, f_geometry_column, geometry_type, coord_dimension, srid, geometry_format) VALUES("territory_street","wkt_geometry",2,2,4326,"WKT")
INSERT INTO geometry_columns (f_table_name, f_geometry_column, geometry_type, coord_dimension, srid, geometry_format) VALUES("territorystreets","wkt_geometry",2,2,4326,"WKT")
