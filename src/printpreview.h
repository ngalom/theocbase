#ifndef PRINTPREVIEW_H
#define PRINTPREVIEW_H

#include <QWidget>
#include <QWebEngineView>
#include <QtWebChannel>
#include <QMovie>
#include "sql_class.h"

namespace Ui {
class PrintPreview;
}

class PrintPreview : public QWidget
{
    Q_OBJECT

public:
    explicit PrintPreview(QWidget *parent = nullptr);
    ~PrintPreview();

    void setPdf(const QString pdffile);
    void setPdf(const QByteArray &pdf);
    Q_INVOKABLE void pdfLoaded(int current, int pages);
    Q_INVOKABLE void pdfLoadingStarted();
signals:
    void textSizeReset();
    void textSizeIncrease();
    void textSizeDecrease();
    void previewReady();

    void mapSizeReset();
    void mapSizeIncrease();
    void mapSizeDecrease();

private slots:
    void on_buttonPrevious_clicked();
    void on_buttonNext_clicked();
    void on_sliderZoom_sliderReleased();

    void on_buttonFontSizeReset_clicked();

    void on_buttonTextSizeDecrease_clicked();

    void on_buttonTextSizeIncrease_clicked();

    void on_buttonMapSizeDecrease_clicked();

    void on_buttonMapSizeIncrease_clicked();

    void on_sliderZoom_sliderPressed();

    void on_sliderZoom_valueChanged(int value);

private:
    void htmlLoaded(bool ok);
    void callPdfJavaScript();
    void setBusy();
    bool clearBusy();
    Ui::PrintPreview *ui;
    sql_class *sql;
    bool pageLoaded;
    QByteArray mPdf;
    QMovie *movie;
    bool sliderPressed = false;
    QWebChannel *channel;
    int currentPage = 0;
    int totalPages = 0;
    bool andZoom = false;
};

#endif // PRINTPREVIEW_H
