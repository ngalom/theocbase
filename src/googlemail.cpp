#include "googlemail.h"

googleMail::googleMail(googleMediator *mediator)
{
    this->mediator = mediator;
}

googleMediator::States googleMail::SendMail(MimeMessage *message)
{
    QByteArray postData;
    postData.append(message->toString());

    return mediator->SendRequest(
        googleMediator::PostWithJsonWrapper,
        "https://www.googleapis.com/gmail/v1/users/me/messages/send",
        &postData);
}
