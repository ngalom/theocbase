#ifndef LMM_ASSIGNMENT_EX_H
#define LMM_ASSIGNMENT_EX_H

#include "lmm_assignment.h"
#include "person.h"
#include "school.h"

class LMM_Assignment_ex : public LMM_Assignment
{
    Q_OBJECT
    Q_PROPERTY(person *assistant READ assistant WRITE setAssistant NOTIFY assistantChanged)
    Q_PROPERTY(person *volunteer READ volunteer WRITE setVolunteer NOTIFY volunteerChanged)
    Q_PROPERTY(QString timing READ timing WRITE setTiming NOTIFY timingChanged)
    Q_PROPERTY(bool completed READ completed WRITE setCompleted NOTIFY completedChanged)
    Q_PROPERTY(QString setting READ setting WRITE setSetting NOTIFY settingChanged)

public:
    LMM_Assignment_ex(QObject *parent = nullptr);
    LMM_Assignment_ex(int talkId, int sequence, int scheduleDbId, int assignmentDbId, QObject *parent = nullptr);

    person *assistant() const;
    void setAssistant(person *assistant);

    person *volunteer() const;
    void setVolunteer(person *volunteer);

    QString timing() const;
    void setTiming(QString time);

    bool completed() const;
    void setCompleted(bool completed);

    QString setting() const;
    void setSetting(QString setting);

    Q_INVOKABLE SortFilterProxyModel *getAssistantList();
    Q_INVOKABLE QString getReminderText() const;
    Q_INVOKABLE bool save();

    /**
     * @brief getCounselPoint - Get speaker's counsel point for this assignment
     * @param notice - Use this argument to get validitation of cousel point.
     *                 This can be used case when an earlier assignment is not yet
     *                 completed and thus counsel point for this assignment is unknown.
     * @return - schoolStudentStudy
     */
    schoolStudentStudy *getCounselPoint(QString &notice);

signals:
    void assistantChanged();
    void volunteerChanged();
    void timingChanged(QString timing);
    void completedChanged(bool completed);
    void settingChanged(QString setting);
private:
    person *m_assistant = nullptr;
    person *m_volunteer = nullptr;
};

#endif // LMM_ASSIGNMENT_EX_H
