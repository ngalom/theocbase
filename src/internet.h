#ifndef INTERNET_H
#define INTERNET_H

#include <QEventLoop>
#include <QTimer>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QString>

class internet
{
public:
    internet();
    QNetworkReply* download(QUrl url, int timeoutSeconds = 10); // this doesn't work.  I think we need to do an emit so the variables stay in scope

    QString lastErr;

private:
    QNetworkAccessManager manager;
    QNetworkRequest request;

};

#endif // INTERNET_H
