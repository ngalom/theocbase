#include "dropboxsyncbutton.h"

DropboxSyncButton::DropboxSyncButton(QWidget *parent) : QToolButton(parent)
{
    m_panel = new QQuickWidget(this->parentWidget());
    m_panel->installEventFilter(this);
    m_panel->setGeometry(QRect(0,0,0,0));
    m_panel->setWindowFlags(m_panel->windowFlags() | Qt::ToolTip);
    m_panel->setMouseTracking(true);
    m_panel->setResizeMode(QQuickWidget::SizeViewToRootObject);

    m_animation = new QPropertyAnimation(m_panel,"geometry");
    m_animation->setDuration(200);

    connect(m_animation,&QPropertyAnimation::finished,[=](){
        if (m_animation->direction() == QAbstractAnimation::Backward &&
                m_panel->isVisible())
            m_panel->close();        
    });
    connect(this,&DropboxSyncButton::panelLeave,[=](){
        if (m_panel->isVisible()){

            QRect testrect = QRect(this->mapToGlobal(this->rect().topLeft()),this->size());
            if (!testrect.contains(QCursor::pos())) {
                m_animation->setDirection(QAbstractAnimation::Backward);
                m_animation->start();
            }
        }
    });

    qreal dpr = qApp->devicePixelRatio();
    m_renderer = new QSvgRenderer(QString(":/icons/animated_sync.svg"),this);
    pixmap = QPixmap(24*dpr,24*dpr);
    pixmap.fill(Qt::transparent);
    pixmap.setDevicePixelRatio(dpr);

    connect(m_renderer,&QSvgRenderer::repaintNeeded,[this]{
        if (m_animation_running) {
            QPainter p(&pixmap);
            pixmap.fill(Qt::transparent);
            m_renderer->render(&p,QRectF(0,0,24,24));
            setIcon(pixmap);
        }
    });
}

void DropboxSyncButton::enterEvent(QEvent *e)
{
    if (!m_panel->isVisible()) {

        if (m_animation->state() == QAbstractAnimation::Running) {
            if (m_animation->direction() == QAbstractAnimation::Forward)
                return;
            m_animation->stop();
        }
        if (m_panel->source().isEmpty())
            m_panel->setSource(QUrl("qrc:/qml/DropboxSettings.qml"));

        int height = 110;
        if (m_panel->rootObject())
            height = m_panel->rootObject()->property("preferredHeight").toInt();

        QPoint p = this->mapToGlobal(this->rect().bottomLeft());
        p.setY(p.y()+1);
        QPoint p1 = QPoint(p.x() - 100, p.y());
        QPoint p2 = QPoint(p.x() + 200, p.y() + height);

        QScreen *actualScreen = QGuiApplication::screenAt(p);
        QRect screenRect = actualScreen->availableGeometry();
        if (p2.x() > screenRect.x() + screenRect.width()){
            int offset = p2.x() - (screenRect.x() + screenRect.width());
            p1.setX(p1.x() - offset);
            p2.setX(p2.x() - offset);
        }
        m_panel->setGeometry(QRect(p,p));
        m_animation->setStartValue(QRect(p,p));
        m_animation->setEndValue(QRect(p1,p2));

        m_animation->setDirection(QAbstractAnimation::Forward);
        m_panel->show();
        m_animation->start();
        m_panel->setMouseTracking(true);
    }
    QToolButton::enterEvent(e);
}

void DropboxSyncButton::leaveEvent(QEvent *e)
{
    QRect testrect = QRect(this->mapToGlobal(m_panel->rect().topLeft()),
                     m_panel->size());
    QPoint mousePos = QCursor::pos();

    if (m_panel->isVisible() && testrect.contains(mousePos))
        return;

    if (m_animation->state() == QAbstractAnimation::Running)
            m_animation->stop();
    m_animation->setDirection(QAbstractAnimation::Backward);
    m_animation->start();
    QToolButton::leaveEvent(e);
}

void DropboxSyncButton::hideEvent(QHideEvent *event)
{
    if (m_panel->isVisible())
        m_panel->close();
    QToolButton::hideEvent(event);
}

bool DropboxSyncButton::eventFilter(QObject *object, QEvent *event)
{
    if (object == m_panel) {
        if (event->type() == QEvent::MouseMove) {
            if (!m_panel_entered)
                emit panelEnter();
            m_panel_entered = true;
        } else if (event->type() == QEvent::Leave) {
            if (m_panel_entered) {
                emit panelLeave();
                m_panel_entered = false;
            }
        }
    }
    return QToolButton::eventFilter(object, event);
}

QQuickWidget *DropboxSyncButton::panel() const
{
    return m_panel;
}

void DropboxSyncButton::startIconAnimation()
{
    m_animation_running = true;
    m_restore_icon = this->icon();
}

void DropboxSyncButton::stopIconAnimation()
{
    m_animation_running = false;
    this->setIcon(m_restore_icon);
}

void DropboxSyncButton::closePanel()
{
    m_panel->close();
}
