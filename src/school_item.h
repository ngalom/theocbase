/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHOOL_ITEM_H
#define SCHOOL_ITEM_H

#include <QObject>
#include <QString>
#include "person.h"
#include "school_setting.h"

/**
 * @brief The school_item class to represent a one school assignment
 *        Save the changes using save() function
 */

class school_item : public QObject
{
    Q_OBJECT    
public:
    school_item();
    ~school_item();

    enum AssignmentType {
        Highlights,
        Reading,
        Demonstration,
        Discource,
        Review
    };
    Q_ENUM(AssignmentType)

    /**
     * @brief getId Get id number
     * @return id number or -1
     */
    int getId();

    /**
     * @brief setId Set id number
     * @param id id number
     */
    void setId(int id);

    /**
     * @brief getScheduleId Get related schedule id
     * @return id number or -1
     */
    int getScheduleId();

    /**
     * @brief setScheduleId Set related schedule id
     * @param id id number
     */
    void setScheduleId(int id);

    /**
     * @brief getAssignmentNumber Get assignment number
     * @return Assignment number (0 = Bible highlights, 1-3 or 10 = Review reader)
     */
    int getAssignmentNumber() const;

    /**
     * @brief setAssignmentNumber Set assignment number
     * @param number Assignment number (0 = Bible highlights, 1-3 or 10 = Review reader)
     */
    void setAssignmentNumber(int number);

    /**
     * @brief getSetting Get used setting in assignment
     * @return school_setting object or 0 if setting is not set
     */
    school_setting *getSetting();

    /**
     * @brief setSetting Set setting for assignment
     *        (Use only for sisters' assignments)
     * @param number A setting number
     */
    void setSetting(int number);

    /**
     * @brief getNumberName Get name of assignment example: No.1
     * @return Name of assignment
     */
    QString getNumberName() const;

    /**
     * @brief getStudent Get assignment's student
     * @return person object (0 is returned when a student is not set)
     */
    person *getStudent() const;

    /**
     * @brief setStudent Set a student for assignment
     * @param value person object
     */
    void setStudent(person *value);

    /**
     * @brief getAssistant Get a assistant for assignment
     * @return person object (0 is returned when a assistant is not set)
     */
    person *getAssistant() const;

    /**
     * @brief setAssistant Set a assistant for assignment
     *        (Should be use only when sisters' assignment)
     * @param value person object
     */
    void setAssistant(person *value);

    /**
     * @brief getVolunteer Get a volunteer for assignment
     *        This value is stored when assignment has marked done
     * @return person object (0 is returned when volunteer is not set)
     */
    person *getVolunteer() const;
    /**
     * @brief setVolunteer Set a volunteer for assignment
     * @param value person object
     */
    void setVolunteer(person *value);

    /**
     * @brief getDate Get date of assignment
     * @return QDate object
     *         Date is first date of week
     */
    QDate getDate() const;

    /**
     * @brief setDate Set date of assignment
     * @param value QDate object
     *              Date is first date of week
     */
    void setDate(const QDate value);

    /**
     * @brief getSource Get source material
     * @return Source
     */
    QString getSource() const;

    /**
     * @brief setSource Set source material
     * @param value Source material
     */
    void setSource(const QString &value);

    /**
     * @brief getClassNumber Get class number (main class is 1, aux. classes are 2 or 3)
     * @return class number
     */
    int getClassNumber() const;

    /**
     * @brief setClassNumber Set class number (main class is 1, aux. classes are 2 or 3)
     * @param value class number
     */
    void setClassNumber(int value);

    /**
     * @brief getTheme Get theme name
     * @return Theme
     */
    QString getTheme() const;

    /**
     * @brief setTheme Set theme name
     * @param value Theme name
     */
    void setTheme(const QString &value);

    /**
     * @brief getNote Get note
     * @return Note
     */
    QString getNote() const;

    /**
     * @brief setNote Set note
     * @param value Note text
     */
    void setNote(const QString &value);

    /**
     * @brief getDone Get value that tells is assignment completed
     * @return Done boolean value
     */
    bool getDone() const;

    /**
     * @brief setDone Set assignment completed
     * @param value true if assignment completed, otherwise false
     */
    void setDone(bool value);

    /**
     * @brief getTiming Get saved timing
     * @return
     */
    QString getTiming() const;

    /**
     * @brief setTiming Set timing
     * @param time
     */
    void setTiming(QString time);

    /**
     * @brief getOnlyBrothers Is assignment only for brothers
     * @return true or false
     */
    bool getOnlyBrothers() const;

    /**
     * @brief setOnlyBrothers Set assignment only for brothers
     * @param arg true or false
     */
    void setOnlyBrothers(bool arg);

    /**
     * @brief getType - Assignment type (Highlights, Reading, Demonstration, Discource, Review)
     * @return AssignmentType enum value
     */
    school_item::AssignmentType getType() const;

    /**
     * @brief save - Save assignment changes to database.
     * @return success or failure
     */
    bool save();

private:
    int m_id;
    int m_schedule_id;
    int m_assisgnment_number;
    school_setting *m_setting;
    person *m_student;
    person *m_assistant;
    person *m_volunteer;
    QDate m_date;
    QString m_source;
    QString m_theme;
    int m_classnumber;
    QString m_note;
    bool m_done;
    QString m_timing;
    bool m_onlybrothers;
};

#endif // SCHOOL_ITEM_H





