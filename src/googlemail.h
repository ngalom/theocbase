#ifndef GOOGLEMAIL_H
#define GOOGLEMAIL_H

#include "googlemediator.h"
#include "smtp/SmtpMime"

class googleMail
{
public:
    googleMail(googleMediator *mediator);
    googleMediator::States SendMail(MimeMessage *message);

private:
    googleMediator *mediator;
};

#endif // GOOGLEMAIL_H
