/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMPORTWIZARD_H
#define IMPORTWIZARD_H

#include <QWizard>
#include <QtWidgets>
#include <QDebug>
#include <QProgressDialog>
#include <QRegularExpression>
#include "sql_class.h"
#include "person.h"
#include "cpersons.h"
#include "ccongregation.h"
#include "school.h"

namespace Ui {
    class importwizard;
}

/**
 * @brief The importwizard class - Import Wizard Dialog Class
 *                                 This class is used when importing school schedule, settings or studies; or public talk themes
 *                                 User copies school data from Watchtower Library or WOL (Online Library);
 *                                 Public talk themes is copied from spreadsheet.
 *                                 User interface is done by Qt Creator's visual editor (see file importwizard.ui)
 */
class importwizard : public QWizard {
    Q_OBJECT
public:
    importwizard(QWidget *parent = 0);
    ~importwizard();

    /**
     * @brief setType - Set import type
     * @param type - Type
     *               0 = School schedule
     *               1 = School studies
     *               2 = School settings
     *               3 = Public talk themes
     *               4 = Public talk speakers (and congregations)
     *               5 = Songs
     */
    void setType(int type);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::importwizard *ui;

    /**
     * @brief fillSchoolSchedule - Fill school schedule. Function is called when validating the first page.
     */
    void fillSchoolSchedule();

    /**
     * @brief fillSchoolSettings - Fill school settings. Function is called when validating the first page.
     */
    void fillSchoolSettings();

    /**
     * @brief fillSchoolStudies - Fill school studies. Function is called when validating the first page.
     */
    void fillSchoolStudies();

    /**
     * @brief fillPublicTalkThemes - Fill public talk themes. Function is called when validating the first page.
     */
    void fillPublicTalkThemes();

    /**
     * @brief fillPublicTalkSpeakers - Fill public talk speakers (Congregations are also included).
     *                                 Function is called when validating the first page.
     */
    void fillPublicTalkSpeakers();

    /**
     * @brief fillSongs - Fill songs. Function is called when validating the first page.
     */
    void fillSongs();

    /**
     * @brief saveToDatabase - Save data to database. Function is called when validating the second page.
     */
    void saveToDatabase();

    int m_importtype;
    QString m_schoolday;
    int m_selectedLanguage;    
    bool validateCurrentPage();
    sql_class *sql;

    /**
     * @brief checkValues - Function is called when validating the first page.
     * @return
     */
    bool checkValues();

private slots:

signals:
    void discontinueTalk(int themeId);
};

#endif // IMPORTWIZARD_H
