#ifndef FILE_H
#define FILE_H

#include <QString>
#include <QFile>
#include <QTextStream>
#include <QDateTime>

extern QString localdatabasedir;

class file
{
public:
    file();
    static QString writeFile(QString fileName, QString contents, bool okAppend = false);
    static QString Debug(QString contents);
    static QString Debug(QString fileName, QString contents);
    static QString readFile(QString fileName);
};

#endif // FILE_H
