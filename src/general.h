#ifndef GENERAL_H
#define GENERAL_H

#include <QProcess>
#include <QFileInfo>
#include <QDir>
#include <QDate>
#include <QLocale>
#include <QDesktopServices>
#include <QUrl>
#include <QPixmap>
#include <QPainter>
#include <QIcon>

class general
{
public:
    static QDate TextToDate(QString text);
    static QString RemoveAccents(QString const& s);
    static void ShowInGraphicalShell(QWidget *parent, const QString &location);
    static QIcon changeIconColor(QIcon icon, QColor color);
};

#endif // GENERAL_H
