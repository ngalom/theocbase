/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHECKUPDATES_H
#define CHECKUPDATES_H

#include <QThread>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QStatusBar>
#include <QToolButton>
#include <QDesktopServices>
#include <QDir>
#include <QProcess>
#include <QApplication>

class checkupdates : public QThread
{
    Q_OBJECT

public:
    checkupdates(QString version, int waitseconds = 0);
    void run();
    void startUpdateInstall();
    bool checkUpdatesFromServer();
    QStatusBar *sb;
private:
    QString theocbaseversion;
    int wait;
    QString updaterpath;

private slots:
    void updatesChecked(QNetworkReply* reply);
    void buttonClicked();
signals:
    void updatesFound();
};

#endif // CHECKUPDATES_H
