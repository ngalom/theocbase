/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENERATEXML_H
#define GENERATEXML_H
#include <QXmlStreamWriter>
#include "cpersons.h"
#include "family.h"
#include "school.h"
#include "ccongregation.h"
#include "cpublictalks.h"
#include "lmm_meeting.h"

class generateXML
{
public:
    generateXML();

    void writeInfo(QXmlStreamWriter *xWriter,int version);
    void writePersons(QXmlStreamWriter* xWriter, bool publishers, bool speakers);
    void writeFamilies(QXmlStreamWriter *xWriter);

    void writePublicMeetings(QXmlStreamWriter* xWriter, QDate tempdate, int spinbox);
    void writeOutgoing(QXmlStreamWriter *xWriter, QDate tempdate, int spinbox);
    void writeMidweekMeeting(QXmlStreamWriter *xWriter, QDate tempdate, int spinbox);
    void writeSchool(QXmlStreamWriter *xWriter, QDate tempdate, int spinbox);
    void writeStudyHistory(QXmlStreamWriter *xWriter);
private:
    sql_class *sql;
    void writeName(QXmlStreamWriter *xWriter, QString elementName, person *person);
};

#endif // GENERATEXML_H
