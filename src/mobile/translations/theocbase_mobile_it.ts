<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation>S</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Non assegnare lo studio seguente</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Abbandona lo studio corrente</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Data non valida</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Aggiungi il tempo?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Materiale</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Studente</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Assistente</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Risultato</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Completato</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontario</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Seleziona un volontario</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Studio corrente</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Esercizio completato</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Prossima lezione</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Seleziona prossima lezione</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Calcolo del tempo</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Programma della Scuola</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Inizio cronometro</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Ferma cronometro</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Ambiente</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Seleziona l&apos;ambiente</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conduttore</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Oratore</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lettore</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Dati</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Note</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>TESORI DELLA PAROLA DI DIO</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>EFFICACI NEL MINISTERO</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VITA CRISTIANA</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Consigliere</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cantico %1 e Preghiera</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cantico</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conduttore</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lettore</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>Principale</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>Seconda</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>Terza</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Commenti Introduttivi</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Commento di anteprima della prossima adunanza</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Preghiera</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Importa Programma...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>A1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>A2</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Abbandona lo studio corrente</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Inizio cronometro</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Ferma cronometro</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Aggiungi il tempo?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Studente</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Ambiente</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Risultato</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Completato</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Calcolo del Tempo</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Studio corrente</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Esercizio completato</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Studio successivo</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Seleziona prossimo studio</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Assistente</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontari</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Cronometro</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>L&apos;assistente non dev&apos;essere qualcuno del sesso opposto.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Studio corrente</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Dettagli</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Trascina per aggiornare ...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Rilasciare per aggiornare ...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>Caricamento ...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Login fallito</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>TheocBase Login</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>Nuovo utente / Password dimenticata</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Nome utente o indirizzo e-mail</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Crea account</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Reimposta Password</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Indirizzo mail non trovato!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Password dimenticata</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Pagina di login</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>Principale</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Seconda</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Terza</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Avvio settimana %1</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Studio biblico di congregazione</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>Scuola di Ministero Teocratico</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>Adunanza di servizio</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>Discorso pubblico</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>Studio Torre di Guardia</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Adunanza Infrasettimanale</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Adunanza fine settimana</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>Infrasettimanale</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>Fine settimana</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>ORATORI ESTERNI</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 Oratore disponibile questo fine settimana</numerusform>
            <numerusform>%1 Oratori disponibili questo fine settimana</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Nessun oratore disponibile questo fine settimana</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Cantico e Preghiera</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cantico %1 e Preghiera</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISCORSO PUBBLICO</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>STUDIO TORRE DI GUARDIA</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Cantico %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conduttore</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lettore</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Importa TG...</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Congregazione</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Oratore</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Cellulare</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefono</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Ospite</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Discorso Pubblico</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Cognome</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Fratello</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sorella</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Nominato</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Famiglia</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Menbri della famiglia associati con</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Contatti</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefono</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>Usa per la scuola</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Tutte le classi</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Solo classe principale</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Solo Classi Secondarie</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>Punti notevoli</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>Nr. 1</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>Nr. 2</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>Nr. 3</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Capofamiglia</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Assistente</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>Vuoi chiamare %1?</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Attivo</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Tesori della Parola di Dio</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>Scaviamo per trovare Gemme Spirituali</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Lettura biblica</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Primo contatto</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Visita ulteriore</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Studio biblico</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Esercitiamoci con le presentazioni di questo mese</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Discorso Vita cristiana</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Studio Biblico di Congregazione</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Lettore Studio biblico di congregazione</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nuovo proclamatore</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Preghiera</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Ospite per Oratori Pubblici</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Cellulare</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Adunanza Infrasettimanale</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Discorso</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Adunanza di Fine Settimana</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Conduttore Studio Torre di Guardia</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Lettore Studio Torre di Guardia</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Proclamatori</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>Principale</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Seconda</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Terza</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Elenco di Selezione</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Esci</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase Homepage</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Suggerimenti, critiche, complimenti</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Ultima sincronizzazione: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Programma</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Mostra il Tempo</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Mostra la durata</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Interfaccia utente</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sincronizzo...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cantico</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Presidente Adunanza Fine Settimana</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Cantico</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Cantico Torre di Guardia</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Edizione Studio Torre di Guardia</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Articolo</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Conduttore</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lettore</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>Non impostato</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Cronologia</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Numero di settimane prima della data selezionata</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Numero di settimane dopo la data selezionata</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>settimane</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Numero di settimane da saltare dopo l&apos;assegnazione</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>Aggiornamento disponibile. Vuoi aggiornare?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>La stessa variazione sarà eseguita sia localmente che nel cloud (%1 rows). Vuoi mantenerla localmente?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>I dati del cloud sono stati ripristinati. I tuoi dati locali saranno sostituiti. Continuare?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Non assegnare lo studio seguente</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Non impostato</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>PN</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>Let</translation>
    </message>
</context></TS>