import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Switch {
    style: SwitchStyle {
        groove: Rectangle {
            implicitWidth: 51
            implicitHeight: 31
            color: control.checked ? control.enabled ? "#30496F" : "grey" : control.enabled ? "white" : "whitesmoke"
            radius: 20.0
            border.width: control.checked ? 0 : 2
            border.color: "#d7d7d7"
        }

        handle: Rectangle {
            width: 28
            height: 28
            color: "white"
            radius: 90.0
            border.width: control.checked ? 0 : 1
            border.color: "#d7d7d7"
        }

        padding.left: control.checked ? 2 : 1
        padding.right: control.checked ? 2 : 1
        padding.top: control.checked ? 2 : 1
        padding.bottom: control.checked ? 2 : 1
    }
}
