import QtQuick 2.5

Item {
    height: 45*dpi
    width: parent.width

    property bool refresh: state == "pulled" ? true : false
    //property ListView listView
    property Flickable flickable

    Row {
        spacing: 6
        height: childrenRect.height
        anchors.centerIn: parent

        Image {
            id: arrow
            source: "qrc:///refresh.svg"
            height: 30*dpi
            width: 30*dpi
            opacity: 0.5
            transformOrigin: Item.Center
            Behavior on rotation { NumberAnimation { duration: 200 } }
        }

        Text {
            id: label
            anchors.verticalCenter: arrow.verticalCenter
            text: qsTr("Pull to refresh...")
            font.pointSize: app_info.fontsize
            color: "#999999"
            renderType: Text.QtRendering
        }
    }

    states: [
        State {
            name: "base"; when: flickable.contentY >= -80
            PropertyChanges { target: arrow; rotation: 180 }
        },
        State {
            name: "pulled"; when: flickable.contentY < -80
            PropertyChanges { target: label; text: qsTr("Release to refresh...") }
            PropertyChanges { target: arrow; rotation: 0 }
        }
    ]
}
