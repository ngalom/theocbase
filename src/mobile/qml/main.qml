/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import QtQuick.Window 2.3
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import net.theocbase.mobile 1.0
import net.theocbase 1.0
import "js/moment.js" as Moment

ApplicationWindow {   
    id: applicationWindow1
    visible: true
    width: 480
    height: 800
    title: "TheocBase"
    flags: Qt.Window | Qt.MaximizeUsingFullscreenGeometryHint

    property var appState: Qt.application.state
    property real dpi: Math.max((Screen.pixelDensity / 6),1)
    property int activepage: 1
    property int toolbarTopMargin: applicationWindow1.height - Screen.desktopAvailableHeight

    property bool canViewMidweekMeetingSchedule: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanViewMidweekMeetingSchedule)
    property bool canEditMidweekMeetingSchedule: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditMidweekMeetingSchedule)
    property bool canViewWeekendMeetingSchedule: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanViewWeekendMeetingSchedule)
    property bool canEditWeekendMeetingSchedule: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditWeekendMeetingSchedule)
    property bool canViewPublishers: accessControl.user && (accessControl.user.hasPermission(PermissionRule.CanViewPublishers))
    property bool canEditPublishers: accessControl.user && (accessControl.user.hasPermission(PermissionRule.CanEditPublishers))

    function reloadMainpage(){
        stackView.navigation.get(0).refreshList(0)
    }

    // activate home, publishers or settings page
    function openPage(page){
        switch (page) {
        case 1:
            if (stackView.navigation.depth == 0)
                stackView.navigation.push(Qt.resolvedUrl("MainPage.qml"), StackView.Immediate)
            else
                stackView.navigation.pop(null, StackView.Immediate)
            stackView.navigation.get(0).activateTabByDay()
            break
        case 2:
            stackView.navigation.pop(null)
            stackView.navigation.push(Qt.resolvedUrl("PublishersPage.qml"), StackView.Immediate)
            break;
        case 3:
            stackView.navigation.pop(null)
            stackView.navigation.push(Qt.resolvedUrl("SettingsPage.qml"), StackView.Immediate)
            break
        default: return
        }
    }

    function readImportFile() {
        if (shareUtils.receivedUrl !== "") {
            var f = shareUtils.receivedUrl
            shareUtils.receivedUrl = ""

            if (!ccloud.logged) {
                msg.show("Import File", "You are not logged in. Please log in and try again.")
                return
            }

            console.log(f)
            var filename = f.substring(f.lastIndexOf('/')+1)
            console.log(filename)
            var result = ""
            if (/^.*(.thb)$/.test(filename)) {
                // thb file
                var clickedFunc = function(ok) {
                    if (ok) {
                        var p = stackView.find(function(item,index){return item.objectName === "DataExchange"})
                        if (p === null) {
                            stackView.push(Qt.resolvedUrl("DataExchange.qml"),
                                           {objectName: "DataExchange", filePath: f}, StackView.Immediate)
                        }else if (p === stackView.currentItem) {
                            p.readCurrentFile(f)
                        }else{
                            stackView.pop(p,
                                          StackView.Immediate,
                                          {filePath: f})
                        }
                    }
                    msg.onButtonClicked.disconnect(clickedFunc)
                }
                msg.onButtonClicked.connect(clickedFunc)
                msg.showYesNo("Import File","Do you want to import .thb-file?",-1)
                return
            } else if (/^w_.*(.epub)$/.test(filename)) {
                result = wtImport.importFile(f)
            } else if (/^mwb_.*(.epub)$/.test(filename)) {
                result = mwbImport.importFile(f)
            } else {
                result = "Unknown file type"
            }
            msg.show("", result)
            reloadMainpage()
        }
    }

    MWBImport { id: mwbImport }
    WTImport { id: wtImport }
    Connections {
        id: shareUtilsConnection
        target: shareUtils
        onReceivedUrlChanged: {
            if (appState === Qt.ApplicationActive)
                readImportFile()
            else
                console.log("onReceivedUrlChanged !!! app is not in active state")
        }
        onDbcallback: {
            console.log("onDbcallback" + token)
            ccloud.authentication.setToken(token)
            settings_ui.loginRequested = false
        }
    }

    Connections {
        target: Qt.application
        onActiveChanged: {
            if (Qt.application.active) {
                console.log("application activated");
                readImportFile()
            }
        }
    }

    Component.onCompleted: {        
        app_info.dpi = dpi        
    }

    SynchronizePage {
        id: syncpage
        z: 2
    }

    onAppStateChanged: {
        if (appState === Qt.ApplicationActive){
            console.log("application state active")
            cloudtimer.start()
            console.log("login requested ??????? " + settings_ui.loginRequested.toString())
        }else if (appState === Qt.ApplicationInactive){
            console.log("application state inactive")
        }else if (appState === Qt.ApplicationSuspended){
            console.log("application state suspended")
            //console.log("logged = " + ccloud.logged())
            //console.log("updates available = " + ccloud.checkCloudUpdates())
            //backgroundDebug
            //var d = new Date()
            //ccloud.backgroundDebug = "Background test " + Qt.formatDateTime(d,Qt.DefaultLocaleShortDate)
        }else if (appState === Qt.ApplicationHidden){
            console.log("application state hidden")
        }
    }

    Timer {
        id: cloudtimer
        interval: 5000
        onTriggered: {
            var differenceMinutes = Math.floor((new Date().getTime() - settings_ui.lastCloudUpdates.getTime())/1000/60)
            if (differenceMinutes > 5){
                // more than 5 minutes from last check
                if (ccloud.logged){
                    // logged - check updates
                    ccloud.showError = false
                    ccloud.checkCloudUpdates()
                    ccloud.showError = true
                    // save timestamp
                    settings_ui.lastCloudUpdates = new Date()
                }
            }
        }
    }

    Cloud {
        id: ccloud
        property bool autoSync: false
        property bool showError: true
        onStateChanged: {
            if (state == Cloud.Both){
                console.log("Sync state changed: both changes")
            }else if (state == Cloud.Upload){
                console.log("Sync state changed: upload")
            }else if (state == Cloud.Download){
                console.log("Sync state changed: download")
            }
        }
        onSyncConflict: {
            console.log("sync conflict");
            syncpage.visible = false
            msg.showYesNo("TheocBase Cloud",
                          qsTr("The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?")
                          .arg(values.toString()),2);
        }
        onSyncFinished: {
            if (stackView.navigation.currentItem.objectName === "page1"){
                var d = stackView.navigation.currentItem.currentdate
                console.log("sync finished " + d)
                stackView.navigation.replace(Qt.resolvedUrl("MainPage.qml"),
                                             {objectName:"page1", currentdate: d },StackView.Immediate);
                stackView.navigation.get(0).activateTabByDay()
            }else if (stackView.navigation.currentItem.objectName === "page2"){
                stackView.navigation.replace(Qt.resolvedUrl("PublishersPage.qml"),
                                             {objectName:"page2"}, StackView.Immediate);
            }else{
                // settings page
                stackView.navigation.get(0).refreshList(0);
            }
            settings_ui.lastCloudUpdates = new Date()
        }
        onError: {
            syncpage.visible = false;
            if (showError)
                msg.show("TheocBase Cloud",message);
        }
        onDifferentLastDbUser: {
            syncpage.visible = false;
            msg.show("TheocBase Cloud",
                     "The database has been synchronized last time by other cloud user.");
        }

        onCloudResetFound: {
            syncpage.visible = false;
            msg.showYesNo("TheocBase Cloud",
                          qsTr("The cloud data has been reset. Your local data will be replaced. Continue?"),3);
        }
        onLoginRequired: {
            console.log("login required " + url)
            shareUtils.loginDropbox(url)
            settings_ui.loginRequested = true
        }
        onLoggedChanged: {            
            if (ok && !moment(ccloud.lastSyncTime).isValid())
                syncpage.runSync()            
        }
        Component.onCompleted: {
            ccloud.initAccessControl()
        }
    }

    MsgBox {
        id: msg        
        onButtonClicked: {
            switch (id){
            case 1:
                // sync updates
                if (ok) syncpage.runSync();
                break
            case 2:
                // sync conflict
                syncpage.continueSync(ok);                
                break
            case 3:
                // cloud data reset
                if (ok) {
                    ccloud.clearDatabase();
                    syncpage.runSync();
                }
                break;
            default: break
            }
        }
    }
    Settings {
        id: settings_ui
        category: "ui"
        property bool showTime: true
        property date lastCloudUpdates: new Date('2016-01-01')
        property bool loginRequested: false
    }

    NavigationStack {
        id: stackView
        anchors.fill: parent
        Component.onCompleted: {
            openPage(1)
            if (!ccloud.logged){
                if (!settings_ui.loginRequested)
                    ccloud.login()
            }
        }
    }

    ToolBar {
        id: navbar
        z: 2
        height: stackView.useSplitView || stackView.currentDetailitem == undefined ?  50*dpi : 0
        anchors.bottomMargin: 0
        anchors.bottom: parent.bottom

        anchors.right: parent.right
        anchors.left: parent.left
        Material.background: "#e1e2e1"
        RowLayout {
            anchors.fill: parent
            spacing: 0
            Item { Layout.fillWidth: true; Layout.fillHeight: true }
            ToolButton {
                id: todayButton
                icon.source: "qrc:///list.svg"
                Layout.maximumWidth: height*3
                Layout.fillWidth: true
                Layout.fillHeight: true
                checkable: true
                checked: true                
                onClicked: {
                    activepage = 1
                    checked = true
                    publishersButton.checked = false
                    settingsButton.checked = false
                    console.log("today clicked");
                    openPage(1)                    
                }
            }
            ToolButton {
                id: publishersButton
                Layout.maximumWidth: height*3
                Layout.fillWidth: true
                Layout.fillHeight: true
                checkable: true
                icon.source: "qrc:///publishers.svg"
                visible: canViewPublishers
                onClicked: {
                    activepage = 2
                    checked = true
                    todayButton.checked = false
                    settingsButton.checked = false
                    console.log("publishers clicked");
                    openPage(2)
                }
            }
            ToolButton {
                id: settingsButton
                icon.source: "qrc:///settings.svg"
                Layout.maximumWidth: height*3
                Layout.fillWidth: true
                Layout.fillHeight: true
                checkable: true
                onClicked: {
                    activepage = 3
                    checked = true
                    todayButton.checked = false
                    publishersButton.checked = false
                    console.log("settings clicked");
                    openPage(3)
                }
            }
            Item { Layout.fillWidth: true; Layout.fillHeight: true }
        }
    }
}

