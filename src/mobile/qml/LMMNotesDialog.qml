/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import net.theocbase.mobile 1.0

Page {
    id: assignmentDialog
    property string pageTitle: qsTr("Notes", "Page title")
    property alias notes: textAreaNotes.text
    //property double dpi: 1

    signal saveChanges(string notes)

    header: BaseToolbar {
        title: pageTitle
        componentLeft: ToolButton {
            icon.source: "qrc:///back.svg"
            icon.color: "white"
            onClicked: {
                saveChanges(notes)
                stackView.pop()
            }
        }
    }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        spacing: 0

        RowTitle { text: qsTr("Notes") }

        TextArea {
            Layout.fillWidth: true
            id: textAreaNotes
            wrapMode: Text.WordWrap
            Layout.fillHeight: true
            font.pointSize: app_info.fontsize
            leftPadding: 10*dpi
            rightPadding: 10*dpi
            background: Rectangle {
                color: "transparent"
            }
            onEditingFinished: saveChanges(text)
        }
    }
}
