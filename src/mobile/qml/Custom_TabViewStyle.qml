import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

TabViewStyle {
    id: tabstyle
    frameOverlap: 1
    property color backgroundColor: "white"
    property color textColor: "#30496F"
    property int implicitHeight: 25*dpi
    tab: Rectangle {
        implicitWidth: Math.max(text.width + 20, 100)
        implicitHeight: tabstyle.implicitHeight
        color: backgroundColor
        Text {
            id: text
            anchors.centerIn: parent
            text: styleData.title
            font.pointSize: app_info.fontsizeSmall
            color: styleData.selected ? textColor : "grey"
            renderType: Text.QtRendering
        }
        Rectangle{
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height: styleData.selected ? 3*dpi : 0
            color: textColor
        }
    }
    frame: Rectangle { color: backgroundColor }
    tabsAlignment: Qt.AlignHCenter
}
