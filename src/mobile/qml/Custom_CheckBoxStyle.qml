import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

CheckBoxStyle {
    Component.onCompleted: {
        if (Qt.platform.os === "ios"){
            indicator = iosStyle
        }else{
            indicator = androidStyle
        }
    }
    Component {
        id: iosStyle
        Item {
            implicitHeight: control.height
            implicitWidth: control.height
            Image {
                anchors.fill: parent
                source: "qrc:///checkmark.png"
                visible: control.checked
            }
        }
    }
    Component {
        id: androidStyle
        Item {
            implicitHeight: control.height
            implicitWidth: control.height
            Rectangle {
                implicitWidth: control.height
                implicitHeight: control.height
                radius: 3*dpi
                border.color: control.activeFocus ? "darkblue" : "gray"
                border.width: 1
                Rectangle {
                    visible: control.checked
                    color: "#555"
                    border.color: "#333"
                    radius: 1
                    anchors.margins: 4*dpi
                    anchors.fill: parent
                }
            }
        }
    }
}
