import QtQuick 2.4

Item {
    property alias menuContent: loaderItem.sourceComponent
    id: popUpPage
    visible: false

    function show(x, y){
        myRect.x = x - myRect.width/2
        myRect.y = y - myRect.height - 10*dpi
        if ((myRect.x + myRect.width + 10*dpi) > width){
            myRect.x = width - myRect.width - 10*dpi
        }else if((myRect.x < 10*dpi)){
            myRect.x = 10*dpi
        }
        visible = true
    }

    anchors.fill: parent
    Rectangle {
        opacity: 0.2
        color: "grey"
        z: -1
        anchors.fill: parent
        MouseArea {
            anchors.fill: parent
            onClicked: popUpPage.visible = false
        }
    }
    Rectangle {
        id: myRect
        implicitWidth: loaderItem.item === "undefined" ? 50 : loaderItem.item.implicitWidth+10*dpi
        height: 45*dpi
        radius: 10*dpi
        opacity: 1
        color: "#30496F"

        Loader {
            id: loaderItem
            z: 2
            anchors.fill: parent
            anchors.rightMargin: 5*dpi
            anchors.leftMargin: 5*dpi
        }
        MouseArea {
            anchors.fill: parent
            onClicked: popUpPage.visible = false
        }
    }
    Rectangle {
        x: myRect.x+myRect.width/2
        y: myRect.y+myRect.height-5*dpi
        width: 15*dpi; height: 15*dpi
        rotation: -45
        transformOrigin: Item.BottomLeft
        color: "#30496F"
    }
}

