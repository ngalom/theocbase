#include "shareutils.h"
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QtAndroidExtras/QAndroidJniObject>
#include <QDebug>
#include <jni.h>
#include <QDir>
#include <QFile>
#include <QUrl>
#include <QIODevice>

ShareUtils* ShareUtils::mInstance = nullptr;

ShareUtils::ShareUtils(QObject *parent) : QObject(parent)
{
    mInstance = this;
}

ShareUtils* ShareUtils::getInstance()
{
    if (!mInstance)
        mInstance = new ShareUtils();
    return mInstance;
}

void ShareUtils::setReceivedUrl(QString path)
{
    mViewFilePath = path;
}

QString ShareUtils::receivedUrl()
{
    if (mViewFilePath.isEmpty())
        QAndroidJniObject::callStaticMethod<void>("net/theocbase/mobile/TBActivity",
                                                  "checkReceivedUrl");
    return mViewFilePath;
}

void ShareUtils::saveBackup()
{
    QString dbfilename = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/theocbase.sqlite";
    if (!QFile::exists(dbfilename)) {
        qWarning() << "Database file not found" << dbfilename;
        return;
    }

    QString docLocation = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0);
    docLocation.append("/share");
    if (!QDir(docLocation).exists()) {
        if (!QDir("").mkpath(docLocation)) {
            qWarning() << "Failed to create directory" << docLocation;
            return;
        }
    }

    QString tempFilePath = docLocation.append("/theocbase_backup.sqlite");
    if (QFile::exists(tempFilePath))
        QFile::remove(tempFilePath);
    if (!QFile::copy(dbfilename,tempFilePath)) {
        qWarning() << "Failed to copy temporary file. From: " << dbfilename << "To: " << tempFilePath;
        return;
    }
    QFile(tempFilePath).setPermissions(QFileDevice::ReadUser | QFileDevice::WriteUser);

    QUrl url = QUrl::fromLocalFile(tempFilePath);

    QAndroidJniObject javaFileName = QAndroidJniObject::fromString(url.toString());

    QAndroidJniEnvironment env;
    QAndroidJniObject::callStaticMethod<void>("net/theocbase/mobile/TBActivity",
                                              "saveFile",
                                              "(Ljava/lang/String;)V",
                                              javaFileName.object<jstring>());

    if (env->ExceptionCheck())
    {
        qDebug() << " JNI exception ";
        env->ExceptionDescribe();
        env->ExceptionClear();
    }else{
        qDebug() << "OK";
    }
}

void ShareUtils::sendMail()
{
    QAndroidJniEnvironment env;
    QString dbfilename = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/theocbase.sqlite";

    if (!QFile::exists(dbfilename)) {
        qWarning() << "Database file not found" << dbfilename;
        return;
    }

    QAndroidJniObject javaFileName = QAndroidJniObject::fromString(dbfilename);
    qDebug() << "db filename " << dbfilename;
    
    if (QAndroidJniObject::isClassAvailable("net/theocbase/mobile/TBActivity")){
        qDebug() << "class available";
    }else{
        qDebug() << "class not found";
        return;
    }
    
//    QAndroidJniObject data =  QAndroidJniObject::callStaticObjectMethod<jstring>
//            ("net/theocbase/mobile/TBActivity",
//             "myTest");
//    qDebug() << "myTest" << data.toString();
    
//    int intTest = QAndroidJniObject::callStaticMethod<jint>
//            ("net/theocbase/mobile/TBActivity",
//             "myTest2",
//             "(I)I",
//             23);
//    qDebug() << "myTest2" << intTest;
    
    QAndroidJniObject::callStaticMethod<void>("net/theocbase/mobile/TBActivity",
                                              "sendMail",
                                              "(Ljava/lang/String;)V",
                                              javaFileName.object<jstring>());
    if (env->ExceptionCheck())
    {
        // Handle exception here.
        qDebug() << " JNI exception ";
        env->ExceptionDescribe();
        env->ExceptionClear();
    }else{
        qDebug() << "java call OK";
    }
}

void ShareUtils::shareText(const QString text)
{
    QAndroidJniEnvironment env;
    QAndroidJniObject textString = QAndroidJniObject::fromString(text);
    QAndroidJniObject::callStaticMethod<void>("net/theocbase/mobile/TBActivity",
                                              "shareText",
                                              "(Ljava/lang/String;)V",
                                              textString.object<jstring>());

    if (env->ExceptionCheck())
    {
        qDebug() << " JNI exception ";
        env->ExceptionDescribe();
        env->ExceptionClear();
    }else{
        qDebug() << "OK";
    }
}

void ShareUtils::loginDropbox(const QUrl &url)
{
    QUrlQuery query(url.query());
    query.removeQueryItem("redirect_uri");
    query.addQueryItem("redirect_uri","theocbase://dropbox_callback");
    query.removeQueryItem("response_type");
    query.addQueryItem("response_type","token");
    QUrl modifiedUrl(url);
    modifiedUrl.setQuery(query);
    QAndroidJniObject javaUrlString = QAndroidJniObject::fromString(modifiedUrl.toString());
    qDebug() << modifiedUrl.toString();
    QAndroidJniObject::callStaticMethod<void>("net/theocbase/mobile/TBActivity",
                                              "loginDropbox",
                                              "(Ljava/lang/String;)V",
                                              javaUrlString.object<jstring>());
}

void ShareUtils::handleUrlReceived(const QUrl &url)
{
    setReceivedUrl(url.toString());
}

QString ShareUtils::openFile(QString format)
{
    QAndroidJniObject javaUrlString = QAndroidJniObject::fromString(format);
    QAndroidJniObject::callStaticMethod<void>("net/theocbase/mobile/TBActivity",
                                              "openFileBrowser",
                                              "(Ljava/lang/String;)V",
                                              javaUrlString.object<jstring>());
    return format;
}

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL
    Java_net_theocbase_mobile_TBActivity_setUrlReceived(JNIEnv *env,
                                                 jobject obj,
                                                 jstring uri)
{
    Q_UNUSED(obj);
    jboolean isCopy = false;
    const char* utf = env->GetStringUTFChars(uri, &isCopy);
    qDebug() << "C++" << utf;

    QString url = QString(utf);
    env->ReleaseStringUTFChars(uri,utf);

    if (url.startsWith("theocbase")) {
        QUrlQuery query(url.contains("#") ? url.split("#").at(1) : url);
        if (query.hasQueryItem("access_token")) {
            ShareUtils::getInstance()->dbcallback(query.queryItemValue("access_token"));
        }
    }else{
        ShareUtils::getInstance()->setReceivedUrl(url);
    }
}
#ifdef __cplusplus
}
#endif
