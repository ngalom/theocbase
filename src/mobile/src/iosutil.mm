#include "iosutil.h"
#include <UIKit/UIKit.h>
#include "../cloud/cloud_controller.h"

@interface QIOSApplicationDelegate
@end

@interface QIOSApplicationDelegate (MyQtAppDelegate)
@end

@implementation QIOSApplicationDelegate (MyQtAppDelegate)

- (void)applicationDidEnterBackground:(UIApplication *)application
{
#pragma unused(application)
    NSLog(@"In the background");
    cloud_controller c;
    if (c.logged()){
        NSLog(@"Logged in");
    }else{
        NSLog(@"Logged out");
    }
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
#pragma unused(application)
    NSLog(@"background handler called");
    bool upd = false;
    cloud_controller c;
    if (c.logged()){
        NSLog(@"Cloud login ok");
        upd = c.checkCloudUpdates();
        NSLog(@"Updates available: %d",upd);
    }else{
        NSLog(@"Not logged into cloud");
    }
    completionHandler(upd ? UIBackgroundFetchResultNewData : UIBackgroundFetchResultNoData);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(nullable NSDictionary *)launchOptions
{
#pragma unused(launchOptions)
    application.statusBarStyle = UIStatusBarStyleLightContent;
    
    return YES;
}

@end

void iosutil::initiOS()
{
    setStatusBarColorLight();
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    NSLog(@"iOS init");
}

void iosutil::setStatusBarColorLight()
{
    //[[UIApplication sharedApplication].keyWindow.rootViewController preferredStatusBarStyle: UIStatusBarStyleLightContent];
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

QString iosutil::getDeviceName()
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        return "ipad";
    }else if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        return "iphone";
    }else{
        return "unsupported ios device";
    }
}
