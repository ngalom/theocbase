#ifndef ICAL_H
#define ICAL_H

#include <QString>
#include <QList>
#include <QDateTime>
#include <QRegularExpression>


class iCal
{
public:

    iCal();

    class Item {
    public:
        Item(QString name);
        QString getName();
        void setName(QString name);
        virtual QString dataAsString()=0;
        virtual void setDataAsString(QString data)=0;
        QString getValueType();
        void setValueType(QString valueType);
        bool getHidden();
        void setHidden(bool hidden);
        virtual QString toString();

    protected:
        QString name;
        QString valueType;
        bool hidden = 0;
    };

    class StringItem : public Item {
    public:
        StringItem(QString name, QString data = "");
        QString dataAsString();
        void setDataAsString(QString data);

    protected:
        QString data;
    };

    class BeginEndItem : public Item {
    public:
        BeginEndItem(QString name);
        QString getName();
        void setName(QString name);
        Item *getItemByName(QString name);
        QString dataAsString() {return toString();}
        void setDataAsString(QString) {}
        QString toString();

    protected:
        QList<Item*> items;

    private:
        StringItem *beginning;
        StringItem *end;
    };

    class DateItem : public Item {
    public:
          DateItem(QString name, QDateTime data);

          QString dataAsString();
          QDateTime getData();
          void setData(QDateTime data);
          void setDataAsString(QString sData);          
          bool isAllDay();

    protected:
          QDateTime data;

    private:
          QTime midnight;
    };

    class TimeSpanItem : public Item {
    public:
        TimeSpanItem(QString name, int days, QTime time);
        QString dataAsString();
        void setDataAsString(QString data);

    protected:
        bool isValid;
        int days;
        QTime time;
    };

    class VALARM : public BeginEndItem {
    public:
        VALARM(QString description, int triggerDay, QTime triggerTime): BeginEndItem("VALARM") {
            items.append(new StringItem("ACTION", "DISPLAY"));
            items.append(new StringItem("DESCRIPTION", description));
            items.append(new TimeSpanItem("TRIGGER", triggerDay, triggerTime));
        }

        QString description() {
            return getItemByName("DESCRIPTION")->dataAsString();
        }

        void setDescription(QString description) {
            getItemByName("DESCRIPTION")->setDataAsString(description);
        }

        TimeSpanItem *trigger() {
            return (TimeSpanItem*)getItemByName("TRIGGER");
        }

        void setTrigger(TimeSpanItem *newTrigger) {
            for (Item *i : items) {
                if (i->getName().compare("TRIGGER", Qt::CaseInsensitive)) {
                    i = newTrigger;
                    return;
                }
            }
        }
    };

    class VEVENT : public BeginEndItem {
    public:
       VEVENT();
       VEVENT(QString Summary, QDateTime Start, QDateTime End, QDateTime Stamp, QString Description, QString Location/*, string Status,*/);
       void addAlarm(QString description, int minutesBefore);
       void addAlarm(QString description, QDateTime trigger);
       QString summary();
       void setSummary(QString data);
       DateItem* startItem();
       QDateTime start();
       void setStart(QDateTime data);
       DateItem* endItem();
       QDateTime end();
       void setEnd(QDateTime data);
       QDateTime stamp();
       void setStamp(QDateTime data);
       QString description();
       void setDescription(QString data);
       QString location();
       void setLocation(QString data);
       QString uid();
       void setUid(QString uid);
       QString status();
       void setStatus(QString status);
       QString toString() override;
    };

    class VCALENDAR : public BeginEndItem {
    public:
        VCALENDAR(QString CalendarName);

        void AddEvent(VEVENT *vevent);

        VEVENT* LastEvent();

    private:
        VEVENT *lastEvent;
    };
};

#endif // ICAL_H
